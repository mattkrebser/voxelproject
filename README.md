# README #

A Simple Voxel Terrain Generator
(this is a really old voxel thingy that I made in college to learn programming just saying)

### Features ###

* Polygon Voxel Generation and drawing
* Has Run Length Encoded Saving and Loading of voxel data
* Multithreaded loading/drawing of voxel data
* Dynamic per-material mesh batching
* Unity Editor Window block editor
* Blocks can use separate materials, textures, and tile tiletextures for different effects
* Cubes and different sloped blocks. Also has several billboard block types and rectangle block type.
* Custom physics implementation and collision resolution
* Voxel ray casting, voxel sphere casting, voxel convex collider casting
* Voxel based character mover and multi-mode camera
* unity 5.3.4f1

### How do I get set up? ###

* This repository is a Unity Project folder, download the repository and open the folder in Unity!
* In the scene view, press play button
* Then press 'Editor'
* Change 'World Name' to whatever you want
* Double click 'Done' or 'Make World', then confirm
* Once in the loading menu, select the world you made and press 'Play' or 'Edit'
* There currently aren't instructions for the editor so it will be confusing

### Notes ###

* Program is incomplete/very primitive
* Currently no modification of voxel terrain implemented
* Currently no (dynamic)chunk saving implemented
* More complete features to come eventually when I have time
* To load in chunks  further away, drag the 'LOAD HERE!!!' gameobject around in scene view
* Only tested on Windows 10

![VoxelScreenShot.png](https://bitbucket.org/repo/Rkdz4o/images/1441116101-VoxelScreenShot.png)