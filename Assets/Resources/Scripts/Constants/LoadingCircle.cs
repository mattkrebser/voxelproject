﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Contains an Array of Vector2[] in which each vector, is sorted by the
/// distance from the middle of a circle with a center (0,0). Sorted in descending order,
/// meaning the furthest positions are at the front
/// </summary>
public class LoadingCircle
{
	//If you want, you can attatch the block information to items and then delete the arrays here.

	//positions in the arrays are represented as block ID's(the primary ID). There are 256 members in each array

	//Editing this script may sometimes cause monodevelope to crash.   >:}  enjoy!!

	//each render distance end position
	//Example: renderPositions[8] is 3015. chunksToRender[array_length-1] to chunksToRender[3015] are all
    //of the chunks (ordered from closest to 0,0 to furthest) that are within a distance of 8 chunks
	public int[] renderPositions = new int[]
	{
		0,
		3199,
		3199,
		3183,
		3163,
		3139,
		3099,
		3063,
		3015,
		2959,
		2903,
		2835,
		2771,
		2691,
		2599,
		2511,
		2415,
		2319,
		2203,
		2083,
		1963,
		1839,
		1695,
		1559,
		1419,
		1267,
		1099,
		923,
		759,
		591,
		399,
		211,
		1
	};

	//4096 vector2's, not that much, it just looks like a lot
	//These are all possible int coordinates inside a circle with radius 32
	//They are ordered from the furthest to the center of the circle to the closest
	public Vector2[] chunksToRender = new[]
	{
		new Vector2(32,0),

		new Vector2(-32,0),

		new Vector2(0,32),

		new Vector2(0,-32),

		new Vector2(-30,-11),

		new Vector2(-30,11),

		new Vector2(-11,-30),

		new Vector2(-11,30),

		new Vector2(11,-30),

		new Vector2(11,30),

		new Vector2(30,-11),

		new Vector2(30,11),

		new Vector2(-27,-17),

		new Vector2(-27,17),

		new Vector2(-17,-27),

		new Vector2(-17,27),

		new Vector2(17,-27),

		new Vector2(17,27),

		new Vector2(27,-17),

		new Vector2(27,17),

		new Vector2(-24,-21),

		new Vector2(-24,21),

		new Vector2(-21,-24),

		new Vector2(-21,24),

		new Vector2(21,-24),

		new Vector2(21,24),

		new Vector2(24,-21),

		new Vector2(24,21),

		new Vector2(-23,-22),

		new Vector2(-23,22),

		new Vector2(-22,-23),

		new Vector2(-22,23),

		new Vector2(22,-23),

		new Vector2(22,23),

		new Vector2(23,-22),

		new Vector2(23,22),

		new Vector2(-31,-7),

		new Vector2(-31,7),

		new Vector2(-29,-13),

		new Vector2(-29,13),

		new Vector2(-13,-29),

		new Vector2(-13,29),

		new Vector2(-7,-31),

		new Vector2(-7,31),

		new Vector2(7,-31),

		new Vector2(7,31),

		new Vector2(13,-29),

		new Vector2(13,29),

		new Vector2(29,-13),

		new Vector2(29,13),

		new Vector2(31,-7),

		new Vector2(31,7),

		new Vector2(-28,-15),

		new Vector2(-28,15),

		new Vector2(-15,-28),

		new Vector2(-15,28),

		new Vector2(15,-28),

		new Vector2(15,28),

		new Vector2(28,-15),

		new Vector2(28,15),

		new Vector2(-30,-10),

		new Vector2(-30,10),

		new Vector2(-26,-18),

		new Vector2(-26,18),

		new Vector2(-18,-26),

		new Vector2(-18,26),

		new Vector2(-10,-30),

		new Vector2(-10,30),

		new Vector2(10,-30),

		new Vector2(10,30),

		new Vector2(18,-26),

		new Vector2(18,26),

		new Vector2(26,-18),

		new Vector2(26,18),

		new Vector2(30,-10),

		new Vector2(30,10),

		new Vector2(-31,-6),

		new Vector2(-31,6),

		new Vector2(-6,-31),

		new Vector2(-6,31),

		new Vector2(6,-31),

		new Vector2(6,31),

		new Vector2(31,-6),

		new Vector2(31,6),

		new Vector2(-31,-5),

		new Vector2(-31,5),

		new Vector2(-25,-19),

		new Vector2(-25,19),

		new Vector2(-19,-25),

		new Vector2(-19,25),

		new Vector2(-5,-31),

		new Vector2(-5,31),

		new Vector2(5,-31),

		new Vector2(5,31),

		new Vector2(19,-25),

		new Vector2(19,25),

		new Vector2(25,-19),

		new Vector2(25,19),

		new Vector2(31,-5),

		new Vector2(31,5),

		new Vector2(-29,-12),

		new Vector2(-29,12),

		new Vector2(-27,-16),

		new Vector2(-27,16),

		new Vector2(-16,-27),

		new Vector2(-16,27),

		new Vector2(-12,-29),

		new Vector2(-12,29),

		new Vector2(12,-29),

		new Vector2(12,29),

		new Vector2(16,-27),

		new Vector2(16,27),

		new Vector2(27,-16),

		new Vector2(27,16),

		new Vector2(29,-12),

		new Vector2(29,12),

		new Vector2(-30,-9),

		new Vector2(-30,9),

		new Vector2(-9,-30),

		new Vector2(-9,30),

		new Vector2(9,-30),

		new Vector2(9,30),

		new Vector2(30,-9),

		new Vector2(30,9),

		new Vector2(-28,-14),

		new Vector2(-28,14),

		new Vector2(-14,-28),

		new Vector2(-14,28),

		new Vector2(14,-28),

		new Vector2(14,28),

		new Vector2(28,-14),

		new Vector2(28,14),

		new Vector2(-31,-4),

		new Vector2(-31,4),

		new Vector2(-4,-31),

		new Vector2(-4,31),

		new Vector2(4,-31),

		new Vector2(4,31),

		new Vector2(31,-4),

		new Vector2(31,4),

		new Vector2(-24,-20),

		new Vector2(-24,20),

		new Vector2(-20,-24),

		new Vector2(-20,24),

		new Vector2(20,-24),

		new Vector2(20,24),

		new Vector2(24,-20),

		new Vector2(24,20),

		new Vector2(-31,-3),

		new Vector2(-31,3),

		new Vector2(-23,-21),

		new Vector2(-23,21),

		new Vector2(-21,-23),

		new Vector2(-21,23),

		new Vector2(-3,-31),

		new Vector2(-3,31),

		new Vector2(3,-31),

		new Vector2(3,31),

		new Vector2(21,-23),

		new Vector2(21,23),

		new Vector2(23,-21),

		new Vector2(23,21),

		new Vector2(31,-3),

		new Vector2(31,3),

		new Vector2(-22,-22),

		new Vector2(-22,22),

		new Vector2(22,-22),

		new Vector2(22,22),

		new Vector2(-31,-2),

		new Vector2(-31,2),

		new Vector2(-26,-17),

		new Vector2(-26,17),

		new Vector2(-17,-26),

		new Vector2(-17,26),

		new Vector2(-2,-31),

		new Vector2(-2,31),

		new Vector2(2,-31),

		new Vector2(2,31),

		new Vector2(17,-26),

		new Vector2(17,26),

		new Vector2(26,-17),

		new Vector2(26,17),

		new Vector2(31,-2),

		new Vector2(31,2),

		new Vector2(-30,-8),

		new Vector2(-30,8),

		new Vector2(-8,-30),

		new Vector2(-8,30),

		new Vector2(8,-30),

		new Vector2(8,30),

		new Vector2(30,-8),

		new Vector2(30,8),

		new Vector2(-31,-1),

		new Vector2(-31,1),

		new Vector2(-29,-11),

		new Vector2(-29,11),

		new Vector2(-11,-29),

		new Vector2(-11,29),

		new Vector2(-1,-31),

		new Vector2(-1,31),

		new Vector2(1,-31),

		new Vector2(1,31),

		new Vector2(11,-29),

		new Vector2(11,29),

		new Vector2(29,-11),

		new Vector2(29,11),

		new Vector2(31,-1),

		new Vector2(31,1),

		new Vector2(-31,0),

		new Vector2(0,-31),

		new Vector2(0,31),

		new Vector2(31,0),

		new Vector2(-27,-15),

		new Vector2(-27,15),

		new Vector2(-15,-27),

		new Vector2(-15,27),

		new Vector2(15,-27),

		new Vector2(15,27),

		new Vector2(27,-15),

		new Vector2(27,15),

		new Vector2(-28,-13),

		new Vector2(-28,13),

		new Vector2(-13,-28),

		new Vector2(-13,28),

		new Vector2(13,-28),

		new Vector2(13,28),

		new Vector2(28,-13),

		new Vector2(28,13),

		new Vector2(-30,-7),

		new Vector2(-30,7),

		new Vector2(-25,-18),

		new Vector2(-25,18),

		new Vector2(-18,-25),

		new Vector2(-18,25),

		new Vector2(-7,-30),

		new Vector2(-7,30),

		new Vector2(7,-30),

		new Vector2(7,30),

		new Vector2(18,-25),

		new Vector2(18,25),

		new Vector2(25,-18),

		new Vector2(25,18),

		new Vector2(30,-7),

		new Vector2(30,7),

		new Vector2(-29,-10),

		new Vector2(-29,10),

		new Vector2(-10,-29),

		new Vector2(-10,29),

		new Vector2(10,-29),

		new Vector2(10,29),

		new Vector2(29,-10),

		new Vector2(29,10),

		new Vector2(-24,-19),

		new Vector2(-24,19),

		new Vector2(-19,-24),

		new Vector2(-19,24),

		new Vector2(19,-24),

		new Vector2(19,24),

		new Vector2(24,-19),

		new Vector2(24,19),

		new Vector2(-30,-6),

		new Vector2(-30,6),

		new Vector2(-6,-30),

		new Vector2(-6,30),

		new Vector2(6,-30),

		new Vector2(6,30),

		new Vector2(30,-6),

		new Vector2(30,6),

		new Vector2(-26,-16),

		new Vector2(-26,16),

		new Vector2(-16,-26),

		new Vector2(-16,26),

		new Vector2(16,-26),

		new Vector2(16,26),

		new Vector2(26,-16),

		new Vector2(26,16),

		new Vector2(-23,-20),

		new Vector2(-23,20),

		new Vector2(-20,-23),

		new Vector2(-20,23),

		new Vector2(20,-23),

		new Vector2(20,23),

		new Vector2(23,-20),

		new Vector2(23,20),

		new Vector2(-28,-12),

		new Vector2(-28,12),

		new Vector2(-12,-28),

		new Vector2(-12,28),

		new Vector2(12,-28),

		new Vector2(12,28),

		new Vector2(28,-12),

		new Vector2(28,12),

		new Vector2(-30,-5),

		new Vector2(-30,5),

		new Vector2(-27,-14),

		new Vector2(-27,14),

		new Vector2(-22,-21),

		new Vector2(-22,21),

		new Vector2(-21,-22),

		new Vector2(-21,22),

		new Vector2(-14,-27),

		new Vector2(-14,27),

		new Vector2(-5,-30),

		new Vector2(-5,30),

		new Vector2(5,-30),

		new Vector2(5,30),

		new Vector2(14,-27),

		new Vector2(14,27),

		new Vector2(21,-22),

		new Vector2(21,22),

		new Vector2(22,-21),

		new Vector2(22,21),

		new Vector2(27,-14),

		new Vector2(27,14),

		new Vector2(30,-5),

		new Vector2(30,5),

		new Vector2(-29,-9),

		new Vector2(-29,9),

		new Vector2(-9,-29),

		new Vector2(-9,29),

		new Vector2(9,-29),

		new Vector2(9,29),

		new Vector2(29,-9),

		new Vector2(29,9),

		new Vector2(-30,-4),

		new Vector2(-30,4),

		new Vector2(-4,-30),

		new Vector2(-4,30),

		new Vector2(4,-30),

		new Vector2(4,30),

		new Vector2(30,-4),

		new Vector2(30,4),

		new Vector2(-25,-17),

		new Vector2(-25,17),

		new Vector2(-17,-25),

		new Vector2(-17,25),

		new Vector2(17,-25),

		new Vector2(17,25),

		new Vector2(25,-17),

		new Vector2(25,17),

		new Vector2(-30,-3),

		new Vector2(-30,3),

		new Vector2(-3,-30),

		new Vector2(-3,30),

		new Vector2(3,-30),

		new Vector2(3,30),

		new Vector2(30,-3),

		new Vector2(30,3),

		new Vector2(-29,-8),

		new Vector2(-29,8),

		new Vector2(-28,-11),

		new Vector2(-28,11),

		new Vector2(-11,-28),

		new Vector2(-11,28),

		new Vector2(-8,-29),

		new Vector2(-8,29),

		new Vector2(8,-29),

		new Vector2(8,29),

		new Vector2(11,-28),

		new Vector2(11,28),

		new Vector2(28,-11),

		new Vector2(28,11),

		new Vector2(29,-8),

		new Vector2(29,8),

		new Vector2(-30,-2),

		new Vector2(-30,2),

		new Vector2(-2,-30),

		new Vector2(-2,30),

		new Vector2(2,-30),

		new Vector2(2,30),

		new Vector2(30,-2),

		new Vector2(30,2),

		new Vector2(-30,-1),

		new Vector2(-30,1),

		new Vector2(-26,-15),

		new Vector2(-26,15),

		new Vector2(-15,-26),

		new Vector2(-15,26),

		new Vector2(-1,-30),

		new Vector2(-1,30),

		new Vector2(1,-30),

		new Vector2(1,30),

		new Vector2(15,-26),

		new Vector2(15,26),

		new Vector2(26,-15),

		new Vector2(26,15),

		new Vector2(30,-1),

		new Vector2(30,1),

		new Vector2(-30,0),

		new Vector2(-24,-18),

		new Vector2(-24,18),

		new Vector2(-18,-24),

		new Vector2(-18,24),

		new Vector2(0,-30),

		new Vector2(0,30),

		new Vector2(18,-24),

		new Vector2(18,24),

		new Vector2(24,-18),

		new Vector2(24,18),

		new Vector2(30,0),

		new Vector2(-27,-13),

		new Vector2(-27,13),

		new Vector2(-13,-27),

		new Vector2(-13,27),

		new Vector2(13,-27),

		new Vector2(13,27),

		new Vector2(27,-13),

		new Vector2(27,13),

		new Vector2(-29,-7),

		new Vector2(-29,7),

		new Vector2(-23,-19),

		new Vector2(-23,19),

		new Vector2(-19,-23),

		new Vector2(-19,23),

		new Vector2(-7,-29),

		new Vector2(-7,29),

		new Vector2(7,-29),

		new Vector2(7,29),

		new Vector2(19,-23),

		new Vector2(19,23),

		new Vector2(23,-19),

		new Vector2(23,19),

		new Vector2(29,-7),

		new Vector2(29,7),

		new Vector2(-28,-10),

		new Vector2(-28,10),

		new Vector2(-22,-20),

		new Vector2(-22,20),

		new Vector2(-20,-22),

		new Vector2(-20,22),

		new Vector2(-10,-28),

		new Vector2(-10,28),

		new Vector2(10,-28),

		new Vector2(10,28),

		new Vector2(20,-22),

		new Vector2(20,22),

		new Vector2(22,-20),

		new Vector2(22,20),

		new Vector2(28,-10),

		new Vector2(28,10),

		new Vector2(-21,-21),

		new Vector2(-21,21),

		new Vector2(21,-21),

		new Vector2(21,21),

		new Vector2(-25,-16),

		new Vector2(-25,16),

		new Vector2(-16,-25),

		new Vector2(-16,25),

		new Vector2(16,-25),

		new Vector2(16,25),

		new Vector2(25,-16),

		new Vector2(25,16),

		new Vector2(-29,-6),

		new Vector2(-29,6),

		new Vector2(-6,-29),

		new Vector2(-6,29),

		new Vector2(6,-29),

		new Vector2(6,29),

		new Vector2(29,-6),

		new Vector2(29,6),

		new Vector2(-27,-12),

		new Vector2(-27,12),

		new Vector2(-12,-27),

		new Vector2(-12,27),

		new Vector2(12,-27),

		new Vector2(12,27),

		new Vector2(27,-12),

		new Vector2(27,12),

		new Vector2(-26,-14),

		new Vector2(-26,14),

		new Vector2(-14,-26),

		new Vector2(-14,26),

		new Vector2(14,-26),

		new Vector2(14,26),

		new Vector2(26,-14),

		new Vector2(26,14),

		new Vector2(-29,-5),

		new Vector2(-29,5),

		new Vector2(-5,-29),

		new Vector2(-5,29),

		new Vector2(5,-29),

		new Vector2(5,29),

		new Vector2(29,-5),

		new Vector2(29,5),

		new Vector2(-28,-9),

		new Vector2(-28,9),

		new Vector2(-24,-17),

		new Vector2(-24,17),

		new Vector2(-17,-24),

		new Vector2(-17,24),

		new Vector2(-9,-28),

		new Vector2(-9,28),

		new Vector2(9,-28),

		new Vector2(9,28),

		new Vector2(17,-24),

		new Vector2(17,24),

		new Vector2(24,-17),

		new Vector2(24,17),

		new Vector2(28,-9),

		new Vector2(28,9),

		new Vector2(-29,-4),

		new Vector2(-29,4),

		new Vector2(-4,-29),

		new Vector2(-4,29),

		new Vector2(4,-29),

		new Vector2(4,29),

		new Vector2(29,-4),

		new Vector2(29,4),

		new Vector2(-23,-18),

		new Vector2(-23,18),

		new Vector2(-18,-23),

		new Vector2(-18,23),

		new Vector2(18,-23),

		new Vector2(18,23),

		new Vector2(23,-18),

		new Vector2(23,18),

		new Vector2(-29,-3),

		new Vector2(-29,3),

		new Vector2(-27,-11),

		new Vector2(-27,11),

		new Vector2(-25,-15),

		new Vector2(-25,15),

		new Vector2(-15,-25),

		new Vector2(-15,25),

		new Vector2(-11,-27),

		new Vector2(-11,27),

		new Vector2(-3,-29),

		new Vector2(-3,29),

		new Vector2(3,-29),

		new Vector2(3,29),

		new Vector2(11,-27),

		new Vector2(11,27),

		new Vector2(15,-25),

		new Vector2(15,25),

		new Vector2(25,-15),

		new Vector2(25,15),

		new Vector2(27,-11),

		new Vector2(27,11),

		new Vector2(29,-3),

		new Vector2(29,3),

		new Vector2(-28,-8),

		new Vector2(-28,8),

		new Vector2(-8,-28),

		new Vector2(-8,28),

		new Vector2(8,-28),

		new Vector2(8,28),

		new Vector2(28,-8),

		new Vector2(28,8),

		new Vector2(-29,-2),

		new Vector2(-29,2),

		new Vector2(-26,-13),

		new Vector2(-26,13),

		new Vector2(-22,-19),

		new Vector2(-22,19),

		new Vector2(-19,-22),

		new Vector2(-19,22),

		new Vector2(-13,-26),

		new Vector2(-13,26),

		new Vector2(-2,-29),

		new Vector2(-2,29),

		new Vector2(2,-29),

		new Vector2(2,29),

		new Vector2(13,-26),

		new Vector2(13,26),

		new Vector2(19,-22),

		new Vector2(19,22),

		new Vector2(22,-19),

		new Vector2(22,19),

		new Vector2(26,-13),

		new Vector2(26,13),

		new Vector2(29,-2),

		new Vector2(29,2),

		new Vector2(-29,-1),

		new Vector2(-29,1),

		new Vector2(-1,-29),

		new Vector2(-1,29),

		new Vector2(1,-29),

		new Vector2(1,29),

		new Vector2(29,-1),

		new Vector2(29,1),

		new Vector2(-29,0),

		new Vector2(-21,-20),

		new Vector2(-21,20),

		new Vector2(-20,-21),

		new Vector2(-20,21),

		new Vector2(0,-29),

		new Vector2(0,29),

		new Vector2(20,-21),

		new Vector2(20,21),

		new Vector2(21,-20),

		new Vector2(21,20),

		new Vector2(29,0),

		new Vector2(-28,-7),

		new Vector2(-28,7),

		new Vector2(-7,-28),

		new Vector2(-7,28),

		new Vector2(7,-28),

		new Vector2(7,28),

		new Vector2(28,-7),

		new Vector2(28,7),

		new Vector2(-24,-16),

		new Vector2(-24,16),

		new Vector2(-16,-24),

		new Vector2(-16,24),

		new Vector2(16,-24),

		new Vector2(16,24),

		new Vector2(24,-16),

		new Vector2(24,16),

		new Vector2(-27,-10),

		new Vector2(-27,10),

		new Vector2(-10,-27),

		new Vector2(-10,27),

		new Vector2(10,-27),

		new Vector2(10,27),

		new Vector2(27,-10),

		new Vector2(27,10),

		new Vector2(-25,-14),

		new Vector2(-25,14),

		new Vector2(-14,-25),

		new Vector2(-14,25),

		new Vector2(14,-25),

		new Vector2(14,25),

		new Vector2(25,-14),

		new Vector2(25,14),

		new Vector2(-28,-6),

		new Vector2(-28,6),

		new Vector2(-26,-12),

		new Vector2(-26,12),

		new Vector2(-12,-26),

		new Vector2(-12,26),

		new Vector2(-6,-28),

		new Vector2(-6,28),

		new Vector2(6,-28),

		new Vector2(6,28),

		new Vector2(12,-26),

		new Vector2(12,26),

		new Vector2(26,-12),

		new Vector2(26,12),

		new Vector2(28,-6),

		new Vector2(28,6),

		new Vector2(-23,-17),

		new Vector2(-23,17),

		new Vector2(-17,-23),

		new Vector2(-17,23),

		new Vector2(17,-23),

		new Vector2(17,23),

		new Vector2(23,-17),

		new Vector2(23,17),

		new Vector2(-27,-9),

		new Vector2(-27,9),

		new Vector2(-9,-27),

		new Vector2(-9,27),

		new Vector2(9,-27),

		new Vector2(9,27),

		new Vector2(27,-9),

		new Vector2(27,9),

		new Vector2(-28,-5),

		new Vector2(-28,5),

		new Vector2(-5,-28),

		new Vector2(-5,28),

		new Vector2(5,-28),

		new Vector2(5,28),

		new Vector2(28,-5),

		new Vector2(28,5),

		new Vector2(-22,-18),

		new Vector2(-22,18),

		new Vector2(-18,-22),

		new Vector2(-18,22),

		new Vector2(18,-22),

		new Vector2(18,22),

		new Vector2(22,-18),

		new Vector2(22,18),

		new Vector2(-21,-19),

		new Vector2(-21,19),

		new Vector2(-19,-21),

		new Vector2(-19,21),

		new Vector2(19,-21),

		new Vector2(19,21),

		new Vector2(21,-19),

		new Vector2(21,19),

		new Vector2(-24,-15),

		new Vector2(-24,15),

		new Vector2(-15,-24),

		new Vector2(-15,24),

		new Vector2(15,-24),

		new Vector2(15,24),

		new Vector2(24,-15),

		new Vector2(24,15),

		new Vector2(-28,-4),

		new Vector2(-28,4),

		new Vector2(-20,-20),

		new Vector2(-20,20),

		new Vector2(-4,-28),

		new Vector2(-4,28),

		new Vector2(4,-28),

		new Vector2(4,28),

		new Vector2(20,-20),

		new Vector2(20,20),

		new Vector2(28,-4),

		new Vector2(28,4),

		new Vector2(-26,-11),

		new Vector2(-26,11),

		new Vector2(-11,-26),

		new Vector2(-11,26),

		new Vector2(11,-26),

		new Vector2(11,26),

		new Vector2(26,-11),

		new Vector2(26,11),

		new Vector2(-25,-13),

		new Vector2(-25,13),

		new Vector2(-13,-25),

		new Vector2(-13,25),

		new Vector2(13,-25),

		new Vector2(13,25),

		new Vector2(25,-13),

		new Vector2(25,13),

		new Vector2(-28,-3),

		new Vector2(-28,3),

		new Vector2(-27,-8),

		new Vector2(-27,8),

		new Vector2(-8,-27),

		new Vector2(-8,27),

		new Vector2(-3,-28),

		new Vector2(-3,28),

		new Vector2(3,-28),

		new Vector2(3,28),

		new Vector2(8,-27),

		new Vector2(8,27),

		new Vector2(27,-8),

		new Vector2(27,8),

		new Vector2(28,-3),

		new Vector2(28,3),

		new Vector2(-28,-2),

		new Vector2(-28,2),

		new Vector2(-2,-28),

		new Vector2(-2,28),

		new Vector2(2,-28),

		new Vector2(2,28),

		new Vector2(28,-2),

		new Vector2(28,2),

		new Vector2(-28,-1),

		new Vector2(-28,1),

		new Vector2(-23,-16),

		new Vector2(-23,16),

		new Vector2(-16,-23),

		new Vector2(-16,23),

		new Vector2(-1,-28),

		new Vector2(-1,28),

		new Vector2(1,-28),

		new Vector2(1,28),

		new Vector2(16,-23),

		new Vector2(16,23),

		new Vector2(23,-16),

		new Vector2(23,16),

		new Vector2(28,-1),

		new Vector2(28,1),

		new Vector2(-28,0),

		new Vector2(0,-28),

		new Vector2(0,28),

		new Vector2(28,0),

		new Vector2(-27,-7),

		new Vector2(-27,7),

		new Vector2(-7,-27),

		new Vector2(-7,27),

		new Vector2(7,-27),

		new Vector2(7,27),

		new Vector2(27,-7),

		new Vector2(27,7),

		new Vector2(-26,-10),

		new Vector2(-26,10),

		new Vector2(-10,-26),

		new Vector2(-10,26),

		new Vector2(10,-26),

		new Vector2(10,26),

		new Vector2(26,-10),

		new Vector2(26,10),

		new Vector2(-22,-17),

		new Vector2(-22,17),

		new Vector2(-17,-22),

		new Vector2(-17,22),

		new Vector2(17,-22),

		new Vector2(17,22),

		new Vector2(22,-17),

		new Vector2(22,17),

		new Vector2(-24,-14),

		new Vector2(-24,14),

		new Vector2(-14,-24),

		new Vector2(-14,24),

		new Vector2(14,-24),

		new Vector2(14,24),

		new Vector2(24,-14),

		new Vector2(24,14),

		new Vector2(-25,-12),

		new Vector2(-25,12),

		new Vector2(-12,-25),

		new Vector2(-12,25),

		new Vector2(12,-25),

		new Vector2(12,25),

		new Vector2(25,-12),

		new Vector2(25,12),

		new Vector2(-27,-6),

		new Vector2(-27,6),

		new Vector2(-21,-18),

		new Vector2(-21,18),

		new Vector2(-18,-21),

		new Vector2(-18,21),

		new Vector2(-6,-27),

		new Vector2(-6,27),

		new Vector2(6,-27),

		new Vector2(6,27),

		new Vector2(18,-21),

		new Vector2(18,21),

		new Vector2(21,-18),

		new Vector2(21,18),

		new Vector2(27,-6),

		new Vector2(27,6),

		new Vector2(-20,-19),

		new Vector2(-20,19),

		new Vector2(-19,-20),

		new Vector2(-19,20),

		new Vector2(19,-20),

		new Vector2(19,20),

		new Vector2(20,-19),

		new Vector2(20,19),

		new Vector2(-26,-9),

		new Vector2(-26,9),

		new Vector2(-9,-26),

		new Vector2(-9,26),

		new Vector2(9,-26),

		new Vector2(9,26),

		new Vector2(26,-9),

		new Vector2(26,9),

		new Vector2(-27,-5),

		new Vector2(-27,5),

		new Vector2(-23,-15),

		new Vector2(-23,15),

		new Vector2(-15,-23),

		new Vector2(-15,23),

		new Vector2(-5,-27),

		new Vector2(-5,27),

		new Vector2(5,-27),

		new Vector2(5,27),

		new Vector2(15,-23),

		new Vector2(15,23),

		new Vector2(23,-15),

		new Vector2(23,15),

		new Vector2(27,-5),

		new Vector2(27,5),

		new Vector2(-25,-11),

		new Vector2(-25,11),

		new Vector2(-11,-25),

		new Vector2(-11,25),

		new Vector2(11,-25),

		new Vector2(11,25),

		new Vector2(25,-11),

		new Vector2(25,11),

		new Vector2(-27,-4),

		new Vector2(-27,4),

		new Vector2(-24,-13),

		new Vector2(-24,13),

		new Vector2(-13,-24),

		new Vector2(-13,24),

		new Vector2(-4,-27),

		new Vector2(-4,27),

		new Vector2(4,-27),

		new Vector2(4,27),

		new Vector2(13,-24),

		new Vector2(13,24),

		new Vector2(24,-13),

		new Vector2(24,13),

		new Vector2(27,-4),

		new Vector2(27,4),

		new Vector2(-26,-8),

		new Vector2(-26,8),

		new Vector2(-22,-16),

		new Vector2(-22,16),

		new Vector2(-16,-22),

		new Vector2(-16,22),

		new Vector2(-8,-26),

		new Vector2(-8,26),

		new Vector2(8,-26),

		new Vector2(8,26),

		new Vector2(16,-22),

		new Vector2(16,22),

		new Vector2(22,-16),

		new Vector2(22,16),

		new Vector2(26,-8),

		new Vector2(26,8),

		new Vector2(-27,-3),

		new Vector2(-27,3),

		new Vector2(-3,-27),

		new Vector2(-3,27),

		new Vector2(3,-27),

		new Vector2(3,27),

		new Vector2(27,-3),

		new Vector2(27,3),

		new Vector2(-27,-2),

		new Vector2(-27,2),

		new Vector2(-2,-27),

		new Vector2(-2,27),

		new Vector2(2,-27),

		new Vector2(2,27),

		new Vector2(27,-2),

		new Vector2(27,2),

		new Vector2(-27,-1),

		new Vector2(-27,1),

		new Vector2(-21,-17),

		new Vector2(-21,17),

		new Vector2(-17,-21),

		new Vector2(-17,21),

		new Vector2(-1,-27),

		new Vector2(-1,27),

		new Vector2(1,-27),

		new Vector2(1,27),

		new Vector2(17,-21),

		new Vector2(17,21),

		new Vector2(21,-17),

		new Vector2(21,17),

		new Vector2(27,-1),

		new Vector2(27,1),

		new Vector2(-27,0),

		new Vector2(0,-27),

		new Vector2(0,27),

		new Vector2(27,0),

		new Vector2(-26,-7),

		new Vector2(-26,7),

		new Vector2(-25,-10),

		new Vector2(-25,10),

		new Vector2(-23,-14),

		new Vector2(-23,14),

		new Vector2(-14,-23),

		new Vector2(-14,23),

		new Vector2(-10,-25),

		new Vector2(-10,25),

		new Vector2(-7,-26),

		new Vector2(-7,26),

		new Vector2(7,-26),

		new Vector2(7,26),

		new Vector2(10,-25),

		new Vector2(10,25),

		new Vector2(14,-23),

		new Vector2(14,23),

		new Vector2(23,-14),

		new Vector2(23,14),

		new Vector2(25,-10),

		new Vector2(25,10),

		new Vector2(26,-7),

		new Vector2(26,7),

		new Vector2(-20,-18),

		new Vector2(-20,18),

		new Vector2(-18,-20),

		new Vector2(-18,20),

		new Vector2(18,-20),

		new Vector2(18,20),

		new Vector2(20,-18),

		new Vector2(20,18),

		new Vector2(-19,-19),

		new Vector2(-19,19),

		new Vector2(19,-19),

		new Vector2(19,19),

		new Vector2(-24,-12),

		new Vector2(-24,12),

		new Vector2(-12,-24),

		new Vector2(-12,24),

		new Vector2(12,-24),

		new Vector2(12,24),

		new Vector2(24,-12),

		new Vector2(24,12),

		new Vector2(-26,-6),

		new Vector2(-26,6),

		new Vector2(-6,-26),

		new Vector2(-6,26),

		new Vector2(6,-26),

		new Vector2(6,26),

		new Vector2(26,-6),

		new Vector2(26,6),

		new Vector2(-22,-15),

		new Vector2(-22,15),

		new Vector2(-15,-22),

		new Vector2(-15,22),

		new Vector2(15,-22),

		new Vector2(15,22),

		new Vector2(22,-15),

		new Vector2(22,15),

		new Vector2(-25,-9),

		new Vector2(-25,9),

		new Vector2(-9,-25),

		new Vector2(-9,25),

		new Vector2(9,-25),

		new Vector2(9,25),

		new Vector2(25,-9),

		new Vector2(25,9),

		new Vector2(-26,-5),

		new Vector2(-26,5),

		new Vector2(-5,-26),

		new Vector2(-5,26),

		new Vector2(5,-26),

		new Vector2(5,26),

		new Vector2(26,-5),

		new Vector2(26,5),

		new Vector2(-23,-13),

		new Vector2(-23,13),

		new Vector2(-13,-23),

		new Vector2(-13,23),

		new Vector2(13,-23),

		new Vector2(13,23),

		new Vector2(23,-13),

		new Vector2(23,13),

		new Vector2(-24,-11),

		new Vector2(-24,11),

		new Vector2(-21,-16),

		new Vector2(-21,16),

		new Vector2(-16,-21),

		new Vector2(-16,21),

		new Vector2(-11,-24),

		new Vector2(-11,24),

		new Vector2(11,-24),

		new Vector2(11,24),

		new Vector2(16,-21),

		new Vector2(16,21),

		new Vector2(21,-16),

		new Vector2(21,16),

		new Vector2(24,-11),

		new Vector2(24,11),

		new Vector2(-26,-4),

		new Vector2(-26,4),

		new Vector2(-4,-26),

		new Vector2(-4,26),

		new Vector2(4,-26),

		new Vector2(4,26),

		new Vector2(26,-4),

		new Vector2(26,4),

		new Vector2(-25,-8),

		new Vector2(-25,8),

		new Vector2(-20,-17),

		new Vector2(-20,17),

		new Vector2(-17,-20),

		new Vector2(-17,20),

		new Vector2(-8,-25),

		new Vector2(-8,25),

		new Vector2(8,-25),

		new Vector2(8,25),

		new Vector2(17,-20),

		new Vector2(17,20),

		new Vector2(20,-17),

		new Vector2(20,17),

		new Vector2(25,-8),

		new Vector2(25,8),

		new Vector2(-26,-3),

		new Vector2(-26,3),

		new Vector2(-19,-18),

		new Vector2(-19,18),

		new Vector2(-18,-19),

		new Vector2(-18,19),

		new Vector2(-3,-26),

		new Vector2(-3,26),

		new Vector2(3,-26),

		new Vector2(3,26),

		new Vector2(18,-19),

		new Vector2(18,19),

		new Vector2(19,-18),

		new Vector2(19,18),

		new Vector2(26,-3),

		new Vector2(26,3),

		new Vector2(-26,-2),

		new Vector2(-26,2),

		new Vector2(-22,-14),

		new Vector2(-22,14),

		new Vector2(-14,-22),

		new Vector2(-14,22),

		new Vector2(-2,-26),

		new Vector2(-2,26),

		new Vector2(2,-26),

		new Vector2(2,26),

		new Vector2(14,-22),

		new Vector2(14,22),

		new Vector2(22,-14),

		new Vector2(22,14),

		new Vector2(26,-2),

		new Vector2(26,2),

		new Vector2(-26,-1),

		new Vector2(-26,1),

		new Vector2(-1,-26),

		new Vector2(-1,26),

		new Vector2(1,-26),

		new Vector2(1,26),

		new Vector2(26,-1),

		new Vector2(26,1),

		new Vector2(-26,0),

		new Vector2(-24,-10),

		new Vector2(-24,10),

		new Vector2(-10,-24),

		new Vector2(-10,24),

		new Vector2(0,-26),

		new Vector2(0,26),

		new Vector2(10,-24),

		new Vector2(10,24),

		new Vector2(24,-10),

		new Vector2(24,10),

		new Vector2(26,0),

		new Vector2(-25,-7),

		new Vector2(-25,7),

		new Vector2(-7,-25),

		new Vector2(-7,25),

		new Vector2(7,-25),

		new Vector2(7,25),

		new Vector2(25,-7),

		new Vector2(25,7),

		new Vector2(-23,-12),

		new Vector2(-23,12),

		new Vector2(-12,-23),

		new Vector2(-12,23),

		new Vector2(12,-23),

		new Vector2(12,23),

		new Vector2(23,-12),

		new Vector2(23,12),

		new Vector2(-21,-15),

		new Vector2(-21,15),

		new Vector2(-15,-21),

		new Vector2(-15,21),

		new Vector2(15,-21),

		new Vector2(15,21),

		new Vector2(21,-15),

		new Vector2(21,15),

		new Vector2(-25,-6),

		new Vector2(-25,6),

		new Vector2(-6,-25),

		new Vector2(-6,25),

		new Vector2(6,-25),

		new Vector2(6,25),

		new Vector2(25,-6),

		new Vector2(25,6),

		new Vector2(-24,-9),

		new Vector2(-24,9),

		new Vector2(-9,-24),

		new Vector2(-9,24),

		new Vector2(9,-24),

		new Vector2(9,24),

		new Vector2(24,-9),

		new Vector2(24,9),

		new Vector2(-20,-16),

		new Vector2(-20,16),

		new Vector2(-16,-20),

		new Vector2(-16,20),

		new Vector2(16,-20),

		new Vector2(16,20),

		new Vector2(20,-16),

		new Vector2(20,16),

		new Vector2(-22,-13),

		new Vector2(-22,13),

		new Vector2(-13,-22),

		new Vector2(-13,22),

		new Vector2(13,-22),

		new Vector2(13,22),

		new Vector2(22,-13),

		new Vector2(22,13),

		new Vector2(-25,-5),

		new Vector2(-25,5),

		new Vector2(-23,-11),

		new Vector2(-23,11),

		new Vector2(-19,-17),

		new Vector2(-19,17),

		new Vector2(-17,-19),

		new Vector2(-17,19),

		new Vector2(-11,-23),

		new Vector2(-11,23),

		new Vector2(-5,-25),

		new Vector2(-5,25),

		new Vector2(5,-25),

		new Vector2(5,25),

		new Vector2(11,-23),

		new Vector2(11,23),

		new Vector2(17,-19),

		new Vector2(17,19),

		new Vector2(19,-17),

		new Vector2(19,17),

		new Vector2(23,-11),

		new Vector2(23,11),

		new Vector2(25,-5),

		new Vector2(25,5),

		new Vector2(-18,-18),

		new Vector2(-18,18),

		new Vector2(18,-18),

		new Vector2(18,18),

		new Vector2(-25,-4),

		new Vector2(-25,4),

		new Vector2(-4,-25),

		new Vector2(-4,25),

		new Vector2(4,-25),

		new Vector2(4,25),

		new Vector2(25,-4),

		new Vector2(25,4),

		new Vector2(-24,-8),

		new Vector2(-24,8),

		new Vector2(-8,-24),

		new Vector2(-8,24),

		new Vector2(8,-24),

		new Vector2(8,24),

		new Vector2(24,-8),

		new Vector2(24,8),

		new Vector2(-21,-14),

		new Vector2(-21,14),

		new Vector2(-14,-21),

		new Vector2(-14,21),

		new Vector2(14,-21),

		new Vector2(14,21),

		new Vector2(21,-14),

		new Vector2(21,14),

		new Vector2(-25,-3),

		new Vector2(-25,3),

		new Vector2(-3,-25),

		new Vector2(-3,25),

		new Vector2(3,-25),

		new Vector2(3,25),

		new Vector2(25,-3),

		new Vector2(25,3),

		new Vector2(-25,-2),

		new Vector2(-25,2),

		new Vector2(-23,-10),

		new Vector2(-23,10),

		new Vector2(-10,-23),

		new Vector2(-10,23),

		new Vector2(-2,-25),

		new Vector2(-2,25),

		new Vector2(2,-25),

		new Vector2(2,25),

		new Vector2(10,-23),

		new Vector2(10,23),

		new Vector2(23,-10),

		new Vector2(23,10),

		new Vector2(25,-2),

		new Vector2(25,2),

		new Vector2(-22,-12),

		new Vector2(-22,12),

		new Vector2(-12,-22),

		new Vector2(-12,22),

		new Vector2(12,-22),

		new Vector2(12,22),

		new Vector2(22,-12),

		new Vector2(22,12),

		new Vector2(-25,-1),

		new Vector2(-25,1),

		new Vector2(-1,-25),

		new Vector2(-1,25),

		new Vector2(1,-25),

		new Vector2(1,25),

		new Vector2(25,-1),

		new Vector2(25,1),

		new Vector2(-25,0),

		new Vector2(-24,-7),

		new Vector2(-24,7),

		new Vector2(-20,-15),

		new Vector2(-20,15),

		new Vector2(-15,-20),

		new Vector2(-15,20),

		new Vector2(-7,-24),

		new Vector2(-7,24),

		new Vector2(0,-25),

		new Vector2(0,25),

		new Vector2(7,-24),

		new Vector2(7,24),

		new Vector2(15,-20),

		new Vector2(15,20),

		new Vector2(20,-15),

		new Vector2(20,15),

		new Vector2(24,-7),

		new Vector2(24,7),

		new Vector2(25,0),

		new Vector2(-19,-16),

		new Vector2(-19,16),

		new Vector2(-16,-19),

		new Vector2(-16,19),

		new Vector2(16,-19),

		new Vector2(16,19),

		new Vector2(19,-16),

		new Vector2(19,16),

		new Vector2(-18,-17),

		new Vector2(-18,17),

		new Vector2(-17,-18),

		new Vector2(-17,18),

		new Vector2(17,-18),

		new Vector2(17,18),

		new Vector2(18,-17),

		new Vector2(18,17),

		new Vector2(-24,-6),

		new Vector2(-24,6),

		new Vector2(-6,-24),

		new Vector2(-6,24),

		new Vector2(6,-24),

		new Vector2(6,24),

		new Vector2(24,-6),

		new Vector2(24,6),

		new Vector2(-23,-9),

		new Vector2(-23,9),

		new Vector2(-21,-13),

		new Vector2(-21,13),

		new Vector2(-13,-21),

		new Vector2(-13,21),

		new Vector2(-9,-23),

		new Vector2(-9,23),

		new Vector2(9,-23),

		new Vector2(9,23),

		new Vector2(13,-21),

		new Vector2(13,21),

		new Vector2(21,-13),

		new Vector2(21,13),

		new Vector2(23,-9),

		new Vector2(23,9),

		new Vector2(-22,-11),

		new Vector2(-22,11),

		new Vector2(-11,-22),

		new Vector2(-11,22),

		new Vector2(11,-22),

		new Vector2(11,22),

		new Vector2(22,-11),

		new Vector2(22,11),

		new Vector2(-24,-5),

		new Vector2(-24,5),

		new Vector2(-5,-24),

		new Vector2(-5,24),

		new Vector2(5,-24),

		new Vector2(5,24),

		new Vector2(24,-5),

		new Vector2(24,5),

		new Vector2(-20,-14),

		new Vector2(-20,14),

		new Vector2(-14,-20),

		new Vector2(-14,20),

		new Vector2(14,-20),

		new Vector2(14,20),

		new Vector2(20,-14),

		new Vector2(20,14),

		new Vector2(-23,-8),

		new Vector2(-23,8),

		new Vector2(-8,-23),

		new Vector2(-8,23),

		new Vector2(8,-23),

		new Vector2(8,23),

		new Vector2(23,-8),

		new Vector2(23,8),

		new Vector2(-24,-4),

		new Vector2(-24,4),

		new Vector2(-4,-24),

		new Vector2(-4,24),

		new Vector2(4,-24),

		new Vector2(4,24),

		new Vector2(24,-4),

		new Vector2(24,4),

		new Vector2(-19,-15),

		new Vector2(-19,15),

		new Vector2(-15,-19),

		new Vector2(-15,19),

		new Vector2(15,-19),

		new Vector2(15,19),

		new Vector2(19,-15),

		new Vector2(19,15),

		new Vector2(-24,-3),

		new Vector2(-24,3),

		new Vector2(-21,-12),

		new Vector2(-21,12),

		new Vector2(-12,-21),

		new Vector2(-12,21),

		new Vector2(-3,-24),

		new Vector2(-3,24),

		new Vector2(3,-24),

		new Vector2(3,24),

		new Vector2(12,-21),

		new Vector2(12,21),

		new Vector2(21,-12),

		new Vector2(21,12),

		new Vector2(24,-3),

		new Vector2(24,3),

		new Vector2(-22,-10),

		new Vector2(-22,10),

		new Vector2(-10,-22),

		new Vector2(-10,22),

		new Vector2(10,-22),

		new Vector2(10,22),

		new Vector2(22,-10),

		new Vector2(22,10),

		new Vector2(-24,-2),

		new Vector2(-24,2),

		new Vector2(-18,-16),

		new Vector2(-18,16),

		new Vector2(-16,-18),

		new Vector2(-16,18),

		new Vector2(-2,-24),

		new Vector2(-2,24),

		new Vector2(2,-24),

		new Vector2(2,24),

		new Vector2(16,-18),

		new Vector2(16,18),

		new Vector2(18,-16),

		new Vector2(18,16),

		new Vector2(24,-2),

		new Vector2(24,2),

		new Vector2(-23,-7),

		new Vector2(-23,7),

		new Vector2(-17,-17),

		new Vector2(-17,17),

		new Vector2(-7,-23),

		new Vector2(-7,23),

		new Vector2(7,-23),

		new Vector2(7,23),

		new Vector2(17,-17),

		new Vector2(17,17),

		new Vector2(23,-7),

		new Vector2(23,7),

		new Vector2(-24,-1),

		new Vector2(-24,1),

		new Vector2(-1,-24),

		new Vector2(-1,24),

		new Vector2(1,-24),

		new Vector2(1,24),

		new Vector2(24,-1),

		new Vector2(24,1),

		new Vector2(-24,0),

		new Vector2(0,-24),

		new Vector2(0,24),

		new Vector2(24,0),

		new Vector2(-20,-13),

		new Vector2(-20,13),

		new Vector2(-13,-20),

		new Vector2(-13,20),

		new Vector2(13,-20),

		new Vector2(13,20),

		new Vector2(20,-13),

		new Vector2(20,13),

		new Vector2(-23,-6),

		new Vector2(-23,6),

		new Vector2(-22,-9),

		new Vector2(-22,9),

		new Vector2(-9,-22),

		new Vector2(-9,22),

		new Vector2(-6,-23),

		new Vector2(-6,23),

		new Vector2(6,-23),

		new Vector2(6,23),

		new Vector2(9,-22),

		new Vector2(9,22),

		new Vector2(22,-9),

		new Vector2(22,9),

		new Vector2(23,-6),

		new Vector2(23,6),

		new Vector2(-21,-11),

		new Vector2(-21,11),

		new Vector2(-11,-21),

		new Vector2(-11,21),

		new Vector2(11,-21),

		new Vector2(11,21),

		new Vector2(21,-11),

		new Vector2(21,11),

		new Vector2(-19,-14),

		new Vector2(-19,14),

		new Vector2(-14,-19),

		new Vector2(-14,19),

		new Vector2(14,-19),

		new Vector2(14,19),

		new Vector2(19,-14),

		new Vector2(19,14),

		new Vector2(-23,-5),

		new Vector2(-23,5),

		new Vector2(-5,-23),

		new Vector2(-5,23),

		new Vector2(5,-23),

		new Vector2(5,23),

		new Vector2(23,-5),

		new Vector2(23,5),

		new Vector2(-18,-15),

		new Vector2(-18,15),

		new Vector2(-15,-18),

		new Vector2(-15,18),

		new Vector2(15,-18),

		new Vector2(15,18),

		new Vector2(18,-15),

		new Vector2(18,15),

		new Vector2(-22,-8),

		new Vector2(-22,8),

		new Vector2(-8,-22),

		new Vector2(-8,22),

		new Vector2(8,-22),

		new Vector2(8,22),

		new Vector2(22,-8),

		new Vector2(22,8),

		new Vector2(-23,-4),

		new Vector2(-23,4),

		new Vector2(-17,-16),

		new Vector2(-17,16),

		new Vector2(-16,-17),

		new Vector2(-16,17),

		new Vector2(-4,-23),

		new Vector2(-4,23),

		new Vector2(4,-23),

		new Vector2(4,23),

		new Vector2(16,-17),

		new Vector2(16,17),

		new Vector2(17,-16),

		new Vector2(17,16),

		new Vector2(23,-4),

		new Vector2(23,4),

		new Vector2(-20,-12),

		new Vector2(-20,12),

		new Vector2(-12,-20),

		new Vector2(-12,20),

		new Vector2(12,-20),

		new Vector2(12,20),

		new Vector2(20,-12),

		new Vector2(20,12),

		new Vector2(-21,-10),

		new Vector2(-21,10),

		new Vector2(-10,-21),

		new Vector2(-10,21),

		new Vector2(10,-21),

		new Vector2(10,21),

		new Vector2(21,-10),

		new Vector2(21,10),

		new Vector2(-23,-3),

		new Vector2(-23,3),

		new Vector2(-3,-23),

		new Vector2(-3,23),

		new Vector2(3,-23),

		new Vector2(3,23),

		new Vector2(23,-3),

		new Vector2(23,3),

		new Vector2(-23,-2),

		new Vector2(-23,2),

		new Vector2(-22,-7),

		new Vector2(-22,7),

		new Vector2(-7,-22),

		new Vector2(-7,22),

		new Vector2(-2,-23),

		new Vector2(-2,23),

		new Vector2(2,-23),

		new Vector2(2,23),

		new Vector2(7,-22),

		new Vector2(7,22),

		new Vector2(22,-7),

		new Vector2(22,7),

		new Vector2(23,-2),

		new Vector2(23,2),

		new Vector2(-23,-1),

		new Vector2(-23,1),

		new Vector2(-19,-13),

		new Vector2(-19,13),

		new Vector2(-13,-19),

		new Vector2(-13,19),

		new Vector2(-1,-23),

		new Vector2(-1,23),

		new Vector2(1,-23),

		new Vector2(1,23),

		new Vector2(13,-19),

		new Vector2(13,19),

		new Vector2(19,-13),

		new Vector2(19,13),

		new Vector2(23,-1),

		new Vector2(23,1),

		new Vector2(-23,0),

		new Vector2(0,-23),

		new Vector2(0,23),

		new Vector2(23,0),

		new Vector2(-21,-9),

		new Vector2(-21,9),

		new Vector2(-9,-21),

		new Vector2(-9,21),

		new Vector2(9,-21),

		new Vector2(9,21),

		new Vector2(21,-9),

		new Vector2(21,9),

		new Vector2(-20,-11),

		new Vector2(-20,11),

		new Vector2(-11,-20),

		new Vector2(-11,20),

		new Vector2(11,-20),

		new Vector2(11,20),

		new Vector2(20,-11),

		new Vector2(20,11),

		new Vector2(-22,-6),

		new Vector2(-22,6),

		new Vector2(-18,-14),

		new Vector2(-18,14),

		new Vector2(-14,-18),

		new Vector2(-14,18),

		new Vector2(-6,-22),

		new Vector2(-6,22),

		new Vector2(6,-22),

		new Vector2(6,22),

		new Vector2(14,-18),

		new Vector2(14,18),

		new Vector2(18,-14),

		new Vector2(18,14),

		new Vector2(22,-6),

		new Vector2(22,6),

		new Vector2(-17,-15),

		new Vector2(-17,15),

		new Vector2(-15,-17),

		new Vector2(-15,17),

		new Vector2(15,-17),

		new Vector2(15,17),

		new Vector2(17,-15),

		new Vector2(17,15),

		new Vector2(-16,-16),

		new Vector2(-16,16),

		new Vector2(16,-16),

		new Vector2(16,16),

		new Vector2(-22,-5),

		new Vector2(-22,5),

		new Vector2(-5,-22),

		new Vector2(-5,22),

		new Vector2(5,-22),

		new Vector2(5,22),

		new Vector2(22,-5),

		new Vector2(22,5),

		new Vector2(-21,-8),

		new Vector2(-21,8),

		new Vector2(-19,-12),

		new Vector2(-19,12),

		new Vector2(-12,-19),

		new Vector2(-12,19),

		new Vector2(-8,-21),

		new Vector2(-8,21),

		new Vector2(8,-21),

		new Vector2(8,21),

		new Vector2(12,-19),

		new Vector2(12,19),

		new Vector2(19,-12),

		new Vector2(19,12),

		new Vector2(21,-8),

		new Vector2(21,8),

		new Vector2(-22,-4),

		new Vector2(-22,4),

		new Vector2(-20,-10),

		new Vector2(-20,10),

		new Vector2(-10,-20),

		new Vector2(-10,20),

		new Vector2(-4,-22),

		new Vector2(-4,22),

		new Vector2(4,-22),

		new Vector2(4,22),

		new Vector2(10,-20),

		new Vector2(10,20),

		new Vector2(20,-10),

		new Vector2(20,10),

		new Vector2(22,-4),

		new Vector2(22,4),

		new Vector2(-22,-3),

		new Vector2(-22,3),

		new Vector2(-18,-13),

		new Vector2(-18,13),

		new Vector2(-13,-18),

		new Vector2(-13,18),

		new Vector2(-3,-22),

		new Vector2(-3,22),

		new Vector2(3,-22),

		new Vector2(3,22),

		new Vector2(13,-18),

		new Vector2(13,18),

		new Vector2(18,-13),

		new Vector2(18,13),

		new Vector2(22,-3),

		new Vector2(22,3),

		new Vector2(-21,-7),

		new Vector2(-21,7),

		new Vector2(-7,-21),

		new Vector2(-7,21),

		new Vector2(7,-21),

		new Vector2(7,21),

		new Vector2(21,-7),

		new Vector2(21,7),

		new Vector2(-22,-2),

		new Vector2(-22,2),

		new Vector2(-2,-22),

		new Vector2(-2,22),

		new Vector2(2,-22),

		new Vector2(2,22),

		new Vector2(22,-2),

		new Vector2(22,2),

		new Vector2(-22,-1),

		new Vector2(-22,1),

		new Vector2(-17,-14),

		new Vector2(-17,14),

		new Vector2(-14,-17),

		new Vector2(-14,17),

		new Vector2(-1,-22),

		new Vector2(-1,22),

		new Vector2(1,-22),

		new Vector2(1,22),

		new Vector2(14,-17),

		new Vector2(14,17),

		new Vector2(17,-14),

		new Vector2(17,14),

		new Vector2(22,-1),

		new Vector2(22,1),

		new Vector2(-22,0),

		new Vector2(0,-22),

		new Vector2(0,22),

		new Vector2(22,0),

		new Vector2(-19,-11),

		new Vector2(-19,11),

		new Vector2(-11,-19),

		new Vector2(-11,19),

		new Vector2(11,-19),

		new Vector2(11,19),

		new Vector2(19,-11),

		new Vector2(19,11),

		new Vector2(-20,-9),

		new Vector2(-20,9),

		new Vector2(-16,-15),

		new Vector2(-16,15),

		new Vector2(-15,-16),

		new Vector2(-15,16),

		new Vector2(-9,-20),

		new Vector2(-9,20),

		new Vector2(9,-20),

		new Vector2(9,20),

		new Vector2(15,-16),

		new Vector2(15,16),

		new Vector2(16,-15),

		new Vector2(16,15),

		new Vector2(20,-9),

		new Vector2(20,9),

		new Vector2(-21,-6),

		new Vector2(-21,6),

		new Vector2(-6,-21),

		new Vector2(-6,21),

		new Vector2(6,-21),

		new Vector2(6,21),

		new Vector2(21,-6),

		new Vector2(21,6),

		new Vector2(-18,-12),

		new Vector2(-18,12),

		new Vector2(-12,-18),

		new Vector2(-12,18),

		new Vector2(12,-18),

		new Vector2(12,18),

		new Vector2(18,-12),

		new Vector2(18,12),

		new Vector2(-21,-5),

		new Vector2(-21,5),

		new Vector2(-5,-21),

		new Vector2(-5,21),

		new Vector2(5,-21),

		new Vector2(5,21),

		new Vector2(21,-5),

		new Vector2(21,5),

		new Vector2(-20,-8),

		new Vector2(-20,8),

		new Vector2(-8,-20),

		new Vector2(-8,20),

		new Vector2(8,-20),

		new Vector2(8,20),

		new Vector2(20,-8),

		new Vector2(20,8),

		new Vector2(-19,-10),

		new Vector2(-19,10),

		new Vector2(-10,-19),

		new Vector2(-10,19),

		new Vector2(10,-19),

		new Vector2(10,19),

		new Vector2(19,-10),

		new Vector2(19,10),

		new Vector2(-17,-13),

		new Vector2(-17,13),

		new Vector2(-13,-17),

		new Vector2(-13,17),

		new Vector2(13,-17),

		new Vector2(13,17),

		new Vector2(17,-13),

		new Vector2(17,13),

		new Vector2(-21,-4),

		new Vector2(-21,4),

		new Vector2(-4,-21),

		new Vector2(-4,21),

		new Vector2(4,-21),

		new Vector2(4,21),

		new Vector2(21,-4),

		new Vector2(21,4),

		new Vector2(-16,-14),

		new Vector2(-16,14),

		new Vector2(-14,-16),

		new Vector2(-14,16),

		new Vector2(14,-16),

		new Vector2(14,16),

		new Vector2(16,-14),

		new Vector2(16,14),

		new Vector2(-21,-3),

		new Vector2(-21,3),

		new Vector2(-15,-15),

		new Vector2(-15,15),

		new Vector2(-3,-21),

		new Vector2(-3,21),

		new Vector2(3,-21),

		new Vector2(3,21),

		new Vector2(15,-15),

		new Vector2(15,15),

		new Vector2(21,-3),

		new Vector2(21,3),

		new Vector2(-20,-7),

		new Vector2(-20,7),

		new Vector2(-7,-20),

		new Vector2(-7,20),

		new Vector2(7,-20),

		new Vector2(7,20),

		new Vector2(20,-7),

		new Vector2(20,7),

		new Vector2(-21,-2),

		new Vector2(-21,2),

		new Vector2(-18,-11),

		new Vector2(-18,11),

		new Vector2(-11,-18),

		new Vector2(-11,18),

		new Vector2(-2,-21),

		new Vector2(-2,21),

		new Vector2(2,-21),

		new Vector2(2,21),

		new Vector2(11,-18),

		new Vector2(11,18),

		new Vector2(18,-11),

		new Vector2(18,11),

		new Vector2(21,-2),

		new Vector2(21,2),

		new Vector2(-21,-1),

		new Vector2(-21,1),

		new Vector2(-19,-9),

		new Vector2(-19,9),

		new Vector2(-9,-19),

		new Vector2(-9,19),

		new Vector2(-1,-21),

		new Vector2(-1,21),

		new Vector2(1,-21),

		new Vector2(1,21),

		new Vector2(9,-19),

		new Vector2(9,19),

		new Vector2(19,-9),

		new Vector2(19,9),

		new Vector2(21,-1),

		new Vector2(21,1),

		new Vector2(-21,0),

		new Vector2(0,-21),

		new Vector2(0,21),

		new Vector2(21,0),

		new Vector2(-20,-6),

		new Vector2(-20,6),

		new Vector2(-6,-20),

		new Vector2(-6,20),

		new Vector2(6,-20),

		new Vector2(6,20),

		new Vector2(20,-6),

		new Vector2(20,6),

		new Vector2(-17,-12),

		new Vector2(-17,12),

		new Vector2(-12,-17),

		new Vector2(-12,17),

		new Vector2(12,-17),

		new Vector2(12,17),

		new Vector2(17,-12),

		new Vector2(17,12),

		new Vector2(-20,-5),

		new Vector2(-20,5),

		new Vector2(-19,-8),

		new Vector2(-19,8),

		new Vector2(-16,-13),

		new Vector2(-16,13),

		new Vector2(-13,-16),

		new Vector2(-13,16),

		new Vector2(-8,-19),

		new Vector2(-8,19),

		new Vector2(-5,-20),

		new Vector2(-5,20),

		new Vector2(5,-20),

		new Vector2(5,20),

		new Vector2(8,-19),

		new Vector2(8,19),

		new Vector2(13,-16),

		new Vector2(13,16),

		new Vector2(16,-13),

		new Vector2(16,13),

		new Vector2(19,-8),

		new Vector2(19,8),

		new Vector2(20,-5),

		new Vector2(20,5),

		new Vector2(-18,-10),

		new Vector2(-18,10),

		new Vector2(-10,-18),

		new Vector2(-10,18),

		new Vector2(10,-18),

		new Vector2(10,18),

		new Vector2(18,-10),

		new Vector2(18,10),

		new Vector2(-15,-14),

		new Vector2(-15,14),

		new Vector2(-14,-15),

		new Vector2(-14,15),

		new Vector2(14,-15),

		new Vector2(14,15),

		new Vector2(15,-14),

		new Vector2(15,14),

		new Vector2(-20,-4),

		new Vector2(-20,4),

		new Vector2(-4,-20),

		new Vector2(-4,20),

		new Vector2(4,-20),

		new Vector2(4,20),

		new Vector2(20,-4),

		new Vector2(20,4),

		new Vector2(-19,-7),

		new Vector2(-19,7),

		new Vector2(-17,-11),

		new Vector2(-17,11),

		new Vector2(-11,-17),

		new Vector2(-11,17),

		new Vector2(-7,-19),

		new Vector2(-7,19),

		new Vector2(7,-19),

		new Vector2(7,19),

		new Vector2(11,-17),

		new Vector2(11,17),

		new Vector2(17,-11),

		new Vector2(17,11),

		new Vector2(19,-7),

		new Vector2(19,7),

		new Vector2(-20,-3),

		new Vector2(-20,3),

		new Vector2(-3,-20),

		new Vector2(-3,20),

		new Vector2(3,-20),

		new Vector2(3,20),

		new Vector2(20,-3),

		new Vector2(20,3),

		new Vector2(-18,-9),

		new Vector2(-18,9),

		new Vector2(-9,-18),

		new Vector2(-9,18),

		new Vector2(9,-18),

		new Vector2(9,18),

		new Vector2(18,-9),

		new Vector2(18,9),

		new Vector2(-20,-2),

		new Vector2(-20,2),

		new Vector2(-2,-20),

		new Vector2(-2,20),

		new Vector2(2,-20),

		new Vector2(2,20),

		new Vector2(20,-2),

		new Vector2(20,2),

		new Vector2(-20,-1),

		new Vector2(-20,1),

		new Vector2(-1,-20),

		new Vector2(-1,20),

		new Vector2(1,-20),

		new Vector2(1,20),

		new Vector2(20,-1),

		new Vector2(20,1),

		new Vector2(-20,0),

		new Vector2(-16,-12),

		new Vector2(-16,12),

		new Vector2(-12,-16),

		new Vector2(-12,16),

		new Vector2(0,-20),

		new Vector2(0,20),

		new Vector2(12,-16),

		new Vector2(12,16),

		new Vector2(16,-12),

		new Vector2(16,12),

		new Vector2(20,0),

		new Vector2(-19,-6),

		new Vector2(-19,6),

		new Vector2(-6,-19),

		new Vector2(-6,19),

		new Vector2(6,-19),

		new Vector2(6,19),

		new Vector2(19,-6),

		new Vector2(19,6),

		new Vector2(-15,-13),

		new Vector2(-15,13),

		new Vector2(-13,-15),

		new Vector2(-13,15),

		new Vector2(13,-15),

		new Vector2(13,15),

		new Vector2(15,-13),

		new Vector2(15,13),

		new Vector2(-14,-14),

		new Vector2(-14,14),

		new Vector2(14,-14),

		new Vector2(14,14),

		new Vector2(-17,-10),

		new Vector2(-17,10),

		new Vector2(-10,-17),

		new Vector2(-10,17),

		new Vector2(10,-17),

		new Vector2(10,17),

		new Vector2(17,-10),

		new Vector2(17,10),

		new Vector2(-18,-8),

		new Vector2(-18,8),

		new Vector2(-8,-18),

		new Vector2(-8,18),

		new Vector2(8,-18),

		new Vector2(8,18),

		new Vector2(18,-8),

		new Vector2(18,8),

		new Vector2(-19,-5),

		new Vector2(-19,5),

		new Vector2(-5,-19),

		new Vector2(-5,19),

		new Vector2(5,-19),

		new Vector2(5,19),

		new Vector2(19,-5),

		new Vector2(19,5),

		new Vector2(-19,-4),

		new Vector2(-19,4),

		new Vector2(-16,-11),

		new Vector2(-16,11),

		new Vector2(-11,-16),

		new Vector2(-11,16),

		new Vector2(-4,-19),

		new Vector2(-4,19),

		new Vector2(4,-19),

		new Vector2(4,19),

		new Vector2(11,-16),

		new Vector2(11,16),

		new Vector2(16,-11),

		new Vector2(16,11),

		new Vector2(19,-4),

		new Vector2(19,4),

		new Vector2(-18,-7),

		new Vector2(-18,7),

		new Vector2(-7,-18),

		new Vector2(-7,18),

		new Vector2(7,-18),

		new Vector2(7,18),

		new Vector2(18,-7),

		new Vector2(18,7),

		new Vector2(-19,-3),

		new Vector2(-19,3),

		new Vector2(-17,-9),

		new Vector2(-17,9),

		new Vector2(-9,-17),

		new Vector2(-9,17),

		new Vector2(-3,-19),

		new Vector2(-3,19),

		new Vector2(3,-19),

		new Vector2(3,19),

		new Vector2(9,-17),

		new Vector2(9,17),

		new Vector2(17,-9),

		new Vector2(17,9),

		new Vector2(19,-3),

		new Vector2(19,3),

		new Vector2(-15,-12),

		new Vector2(-15,12),

		new Vector2(-12,-15),

		new Vector2(-12,15),

		new Vector2(12,-15),

		new Vector2(12,15),

		new Vector2(15,-12),

		new Vector2(15,12),

		new Vector2(-19,-2),

		new Vector2(-19,2),

		new Vector2(-14,-13),

		new Vector2(-14,13),

		new Vector2(-13,-14),

		new Vector2(-13,14),

		new Vector2(-2,-19),

		new Vector2(-2,19),

		new Vector2(2,-19),

		new Vector2(2,19),

		new Vector2(13,-14),

		new Vector2(13,14),

		new Vector2(14,-13),

		new Vector2(14,13),

		new Vector2(19,-2),

		new Vector2(19,2),

		new Vector2(-19,-1),

		new Vector2(-19,1),

		new Vector2(-1,-19),

		new Vector2(-1,19),

		new Vector2(1,-19),

		new Vector2(1,19),

		new Vector2(19,-1),

		new Vector2(19,1),

		new Vector2(-19,0),

		new Vector2(0,-19),

		new Vector2(0,19),

		new Vector2(19,0),

		new Vector2(-18,-6),

		new Vector2(-18,6),

		new Vector2(-6,-18),

		new Vector2(-6,18),

		new Vector2(6,-18),

		new Vector2(6,18),

		new Vector2(18,-6),

		new Vector2(18,6),

		new Vector2(-16,-10),

		new Vector2(-16,10),

		new Vector2(-10,-16),

		new Vector2(-10,16),

		new Vector2(10,-16),

		new Vector2(10,16),

		new Vector2(16,-10),

		new Vector2(16,10),

		new Vector2(-17,-8),

		new Vector2(-17,8),

		new Vector2(-8,-17),

		new Vector2(-8,17),

		new Vector2(8,-17),

		new Vector2(8,17),

		new Vector2(17,-8),

		new Vector2(17,8),

		new Vector2(-18,-5),

		new Vector2(-18,5),

		new Vector2(-5,-18),

		new Vector2(-5,18),

		new Vector2(5,-18),

		new Vector2(5,18),

		new Vector2(18,-5),

		new Vector2(18,5),

		new Vector2(-15,-11),

		new Vector2(-15,11),

		new Vector2(-11,-15),

		new Vector2(-11,15),

		new Vector2(11,-15),

		new Vector2(11,15),

		new Vector2(15,-11),

		new Vector2(15,11),

		new Vector2(-18,-4),

		new Vector2(-18,4),

		new Vector2(-14,-12),

		new Vector2(-14,12),

		new Vector2(-12,-14),

		new Vector2(-12,14),

		new Vector2(-4,-18),

		new Vector2(-4,18),

		new Vector2(4,-18),

		new Vector2(4,18),

		new Vector2(12,-14),

		new Vector2(12,14),

		new Vector2(14,-12),

		new Vector2(14,12),

		new Vector2(18,-4),

		new Vector2(18,4),

		new Vector2(-17,-7),

		new Vector2(-17,7),

		new Vector2(-13,-13),

		new Vector2(-13,13),

		new Vector2(-7,-17),

		new Vector2(-7,17),

		new Vector2(7,-17),

		new Vector2(7,17),

		new Vector2(13,-13),

		new Vector2(13,13),

		new Vector2(17,-7),

		new Vector2(17,7),

		new Vector2(-16,-9),

		new Vector2(-16,9),

		new Vector2(-9,-16),

		new Vector2(-9,16),

		new Vector2(9,-16),

		new Vector2(9,16),

		new Vector2(16,-9),

		new Vector2(16,9),

		new Vector2(-18,-3),

		new Vector2(-18,3),

		new Vector2(-3,-18),

		new Vector2(-3,18),

		new Vector2(3,-18),

		new Vector2(3,18),

		new Vector2(18,-3),

		new Vector2(18,3),

		new Vector2(-18,-2),

		new Vector2(-18,2),

		new Vector2(-2,-18),

		new Vector2(-2,18),

		new Vector2(2,-18),

		new Vector2(2,18),

		new Vector2(18,-2),

		new Vector2(18,2),

		new Vector2(-18,-1),

		new Vector2(-18,1),

		new Vector2(-17,-6),

		new Vector2(-17,6),

		new Vector2(-15,-10),

		new Vector2(-15,10),

		new Vector2(-10,-15),

		new Vector2(-10,15),

		new Vector2(-6,-17),

		new Vector2(-6,17),

		new Vector2(-1,-18),

		new Vector2(-1,18),

		new Vector2(1,-18),

		new Vector2(1,18),

		new Vector2(6,-17),

		new Vector2(6,17),

		new Vector2(10,-15),

		new Vector2(10,15),

		new Vector2(15,-10),

		new Vector2(15,10),

		new Vector2(17,-6),

		new Vector2(17,6),

		new Vector2(18,-1),

		new Vector2(18,1),

		new Vector2(-18,0),

		new Vector2(0,-18),

		new Vector2(0,18),

		new Vector2(18,0),

		new Vector2(-16,-8),

		new Vector2(-16,8),

		new Vector2(-8,-16),

		new Vector2(-8,16),

		new Vector2(8,-16),

		new Vector2(8,16),

		new Vector2(16,-8),

		new Vector2(16,8),

		new Vector2(-14,-11),

		new Vector2(-14,11),

		new Vector2(-11,-14),

		new Vector2(-11,14),

		new Vector2(11,-14),

		new Vector2(11,14),

		new Vector2(14,-11),

		new Vector2(14,11),

		new Vector2(-17,-5),

		new Vector2(-17,5),

		new Vector2(-5,-17),

		new Vector2(-5,17),

		new Vector2(5,-17),

		new Vector2(5,17),

		new Vector2(17,-5),

		new Vector2(17,5),

		new Vector2(-13,-12),

		new Vector2(-13,12),

		new Vector2(-12,-13),

		new Vector2(-12,13),

		new Vector2(12,-13),

		new Vector2(12,13),

		new Vector2(13,-12),

		new Vector2(13,12),

		new Vector2(-15,-9),

		new Vector2(-15,9),

		new Vector2(-9,-15),

		new Vector2(-9,15),

		new Vector2(9,-15),

		new Vector2(9,15),

		new Vector2(15,-9),

		new Vector2(15,9),

		new Vector2(-17,-4),

		new Vector2(-17,4),

		new Vector2(-16,-7),

		new Vector2(-16,7),

		new Vector2(-7,-16),

		new Vector2(-7,16),

		new Vector2(-4,-17),

		new Vector2(-4,17),

		new Vector2(4,-17),

		new Vector2(4,17),

		new Vector2(7,-16),

		new Vector2(7,16),

		new Vector2(16,-7),

		new Vector2(16,7),

		new Vector2(17,-4),

		new Vector2(17,4),

		new Vector2(-17,-3),

		new Vector2(-17,3),

		new Vector2(-3,-17),

		new Vector2(-3,17),

		new Vector2(3,-17),

		new Vector2(3,17),

		new Vector2(17,-3),

		new Vector2(17,3),

		new Vector2(-14,-10),

		new Vector2(-14,10),

		new Vector2(-10,-14),

		new Vector2(-10,14),

		new Vector2(10,-14),

		new Vector2(10,14),

		new Vector2(14,-10),

		new Vector2(14,10),

		new Vector2(-17,-2),

		new Vector2(-17,2),

		new Vector2(-2,-17),

		new Vector2(-2,17),

		new Vector2(2,-17),

		new Vector2(2,17),

		new Vector2(17,-2),

		new Vector2(17,2),

		new Vector2(-16,-6),

		new Vector2(-16,6),

		new Vector2(-6,-16),

		new Vector2(-6,16),

		new Vector2(6,-16),

		new Vector2(6,16),

		new Vector2(16,-6),

		new Vector2(16,6),

		new Vector2(-17,-1),

		new Vector2(-17,1),

		new Vector2(-13,-11),

		new Vector2(-13,11),

		new Vector2(-11,-13),

		new Vector2(-11,13),

		new Vector2(-1,-17),

		new Vector2(-1,17),

		new Vector2(1,-17),

		new Vector2(1,17),

		new Vector2(11,-13),

		new Vector2(11,13),

		new Vector2(13,-11),

		new Vector2(13,11),

		new Vector2(17,-1),

		new Vector2(17,1),

		new Vector2(-17,0),

		new Vector2(-15,-8),

		new Vector2(-15,8),

		new Vector2(-8,-15),

		new Vector2(-8,15),

		new Vector2(0,-17),

		new Vector2(0,17),

		new Vector2(8,-15),

		new Vector2(8,15),

		new Vector2(15,-8),

		new Vector2(15,8),

		new Vector2(17,0),

		new Vector2(-12,-12),

		new Vector2(-12,12),

		new Vector2(12,-12),

		new Vector2(12,12),

		new Vector2(-16,-5),

		new Vector2(-16,5),

		new Vector2(-5,-16),

		new Vector2(-5,16),

		new Vector2(5,-16),

		new Vector2(5,16),

		new Vector2(16,-5),

		new Vector2(16,5),

		new Vector2(-14,-9),

		new Vector2(-14,9),

		new Vector2(-9,-14),

		new Vector2(-9,14),

		new Vector2(9,-14),

		new Vector2(9,14),

		new Vector2(14,-9),

		new Vector2(14,9),

		new Vector2(-15,-7),

		new Vector2(-15,7),

		new Vector2(-7,-15),

		new Vector2(-7,15),

		new Vector2(7,-15),

		new Vector2(7,15),

		new Vector2(15,-7),

		new Vector2(15,7),

		new Vector2(-16,-4),

		new Vector2(-16,4),

		new Vector2(-4,-16),

		new Vector2(-4,16),

		new Vector2(4,-16),

		new Vector2(4,16),

		new Vector2(16,-4),

		new Vector2(16,4),

		new Vector2(-13,-10),

		new Vector2(-13,10),

		new Vector2(-10,-13),

		new Vector2(-10,13),

		new Vector2(10,-13),

		new Vector2(10,13),

		new Vector2(13,-10),

		new Vector2(13,10),

		new Vector2(-16,-3),

		new Vector2(-16,3),

		new Vector2(-12,-11),

		new Vector2(-12,11),

		new Vector2(-11,-12),

		new Vector2(-11,12),

		new Vector2(-3,-16),

		new Vector2(-3,16),

		new Vector2(3,-16),

		new Vector2(3,16),

		new Vector2(11,-12),

		new Vector2(11,12),

		new Vector2(12,-11),

		new Vector2(12,11),

		new Vector2(16,-3),

		new Vector2(16,3),

		new Vector2(-15,-6),

		new Vector2(-15,6),

		new Vector2(-6,-15),

		new Vector2(-6,15),

		new Vector2(6,-15),

		new Vector2(6,15),

		new Vector2(15,-6),

		new Vector2(15,6),

		new Vector2(-16,-2),

		new Vector2(-16,2),

		new Vector2(-14,-8),

		new Vector2(-14,8),

		new Vector2(-8,-14),

		new Vector2(-8,14),

		new Vector2(-2,-16),

		new Vector2(-2,16),

		new Vector2(2,-16),

		new Vector2(2,16),

		new Vector2(8,-14),

		new Vector2(8,14),

		new Vector2(14,-8),

		new Vector2(14,8),

		new Vector2(16,-2),

		new Vector2(16,2),

		new Vector2(-16,-1),

		new Vector2(-16,1),

		new Vector2(-1,-16),

		new Vector2(-1,16),

		new Vector2(1,-16),

		new Vector2(1,16),

		new Vector2(16,-1),

		new Vector2(16,1),

		new Vector2(-16,0),

		new Vector2(0,-16),

		new Vector2(0,16),

		new Vector2(16,0),

		new Vector2(-15,-5),

		new Vector2(-15,5),

		new Vector2(-13,-9),

		new Vector2(-13,9),

		new Vector2(-9,-13),

		new Vector2(-9,13),

		new Vector2(-5,-15),

		new Vector2(-5,15),

		new Vector2(5,-15),

		new Vector2(5,15),

		new Vector2(9,-13),

		new Vector2(9,13),

		new Vector2(13,-9),

		new Vector2(13,9),

		new Vector2(15,-5),

		new Vector2(15,5),

		new Vector2(-14,-7),

		new Vector2(-14,7),

		new Vector2(-7,-14),

		new Vector2(-7,14),

		new Vector2(7,-14),

		new Vector2(7,14),

		new Vector2(14,-7),

		new Vector2(14,7),

		new Vector2(-12,-10),

		new Vector2(-12,10),

		new Vector2(-10,-12),

		new Vector2(-10,12),

		new Vector2(10,-12),

		new Vector2(10,12),

		new Vector2(12,-10),

		new Vector2(12,10),

		new Vector2(-11,-11),

		new Vector2(-11,11),

		new Vector2(11,-11),

		new Vector2(11,11),

		new Vector2(-15,-4),

		new Vector2(-15,4),

		new Vector2(-4,-15),

		new Vector2(-4,15),

		new Vector2(4,-15),

		new Vector2(4,15),

		new Vector2(15,-4),

		new Vector2(15,4),

		new Vector2(-15,-3),

		new Vector2(-15,3),

		new Vector2(-3,-15),

		new Vector2(-3,15),

		new Vector2(3,-15),

		new Vector2(3,15),

		new Vector2(15,-3),

		new Vector2(15,3),

		new Vector2(-13,-8),

		new Vector2(-13,8),

		new Vector2(-8,-13),

		new Vector2(-8,13),

		new Vector2(8,-13),

		new Vector2(8,13),

		new Vector2(13,-8),

		new Vector2(13,8),

		new Vector2(-14,-6),

		new Vector2(-14,6),

		new Vector2(-6,-14),

		new Vector2(-6,14),

		new Vector2(6,-14),

		new Vector2(6,14),

		new Vector2(14,-6),

		new Vector2(14,6),

		new Vector2(-15,-2),

		new Vector2(-15,2),

		new Vector2(-2,-15),

		new Vector2(-2,15),

		new Vector2(2,-15),

		new Vector2(2,15),

		new Vector2(15,-2),

		new Vector2(15,2),

		new Vector2(-15,-1),

		new Vector2(-15,1),

		new Vector2(-1,-15),

		new Vector2(-1,15),

		new Vector2(1,-15),

		new Vector2(1,15),

		new Vector2(15,-1),

		new Vector2(15,1),

		new Vector2(-15,0),

		new Vector2(-12,-9),

		new Vector2(-12,9),

		new Vector2(-9,-12),

		new Vector2(-9,12),

		new Vector2(0,-15),

		new Vector2(0,15),

		new Vector2(9,-12),

		new Vector2(9,12),

		new Vector2(12,-9),

		new Vector2(12,9),

		new Vector2(15,0),

		new Vector2(-14,-5),

		new Vector2(-14,5),

		new Vector2(-11,-10),

		new Vector2(-11,10),

		new Vector2(-10,-11),

		new Vector2(-10,11),

		new Vector2(-5,-14),

		new Vector2(-5,14),

		new Vector2(5,-14),

		new Vector2(5,14),

		new Vector2(10,-11),

		new Vector2(10,11),

		new Vector2(11,-10),

		new Vector2(11,10),

		new Vector2(14,-5),

		new Vector2(14,5),

		new Vector2(-13,-7),

		new Vector2(-13,7),

		new Vector2(-7,-13),

		new Vector2(-7,13),

		new Vector2(7,-13),

		new Vector2(7,13),

		new Vector2(13,-7),

		new Vector2(13,7),

		new Vector2(-14,-4),

		new Vector2(-14,4),

		new Vector2(-4,-14),

		new Vector2(-4,14),

		new Vector2(4,-14),

		new Vector2(4,14),

		new Vector2(14,-4),

		new Vector2(14,4),

		new Vector2(-12,-8),

		new Vector2(-12,8),

		new Vector2(-8,-12),

		new Vector2(-8,12),

		new Vector2(8,-12),

		new Vector2(8,12),

		new Vector2(12,-8),

		new Vector2(12,8),

		new Vector2(-14,-3),

		new Vector2(-14,3),

		new Vector2(-13,-6),

		new Vector2(-13,6),

		new Vector2(-6,-13),

		new Vector2(-6,13),

		new Vector2(-3,-14),

		new Vector2(-3,14),

		new Vector2(3,-14),

		new Vector2(3,14),

		new Vector2(6,-13),

		new Vector2(6,13),

		new Vector2(13,-6),

		new Vector2(13,6),

		new Vector2(14,-3),

		new Vector2(14,3),

		new Vector2(-11,-9),

		new Vector2(-11,9),

		new Vector2(-9,-11),

		new Vector2(-9,11),

		new Vector2(9,-11),

		new Vector2(9,11),

		new Vector2(11,-9),

		new Vector2(11,9),

		new Vector2(-14,-2),

		new Vector2(-14,2),

		new Vector2(-10,-10),

		new Vector2(-10,10),

		new Vector2(-2,-14),

		new Vector2(-2,14),

		new Vector2(2,-14),

		new Vector2(2,14),

		new Vector2(10,-10),

		new Vector2(10,10),

		new Vector2(14,-2),

		new Vector2(14,2),

		new Vector2(-14,-1),

		new Vector2(-14,1),

		new Vector2(-1,-14),

		new Vector2(-1,14),

		new Vector2(1,-14),

		new Vector2(1,14),

		new Vector2(14,-1),

		new Vector2(14,1),

		new Vector2(-14,0),

		new Vector2(0,-14),

		new Vector2(0,14),

		new Vector2(14,0),

		new Vector2(-13,-5),

		new Vector2(-13,5),

		new Vector2(-5,-13),

		new Vector2(-5,13),

		new Vector2(5,-13),

		new Vector2(5,13),

		new Vector2(13,-5),

		new Vector2(13,5),

		new Vector2(-12,-7),

		new Vector2(-12,7),

		new Vector2(-7,-12),

		new Vector2(-7,12),

		new Vector2(7,-12),

		new Vector2(7,12),

		new Vector2(12,-7),

		new Vector2(12,7),

		new Vector2(-13,-4),

		new Vector2(-13,4),

		new Vector2(-11,-8),

		new Vector2(-11,8),

		new Vector2(-8,-11),

		new Vector2(-8,11),

		new Vector2(-4,-13),

		new Vector2(-4,13),

		new Vector2(4,-13),

		new Vector2(4,13),

		new Vector2(8,-11),

		new Vector2(8,11),

		new Vector2(11,-8),

		new Vector2(11,8),

		new Vector2(13,-4),

		new Vector2(13,4),

		new Vector2(-10,-9),

		new Vector2(-10,9),

		new Vector2(-9,-10),

		new Vector2(-9,10),

		new Vector2(9,-10),

		new Vector2(9,10),

		new Vector2(10,-9),

		new Vector2(10,9),

		new Vector2(-12,-6),

		new Vector2(-12,6),

		new Vector2(-6,-12),

		new Vector2(-6,12),

		new Vector2(6,-12),

		new Vector2(6,12),

		new Vector2(12,-6),

		new Vector2(12,6),

		new Vector2(-13,-3),

		new Vector2(-13,3),

		new Vector2(-3,-13),

		new Vector2(-3,13),

		new Vector2(3,-13),

		new Vector2(3,13),

		new Vector2(13,-3),

		new Vector2(13,3),

		new Vector2(-13,-2),

		new Vector2(-13,2),

		new Vector2(-2,-13),

		new Vector2(-2,13),

		new Vector2(2,-13),

		new Vector2(2,13),

		new Vector2(13,-2),

		new Vector2(13,2),

		new Vector2(-13,-1),

		new Vector2(-13,1),

		new Vector2(-11,-7),

		new Vector2(-11,7),

		new Vector2(-7,-11),

		new Vector2(-7,11),

		new Vector2(-1,-13),

		new Vector2(-1,13),

		new Vector2(1,-13),

		new Vector2(1,13),

		new Vector2(7,-11),

		new Vector2(7,11),

		new Vector2(11,-7),

		new Vector2(11,7),

		new Vector2(13,-1),

		new Vector2(13,1),

		new Vector2(-13,0),

		new Vector2(-12,-5),

		new Vector2(-12,5),

		new Vector2(-5,-12),

		new Vector2(-5,12),

		new Vector2(0,-13),

		new Vector2(0,13),

		new Vector2(5,-12),

		new Vector2(5,12),

		new Vector2(12,-5),

		new Vector2(12,5),

		new Vector2(13,0),

		new Vector2(-10,-8),

		new Vector2(-10,8),

		new Vector2(-8,-10),

		new Vector2(-8,10),

		new Vector2(8,-10),

		new Vector2(8,10),

		new Vector2(10,-8),

		new Vector2(10,8),

		new Vector2(-9,-9),

		new Vector2(-9,9),

		new Vector2(9,-9),

		new Vector2(9,9),

		new Vector2(-12,-4),

		new Vector2(-12,4),

		new Vector2(-4,-12),

		new Vector2(-4,12),

		new Vector2(4,-12),

		new Vector2(4,12),

		new Vector2(12,-4),

		new Vector2(12,4),

		new Vector2(-11,-6),

		new Vector2(-11,6),

		new Vector2(-6,-11),

		new Vector2(-6,11),

		new Vector2(6,-11),

		new Vector2(6,11),

		new Vector2(11,-6),

		new Vector2(11,6),

		new Vector2(-12,-3),

		new Vector2(-12,3),

		new Vector2(-3,-12),

		new Vector2(-3,12),

		new Vector2(3,-12),

		new Vector2(3,12),

		new Vector2(12,-3),

		new Vector2(12,3),

		new Vector2(-10,-7),

		new Vector2(-10,7),

		new Vector2(-7,-10),

		new Vector2(-7,10),

		new Vector2(7,-10),

		new Vector2(7,10),

		new Vector2(10,-7),

		new Vector2(10,7),

		new Vector2(-12,-2),

		new Vector2(-12,2),

		new Vector2(-2,-12),

		new Vector2(-2,12),

		new Vector2(2,-12),

		new Vector2(2,12),

		new Vector2(12,-2),

		new Vector2(12,2),

		new Vector2(-11,-5),

		new Vector2(-11,5),

		new Vector2(-5,-11),

		new Vector2(-5,11),

		new Vector2(5,-11),

		new Vector2(5,11),

		new Vector2(11,-5),

		new Vector2(11,5),

		new Vector2(-12,-1),

		new Vector2(-12,1),

		new Vector2(-9,-8),

		new Vector2(-9,8),

		new Vector2(-8,-9),

		new Vector2(-8,9),

		new Vector2(-1,-12),

		new Vector2(-1,12),

		new Vector2(1,-12),

		new Vector2(1,12),

		new Vector2(8,-9),

		new Vector2(8,9),

		new Vector2(9,-8),

		new Vector2(9,8),

		new Vector2(12,-1),

		new Vector2(12,1),

		new Vector2(-12,0),

		new Vector2(0,-12),

		new Vector2(0,12),

		new Vector2(12,0),

		new Vector2(-11,-4),

		new Vector2(-11,4),

		new Vector2(-4,-11),

		new Vector2(-4,11),

		new Vector2(4,-11),

		new Vector2(4,11),

		new Vector2(11,-4),

		new Vector2(11,4),

		new Vector2(-10,-6),

		new Vector2(-10,6),

		new Vector2(-6,-10),

		new Vector2(-6,10),

		new Vector2(6,-10),

		new Vector2(6,10),

		new Vector2(10,-6),

		new Vector2(10,6),

		new Vector2(-11,-3),

		new Vector2(-11,3),

		new Vector2(-9,-7),

		new Vector2(-9,7),

		new Vector2(-7,-9),

		new Vector2(-7,9),

		new Vector2(-3,-11),

		new Vector2(-3,11),

		new Vector2(3,-11),

		new Vector2(3,11),

		new Vector2(7,-9),

		new Vector2(7,9),

		new Vector2(9,-7),

		new Vector2(9,7),

		new Vector2(11,-3),

		new Vector2(11,3),

		new Vector2(-8,-8),

		new Vector2(-8,8),

		new Vector2(8,-8),

		new Vector2(8,8),

		new Vector2(-11,-2),

		new Vector2(-11,2),

		new Vector2(-10,-5),

		new Vector2(-10,5),

		new Vector2(-5,-10),

		new Vector2(-5,10),

		new Vector2(-2,-11),

		new Vector2(-2,11),

		new Vector2(2,-11),

		new Vector2(2,11),

		new Vector2(5,-10),

		new Vector2(5,10),

		new Vector2(10,-5),

		new Vector2(10,5),

		new Vector2(11,-2),

		new Vector2(11,2),

		new Vector2(-11,-1),

		new Vector2(-11,1),

		new Vector2(-1,-11),

		new Vector2(-1,11),

		new Vector2(1,-11),

		new Vector2(1,11),

		new Vector2(11,-1),

		new Vector2(11,1),

		new Vector2(-11,0),

		new Vector2(0,-11),

		new Vector2(0,11),

		new Vector2(11,0),

		new Vector2(-9,-6),

		new Vector2(-9,6),

		new Vector2(-6,-9),

		new Vector2(-6,9),

		new Vector2(6,-9),

		new Vector2(6,9),

		new Vector2(9,-6),

		new Vector2(9,6),

		new Vector2(-10,-4),

		new Vector2(-10,4),

		new Vector2(-4,-10),

		new Vector2(-4,10),

		new Vector2(4,-10),

		new Vector2(4,10),

		new Vector2(10,-4),

		new Vector2(10,4),

		new Vector2(-8,-7),

		new Vector2(-8,7),

		new Vector2(-7,-8),

		new Vector2(-7,8),

		new Vector2(7,-8),

		new Vector2(7,8),

		new Vector2(8,-7),

		new Vector2(8,7),

		new Vector2(-10,-3),

		new Vector2(-10,3),

		new Vector2(-3,-10),

		new Vector2(-3,10),

		new Vector2(3,-10),

		new Vector2(3,10),

		new Vector2(10,-3),

		new Vector2(10,3),

		new Vector2(-9,-5),

		new Vector2(-9,5),

		new Vector2(-5,-9),

		new Vector2(-5,9),

		new Vector2(5,-9),

		new Vector2(5,9),

		new Vector2(9,-5),

		new Vector2(9,5),

		new Vector2(-10,-2),

		new Vector2(-10,2),

		new Vector2(-2,-10),

		new Vector2(-2,10),

		new Vector2(2,-10),

		new Vector2(2,10),

		new Vector2(10,-2),

		new Vector2(10,2),

		new Vector2(-10,-1),

		new Vector2(-10,1),

		new Vector2(-1,-10),

		new Vector2(-1,10),

		new Vector2(1,-10),

		new Vector2(1,10),

		new Vector2(10,-1),

		new Vector2(10,1),

		new Vector2(-10,0),

		new Vector2(-8,-6),

		new Vector2(-8,6),

		new Vector2(-6,-8),

		new Vector2(-6,8),

		new Vector2(0,-10),

		new Vector2(0,10),

		new Vector2(6,-8),

		new Vector2(6,8),

		new Vector2(8,-6),

		new Vector2(8,6),

		new Vector2(10,0),

		new Vector2(-7,-7),

		new Vector2(-7,7),

		new Vector2(7,-7),

		new Vector2(7,7),

		new Vector2(-9,-4),

		new Vector2(-9,4),

		new Vector2(-4,-9),

		new Vector2(-4,9),

		new Vector2(4,-9),

		new Vector2(4,9),

		new Vector2(9,-4),

		new Vector2(9,4),

		new Vector2(-9,-3),

		new Vector2(-9,3),

		new Vector2(-3,-9),

		new Vector2(-3,9),

		new Vector2(3,-9),

		new Vector2(3,9),

		new Vector2(9,-3),

		new Vector2(9,3),

		new Vector2(-8,-5),

		new Vector2(-8,5),

		new Vector2(-5,-8),

		new Vector2(-5,8),

		new Vector2(5,-8),

		new Vector2(5,8),

		new Vector2(8,-5),

		new Vector2(8,5),

		new Vector2(-9,-2),

		new Vector2(-9,2),

		new Vector2(-7,-6),

		new Vector2(-7,6),

		new Vector2(-6,-7),

		new Vector2(-6,7),

		new Vector2(-2,-9),

		new Vector2(-2,9),

		new Vector2(2,-9),

		new Vector2(2,9),

		new Vector2(6,-7),

		new Vector2(6,7),

		new Vector2(7,-6),

		new Vector2(7,6),

		new Vector2(9,-2),

		new Vector2(9,2),

		new Vector2(-9,-1),

		new Vector2(-9,1),

		new Vector2(-1,-9),

		new Vector2(-1,9),

		new Vector2(1,-9),

		new Vector2(1,9),

		new Vector2(9,-1),

		new Vector2(9,1),

		new Vector2(-9,0),

		new Vector2(0,-9),

		new Vector2(0,9),

		new Vector2(9,0),

		new Vector2(-8,-4),

		new Vector2(-8,4),

		new Vector2(-4,-8),

		new Vector2(-4,8),

		new Vector2(4,-8),

		new Vector2(4,8),

		new Vector2(8,-4),

		new Vector2(8,4),

		new Vector2(-7,-5),

		new Vector2(-7,5),

		new Vector2(-5,-7),

		new Vector2(-5,7),

		new Vector2(5,-7),

		new Vector2(5,7),

		new Vector2(7,-5),

		new Vector2(7,5),

		new Vector2(-8,-3),

		new Vector2(-8,3),

		new Vector2(-3,-8),

		new Vector2(-3,8),

		new Vector2(3,-8),

		new Vector2(3,8),

		new Vector2(8,-3),

		new Vector2(8,3),

		new Vector2(-6,-6),

		new Vector2(-6,6),

		new Vector2(6,-6),

		new Vector2(6,6),

		new Vector2(-8,-2),

		new Vector2(-8,2),

		new Vector2(-2,-8),

		new Vector2(-2,8),

		new Vector2(2,-8),

		new Vector2(2,8),

		new Vector2(8,-2),

		new Vector2(8,2),

		new Vector2(-8,-1),

		new Vector2(-8,1),

		new Vector2(-7,-4),

		new Vector2(-7,4),

		new Vector2(-4,-7),

		new Vector2(-4,7),

		new Vector2(-1,-8),

		new Vector2(-1,8),

		new Vector2(1,-8),

		new Vector2(1,8),

		new Vector2(4,-7),

		new Vector2(4,7),

		new Vector2(7,-4),

		new Vector2(7,4),

		new Vector2(8,-1),

		new Vector2(8,1),

		new Vector2(-8,0),

		new Vector2(0,-8),

		new Vector2(0,8),

		new Vector2(8,0),

		new Vector2(-6,-5),

		new Vector2(-6,5),

		new Vector2(-5,-6),

		new Vector2(-5,6),

		new Vector2(5,-6),

		new Vector2(5,6),

		new Vector2(6,-5),

		new Vector2(6,5),

		new Vector2(-7,-3),

		new Vector2(-7,3),

		new Vector2(-3,-7),

		new Vector2(-3,7),

		new Vector2(3,-7),

		new Vector2(3,7),

		new Vector2(7,-3),

		new Vector2(7,3),

		new Vector2(-7,-2),

		new Vector2(-7,2),

		new Vector2(-2,-7),

		new Vector2(-2,7),

		new Vector2(2,-7),

		new Vector2(2,7),

		new Vector2(7,-2),

		new Vector2(7,2),

		new Vector2(-6,-4),

		new Vector2(-6,4),

		new Vector2(-4,-6),

		new Vector2(-4,6),

		new Vector2(4,-6),

		new Vector2(4,6),

		new Vector2(6,-4),

		new Vector2(6,4),

		new Vector2(-7,-1),

		new Vector2(-7,1),

		new Vector2(-5,-5),

		new Vector2(-5,5),

		new Vector2(-1,-7),

		new Vector2(-1,7),

		new Vector2(1,-7),

		new Vector2(1,7),

		new Vector2(5,-5),

		new Vector2(5,5),

		new Vector2(7,-1),

		new Vector2(7,1),

		new Vector2(-7,0),

		new Vector2(0,-7),

		new Vector2(0,7),

		new Vector2(7,0),

		new Vector2(-6,-3),

		new Vector2(-6,3),

		new Vector2(-3,-6),

		new Vector2(-3,6),

		new Vector2(3,-6),

		new Vector2(3,6),

		new Vector2(6,-3),

		new Vector2(6,3),

		new Vector2(-5,-4),

		new Vector2(-5,4),

		new Vector2(-4,-5),

		new Vector2(-4,5),

		new Vector2(4,-5),

		new Vector2(4,5),

		new Vector2(5,-4),

		new Vector2(5,4),

		new Vector2(-6,-2),

		new Vector2(-6,2),

		new Vector2(-2,-6),

		new Vector2(-2,6),

		new Vector2(2,-6),

		new Vector2(2,6),

		new Vector2(6,-2),

		new Vector2(6,2),

		new Vector2(-6,-1),

		new Vector2(-6,1),

		new Vector2(-1,-6),

		new Vector2(-1,6),

		new Vector2(1,-6),

		new Vector2(1,6),

		new Vector2(6,-1),

		new Vector2(6,1),

		new Vector2(-6,0),

		new Vector2(0,-6),

		new Vector2(0,6),

		new Vector2(6,0),

		new Vector2(-5,-3),

		new Vector2(-5,3),

		new Vector2(-3,-5),

		new Vector2(-3,5),

		new Vector2(3,-5),

		new Vector2(3,5),

		new Vector2(5,-3),

		new Vector2(5,3),

		new Vector2(-4,-4),

		new Vector2(-4,4),

		new Vector2(4,-4),

		new Vector2(4,4),

		new Vector2(-5,-2),

		new Vector2(-5,2),

		new Vector2(-2,-5),

		new Vector2(-2,5),

		new Vector2(2,-5),

		new Vector2(2,5),

		new Vector2(5,-2),

		new Vector2(5,2),

		new Vector2(-5,-1),

		new Vector2(-5,1),

		new Vector2(-1,-5),

		new Vector2(-1,5),

		new Vector2(1,-5),

		new Vector2(1,5),

		new Vector2(5,-1),

		new Vector2(5,1),

		new Vector2(-5,0),

		new Vector2(-4,-3),

		new Vector2(-4,3),

		new Vector2(-3,-4),

		new Vector2(-3,4),

		new Vector2(0,-5),

		new Vector2(0,5),

		new Vector2(3,-4),

		new Vector2(3,4),

		new Vector2(4,-3),

		new Vector2(4,3),

		new Vector2(5,0),

		new Vector2(-4,-2),

		new Vector2(-4,2),

		new Vector2(-2,-4),

		new Vector2(-2,4),

		new Vector2(2,-4),

		new Vector2(2,4),

		new Vector2(4,-2),

		new Vector2(4,2),

		new Vector2(-3,-3),

		new Vector2(-3,3),

		new Vector2(3,-3),

		new Vector2(3,3),

		new Vector2(-4,-1),

		new Vector2(-4,1),

		new Vector2(-1,-4),

		new Vector2(-1,4),

		new Vector2(1,-4),

		new Vector2(1,4),

		new Vector2(4,-1),

		new Vector2(4,1),

		new Vector2(-4,0),

		new Vector2(0,-4),

		new Vector2(0,4),

		new Vector2(4,0),

		new Vector2(-3,-2),

		new Vector2(-3,2),

		new Vector2(-2,-3),

		new Vector2(-2,3),

		new Vector2(2,-3),

		new Vector2(2,3),

		new Vector2(3,-2),

		new Vector2(3,2),

		new Vector2(-3,-1),

		new Vector2(-3,1),

		new Vector2(-1,-3),

		new Vector2(-1,3),

		new Vector2(1,-3),

		new Vector2(1,3),

		new Vector2(3,-1),

		new Vector2(3,1),

		new Vector2(-3,0),

		new Vector2(0,-3),

		new Vector2(0,3),

		new Vector2(3,0),

		new Vector2(-2,-2),

		new Vector2(-2,2),

		new Vector2(2,-2),

		new Vector2(2,2),

		new Vector2(-2,-1),

		new Vector2(-2,1),

		new Vector2(-1,-2),

		new Vector2(-1,2),

		new Vector2(1,-2),

		new Vector2(1,2),

		new Vector2(2,-1),

		new Vector2(2,1),

		new Vector2(-2,0),

		new Vector2(0,-2),

		new Vector2(0,2),

		new Vector2(2,0),

		new Vector2(-1,-1),

		new Vector2(-1,1),

		new Vector2(1,-1),

		new Vector2(1,1),

		new Vector2(-1,0),

		new Vector2(0,-1),

		new Vector2(0,1),

		new Vector2(1,0),

		new Vector2(0,0)

	};

	//I used this function to create data points. Adjust 'de' and 'i' and 'z' for larger circles
	//current circle has a diamter of 64 chunks or, a 32 chunk render radius around the player
//	IEnumerator makear(){
		//List<Vector2> a = new List<Vector2> ();
		//bool hehe = true;
		//float max = 0, current;
		//int m=0, k=0;
		//StreamWriter sw = new StreamWriter ();
		//for (int de = 0 ; de < 4096; de++)
		//{
			//yield return 0;
			//for (int i = -32; i < 32; i++) 
			//{
				//for (int z = -32; z < 32; z++) 
				//{
					//if (!(i == z && z == 0))
					//{
						//current = Vector2.Distance(new Vector2(i,z),Vector2.zero);
						//if (current > max && current <= 32)
						//{
	//						for ( int b = 0; b < a.Count; b++)
							//{
								//if ((int)(a[b].x) == i && (int)(a[b].y) == z)
									//hehe = false;
							//}
							
							//if (hehe){
								//max = current;
								//m = i;
								//k = z;
							//}
						//}
						//hehe = true;
					//}
				//}
			//}
			
			//yield return 0;
			//sw.WriteLine("new Vector2(" + m + "," + k + "),\n");
			//a.Add (new Vector2 (m, k));
			//max = 0;m = 0;k = 0;
		//}
		//sw.Close ();
	//}
	
	//void evalUVPos(){
		//Vector2[] arr = LoadingCoords.chunksToRender;
		//int e = 1;
		//StreamWriter sw = new StreamWriter ();
		//for (int i = arr.Length-1; i >= 0; i--){
			//if (arr[i].x == e && arr[i].y == 0){
				//e++;
				//sw.WriteLine(i  + ",");}
		//}
		//sw.Close ();
	//}

}
