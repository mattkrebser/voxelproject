using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Every material in a chunk is represented in one of these classes
//This class holds the offset information for where the current
//material data like vertices,uvs,and tris are stored
//It also holds Lists which are used to change the mesh data for this material


public class AttatchedMesh
{
	//Which chunk the belongs to, cx, cy ,cz
	public ChunkVector cPos;

	//distance Bias (How far apart are chunks allowed to be before they aren't batched together)
	public int DistanceBias = 1;


	//current verticy count whilst building verticy lists on DrawChunks
	public int vertCount = 0;

	//used for tile textures.
	public float u = 1;

	//Each chunk needs to know which meshes it owns, so that when the chunk is destroyed,
	//it can tell its meshes that it no longer exists yay :D
	public MeshObject OwnedMesh;

	//Position of this material in vertices and triangles
	public int triOffset = 0;
	public int triLength = 0;
	public int verticyOffset = 0;
	public int verticyLength = 0;

	public int refChunkPos = -1;

	//Constants that help with chunk spacing in mesh arrays
	public int maxSharedChunksPerMesh = 3;
	public int requiredFreeVerts = 0;

	//Position in List<List<Meshobject>> in MeshObjectManager
	// List<List<MeshObject>> [tex][meshobject]
	public int tex = 0;
	//public int meshObjectPos = 0;

	//Position in materialsList
	public int material = 0;

	//number active attatchedMesh objects
	public static int count = 0;

	public string b;

	~AttatchedMesh(){
		count--;
		GameLog.VoxelStatusOutput ("attatchedFinal " + count.ToString());
	}

	//mesh data, these lists are empty except for when the draw takes place.
	//After the draw finishes, they are immedietly emptied. These lists are not
	//represented by the above values. These lists are the vertex,uv,tri data when updating the mesh
	public List<Vector3> verts = new List<Vector3>();
	public List<Vector2> uvs = new List<Vector2>();
	public List<Vector2> uvs2 = new List<Vector2>();
	public List<int> tris = new List<int>();

	//If this is true, the meshObjectmanager will create a new mesh for the given data
	public bool createNewMesh = false;

	public AttatchedMesh(int maxVertSize, float minBatchSpacePercent){
		requiredFreeVerts = (int)(maxVertSize * minBatchSpacePercent);
		count++;
		//Debug.Log ("attatched up " + count);
	}
}

