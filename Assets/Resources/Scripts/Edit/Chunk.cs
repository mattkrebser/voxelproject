﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chunk {
	
	public int RegionSize;//width or length of the world in chunks

	public byte[,,] chunkBlockData;//voxel data
	public byte[,,] secondaryID;
	private int chunkSize=32;
	public int ChunkX = 0;//chunk coords, in global coordinates
	public int ChunkZ = 0;
	public int ChunkY = 0;
	public int chunkx, chunkz, chunky;//chunk coordinates of this chunk in the local chunk array
	public byte loadChunk = 1;//determines if chunk will be drawn

    /// <summary>
    /// Used for determining if the next chunk above will be an air chunk
    /// </summary>
	public static int temp = 0;

	private int topLevelCheck = 0;

	public WorldBiomes worldData;
	public OpenSimplexNoise noiseObj;
	//active non-sub biome being processed (usingBiome can equal activeBiome)
	private BiomeRef activeBiome;
	//current biome being processed at x,z
	private BiomeRef usingBiome;


	//modifies biome parameters to interpolate between biomes
	//mods enum is located on Biome.cs
	private double[] modifiers = new double[17];

	//PI over 2.
	private const double PI_DIVIDE_2 = 1.57079632679d;

    //block list
	private List<L> bl;

	// Use this for initialization
	public Chunk (int cx, int cy, int cz, int rs, OpenSimplexNoise n, WorldBiomes w, ChunkColumnGen cc, List<L> b) 
	{ 
		ChunkX = cx;
		ChunkY = cy;
		ChunkZ = cz;
		RegionSize = rs;
		noiseObj = n;
		worldData = w;
		bl = b;

        //Make the chunk
		makeChunk (cc);

        //positions of this chunk in an 8x8x8 array. Used for sequential generation.
		chunkx = (ChunkX / 32) % 8;
		chunky = (ChunkY / 32) % 8;
		chunkz = (ChunkZ / 32) % 8;

		//We calculated the height map, no need to do it again
		if (!cc.CalculatedHeightMap)
			cc.CalculatedHeightMap = true;
	}

    /// <summary>
    /// Tests to see if the current chunk will be an air chunk.
    /// </summary>
    void testAirChunk()
	{
		temp = (256 - ChunkY) / 32;
	}

	/// <summary>
	/// Generates the chunk data.
	/// </summary>
	/// <param name="cc">Cc.</param>
	void makeChunk(ChunkColumnGen cc)
	{ 
		//optimization if/else only iterates through chunks that
		//might have non empty voxels
		if ( temp > 1)
		{
			loadChunk = 0;
			temp--;
		}
		//iterate
		else
		{
		
		    //make heightmap
		    if (!cc.CalculatedHeightMap)
			    GetHeightMap(cc);

		    int airBlockCount = 0, solidBlockCount = 0, c = 0;
            if (chunkBlockData == null || secondaryID == null)
            {
                chunkBlockData = new byte[chunkSize, chunkSize, chunkSize];
                secondaryID = new byte[chunkSize, chunkSize, chunkSize];
            }

            //swapped z and y
            for (int x = 0; x < chunkSize; x++)
            {
                for (int z = 0; z < chunkSize; z++)
                {
                    int grass = cc.heightMap[x, z];
                    activeBiome = cc.biomesTable[x, z];

                    //Used for determining which block to place
                    BlockIDInt32 InitialBlock = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[0][0];
                    int layer = 0;
                    int rangeTillNext = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layerRange;

                    //Get height relative to Chunk
                    int n = grass - ChunkY;

                    AddSlopes(cc, n, grass, x, z, layer);

                    //If this spot in the chunk is above the heightMap value
                    if (n < 0)
                    {
                        airBlockCount += 32;
                        topLevelCheck++;
                    }
                    //if this spot in the chunk is below the heightMap value
                    else if (n > 31)
                    {
                        solidBlockCount += 32;

                        //Figure out which block to start placing for the next for loop
                        int range = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layerRange;
                        //Set initial to top layer (layer 0)
                        if (n - 31 < range)
                        {
                            //Initial block (block that will fill the data array)
                            InitialBlock = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[0][0];
                            //rangeTillNext is the distance until the algorithm fills the data array with the block at the next level
                            //in this case, the next block is at layer level (1)
                            rangeTillNext = System.Math.Abs(n - 31 - range);
                            //layer level 0 (top layer), this block type is place on top of the others
                            layer = 0;
                        }
                        //Mid layer (layer 1)
                        else if (n - 31 - range < range && activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[1].Count > 0)
                        {
                            InitialBlock = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[1][0];
                            rangeTillNext = System.Math.Abs(n - 31 - range - range);
                            //layer level 1 (middle)
                            layer = 1;
                        }
                        //bottom (layer 2)
                        else
                        {
                            if (activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[2].Count > 0)
                            {
                                InitialBlock = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[2][0];
                                rangeTillNext = 32;
                                layer = 2;
                            }
                            else if (activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[1].Count > 0)
                            {
                                InitialBlock = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[1][0];
                                rangeTillNext = 32;
                                layer = 1;
                            }
                            else
                            {
                                InitialBlock = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[0][0];
                                rangeTillNext = 32;
                                layer = 0;
                            }
                        }

                        //This chunk strip is filled with non-empty blocks, a full iteration is needed
                        //for the length of the entire chunk
                        n = 31;
                    }
                    //else, meaning This chunk contains the top block (height map value)
                    else
                    {
                        airBlockCount += 31 - n;
                        solidBlockCount = n + 1;

                        //Increment top level check if there is an air block present at this (x,z) position
                        if (n != 31)
                        {
                            topLevelCheck++;
                        }
                    }

                    //fill chunk
                    for (int y = n; y >= 0; y--)
                    {
                        //If it is time to change layers, and the current layer is the top layer
                        if (rangeTillNext == 0 && layer == 0)
                        {
                            layer = 1;
                            rangeTillNext = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layerRange;
                            //check to see if layer (1) exists, if not choose layer (2)
                            if (activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[1].Count == 0)
                            {
                                layer = 2;
                                rangeTillNext = 32;
                            }
                            //check to see if layer (2) exsists, if not, choose layer 0 (layer 0 always exists)
                            if (activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[1].Count == 0
                                 && activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[2].Count == 0)
                            {
                                layer = 0;
                                rangeTillNext = 32;
                            }
                            InitialBlock = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[layer][0];
                        }
                        //If it is time to change layers, and the current layer is the middle layer
                        else if (rangeTillNext == 0 && layer == 1)
                        {
                            layer = 2;
                            rangeTillNext = 32;
                            //check to see if layer (2) exists, if not stay on (1)
                            if (activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[layer].Count == 0)
                                layer = 1;
                            InitialBlock = activeBiome.biome.layers[activeBiome.biome.layerRefs[grass]].layers[layer][0];
                        }

                        //Fill array
                        chunkBlockData[x, y, z] = (byte)InitialBlock.blockID1;
                        secondaryID[x, y, z] = (byte)InitialBlock.blockID2;

                        //decrement range till next for each change in (y)
                        rangeTillNext--;
                    }
                }
            }

            //chunk only conatins empty voxels
            if (airBlockCount == 32768)
            {
                loadChunk = 0;
                testAirChunk();
            }
            //chunk above is guarenteed to only contain air voxels
            else if (topLevelCheck == 1024 && airBlockCount != 32768)
            {
                testAirChunk();
            }
            //chunk is completely solid
            if (solidBlockCount == 32768)
            {
                for (int m = 0; m < chunkSize; m++)
                {
                    for (int n = 0; n < chunkSize; n++)
                    {

                        int tempg = cc.heightMap[m, n];

                        if (32 + ChunkY <= tempg)
                            c++;
                    }
                }
            }
            if (c == 1024 && CheckSolidBorders(cc))
            {
                loadChunk = 2;
            }
		}
	}

	/// <summary>
	/// Checks to see if surrounding voxels are all solid.
	/// returns true if they are.
	/// </summary>
	/// <returns><c>true</c>, if solid borders was checked, <c>false</c> otherwise.</returns>
	/// <param name="cc">Cc.</param>
	private bool CheckSolidBorders(ChunkColumnGen cc){

		int n = 0, e = 0, s = 0, w = 0;

		for (int i = 0; i < 32; i++) {
			if (ChunkY + 32 <= cc.west[i])
				w++;
		}
		for (int i = 0; i < 32; i++) {
			if (ChunkY + 32 <= cc.north[i])
				n++;
		}
		for (int i = 0; i < 32; i++) {
			if (ChunkY + 32 <= cc.south[i])
				s++;
		}
		for (int i = 0; i < 32; i++) {
			if (ChunkY + 32 <= cc.east[i])
				e++;
		}

		if (n == 32 && s == 32 && e == 32 && w == 32)
			return true;
		return false;
	}

	/// <summary>
	/// Gets the height map and Biome table
	/// </summary>
	/// <param name="cc">Cc.</param>
	void GetHeightMap(ChunkColumnGen cc){
		BiomeRef bRef = null;
		//fill height map table
		for (int x = 0; x < 32; x++) {
			for (int z = 0; z < 32; z++){
				double val = 0.0d;double vals = 0.0d;
				ModifyActiveBiome(x,z,ref val,cc,ref vals);
				bRef = InterpolateBiomeValues(ref val,ref vals);
				cc.heightMap[x,z] = openSimplexNoise(x + ChunkX,z + ChunkZ,usingBiome.biome, bRef);
			}
		}

		//get sorrounding values
		for (int z = 0; z < 32; z++) {

			double val = 0.0d;double vals = 0.0d;
			ModifyActiveBiome(-1,z,ref val,cc,ref vals);
			bRef = InterpolateBiomeValues(ref val,ref vals);
			cc.west[z] =  openSimplexNoise(ChunkX - 1,z + ChunkZ,usingBiome.biome, bRef);

			double val1 = 0.0d;double vals1 = 0.0d;
			ModifyActiveBiome(32,z,ref val1,cc,ref vals1);
			bRef = InterpolateBiomeValues(ref val1,ref vals1);
			cc.east[z] = openSimplexNoise(ChunkX + 32, z + ChunkZ, usingBiome.biome, bRef);
		}

		for (int x = 0; x < 32; x++) {
			double val = 0.0d;double vals = 0.0d;
			ModifyActiveBiome(x,-1,ref val,cc,ref vals);
			bRef = InterpolateBiomeValues(ref val,ref vals);
			cc.north[x] =  openSimplexNoise(ChunkX + x,ChunkZ + 32,usingBiome.biome, bRef);

			double val1 = 0.0d;double vals1 = 0.0d;
			ModifyActiveBiome(x,32,ref val1,cc,ref vals1);
			bRef = InterpolateBiomeValues(ref val1,ref vals1);
			cc.south[x] = openSimplexNoise(ChunkX + x, ChunkZ - 1, usingBiome.biome, bRef);
		}

		double val3 = 0.0d;double vals3 = 0.0d;
		ModifyActiveBiome(32,32,ref val3,cc,ref vals3);
		bRef = InterpolateBiomeValues(ref val3,ref vals3);
		cc.NECorner =  openSimplexNoise(ChunkX + 32, ChunkZ + 32, usingBiome.biome, bRef);

		double val4 = 0.0d;double vals4 = 0.0d;
		ModifyActiveBiome(32,-1,ref val4,cc,ref vals4);
		bRef = InterpolateBiomeValues(ref val4,ref vals4);
		cc.SECorner =  openSimplexNoise(ChunkX + 32, ChunkZ - 1, usingBiome.biome, bRef);

		double val5 = 0.0d;double vals5 = 0.0d;
		ModifyActiveBiome(-1,32,ref val5,cc,ref vals5);
		bRef = InterpolateBiomeValues(ref val5,ref vals5);
		cc.NWCorner =  openSimplexNoise(ChunkX - 1, ChunkZ + 32, usingBiome.biome, bRef);

		double val6 = 0.0d;double vals6 = 0.0d;
		ModifyActiveBiome(-1,-1,ref val6,cc,ref vals6);
		bRef = InterpolateBiomeValues(ref val6,ref vals6);
		cc.SWCorner =  openSimplexNoise(ChunkX - 1, ChunkZ - 1, usingBiome.biome, bRef);
	}


	/// <summary>
	/// Interpolates the biome values between the current biome
	/// and the biome closest to it.
	/// </summary>
	/// <returns>The biome values.</returns>
	/// <param name="val">Value.</param>
	/// <param name="vals">Vals.</param>
	BiomeRef InterpolateBiomeValues(ref double val,ref double vals){
		//iBiome is the biome that is mathematically closest to the current biome, the current
		//biome will interpolate its modifiers(vertical stretch, baseheight, etc..) towards iBiome
		BiomeRef iBiome = null;
		BiomeRef subCanidate = null;
		BiomeRef biomeCanidate = null;
		//ratio of current biome values to adjacent biome values at a distance
		//between the biomes
		double ratio = 1.0d;

		//get length to nearest biome in subBiomes array for the parent biome
		double subLength = double.MaxValue; double subRatio = 0.0d;
		//closer to biome with greater noise range..
		if (vals > usingBiome.UQuartile) {
			if (usingBiome.UBiomeRef != null){
				subLength = (usingBiome.UBiomeRef.LQuartile - vals) * worldData.SubBiomeSize;
				subRatio = subLength / ((usingBiome.UBiomeRef.LQuartile - usingBiome.UQuartile) * worldData.SubBiomeSize);
				subCanidate = usingBiome.UBiomeRef;
			}
		}
		//closer to biome with smaller noise range
		else if (vals < usingBiome.LQuartile){
			if (usingBiome.LBiomeRef != null){
				subLength = (vals - usingBiome.LBiomeRef.UQuartile) * worldData.SubBiomeSize;
				subRatio = subLength / ((usingBiome.LQuartile - usingBiome.LBiomeRef.UQuartile) * worldData.SubBiomeSize);
				subCanidate = usingBiome.LBiomeRef;
			}
		}

		//get length to nearest adjacent biome that is not a sub biome of the active parent biome
		double biomeLength = double.MaxValue; double biomeRatio = 0.0d;
		if (val > activeBiome.UQuartile) {
			if (activeBiome.UBiomeRef != null){
				biomeLength = activeBiome.UBiomeRef.LQuartile - val;
				biomeRatio = biomeLength / (activeBiome.UBiomeRef.LQuartile - activeBiome.UQuartile);
				biomeCanidate = activeBiome.UBiomeRef.biome.NearSubBiomes[(int)vals];
			}
		}
		else if (val < activeBiome.LQuartile){
			if (activeBiome.LBiomeRef != null){
				biomeLength = val - activeBiome.LBiomeRef.UQuartile;
				biomeRatio = biomeLength / (activeBiome.LQuartile - activeBiome.LBiomeRef.UQuartile);
				biomeCanidate = activeBiome.LBiomeRef.biome.NearSubBiomes[(int)vals];
			}
		}

		//Assign iBiome(interpolated biome)
		if (biomeCanidate == null && subCanidate == null)
			iBiome = null;
		else if (biomeLength < subLength){
			iBiome = biomeCanidate;ratio = biomeRatio;}
		else{
			iBiome = subCanidate;ratio = subRatio;}

		//Interpolate all modifiers in usingBiome towards iBiome modifier values based off ratio, ratio * b1 + (1 - ratio) * b2
		if (iBiome != null){
			//modifiers [(int)mods.HorizontalStretch] = ratio * usingBiome.biome.HorizontalStretch + (1 - ratio) * iBiome.biome.HorizontalStretch;
			//modifiers [(int)mods.VerticalStretch] = ratio * usingBiome.biome.VerticalStretch + (1 - ratio) * iBiome.biome.VerticalStretch;
			modifiers[(int)mods.BaseHeight] = ratio * usingBiome.biome.BaseHeight + (1 - ratio) * iBiome.biome.BaseHeight;
			modifiers [(int)mods.MinimumHeight] = ratio * usingBiome.biome.MinimumHeight + (1 - ratio) * iBiome.biome.MinimumHeight;
			modifiers [(int)mods.InversionHeight] = ratio * usingBiome.biome.InversionHeight + (1 - ratio) * iBiome.biome.InversionHeight;
			//modifiers [(int)mods.TurbulenceWidth1] = ratio * usingBiome.biome.TurbulenceWidth1 + (1 - ratio) * iBiome.biome.TurbulenceWidth1;
			//modifiers [(int)mods.TurbulenceHeight1] = ratio * usingBiome.biome.TurbulenceHeight1 + (1 - ratio) * iBiome.biome.TurbulenceHeight1;
			//modifiers [(int)mods.TurbulenceWidth2] = ratio * usingBiome.biome.TurbulenceWidth2 + (1 - ratio) * iBiome.biome.TurbulenceWidth2;
			//modifiers [(int)mods.TurbulenceHeight2] = ratio * usingBiome.biome.TurbulenceHeight2 + (1 - ratio) * iBiome.biome.TurbulenceHeight2;
			modifiers [(int)mods.power1] = ratio * usingBiome.biome.power1 + (1 - ratio) * iBiome.biome.power1;
			modifiers [(int)mods.power1Height] = ratio * usingBiome.biome.power1Height + (1 - ratio) * iBiome.biome.power1Height;
			modifiers [(int)mods.power2] = ratio * usingBiome.biome.power2 + (1 - ratio) * iBiome.biome.power2;
			modifiers [(int)mods.power2Height] = ratio * usingBiome.biome.power2Height + (1 - ratio) * iBiome.biome.power2Height;
			modifiers [(int)mods.FallOffDistance] = ratio * usingBiome.biome.FallOffDistance + (1 - ratio) * iBiome.biome.FallOffDistance;
			modifiers[(int)mods.ReverseInversionHeight] = ratio * usingBiome.biome.ReverseInversionHeight + (1 - ratio) * iBiome.biome.ReverseInversionHeight;
			//use a sine-shaped ratio instead of linear for blending between biomes
			modifiers[(int)mods.Ratio] = FastSine(ratio * Mathf.PI - PI_DIVIDE_2)*0.5f + 0.5f;
			modifiers[(int)mods.FallOffVariability] = ratio * usingBiome.biome.FallOffVariability + (1 - ratio) * iBiome.biome.FallOffVariability;
		}

		//no change

		return iBiome;
	}

	/// <summary>
	/// Determines which biome will be used at x, z.
	/// </summary>
	/// <param name="x">The x coordinate.</param>
	/// <param name="z">The z coordinate.</param>
	/// <param name="val">Value.</param>
	/// <param name="cc">Cc.</param>
	/// <param name="vals">Vals.</param>
	//This function uses OpenSimplexNoise to determine which biome is located at the input coordinates
	void ModifyActiveBiome(int x, int z, ref double val, ChunkColumnGen cc, ref double vals){

		//Assign value
		val = noiseObj.eval ((double)(x + ChunkX) / (double)worldData.biomeSize, (double)(z + ChunkZ) / (double)worldData.biomeSize);
		//Make the value range from 0 to 99
		val = (val * 0.5f + 0.5f) * 99;

		//Get activeBiome in array
		activeBiome = worldData.biomes[(int)(val)];

		//Sub biome value
		vals = noiseObj.eval((double)(x + ChunkX + 1234) / ((double)worldData.biomeSize * worldData.SubBiomeSize)
		                     , (double)(z + ChunkZ + 6789) / ((double)worldData.biomeSize * worldData.SubBiomeSize));
		vals = (vals * 0.5f + 0.5f) * 99;

		//assign biome to use
		usingBiome = activeBiome.biome.NearSubBiomes[(int)(vals)];

		if (x > -1 && x < 32 && z > -1 && z < 32)
			cc.biomesTable [x, z] = usingBiome;
	}

	/// <summary>
	/// Adds Sloped blocks to the world if there are any.
	/// </summary>
	/// <param name="cc">Cc.</param>
	/// <param name="n">N.</param>
	/// <param name="height">Height.</param>
	/// <param name="x">The x coordinate.</param>
	/// <param name="z">The z coordinate.</param>
	/// <param name="layer">Layer.</param>
	void AddSlopes(ChunkColumnGen cc, int n, int height, int x, int z, int layer){

		BlockIDInt32 block;
		block.blockID1 = -1;
		block.blockID2 = -1;

		//if n is the top block
		if (n < 31 && n > -2){

			int y = n;
			n = height;

            //place sunken slopes, note '0' means only placing 0th layer instead of 0, 1, or 2, so this shoulkd be fixed
            //to fix this, the region script will also need to be modified as it only sorts slope blocks on the top layer
            if (cc.GetHeight(x + 1, z) > n && cc.GetHeight(x, z + 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Sslopes[0][1];
            else if (cc.GetHeight(x + 1, z) > n && cc.GetHeight(x, z - 1) > n)
                //the '3' in the Sslopes access corresponds to the Block.Rotations values
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Sslopes[0][3];
            else if (cc.GetHeight(x - 1, z) > n && cc.GetHeight(x, z + 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Sslopes[0][2];
            else if (cc.GetHeight(x - 1, z) > n && cc.GetHeight(x, z - 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Sslopes[0][4];
            //place normal slopes
            else if (cc.GetHeight(x + 1, z) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].slopes[0][2];
            else if (cc.GetHeight(x - 1, z) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].slopes[0][4];
            else if (cc.GetHeight(x, z + 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].slopes[0][1];
            else if (cc.GetHeight(x, z - 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].slopes[0][3];
            //place corner slopes
            else if (cc.GetHeight(x + 1, z + 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Cslopes[0][1];
            else if (cc.GetHeight(x + 1, z - 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Cslopes[0][3];
            else if (cc.GetHeight(x - 1, z + 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Cslopes[0][2];
            else if (cc.GetHeight(x - 1, z - 1) > n)
                block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Cslopes[0][4];

			//block received from if/else above is not null
			if (block.blockID1 >= 0 && block.blockID2 >= 0){
				//fill data arrays
				chunkBlockData[x,y+1,z] = (byte)block.blockID1;
				secondaryID[x,y+1,z] = (byte)block.blockID2;

				//Add corner blocks on top of sunken blocks if needed
				if (bl[block.blockID1].l[block.blockID2].blockType == primitiveBlockType.sunkenCornerSlope && y + 2 < 32){
					if (cc.GetHeight(x+1,z+1) > n + 1){
						block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Cslopes[0][1];
						chunkBlockData[x,y+2,z] = (byte)block.blockID1;secondaryID[x,y+2,z] = (byte)block.blockID2;
					}
					else if (cc.GetHeight(x+1,z-1) > n + 1){
						block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Cslopes[0][3];
						chunkBlockData[x,y+2,z] = (byte)block.blockID1;secondaryID[x,y+2,z] = (byte)block.blockID2;
					}
					else if (cc.GetHeight(x-1,z+1) > n + 1){
						block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Cslopes[0][2];
						chunkBlockData[x,y+2,z] = (byte)block.blockID1;secondaryID[x,y+2,z] = (byte)block.blockID2;
					}
					else if (cc.GetHeight(x-1,z-1) > n + 1){
						block = activeBiome.biome.layers[activeBiome.biome.layerRefs[height]].Cslopes[0][4];
						chunkBlockData[x,y+2,z] = (byte)block.blockID1;secondaryID[x,y+2,z] = (byte)block.blockID2;
					}
				}
			}
		}
	}

	/// <summary>
	/// Returns a height value for the height map at x, z.
	/// </summary>
	/// <returns>The simplex noise.</returns>
	/// <param name="x">The x coordinate.</param>
	/// <param name="z">The z coordinate.</param>
	/// <param name="b">The current biome.</param>
	/// <param name="bMods"> Interpolate height map value towards this</param>
	int openSimplexNoise(int x, int z, Biome b, BiomeRef bMods)
	{
		if (b.HorizontalStretch <= 0)
			b.HorizontalStretch = 0.0001f;
		
		if (b.TurbulenceWidth1 <= 0)
			b.TurbulenceWidth1  = 0.0001f;
		
		if (b.TurbulenceWidth2 <= 0)
			b.TurbulenceWidth2 = 0.0001f;

		//bMods is null means that no biome interpolation is occuring
		if (bMods == null){

			//Main terrain features
			double rValue;
			rValue= noiseObj.eval(((double)x) / b.HorizontalStretch, ((double)z) / b.HorizontalStretch);
			rValue = rValue * 0.5f + 0.5f;

			//Multiply by stretch height
			rValue*= b.VerticalStretch;

			rValue += b.BaseHeight;

			if (b.TurbulenceWidth1 != 1 && b.TurbulenceHeight1 != 1){
				//Terrain interference noise
				double iValue= noiseObj.eval(((double)x) / (b.HorizontalStretch * b.TurbulenceWidth1), 
			                             ((double)z) / (b.HorizontalStretch * b.TurbulenceWidth1));
				iValue = iValue * 0.5f + 0.5f;
				iValue *= b.VerticalStretch * b.TurbulenceHeight1;

				//Add turbulance
				rValue += iValue;
			}

			//Clamp height (Or explode it if power > 1) (loose clamping)
			if ((b.power1 > 1.05f || b.power1 < 1.05f) && rValue > b.power1Height){
				rValue = b.power1Height + System.Math.Pow(rValue - b.power1Height, b.power1);
				if ((b.power2 > 1.05f || b.power2 < 1.05f) && rValue > b.power2Height){
					rValue = b.power2Height + System.Math.Pow(rValue - b.power2Height, b.power2);
				}
			}

			if ( b.TurbulenceWidth2 != 1 && b.TurbulenceHeight2 != 1){
				//Terrain interference noise (secondary)
				double iValue2= noiseObj.eval(((double)x) / (b.HorizontalStretch * b.TurbulenceWidth2), 
			                              ((double)z) / (b.HorizontalStretch * b.TurbulenceWidth2));
				iValue2 = iValue2 * 0.5f + 0.5f;
				iValue2 *= b.VerticalStretch * b.TurbulenceHeight2;
			
				//Add turbulance
				rValue += iValue2;
			}

			//Inversion of heights at desired levels
			if (b.InversionHeight >= 1)
				if (rValue < b.InversionHeight)
					rValue = b.InversionHeight + (b.InversionHeight - rValue);

			if (b.ReverseInversionHeight >= 1)
				if (rValue > b.ReverseInversionHeight)
					rValue = b.ReverseInversionHeight - (rValue - b.ReverseInversionHeight);

			//Shorten height gradually around world edges to 0
			if (b.FallOffDistance > 0) {

				if (b.FallOffVariability <= 0)
					b.FallOffVariability = 0.001f;

				//if (x >= 0 && z >= 0){
					int xdistm = x;
					int zdistm = z;
					int xdist = RegionSize * 32 - x;
					int zdist = RegionSize * 32 - z;
					int smallest = xdistm;

					//get smallest dist
					if (zdistm < smallest)
						smallest = zdistm;
					if (xdist < smallest)
						smallest = xdist;
					if (zdist < smallest)
						smallest = zdist;

					double fV = noiseObj.eval(x * b.FallOffVariability / b.FallOffDistance,
					                          z * b.FallOffVariability / b.FallOffDistance);
					fV = fV * 0.5f + 0.5f;
					fV *= b.FallOffDistance;

					//implement
					if (smallest <= fV){
						//use large number for accurate ratio at each level
						float multiplier = (float)(1000000 / fV);
						float ratio = (smallest * multiplier) / 1000000;
						rValue *= ratio;
					}
				//}
			}

			//assign minHeight
			if (rValue < b.MinimumHeight)
				rValue = b.MinimumHeight;

			if (rValue > 255)
				rValue = 255;
		
			return (int) rValue;
		}
		//same as above but it uses different modifiers
		else{

			//blending towards this height
			double rValue = GetR1(x,z,b);
			double rValue2 = GetR1 (x,z,bMods.biome);

			double blendValue = rValue * modifiers[(int)mods.Ratio] + ( 1 - modifiers[(int)mods.Ratio]) * rValue2;
			rValue = blendValue;

			//assign minHeight
			if (rValue < modifiers[(int)mods.MinimumHeight])
				rValue = modifiers[(int)mods.MinimumHeight];
			
			if (rValue > 255)
				rValue = 255;
			
			return (int) rValue;
		}
	}

	private double GetR1(int x, int z, Biome b){
		
		//blending towards this height
		double rValue = noiseObj.eval(((double)x) / b.HorizontalStretch, ((double)z) / b.HorizontalStretch);
		rValue = rValue * 0.5f + 0.5f;
		
		//Multiply by stretch height, Add baseHeight
		rValue*= b.VerticalStretch;
		rValue += b.BaseHeight;
		
		if (b.TurbulenceWidth1 != 1 && b.TurbulenceHeight1 != 1){
			//Terrain interference noise
			double iValue= noiseObj.eval(((double)x) / (b.HorizontalStretch * b.TurbulenceWidth1), 
			                             ((double)z) / (b.HorizontalStretch * b.TurbulenceWidth1));
			iValue = iValue * 0.5f + 0.5f;
			iValue *= b.VerticalStretch * b.TurbulenceHeight1;
			
			//Add turbulance
			rValue += iValue;
		}
		
		//Clamp height (Or explode it if power > 1) (loose clamping)
		if ((modifiers[(int)mods.power1] > 1.05f || modifiers[(int)mods.power1] < 1.05f) && rValue > modifiers[(int)mods.power1Height]){
			rValue = modifiers[(int)mods.power1Height] + System.Math.Pow(rValue - modifiers[(int)mods.power1Height], modifiers[(int)mods.power1]);
			if ((modifiers[(int)mods.power2] > 1.05f || modifiers[(int)mods.power2] < 1.05f) && rValue > modifiers[(int)mods.power2Height]){
				rValue = modifiers[(int)mods.power2Height] + System.Math.Pow(rValue - modifiers[(int)mods.power2Height], modifiers[(int)mods.power2]);
			}
		}
		
		if (b.TurbulenceWidth2 != 1 && b.TurbulenceHeight2 != 1){
			//Terrain interference noise (secondary)
			double iValue2= noiseObj.eval(((double)x) / (b.HorizontalStretch * b.TurbulenceWidth2), 
			                              ((double)z) / (b.HorizontalStretch * b.TurbulenceWidth2));
			iValue2 = iValue2 * 0.5f + 0.5f;
			iValue2 *= b.VerticalStretch * b.TurbulenceHeight2;
			
			//Add turbulance
			rValue += iValue2;
		}
		
		//Inversion of heights at desired levels
		if (modifiers[(int)mods.InversionHeight] >= 1)
			if (rValue < modifiers[(int)mods.InversionHeight])
				rValue = modifiers[(int)mods.InversionHeight] + (modifiers[(int)mods.InversionHeight] - rValue);
		
		if (modifiers[(int)mods.ReverseInversionHeight] >= 1)
			if (rValue > modifiers[(int)mods.ReverseInversionHeight])
				rValue = modifiers[(int)mods.ReverseInversionHeight] - (rValue - modifiers[(int)mods.ReverseInversionHeight]);
		
		//Shorten height gradually around world edges to 0
		if (modifiers[(int)mods.FallOffDistance] > 0) {

			if (modifiers[(int)mods.FallOffVariability] <= 0)
				modifiers[(int)mods.FallOffVariability] = 0.001f;

			//if (x >= 0 && z >= 0){
				int xdistm = x;
				int zdistm = z;
				int xdist = RegionSize * 32 - x;
				int zdist = RegionSize * 32 - z;
				int smallest = xdistm;
				
				//get smallest dist
				if (zdistm < smallest)
					smallest = zdistm;
				if (xdist < smallest)
					smallest = xdist;
				if (zdist < smallest)
					smallest = zdist;

				//fall off distance variability
				double fV = noiseObj.eval(x * modifiers[(int)mods.FallOffVariability] / modifiers[(int)mods.FallOffDistance],
				                          z * modifiers[(int)mods.FallOffVariability] / modifiers[(int)mods.FallOffDistance]);
				fV = fV * 0.5f + 0.5f;
				fV *= modifiers[(int)mods.FallOffDistance];
				
				//implement
				if (smallest <= fV){
					//use large number for accurate ratio at each level
					float multiplier = (float)(1000000 / fV);
					float ratio = (smallest * multiplier) / 1000000;
					rValue *= ratio;
				}
			//}
		}
		
		return rValue;
	}

	//Only uses values between -pi and pi
	private double FastSine(double x){

		double sin = 0.0d;

		if (x < 0)
			sin = 1.27323954 * x + 0.405284735 * x * x;
		else
			sin = 1.27323954 * x - 0.405284735 * x * x;

		return sin;
	}
}

/*rotations: 

1: east/ ne   x+ / x+z+
2: west/ nw  x- / x-z+
3: north/ se  z+ / x+z-
4: south/ sw  z- / x-z-

*/








