using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public sealed class ThreadManager : MonoBehaviour
{
	//loadthread object, loading voxel data
	Thread loadThread;
	ThreadTask loadTask;
    ManualResetEvent MRE = new ManualResetEvent(false);

    //drawing voxel data
    Thread drawThread;
	ThreadTask drawTask;
    ManualResetEvent MRE1 = new ManualResetEvent(false);

    //tasks to be executed on the mian thread
    private Queue<DoTask> tasks = new Queue<DoTask>();

	private HashSet<string> forbidden = new HashSet<string>();

    /// <summary>
    /// Only allows for one task from draw + voxel load + main threads to be assigned at a time.
    /// </summary>
	private object lockObject = new object();

    /// <summary>
    /// Number of Tasks currently Queued on the Draw Thread
    /// </summary>
    public int DrawThreadTaskCount
    {
        get
        {
            return drawTask.TaskCount;
        }
    }
    /// <summary>
    /// Number of Tasks Currently Queued on the loading thread
    /// </summary>
    public int LoadThreadTaskCount
    {
        get
        {
            return loadTask.TaskCount;
        }
    }

	//Initialize stuff
	void Awake(){

		loadTask = new ThreadTask (MRE);
		loadThread = new Thread (loadTask.Run);
		loadThread.Start ();

		drawTask = new ThreadTask (MRE1);
		drawThread = new Thread (drawTask.Run);
		drawThread.Start ();

	}
	
	//each Thread executes one of it's delegates every frame
	void Update(){
		//set wait handle to unblock threads
		MRE.Set ();
		MRE1.Set ();

		bool tasksC = false;

		//Continue executing items in tasks Queue until one of them returns true
		//Main thread execution takes place here.
		while (!tasksC && tasks.Count > 0){

			DoTask t = tasks.Dequeue ();
			if (t != null){
				tasksC = t ();
			}
		}

        //Using the update loop to track number of logs per frame. See GameLog.cs
        GameLog.GameLogFunction();
    }

	void OnApplicationQuit(){
		tasks.Clear ();
		EndLoadThread ();
		EndDrawThread ();
	}

    /// <summary>
    /// Will prevent any more tasks being added to Main, Draw, or load Queues
    /// from the input world (unless the ignoreIdentifier parameter is true)
    /// </summary>
    /// <param name="worldName"></param>
    internal void StopFutureTasksOnWorld(string worldName)
    {
        lock (lockObject)
        {
            if (worldName == null)
            {
                GameLog.OutputToLog("Internal Error, input a null WorldName to StopFutureTasking request!!");
            }
            forbidden.Add(worldName);
        }
    }

    /// <summary>
    /// Allows tasks from the input worldName to be added to thread queues again
    /// </summary>
    /// <param name="worldName"></param>
    internal void ResumeTaskingOnWorld(string worldName)
    {
        lock(lockObject)
        {
            if (worldName == null)
            {
                GameLog.OutputToLog("Internal Error, input a null WorldName to ResumeFutureTasking request!!");
            }
            forbidden.Remove(worldName);
        }
    }

	/// <summary>
	/// Adds a task to load thread. The task belongs to the designated world.
    /// Set ignoreIdentifier = true, if this task must run no matter what.
    /// Eg. a cleanup task
	/// </summary>
	/// <param name="t">T.</param>
	/// <param name="i">The index.</param>
	internal void AddToLoadThread(DoTask t, string worldName, bool ignoreIdentifier = false){
		lock (lockObject)
		{
            if (worldName == null)
            {
                GameLog.OutputToLog("Internal Error, input a null WorldName to AddToLoadThreadRequest!");
            }
            if (!forbidden.Contains(worldName) || ignoreIdentifier)
	            loadTask.AddTask(t);
		}
	}

	/// <summary>
	/// Adds to draw thread.
	/// </summary>
	/// <param name="t">T.</param>
	/// <param name="i">The index.</param>
	internal void AddToDrawThread(DoTask t, string worldName, bool ignoreIdentifier = false){
		lock (lockObject)
		{
            if (worldName == null)
            {
                GameLog.OutputToLog("Internal Error, input a null WorldName to AddToDrawThreadRequest!");
            }
            if (!forbidden.Contains(worldName) || ignoreIdentifier)
                drawTask.AddTask(t);
        }
	}


	/// <summary>
	/// Ends the load thread.
	/// </summary>
	public void EndLoadThread(){
		lock (lockObject)
		{
			loadTask.Stop();
		}
	}

	/// <summary>
	/// Ends the draw thread.
	/// </summary>
	public void EndDrawThread(){
		lock (lockObject)
		{
			drawTask.Stop();
		}
	}

	/// <summary>
	/// Queues a task on the main thread. Worldname this task is running on.
	/// </summary>
	/// <param name="t">T.</param>
	/// <param name="i">The index.</param>
	public void QueueOnMain(DoTask t, string worldName, bool ignoreIdentifier = false){
		lock (lockObject)
		{
            if (worldName == null)
            {
                GameLog.OutputToLog("Internal Error, input a null WorldName to AddToMainThreadRequest!");
            }
            if (!forbidden.Contains(worldName) || ignoreIdentifier)
                tasks.Enqueue(t);
        }
	}

	~ThreadManager(){
        lock (lockObject)
        {
            drawTask.Stop();
            loadTask.Stop();
        }
	}
}

public delegate bool DoTask();

/// <summary>
/// Used for multi threading. This object represents a signle worker thread object.
/// </summary>
internal sealed class ThreadTask{

	private volatile bool Running = true;

	//list of functions to be called
	private BlockingQueue<DoTask> taskList = new BlockingQueue<DoTask> ();
	
	ManualResetEvent m;
	public ThreadTask(ManualResetEvent mre){
		m = mre;
	}

	public void Run(){

		if (!Running)
			Running = true;

		//This while loop executes one successful (returns true) function
		//every frame
		while (Running){

            //block thread until next update tick
            m.Reset();
            //Wait for the waithandle
            m.WaitOne();

            bool cont = false;
			DoTask node;

            //execute tasks until one of them returns true
            while (!cont && taskList.Count > 0){
				node = taskList.Dequeue();

                if (node != null)
                {
                    cont = node();
                }

            }

            if (!Running)
				taskList.Clear();
		}
    }

    public int TaskCount
    {
        get
        {
            return taskList.Count;
        }
    }

	public void AddTask(DoTask t){
        if (Running)
        {
            taskList.Enqueue(t);
        }
	}

	public void Stop(){
		Running = false;

		if (taskList.Count > 0)
			taskList.Clear ();
	}
}

















