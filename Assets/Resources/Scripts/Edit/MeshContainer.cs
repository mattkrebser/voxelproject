using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshContainer : MonoBehaviour
{
	[System.NonSerialized]
	public Mesh mesh;
	
	public void UpdateMesh(Vector3[] v, Vector2[] u, Vector2[] u2, int[] t, GameObject NG, List<BlockMaterial> texList, 
	                       List<Material> materialsList, WorldData world, 
	                       int matPos, int texPos, string worldName, string b){
		if (mesh == null) {
			
			//parent new object to the world
			NG.transform.SetParent (world.chunk_parent.transform);
			//Add mesh filter, renderer, set material

			NG.AddComponent<MeshRenderer> ();
			NG.AddComponent<MeshFilter> ();
			NG.GetComponent<MeshRenderer> ().material = materialsList [matPos];

			if (texList [texPos].texture == null){

				if (texList[texPos].BaseTexture){
					texList [texPos].texture = Resources.Load("Textures/Blocks/" + texList [texPos].texture_name) as Texture2D;
					GameLog.VoxelStatusOutput("Loaded a Base Texture: " + texList [texPos].texture_name);
				}
				else{
					texList [texPos].texture = FileClass.TextureLoader.LoadBlockTexture(b,worldName) as Texture2D;
					GameLog.VoxelStatusOutput("Loaded a Dyanmic Texture: " + b);
				}
			}

			if (texList [texPos].texture != null)
				NG.GetComponent<MeshRenderer> ().material.mainTexture = texList [texPos].texture;
			
			//NG.transform.name = texList[m.tex].texture_name + "." + m.meshObjectPos.ToString ();
			NG.transform.name = texList[texPos].texture_name;
			
			mesh = GetComponent<MeshFilter>().mesh;
		}
		
		mesh.Clear ();
		mesh.vertices = v;
		mesh.uv = u;
		mesh.uv2 = u2;
		mesh.triangles = t;
		mesh.RecalculateNormals ();
		mesh.Optimize ();
	}
	
	public void UpdateMesh(Vector3[] v, Vector2[] u, Vector2[] u2, int[] t){
		mesh.Clear ();
		mesh.vertices = v;
		mesh.uv = u;
		mesh.uv2 = u2;
		mesh.triangles = t;
		mesh.RecalculateNormals ();
		mesh.Optimize ();
	}
	
	public void Destroy(){
		Destroy (gameObject.GetComponent<MeshRenderer> ().material);
		Destroy (gameObject.GetComponent<MeshFilter>().mesh);
		Destroy (gameObject);
	}
	
}

