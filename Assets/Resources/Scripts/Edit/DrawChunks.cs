﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;



public class DrawChunks {

	//Holds the materials, not all of the materials in here are being drawn
	private LinkedList<List<AttatchedMesh>> materials = new LinkedList<List<AttatchedMesh>>();

	//each chunk is 32x32x32 blocks, each region is 8x8x8 chunks, the world is worldSize x worldSize regions

	//Global block coordinates of chunks and regionSize(in chunk coordinates)
	public int ChunkX, ChunkY, ChunkZ, regionSize;
	//Global chunk coordinates of this chunk
	public int ChunkXChunk, ChunkYChunk, ChunkZChunk;	
	//chunk coordinate in its region that it is located in
	public int mChunkX, mChunkZ;	
	//region coordinates, mRegionSize = 8 if regionSize > 8, otherwise mRegionSize = regionSize
	//worldSize is the size of the world in region coordinates
	public int regionX, regionZ, mRegionSize, worldSize, cRX, cRZ;
	
	/// <summary>
    /// The primary ID for a voxel
    /// </summary>
	public byte[,,] voxelData;
	/// <summary>
    /// The secondary ID for a voxel.
    /// </summary>
	public byte[,,] data;

	//reference to regions array
	private RegionData[,] regions;

	//reference to where we store all of our blocks >:)
	private List<L> blocks;

	//LoadChunks GameObject reference
	private WorldData l;

    //****please not lighting is currently disabled
    static byte[,,] lightd = new byte[32, 32, 32];
	//light data for this chunk
	public byte[,,] _lightData;
    public byte[,,] lightData
    {
        get
        {
            return lightd;
        }
    }

	/// <summary>
    /// Has the chunk been drawn after being loaded into the world?
    /// Has the chunk been drawn after being removed from memory?
    /// </summary>
	public bool IsFullyDrawn = false;

	//used in light calculation to determine if chunk actually changed light data
	//value of true means that chunk has changed lightData and needs to be redrawn
	public bool lightUpdate = false;

	//reference to MeshobjectManager
	private MeshObjectManager MOM;

	//number of drawchunk objects
	public static int count = 0;

	//function pointers to drawing functions
	private List<Action<int,int,int,AttatchedMesh>> drawFunctionsList;

	//##################################################################################
	//In order to make attatchedMesh Object elligible for disposal, remove them from
	//their respective meshobjects

	// Use this for initialization
	public DrawChunks(byte[,,] vD,byte[,,] vD2, byte[,,] lD, int cx,int cy,int cz, int rS,
        WorldData L, MeshObjectManager M, List<L> b) 
	{
		//Initialize
		voxelData = vD;
		data = vD2;
		_lightData = lD;
		ChunkX = cx;
		ChunkY = cy;
		ChunkZ = cz;
		regionSize = rS;

		//initialize more
		ChunkXChunk = ChunkX;ChunkYChunk = ChunkY;ChunkZChunk = ChunkZ;
		ChunkX = ChunkX * 32;ChunkY = ChunkY * 32;ChunkZ = ChunkZ * 32;
		mChunkX = ChunkXChunk % 8; mChunkZ = ChunkZChunk % 8;
		regionX = ChunkXChunk / 8;regionZ = ChunkZChunk / 8;
		worldSize = (regionSize-1) / 8 + 1;
		mRegionSize = regionSize;
		if (regionSize > 8)
			mRegionSize = 8;

		cRX = regionSize - regionX * 8;
		cRZ = regionSize - regionZ * 8;
		if (cRX > 8)
			cRX = 8;
		if (cRZ > 8)
			cRZ = 8;

		l = L;
		MOM = M;
		blocks = b;

		regions = l.regionData;

		//Add functions to functions list
		drawFunctionsList = new List<Action<int,int,int,AttatchedMesh>>();
		drawFunctionsList.Add (drawCube);
		drawFunctionsList.Add (drawCube);
		drawFunctionsList.Add (drawSloped);
		drawFunctionsList.Add (drawSlopedCorner);
		drawFunctionsList.Add (drawSlopedSunken);
		drawFunctionsList.Add (drawRectangle);
		drawFunctionsList.Add (drawSmallBillboard);
		drawFunctionsList.Add (drawMediumBillboard);
		drawFunctionsList.Add (drawLargeBillboard);
		drawFunctionsList.Add (drawOther);

		count++;
	}

	~DrawChunks(){
		count--;
		GameLog.VoxelStatusOutput ("ChunkDown " + count.ToString());
	}

    /// <summary>
    /// Set a voxel.
    /// </summary>
    /// <param name="b"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    public void SetVoxel(blockID b, int x, int y, int z)
    {
        voxelData[x, y, z] = b.blockID1;
        data[x, y, z] = b.blockID2;
    }
    /// <summary>
    /// Gets a voxel.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public blockID GetVoxel(int x, int y, int z)
    {
        return new blockID(voxelData[x, y, z], data[x, y, z]);
    }

	public bool ChunkExists(int x, int y, int z){
		return false;
	}

	/// <summary>
    /// Returns true if the chunk at the given chunk relative coordinates has any non empty voxels
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
	public bool AccessLoadChunkDat(int x, int y, int z)
	{
		//if the coordinate supplied is out of bounds.. (x,y,z are chunk coordinates)
		if (x < 0) {
            //if the next region over is out of bounds
            if (regionX - 1 < 0)
                return false;
            //if the next region over exists, and loadValues has been initialized
            else if (!System.Object.ReferenceEquals(regions[regionX - 1, regionZ], null) &&
                regions[regionX - 1, regionZ].LoadValLength > 0)
            {
                if (System.Object.ReferenceEquals(regions[regionX - 1, regionZ].chunks[7, y, z], null) &&
                    regions[regionX - 1, regionZ].GetLoadValue(7, y, z) == 1)
                    return false;
                return regions[regionX - 1, regionZ].GetLoadValue(7, y, z) > 0 ? true : false;
            }
            return false;
		}
		else if (x >= cRX){
            if (regionX + 1 >= worldSize)
                return false;
            else if (!System.Object.ReferenceEquals(regions[regionX + 1, regionZ], null) &&
                regions[regionX + 1, regionZ].LoadValLength > 0)
            {
                if (System.Object.ReferenceEquals(regions[regionX + 1, regionZ].chunks[0, y, z], null) &&
                    regions[regionX + 1, regionZ].GetLoadValue(0, y, z) == 1)
                    return false;
                return regions[regionX + 1, regionZ].GetLoadValue(0, y, z) > 0 ? true : false;
            }
            return false;
		}
		if (y < 0) {
            return false;
		}
		else if (y >= 8){
            return false;
		}
		if (z < 0) {
            if (regionZ - 1 < 0)
                return false;
            else if (!System.Object.ReferenceEquals(regions[regionX, regionZ - 1], null) &&
                regions[regionX, regionZ - 1].LoadValLength > 0)
            {
                if (System.Object.ReferenceEquals(regions[regionX, regionZ - 1].chunks[x, y, 7], null) &&
                    regions[regionX, regionZ - 1].GetLoadValue(x, y, 7) == 1)
                    return false;
                return regions[regionX, regionZ - 1].GetLoadValue(x, y, 7) > 0 ? true : false;
            }
            return false;
		}
		else if (z >= cRZ){
            if (regionZ + 1 >= worldSize)
                return false;
            else if (!System.Object.ReferenceEquals(regions[regionX, regionZ + 1], null) &&
                regions[regionX, regionZ + 1].LoadValLength > 0)
            {
                if (System.Object.ReferenceEquals(regions[regionX, regionZ + 1].chunks[x, y, 0], null) &&
                    regions[regionX, regionZ + 1].GetLoadValue(x, y, 0) == 1)
                    return false;
                return regions[regionX, regionZ + 1].GetLoadValue(x, y, 0) > 0 ? true : false;
            }
            return false;
		}
		else{
            if (System.Object.ReferenceEquals(regions[regionX, regionZ].chunks[x, y, z], null) &&
                regions[regionX, regionZ].GetLoadValue(x, y, z) == 1)
                return false;

            return regions[regionX, regionZ].GetLoadValue(x, y, z) > 0 ? true : false;
		}
	}
	
	//Overloaded versions that return both blockIDs
	public blockID BlockXPlus(int x, int y, int z, bool r){
		blockID block;
		block.blockID1 = 0;
		block.blockID2 = 0;
		if( x>31 )
		{
			//If AccessLoadChunkDat return 1, the chunk is guarenteed to exist
			if (AccessLoadChunkDat(mChunkX + 1,ChunkYChunk,mChunkZ)){
				//If the block being checked is in a different region...
				if (mChunkX + 1 >= mRegionSize){
					block.blockID1 = regions[regionX + 1,regionZ].chunks[0,ChunkYChunk,mChunkZ].voxelData[0,y,z];
					block.blockID2 = regions[regionX + 1,regionZ].chunks[0,ChunkYChunk,mChunkZ].data[0,y,z];
					return block;
				}
				else{
					block.blockID1 = regions[regionX,regionZ].chunks[mChunkX + 1,ChunkYChunk,mChunkZ].voxelData[0,y,z];
					block.blockID2 = regions[regionX,regionZ].chunks[mChunkX + 1,ChunkYChunk,mChunkZ].data[0,y,z];
					return block;
				}
			}
			else if (!AccessLoadChunkDat(mChunkX + 1,ChunkYChunk,mChunkZ))
				return block;

			block.blockID2 = 1;
			return block;
		}
		else{
			block.blockID1 = voxelData[x,y,z];
			block.blockID2 = data[x,y,z];
			return block;
		}
	}
	
	public blockID BlockXMinus(int x, int y, int z, bool r){
		blockID block;
		block.blockID1 = 0;
		block.blockID2 = 0;
		if (x < 0)
		{
			if (AccessLoadChunkDat(mChunkX - 1,ChunkYChunk,mChunkZ)){
				if (mChunkX - 1 < 0){
					block.blockID1 = regions[regionX - 1,regionZ].chunks[7,ChunkYChunk,mChunkZ].voxelData[31,y,z];
					block.blockID2 = regions[regionX - 1,regionZ].chunks[7,ChunkYChunk,mChunkZ].data[31,y,z];
					return block;
				}
				else{
					block.blockID1 = regions[regionX,regionZ].chunks[mChunkX - 1,ChunkYChunk,mChunkZ].voxelData[31,y,z];
					block.blockID2 = regions[regionX,regionZ].chunks[mChunkX - 1,ChunkYChunk,mChunkZ].data[31,y,z];
					return block;
				}
			}
			else if (!AccessLoadChunkDat(mChunkX - 1,ChunkYChunk,mChunkZ))
				return block;
			block.blockID2 = 1;
			return block;
		}
		else{
			block.blockID1 = voxelData[x,y,z];
			block.blockID2 = data[x,y,z];
			return block;
		}
	}
	
	public blockID BlockYPlus(int x, int y, int z, bool r){
		blockID block;
		block.blockID1 = 0;
		block.blockID2 = 0;
		if( y>31 )
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk + 1,mChunkZ)){
				block.blockID1 = regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk + 1,mChunkZ].voxelData[x,0,z];
				block.blockID2 = regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk + 1,mChunkZ].data[x,0,z];
				return block;
			}
			else if (!AccessLoadChunkDat(mChunkX,ChunkYChunk + 1,mChunkZ))
				return block;
			block.blockID2 = 1;
			return block;
		}
		else{
			block.blockID1 = voxelData[x,y,z];
			block.blockID2 = data[x,y,z];
			return block;
		}
	}
	
	public blockID BlockYMinus(int x, int y, int z, bool r){
		blockID block;
		block.blockID1 = 0;
		block.blockID2 = 0;
		if (y < 0)
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk - 1,mChunkZ)){
				block.blockID1 = regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk - 1,mChunkZ].voxelData[x,31,z];
				block.blockID2 = regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk - 1,mChunkZ].data[x,31,z];
				return block;
			}
			else if (ChunkYChunk - 1 < 0){
				block.blockID2 = 1;
				return block;
			}
			else if (!AccessLoadChunkDat(mChunkX,ChunkYChunk - 1,mChunkZ))
				return block;
			block.blockID2 = 1;
			return block;
		}
		else{
			block.blockID1 = voxelData[x,y,z];
			block.blockID2 = data[x,y,z];
			return block;
		}
	}
	
	public blockID BlockZPlus(int x, int y, int z, bool r){
		blockID block;
		block.blockID1 = 0;
		block.blockID2 = 0;
		if( z>31 )
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk,mChunkZ + 1)){
				if (mChunkZ + 1 >= mRegionSize){
					block.blockID1 = regions[regionX,regionZ + 1].chunks[mChunkX,ChunkYChunk,0].voxelData[x,y,0];
					block.blockID2 = regions[regionX,regionZ + 1].chunks[mChunkX,ChunkYChunk,0].data[x,y,0];
					return block;
				}
				else{
					block.blockID1 = regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ + 1].voxelData[x,y,0];
					block.blockID2 = regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ + 1].data[x,y,0];
					return block;
				}
			}
			else if (!AccessLoadChunkDat(mChunkX,ChunkYChunk,mChunkZ + 1))
				return block;
			block.blockID2 = 1;
			return block;
		}
		else{
			block.blockID1 = voxelData[x,y,z];
			block.blockID2 = data[x,y,z];
			return block;
		}
	}
	
	public blockID BlockZMinus(int x, int y, int z, bool r){
		blockID block;
		block.blockID1 = 0;
		block.blockID2 = 0;
		if (z < 0)
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk,mChunkZ - 1)){
				if (mChunkZ - 1 < 0){
					block.blockID1 =  regions[regionX,regionZ - 1].chunks[mChunkX,ChunkYChunk,7].voxelData[x,y,31];
					block.blockID2 = regions[regionX,regionZ - 1].chunks[mChunkX,ChunkYChunk,7].data[x,y,31];
					return block;
				}
				else{
					block.blockID1 = regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ - 1].voxelData[x,y,31];
					block.blockID2 = regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ - 1].data[x,y,31];
					return block;
				}
			}
			else if (!AccessLoadChunkDat(mChunkX,ChunkYChunk,mChunkZ - 1))
				return block;
			block.blockID2 = 1;
			return block;
		}
		else{
			block.blockID1 = voxelData[x,y,z];
			block.blockID2 = data[x,y,z];
			return block;
		}
	}

    /// <summary>
    /// Returns 1 if the chunk at the input position exists and is a non air chunk.
    /// Can accept values up to 32 blocks away from this chunk.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public byte AccessLoadChunkDatPlus(int x, int y, int z)
	{
		int tregionX = regionX;
		int tregionZ = regionZ;
		//if the coordinate supplied is out of bounds.. (x,y,z are chunk coordinates)
		if (x < 0) {
			//if the next region over is out of bounds
			if (regionX - 1 < 0)
				return 255;
			x=7;
			tregionX --;
		}
		else if (x >= cRX){
			if (regionX + 1 >= worldSize)
				return 255;
			x=0;
			tregionX++;
		}
		if (y < 0) {
			return 255;
		}
		else if (y >= 8){
			return 0;
		}
		if (z < 0) {
			if (regionZ - 1 < 0)
				return 255;
			z=7;
			tregionZ--;
		}
		else if (z >= cRZ){
			if (regionZ + 1 >= worldSize)
				return 255;
			z=0;
			tregionZ++;
		}

		if (!System.Object.ReferenceEquals(regions [tregionX, tregionZ],null) && 
            regions [tregionX, tregionZ].LoadValLength > 0) {
			if (System.Object.ReferenceEquals(regions[tregionX,tregionZ].chunks[x,y,z], null) &&
                regions[tregionX,tregionZ].GetLoadValue(x,y,z) == 1)
				return (byte)(0);
			return regions[tregionX,tregionZ].GetLoadValue(x,y,z);
		}
		else{
			return 0;
		}
	}
	
	/// <summary>
    /// Gets the blockID of a block that is less than 32 blocks from the chunk.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="returningBlockid2">Not used anymore. This parameter has no effect.</param>
    /// <returns></returns>
	public blockID BlockExtended(int x, int y, int z,bool returningBlockid2)
	{
		blockID block;
		//reset our temp chunks to this chunks position
		int tempChunkX=mChunkX;
		int tempChunkY=ChunkYChunk;
		int tempChunkZ=mChunkZ;
		int tregX=regionX, tregZ=regionZ;
		byte loadval = 0;
		
		if (x >= 32)
		{
			x-=32;
			tempChunkX = mChunkX + 1;
		}
		else if (x < 0)
		{
			x+=32;
			tempChunkX = mChunkX - 1;
		}
		if (y >= 32)
		{
			y-=32;
			tempChunkY = ChunkYChunk + 1;
		}
		else if(y < 0)
		{
			y+=32;
			tempChunkY = ChunkYChunk - 1;
		}
		if (z >= 32)
		{
			z-=32;
			tempChunkZ = mChunkZ + 1;
		}
		else if (z < 0)
		{
			z+=32;
			tempChunkZ = mChunkZ - 1;
		}
		
		loadval = AccessLoadChunkDatPlus (tempChunkX, tempChunkY, tempChunkZ);
		//returns a 1(byte) if chunk that holds the desired block exists
		if (loadval == 1){
			//Change chunk and region coordinates so that they are in bounds
			if (tempChunkX >= mRegionSize){
				tempChunkX = 0;
				tregX++;
			}
			else if (tempChunkX < 0){
				tempChunkX = 7;
				tregX--;
			}
			if (tempChunkZ >= mRegionSize){
				tempChunkZ = 0;
				tregZ++;
			}
			else if (tempChunkZ < 0){
				tempChunkZ = 7;
				tregZ--;
			}

			block.blockID1 = regions[tregX,tregZ].chunks[tempChunkX,tempChunkY,tempChunkZ].voxelData[x,y,z];
			block.blockID2 = regions[tregX,tregZ].chunks[tempChunkX,tempChunkY,tempChunkZ].data[x,y,z];
			
			return block;
		}
		else if (loadval == 0)
		{
			block.blockID1 = 0;
			block.blockID2 = 0;
			return block;
		}
		block.blockID2 = 1;
		block.blockID1 = 0;
		return block;
	}

	//uses same variables as function above
	//returns a light value up to one chunk(32 blocks) away
	byte LightExtended(int x, int y, int z)
	{
		//reset our temp chunks to this chunks position
		int tempChunkX=mChunkX;
		int tempChunkY=ChunkYChunk;
		int tempChunkZ=mChunkZ;
		int tregX=regionX, tregZ=regionZ;
		byte loadval = 0;
		
		if (x >= 32)
		{
			x-=32;
			tempChunkX = mChunkX + 1;
		}
		else if (x < 0)
		{
			x+=32;
			tempChunkX = mChunkX - 1;
		}
		if (y >= 32)
		{
			y-=32;
			tempChunkY = ChunkYChunk + 1;
		}
		else if(y < 0)
		{
			y+=32;
			tempChunkY = ChunkYChunk - 1;
		}
		if (z >= 32)
		{
			z-=32;
			tempChunkZ = mChunkZ + 1;
		}
		else if (z < 0)
		{
			z+=32;
			tempChunkZ = mChunkZ - 1;
		}
		
		loadval = AccessLoadChunkDatPlus (tempChunkX, tempChunkY, tempChunkZ);
		//returns a 1(byte) if chunk that holds the desired block exists
		if (loadval == 1){
			//Change chunk and region coordinates so that they are in bounds
			if (tempChunkX >= mRegionSize){
				tempChunkX = 0;
				tregX++;
			}
			else if (tempChunkX < 0){
				tempChunkX = 7;
				tregX--;
			}
			if (tempChunkZ >= mRegionSize){
				tempChunkZ = 0;
				tregZ++;
			}
			else if (tempChunkZ < 0){
				tempChunkZ = 7;
				tregZ--;
			}
			
			return regions[tregX,tregZ].chunks[tempChunkX,tempChunkY,tempChunkZ].lightData[x,y,z];
		}
		else
			return 0;
	}

	//uses same variables as function above, returns light beam height up to one chunk away
	byte GetLightBeamExtended(int x, int z)
	{
		//reset our temp chunks to this chunks position
		int tempChunkX=mChunkX;
		int tempChunkZ=mChunkZ;
		int tregX=regionX, tregZ=regionZ;

		//modify block position relative to chunk
		if (x >= 32)
		{
			x-=32;
			tempChunkX = mChunkX + 1;
		}
		else if (x < 0)
		{
			x+=32;
			tempChunkX = mChunkX - 1;
		}
		if (z >= 32)
		{
			z-=32;
			tempChunkZ = mChunkZ + 1;
		}
		else if (z < 0)
		{
			z+=32;
			tempChunkZ = mChunkZ - 1;
		}

		//Change chunk and region coordinates so that they are in bounds
		if (tempChunkX >= mRegionSize){
			tempChunkX = 0;
			tregX++;
		}
		else if (tempChunkX < 0){
			tempChunkX = 7;
			tregX--;
		}
		if (tempChunkZ >= mRegionSize){
			tempChunkZ = 0;
			tregZ++;
		}
		else if (tempChunkZ < 0){
			tempChunkZ = 7;
			tregZ--;
		}

		//if the region exists and has been initialized
		if (tregX >= 0 && tregX < worldSize && tregZ >= 0 && tregZ < worldSize && !System.Object.ReferenceEquals(regions[tregX,tregZ], null) && regions[tregX,tregZ].naturalLight.Length > 0)
			return regions[tregX,tregZ].naturalLight[tempChunkX,tempChunkZ,x,z];
		else
			return 0;

	}

	//returns light beam that is up to one block away
	byte GetLightBeam(int x, int z)
	{
		if (x > 31) {
			if (mChunkX + 1 >= mRegionSize)
			{
				if (regionX + 1 >= worldSize)
					return 0;
				else if (!System.Object.ReferenceEquals(regions[regionX+ 1,regionZ], null) && regions[regionX + 1,regionZ].naturalLight.Length > 0)
					return regions[regionX + 1,regionZ].naturalLight[0,mChunkZ,0,z];
				return 0;
			}
			else
				return regions[regionX,regionZ].naturalLight[mChunkX + 1, mChunkZ, 0, z];
		}
		else if (x < 0){
			if (mChunkX - 1 < 0)
			{
				if (regionX - 1 < 0)
					return 0;
				else if (!System.Object.ReferenceEquals(regions[regionX- 1,regionZ], null) && regions[regionX - 1,regionZ].naturalLight.Length > 0)
					return regions[regionX - 1,regionZ].naturalLight[7,mChunkZ,31,z];
				return 0;
			}
			else
				return regions[regionX,regionZ].naturalLight[mChunkX - 1, mChunkZ, 31, z];
		}
		else if (z > 31){
			if (mChunkZ + 1 >= mRegionSize)
			{
				if (regionZ + 1 >= worldSize)
					return 0;
				else if (!System.Object.ReferenceEquals(regions[regionX,regionZ + 1], null) && regions[regionX ,regionZ + 1].naturalLight.Length > 0)
					return regions[regionX,regionZ + 1].naturalLight[mChunkX ,0,x,0];
				return 0;
			}
			else
				return regions[regionX,regionZ].naturalLight[mChunkX, mChunkZ + 1, x, 0];
		}
		else if (z < 0){
			if (mChunkZ - 1 < 0)
			{
				if (regionZ - 1 < 0)
					return 0;
				else if (!System.Object.ReferenceEquals(regions[regionX,regionZ - 1], null) && regions[regionX ,regionZ - 1].naturalLight.Length > 0)
					return regions[regionX,regionZ - 1].naturalLight[mChunkX ,7,x,31];
				return 0;
			}
			else
				return regions[regionX,regionZ].naturalLight[mChunkX, mChunkZ - 1, x, 31];
		}
		else
			return regions[regionX,regionZ].naturalLight[mChunkX,mChunkZ,x,z];
		
	}

	//Get light values in chunks up to one block away
	public byte LightVal(int x, int y, int z)
	{
		//each part pretty much works the same. I am not commenting on all of them.
		//If the block being checked is not located in the data array for the current chunk...
		if( x>31 )
		{
			//If AccessLoadChunkDat return 1, the chunk is guarenteed to exist
			if (AccessLoadChunkDat(mChunkX + 1,ChunkYChunk,mChunkZ)){
				//If the block being checked is in a different region...
				if (mChunkX + 1 >= mRegionSize)
					return regions[regionX + 1,regionZ].chunks[0,ChunkYChunk,mChunkZ].lightData[0,y,z];
				//if the block being checked is not in a different region
				else
					return regions[regionX,regionZ].chunks[mChunkX + 1,ChunkYChunk,mChunkZ].lightData[0,y,z];
			}
			else 
				return 0;
		}
		else if (x < 0)
		{
			if (AccessLoadChunkDat(mChunkX - 1,ChunkYChunk,mChunkZ)){
				if (mChunkX - 1 < 0)
					return regions[regionX - 1,regionZ].chunks[7,ChunkYChunk,mChunkZ].lightData[31,y,z];
				else
					return regions[regionX,regionZ].chunks[mChunkX - 1,ChunkYChunk,mChunkZ].lightData[31,y,z];
			}
			else 
				return 0;
		}
		else if( y>31 )
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk + 1,mChunkZ)){
				return regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk + 1,mChunkZ].lightData[x,0,z];
			}
			else 
				return 0;
		}
		else if (y < 0)
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk - 1,mChunkZ)){
				return regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk - 1,mChunkZ].lightData[x,31,z];
			}
			else 
				return 0;
		}
		else if( z>31 )
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk,mChunkZ + 1)){
				if (mChunkZ + 1 >= mRegionSize)
					return regions[regionX,regionZ + 1].chunks[mChunkX,ChunkYChunk,0].lightData[x,y,0];
				else
					return regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ + 1].lightData[x,y,0];
			}
			else 
				return 0;
		}
		else if (z < 0)
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk,mChunkZ - 1)){
				if (mChunkZ - 1 < 0)
					return regions[regionX,regionZ - 1].chunks[mChunkX,ChunkYChunk,7].lightData[x,y,31];
				else
					return regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ - 1].lightData[x,y,31];
			}
			else 
				return 0;
		}
		else
			return lightData[x,y,z];
	}

    /// <summary>
    /// Returns the rotation of the block at the input position. The positon can be off by one voxel.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <returns></returns>
	public int GetRotation(int x, int y, int z){
		//each part pretty much works the same. I am not commenting on all of them.
		//If the block being checked is not located in the data array for the current chunk...
		if( x>31 )
		{
			//If AccessLoadChunkDat return 1, the chunk is guarenteed to exist
			if (AccessLoadChunkDat(mChunkX + 1,ChunkYChunk,mChunkZ)){
				//If the block being checked is in a different region...
				if (mChunkX + 1 >= mRegionSize)
					return blocks[regions[regionX + 1,regionZ].chunks[0,ChunkYChunk,mChunkZ].voxelData[0,y,z]]
					.l[regions[regionX + 1,regionZ].chunks[0,ChunkYChunk,mChunkZ].data[0,y,z]].Rotation;
				//if the block being checked is not in a different region
				else
					return blocks[regions[regionX,regionZ].chunks[mChunkX + 1,ChunkYChunk,mChunkZ].voxelData[0,y,z]]
					.l[regions[regionX,regionZ].chunks[mChunkX + 1,ChunkYChunk,mChunkZ].data[0,y,z]].Rotation;
			}
			else 
				return 0;
		}
		else if (x < 0)
		{
			if (AccessLoadChunkDat(mChunkX - 1,ChunkYChunk,mChunkZ)){
				if (mChunkX - 1 < 0)
					return blocks[regions[regionX - 1,regionZ].chunks[7,ChunkYChunk,mChunkZ].voxelData[31,y,z]]
					.l[regions[regionX - 1,regionZ].chunks[7,ChunkYChunk,mChunkZ].data[31,y,z]].Rotation;
				else
					return blocks[regions[regionX,regionZ].chunks[mChunkX - 1,ChunkYChunk,mChunkZ].voxelData[31,y,z]]
					.l[regions[regionX,regionZ].chunks[mChunkX - 1,ChunkYChunk,mChunkZ].data[31,y,z]].Rotation;
			}
			else 
				return 0;
		}
		else if( y>31 )
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk + 1,mChunkZ)){
				return blocks[regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk + 1,mChunkZ].voxelData[x,0,z]]
				.l[regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk + 1,mChunkZ].data[x,0,z]].Rotation;
			}
			else 
				return 0;
		}
		else if (y < 0)
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk - 1,mChunkZ)){
				return blocks[regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk - 1,mChunkZ].voxelData[x,31,z]]
				.l[regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk - 1,mChunkZ].data[x,31,z]].Rotation;
			}
			else 
				return 0;
		}
		else if( z>31 )
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk,mChunkZ + 1)){
				if (mChunkZ + 1 >= mRegionSize)
					return blocks[regions[regionX,regionZ + 1].chunks[mChunkX,ChunkYChunk,0].voxelData[x,y,0]]
					.l[regions[regionX,regionZ + 1].chunks[mChunkX,ChunkYChunk,0].data[x,y,0]].Rotation;
				else
					return blocks[regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ + 1].voxelData[x,y,0]]
					.l[regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ + 1].data[x,y,0]].Rotation;
			}
			else 
				return 0;
		}
		else if (z < 0)
		{
			if (AccessLoadChunkDat(mChunkX,ChunkYChunk,mChunkZ - 1)){
				if (mChunkZ - 1 < 0)
					return blocks[regions[regionX,regionZ - 1].chunks[mChunkX,ChunkYChunk,7].voxelData[x,y,31]]
					.l[regions[regionX,regionZ - 1].chunks[mChunkX,ChunkYChunk,7].data[x,y,31]].Rotation;
				else
					return blocks[regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ - 1].voxelData[x,y,31]]
					.l[regions[regionX,regionZ].chunks[mChunkX,ChunkYChunk,mChunkZ - 1].data[x,y,31]].Rotation;
			}
			else 
				return 0;
		}
		else
			return blocks[voxelData[x,y,z]].l[data[x,y,z]].Rotation;
	}

	void Slopes(int x, int y, int z, AttatchedMesh m){
		if (GetRotation(x,y,z) == 1)
		{
			SlopedNorth(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockXPlus(x+1,y,z,true)) && GetRotation(x+1,y,z) == 0)//if there is an transparent voxel directly east..
				TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x+1,y,z) != 1 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x+1,y,z) == 3 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x+1,y,z) != 2 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockXMinus(x-1,y,z,true)) && GetRotation(x-1,y,z) == 0)//if there is an air voxel directly west..
				TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x-1,y,z) != 1 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x-1,y,z) == 4 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x-1,y,z) != 1 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 2)
		{
			SlopedEast(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockZPlus(x,y,z+1,true)) && GetRotation(x,y,z+1) == 0)//each face that is drawn has these two if statements which check if there is air next to the face/voxel that was drawn
				TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z+1) != 2 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z+1) == 2 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z+1) != 3 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockZMinus(x,y,z-1,true)) && GetRotation(x,y,z-1) == 0)
				TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z-1) != 2 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z-1) == 4 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z-1) != 1 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 3)
		{
			SlopedSouth(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockXPlus(x+1,y,z,true)) && GetRotation(x+1,y,z) == 0)
				TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x+1,y,z) != 3 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x+1,y,z) == 1 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x+1,y,z) != 4 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockXMinus(x-1,y,z,true)) && GetRotation(x-1,y,z) == 0)
				TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x-1,y,z) != 3 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x-1,y,z) == 2 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x-1,y,z) != 3 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 4)
		{
			SlopedWest(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockZPlus(x,y,z+1,true)) && GetRotation(x,y,z+1) == 0)
				TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z+1) != 4 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z+1) == 1 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z+1) != 4 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockZMinus(x,y,z-1,true)) && GetRotation(x,y,z-1) == 0)
				TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z-1) != 4 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z-1) == 3 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z-1) != 2 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
	}

	void SunkenSlopes(int x, int y, int z, AttatchedMesh m){
		if (GetRotation(x,y,z) == 1)
		{
			SunkenSlopedNE(x,y,z,voxelData[x,y,z],m);
			if(transparentVoxel(BlockZMinus(x,y,z-1,true)) && GetRotation(x,y,z-1) == 0)
				TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z-1) != 2 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z-1) != 1 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z-1) == 4 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockXMinus(x-1,y,z,true)) && GetRotation(x-1,y,z) == 0)
				TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x-1,y,z) != 1 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x-1,y,z) != 1 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x-1,y,z) == 4 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 2)
		{
			SunkenSlopedNW(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockXPlus(x+1,y,z,true)) && GetRotation(x+1,y,z) == 0)
				TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x+1,y,z) != 1 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x+1,y,z) != 2 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x+1,y,z) == 3 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockZMinus(x,y,z-1,true)) && GetRotation(x,y,z-1) == 0)
				TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z-1) != 4 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z-1) != 2 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z-1) == 3 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 3)
		{
			SunkenSlopedSE(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockXMinus(x-1,y,z,true)) && GetRotation(x-1,y,z) == 0)
				TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x-1,y,z) != 3 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x-1,y,z) != 3 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x-1,y,z) == 2 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockZPlus(x,y,z+1,true)) && GetRotation(x,y,z+1) == 0)
				TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z+1) != 2 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z+1) != 3 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z+1) == 2 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 4)
		{
			SunkenSlopedSW(x,y,z,voxelData[x,y,z],m);
			if(transparentVoxel(BlockZPlus(x,y,z+1,true)) && GetRotation(x,y,z+1) == 0)
				TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z+1) != 4 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z+1) != 4 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z+1) == 1 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
			}
			if (transparentVoxel(BlockXPlus(x+1,y,z,true)) && GetRotation(x+1,y,z) == 0)
				TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x+1,y,z) != 3 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x+1,y,z) != 4 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x+1,y,z) == 1 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
	}

	void CornerSlopes(int x, int y, int z, AttatchedMesh m){
		if (GetRotation(x,y,z) == 1)
		{
			CornerSlopedFaceNE(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockXPlus(x+1,y,z,true)) && GetRotation(x+1,y,z) == 0)
				TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x+1,y,z) != 1 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x+1,y,z) != 2 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x+1,y,z) == 3 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockZPlus(x,y,z+1,true)) && GetRotation(x,y,z+1) == 0)
				TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z+1) != 2 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z+1) != 3 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z+1) == 2 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 2)
		{
			CornerSlopedFaceNW(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockXMinus(x-1,y,z,true)) && GetRotation(x-1,y,z) == 0)
				TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x-1,y,z) != 1 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x-1,y,z) != 1 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x-1,y,z) == 4 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedNorth(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockZPlus(x,y,z+1,true)) && GetRotation(x,y,z+1) == 0)
				TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z+1) != 4 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z+1) != 4 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z+1) == 1 && GetRotation(x,y,z+1) != 0)
						TriNorthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 3)
		{
			CornerSlopedFaceSE(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockXPlus(x+1,y,z,true)) && GetRotation(x+1,y,z) == 0)
				TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x+1,y,z) != 3 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x+1,y,z) != 4 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x+1,y,z) == 1 && GetRotation(x+1,y,z) != 0)
						TriEastSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockZMinus(x,y,z-1,true)) && GetRotation(x,y,z-1) == 0)
				TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z-1) != 2 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z-1) != 1 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z-1) == 4 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedEast(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
		else if (GetRotation(x,y,z) == 4)
		{
			CornerSlopedFaceSW(x,y,z,voxelData[x,y,z],m);
			if (transparentVoxel(BlockXMinus(x-1,y,z,true)) && GetRotation(x-1,y,z) == 0)
				TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.slope){
					if (GetRotation(x-1,y,z) != 3 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x-1,y,z) != 3 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x-1,y,z) == 2 && GetRotation(x-1,y,z) != 0)
						TriWestSlopedSouth(x,y,z,voxelData[x,y,z],m);
				}
			}
			if(transparentVoxel(BlockZMinus(x,y,z-1,true)) && GetRotation(x,y,z-1) == 0)
				TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
			else{ 
				if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.slope){
					if (GetRotation(x,y,z-1) != 4 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.cornerSlope){
					if (GetRotation(x,y,z-1) != 2 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
				else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.sunkenCornerSlope){
					if (GetRotation(x,y,z-1) == 3 && GetRotation(x,y,z-1) != 0)
						TriSouthSlopedWest(x,y,z,voxelData[x,y,z],m);
				}
			}
		}
	}


	//is this voxel transparent?
	private bool transparentVoxel(byte blockid1, byte blockid2){
		return blocks [blockid1].l [blockid2].isTransparent;
	}

	//transparent?
	private bool transparentVoxel(blockID block){
		return blocks [block.blockID1].l [block.blockID2].isTransparent;
	}

	//does this voxel collide with objects?
	private bool disabledPhysics(byte blockid1, byte blockid2){
		return blocks [blockid1].l [blockid2].physicsDisabled;
	}

	//transparent?
	private primitiveBlockType GetBlockType(blockID block){
		return blocks [block.blockID1].l [block.blockID2].blockType;
	}

	private bool isCube(blockID block){
		if (blocks[block.blockID1].l[block.blockID2].blockType == primitiveBlockType.cube)
			return true;
		return false;
	}

    //draw cubes
    private void drawCube(int x, int y, int z, AttatchedMesh m){
		if (transparentVoxel(BlockYPlus(x,y+1,z,true)))
			CubeTop(x,y,z,voxelData[x,y,z],m);

        //draw the bottom face only if the cube below is transparent and is in bounds
		if (transparentVoxel(BlockYMinus(x,y-1,z,true)) && !(ChunkY + y < 0))
			CubeBot(x,y,z,voxelData[x,y,z],m);

		if (transparentVoxel(BlockXPlus(x+1,y,z,true)) && !(ChunkX + x + 1 >= l.max_xzcoord))
			CubeEast(x,y,z,voxelData[x,y,z],m);
		else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.slope && GetRotation(x+1,y,z) != 4 &&
            !(ChunkX + x + 1 >= l.max_xzcoord))
			CubeEast(x,y,z,voxelData[x,y,z],m);
		else if (GetBlockType(BlockXPlus(x+1,y,z,true)) == primitiveBlockType.sunkenCornerSlope && GetRotation(x+1,y,z) != 4 &&
            GetRotation(x+1,y,z) != 2 && !(ChunkX + x + 1 >= l.max_xzcoord))
			CubeEast(x,y,z,voxelData[x,y,z],m);


		if (transparentVoxel(BlockXMinus(x-1,y,z,true)) && !(ChunkX + x - 1 < 0))
			CubeWest(x,y,z,voxelData[x,y,z],m);
		else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.slope && GetRotation(x-1,y,z) != 2 &&
            !(ChunkX + x - 1 < 0))
			CubeWest(x,y,z,voxelData[x,y,z],m);
		else if (GetBlockType(BlockXMinus(x-1,y,z,true)) == primitiveBlockType.sunkenCornerSlope && GetRotation(x-1,y,z) != 1 &&
            GetRotation(x-1,y,z) != 3 && !(ChunkX + x - 1 < 0))
			CubeWest(x,y,z,voxelData[x,y,z],m);

		if (transparentVoxel(BlockZPlus(x,y,z+1,true)) && !(ChunkZ + z + 1 >= l.max_xzcoord))
			CubeNorth(x,y,z,voxelData[x,y,z],m);
		else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.slope && GetRotation(x,y,z+1) != 3 &&
            !(ChunkZ + z + 1 >= l.max_xzcoord))
			CubeNorth(x,y,z,voxelData[x,y,z],m);
		else if (GetBlockType(BlockZPlus(x,y,z+1,true)) == primitiveBlockType.sunkenCornerSlope && GetRotation(x,y,z+1) != 3 &&
            GetRotation(x,y,z+1) != 4 && !(ChunkZ + z + 1 >= l.max_xzcoord))
			CubeNorth(x,y,z,voxelData[x,y,z],m);

		if (transparentVoxel(BlockZMinus(x,y,z-1,true)) && !(ChunkZ + z - 1 < 0))
			CubeSouth(x,y,z,voxelData[x,y,z],m);
		else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.slope && GetRotation(x,y,z-1) != 1 &&
            !(ChunkZ + z - 1 < 0))
			CubeSouth(x,y,z,voxelData[x,y,z],m);
		else if (GetBlockType(BlockZMinus(x,y,z-1,true)) == primitiveBlockType.sunkenCornerSlope && GetRotation(x,y,z-1) != 1 &&
            GetRotation(x,y,z-1) != 2 && !(ChunkZ + z - 1 < 0))
			CubeSouth(x,y,z,voxelData[x,y,z],m);
	}

	//draw sloped
	private void drawSloped(int x, int y, int z, AttatchedMesh m){
		Slopes(x,y,z,m);
	}

	//etc...
	private void drawSlopedCorner(int x, int y, int z, AttatchedMesh m){
		CornerSlopes(x,y,z,m);
	}

	private void drawSlopedSunken(int x, int y, int z, AttatchedMesh m){
		SunkenSlopes(x,y,z,m);
	}

	private void drawRectangle(int x, int y, int z, AttatchedMesh m){
		if (transparentVoxel(BlockYPlus(x,y+1,z,true)))//tree top is conditionally drawn
			DrawTreeTop(x,y,z,voxelData[x,y,z],m);
		DrawTreeNorth(x,y,z,voxelData[x,y,z],m);
		DrawTreeSouth(x,y,z,voxelData[x,y,z],m);
		DrawTreeWest(x,y,z,voxelData[x,y,z],m);
		DrawTreeEast(x,y,z,voxelData[x,y,z],m);
	}

	private void drawSmallBillboard(int x, int y, int z, AttatchedMesh m){
		DrawGrassOne(x,y,z,m);
		DrawGrassTwo(x,y,z,m);
		DrawGrassThree(x,y,z,m);
		DrawGrassFour(x,y,z,m);
	}

	private void drawMediumBillboard(int x, int y, int z, AttatchedMesh m){
		DrawBushSWToNE(x,y,z,m);
		DrawBushSEToNW(x,y,z,m);
		DrawBushNWToSE(x,y,z,m);
		DrawBushNEToSW(x,y,z,m);
	}

	private void drawLargeBillboard(int x, int y, int z, AttatchedMesh m){
		DrawTreeNToS(x,y,z,m);
		DrawTreeSToN(x,y,z,m);
		DrawTreeWToE(x,y,z,m);
		DrawTreeEToW(x,y,z,m);
		DrawTreeSWToNEDown(x,y,z,m);
		DrawTreeNEToSWDown(x,y,z,m);
		DrawTreeSEToNWDown(x,y,z,m);
		DrawTreeNWToSEDown(x,y,z,m);
	}

	private void drawOther(int x, int y, int z, AttatchedMesh m){

	}









	











//**********************************************************************************************************************************************************************
//      These functions are mesh face definitions. They each draw a specific face and assign a texture Based on Block Type
//**********************************************************************************************************************************************************************
	void CubeTop (int x, int y, int z, byte block,AttatchedMesh m) {

		int high = 0;
		//This corrects lighting issues that happen in rare cases, this if statement executes
		//when the voxel space above a solid(block) voxel is a special type such as a tree trunk or fence
		if (!transparentVoxel(BlockYPlus(x,y+1,z,true))){
			if (GetLightBeamExtended(x,z) > (y + ChunkY + 2)){
				if (LightExtended(x,y+2,z) > high)
					high = LightExtended(x,y+2,z);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x+1,z) > (y + ChunkY + 1)){
				if (LightExtended(x+1,y+1,z) > high)
					high = LightExtended(x+1,y+1,z);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x-1,z) > (y + ChunkY + 1)){
				if (LightExtended(x-1,y+1,z) > high)
					high = LightExtended(x-1,y+1,z);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x,z-1) > (y + ChunkY + 1)){
				if (LightExtended(x,y+1,z-1) > high)
					high = LightExtended(x,y+1,z-1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x,z+1) > (y + ChunkY + 1)){
				if (LightExtended(x,y+1,z+1) > high)
					high = LightExtended(x,y+1,z+1);
			}
			else
				high = 16;

			if (high == 0)
				high = 17;
		}

		//Add vertices, then light information to uv2
		m.verts.Add(new Vector3 (ChunkX + x,  ChunkY + y,  ChunkZ + z + 1));
		vertexCubeAOUp (x, y, z + 1,high,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y, ChunkZ + z + 1));
		vertexCubeAOUp (x + 1, y, z + 1,high,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y, ChunkZ + z ));
		vertexCubeAOUp (x + 1, y, z,high,m);
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y, ChunkZ + z ));
		vertexCubeAOUp (x, y, z,high,m);

		//Verify that light values added from earleir are correct
		verifyLightPoints (m);

		//texturePos is the xy location on the tile texture of each texture
		Vector2 texturePos;
		texturePos = blocks [block].l[data[x,y,z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
		
	}
	
	void CubeNorth (int x, int y, int z, byte block,AttatchedMesh m) {

		int high = 0;
		//This corrects lighting issues that happen in rare cases, this if statement executes
		//when the voxel space next to a solid(block) voxel is a special type such as a tree trunk or fence
		if (!transparentVoxel(BlockZPlus(x,y,z+1,true))){
			if (GetLightBeamExtended(x,z+2) > (y + ChunkY + 1)){
				if (LightExtended(x,y,z+2) > high)
					high = LightExtended(x,y,z+2);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x+1,z+1) > (y + ChunkY + 1)){
				if (LightExtended(x+1,y,z+1) > high)
					high = LightExtended(x+1,y,z+1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x-1,z+1) > (y + ChunkY + 1)){
				if (LightExtended(x-1,y,z+1) > high)
					high = LightExtended(x-1,y+1,z+1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x,z+1) > (y + ChunkY + 1)){
				if (LightExtended(x,y+1,z+1) > high)
					high = LightExtended(x,y+1,z+1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x,z+1) > (y + ChunkY + 1)){
				if (LightExtended(x,y-1,z+1) > high)
					high = LightExtended(x,y-1,z+1);
			}
			else
				high = 16;
			
			if (high == 0)
				high = 17;
		}
		
		m.verts.Add(new Vector3 (ChunkX + x + 1,ChunkY +  y-1,ChunkZ +  z + 1));
		vertexCubeAONorth (x + 1, y - 1, z + 1,high,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1,ChunkY +  y, ChunkZ + z + 1));
		vertexCubeAONorth (x + 1, y, z + 1,high,m);
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y, ChunkZ + z + 1));
		vertexCubeAONorth (x, y, z + 1,high,m);
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y-1, ChunkZ + z + 1));
		vertexCubeAONorth (x, y - 1, z + 1,high,m);

		verifyLightPoints (m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l[data[x,y,z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void CubeEast (int x, int y, int z, byte block,AttatchedMesh m) {

		int high = 0;
		//This corrects lighting issues that happen in rare cases, this if statement executes
		//when the voxel space next to a solid(block) voxel is a special type such as a tree trunk or fence
		if (!transparentVoxel(BlockXPlus(x+1,y,z,true))){
			if (GetLightBeamExtended(x+2,z) > (y + ChunkY + 1)){
				if (LightExtended(x+2,y,z) > high)
					high = LightExtended(x+2,y,z);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x+1,z+1) > (y + ChunkY + 1)){
				if (LightExtended(x+1,y,z+1) > high)
					high = LightExtended(x+1,y,z+1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x+1,z-1) > (y + ChunkY + 1)){
				if (LightExtended(x+1,y,z-1) > high)
					high = LightExtended(x+1,y,z-1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x+1,z) > (y + ChunkY + 1)){
				if (LightExtended(x+1,y+1,z) > high)
					high = LightExtended(x+1,y+1,z);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x+1,z) > (y + ChunkY + 1)){
				if (LightExtended(x+1,y-1,z) > high)
					high = LightExtended(x+1,y-1,z);
			}
			else
				high = 16;
			
			if (high == 0)
				high = 17;
		}
		
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z));
		vertexCubeAOEast (x + 1, y - 1, z,high,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y, ChunkZ + z));
		vertexCubeAOEast (x + 1, y, z,high,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y, ChunkZ + z + 1));
		vertexCubeAOEast (x + 1, y, z + 1,high,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAOEast (x + 1, y - 1, z + 1,high,m);

		verifyLightPoints (m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void CubeSouth (int x, int y, int z, byte block,AttatchedMesh m) {

		int high = 0;
		//This corrects lighting issues that happen in rare cases, this if statement executes
		//when the voxel space next to a solid(block) voxel is a special type such as a tree trunk or fence
		if (!transparentVoxel(BlockZMinus(x,y,z-1,true))){
			if (GetLightBeamExtended(x,z-2) > (y + ChunkY + 1)){
				if (LightExtended(x,y,z-2) > high)
					high = LightExtended(x,y,z-2);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x+1,z-1) > (y + ChunkY + 1)){
				if (LightExtended(x+1,y,z-1) > high)
					high = LightExtended(x+1,y,z-1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x-1,z-1) > (y + ChunkY + 1)){
				if (LightExtended(x-1,y,z-1) > high)
					high = LightExtended(x-1,y+1,z-1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x,z-1) > (y + ChunkY + 1)){
				if (LightExtended(x,y+1,z-1) > high)
					high = LightExtended(x,y+1,z-1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x,z-1) > (y + ChunkY + 1)){
				if (LightExtended(x,y-1,z-1) > high)
					high = LightExtended(x,y-1,z-1);
			}
			else
				high = 16;
			
			if (high == 0)
				high = 17;
		}
		
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y - 1, ChunkZ + z));
		vertexCubeAOSouth(x, y - 1, z,high,m);
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y, ChunkZ + z));
		vertexCubeAOSouth (x, y, z,high,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y, ChunkZ + z));
		vertexCubeAOSouth (x + 1, y, z,high,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z));
		vertexCubeAOSouth(x + 1, y - 1, z,high,m);

		verifyLightPoints (m);

		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void CubeWest (int x, int y, int z, byte block,AttatchedMesh m) {

		int high = 0;
		//This corrects lighting issues that happen in rare cases, this if statement executes
		//when the voxel space next to a solid(block) voxel is a special type such as a tree trunk or fence
		if (!transparentVoxel(BlockXMinus(x-1,y,z,true))){
			if (GetLightBeamExtended(x-2,z) > (y + ChunkY + 1)){
				if (LightExtended(x-2,y,z) > high)
					high = LightExtended(x-2,y,z);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x-1,z+1) > (y + ChunkY + 1)){
				if (LightExtended(x-1,y,z+1) > high)
					high = LightExtended(x-1,y,z+1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x-1,z-1) > (y + ChunkY + 1)){
				if (LightExtended(x-1,y,z-1) > high)
					high = LightExtended(x-1,y,z-1);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x-1,z) > (y + ChunkY + 1)){
				if (LightExtended(x-1,y+1,z) > high)
					high = LightExtended(x-1,y+1,z);
			}
			else
				high = 16;
			if (GetLightBeamExtended(x-1,z) > (y + ChunkY + 1)){
				if (LightExtended(x-1,y-1,z) > high)
					high = LightExtended(x-1,y-1,z);
			}
			else
				high = 16;
			
			if (high == 0)
				high = 17;
		}
		
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAOWest (x, y - 1, z + 1,high,m);
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y, ChunkZ + z + 1));
		vertexCubeAOWest (x, y, z + 1,high,m);
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y, ChunkZ + z));
		vertexCubeAOWest (x, y, z,high,m);
		m.verts.Add(new Vector3 (ChunkX + x, ChunkY + y - 1, ChunkZ + z));
		vertexCubeAOWest (x, y - 1, z,high,m);

		verifyLightPoints (m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void CubeBot (int x, int y, int z, byte block,AttatchedMesh m) {

		m.verts.Add(new Vector3 (ChunkX + x,  ChunkY + y-1,  ChunkZ + z ));
		vertexCubeAOBot (x,  y-1,  z,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y-1,  ChunkZ + z ));
		vertexCubeAOBot (x+1,  y-1,  z,m);
		m.verts.Add(new Vector3 (ChunkX + x + 1, ChunkY + y-1,  ChunkZ + z + 1));
		vertexCubeAOBot (x+1,  y-1,  z+1,m);
		m.verts.Add(new Vector3 (ChunkX + x,  ChunkY + y-1,  ChunkZ + z + 1));
		vertexCubeAOBot (x,  y-1,  z+1,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}

	void DrawTreeNorth(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add(new Vector3 (ChunkX + x+.65f,  ChunkY + y-1,  ChunkZ + z+.65f ));
		m.verts.Add(new Vector3 (ChunkX + x + .65f, ChunkY + y,  ChunkZ + z + .65f));
		m.verts.Add(new Vector3 (ChunkX + x + .35f, ChunkY + y,  ChunkZ + z + .65f));
		m.verts.Add(new Vector3 (ChunkX + x + .35f,  ChunkY + y-1,  ChunkZ + z + .65f));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));

		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void DrawTreeEast(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add(new Vector3 (ChunkX + x+.65f,  ChunkY + y-1, ChunkZ +  z+.35f ));
		m.verts.Add(new Vector3 (ChunkX + x + .65f, ChunkY + y, ChunkZ +  z + .35f));
		m.verts.Add(new Vector3 (ChunkX + x + .65f, ChunkY + y,  ChunkZ + z + .65f));
		m.verts.Add(new Vector3 (ChunkX + x + .65f,  ChunkY + y-1,  ChunkZ + z + .65f));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));

		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void DrawTreeWest(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add(new Vector3 (ChunkX + x+.35f,  ChunkY + y-1,  ChunkZ + z+.65f ));
		m.verts.Add(new Vector3 (ChunkX + x + .35f, ChunkY + y, ChunkZ +  z + .65f));
		m.verts.Add(new Vector3 (ChunkX + x + .35f, ChunkY + y,  ChunkZ + z + .35f));
		m.verts.Add(new Vector3 (ChunkX + x + .35f,  ChunkY + y-1, ChunkZ +  z + .35f));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));

		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void DrawTreeSouth(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add(new Vector3 (ChunkX + x+.35f,  ChunkY + y-1, ChunkZ +  z+.35f ));
		m.verts.Add(new Vector3 (ChunkX + x + .35f, ChunkY + y,  ChunkZ + z + .35f));
		m.verts.Add(new Vector3 (ChunkX + x + .65f, ChunkY + y,  ChunkZ + z + .35f));
		m.verts.Add(new Vector3 (ChunkX + x + .65f,  ChunkY + y-1, ChunkZ +  z + .35f));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void DrawTreeTop(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add(new Vector3 (ChunkX + x+.35f,  ChunkY + y, ChunkZ +  z+.35f ));
		m.verts.Add(new Vector3 (ChunkX + x + .35f, ChunkY + y, ChunkZ +  z + .65f));
		m.verts.Add(new Vector3 (ChunkX + x + .65f, ChunkY + y, ChunkZ +  z + .65f));
		m.verts.Add(new Vector3 (ChunkX + x + .65f,  ChunkY + y,  ChunkZ + z + .35f));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));
		m.uvs2.Add (new Vector2(0,lightData[x,y,z]));

		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Cube (texturePos,m);
	}
	
	void Cube (Vector2 texturePos,AttatchedMesh m) {
		
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 1); //2
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount + 3 ); //4
		
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y));
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y));
		
		m.vertCount+=4; // Add this line
	}

	void SlopedNorth(int x, int y, int z, byte block,AttatchedMesh m)//sloped up towards increasing Z coord
	{
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y, ChunkZ + z + 1));
		vertexSlopedAOUp (x + 1, y, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y-1, ChunkZ + z ));
		vertexAOSlopedNorth (x + 1, y - 1, z,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y-1, ChunkZ + z ));
		vertexAOSlopedNorth(x, y - 1, z,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y, ChunkZ + z + 1));
		vertexSlopedAOUp (x, y, z + 1,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Sloped (texturePos,m);
	}
	
	void SlopedSouth(int x, int y, int z, byte block,AttatchedMesh m)//sloped up towards decreasing Z coord
	{
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y  ,ChunkZ +  z));
		vertexSlopedAOUp (x, y, z,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y-1, ChunkZ + z + 1));
		vertexAOSlopedSouth (x, y - 1, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x+1, ChunkY + y-1, ChunkZ + z +1));
		vertexAOSlopedSouth  (x + 1, y - 1, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x+1 , ChunkY + y, ChunkZ + z ));
		vertexSlopedAOUp (x + 1, y, z,m);

		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Sloped (texturePos,m);
	}
	
	void SlopedEast(int x, int y, int z, byte block,AttatchedMesh m)//sloped up towards decreasing Z coord
	{
		m.verts.Add (new Vector3(ChunkX + x + 1 , ChunkY + y  , ChunkZ + z));
		vertexSlopedAOUp (x + 1, y, z,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y-1, ChunkZ + z ));
		vertexAOSlopedEast(x, y - 1, z,m);
		m.verts.Add (new Vector3(ChunkX + x, ChunkY + y-1, ChunkZ + z +1));
		vertexAOSlopedEast (x, y - 1, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x+1 , ChunkY + y, ChunkZ + z +1));
		vertexSlopedAOUp (x + 1, y, z + 1,m);

		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Sloped (texturePos,m);
	}
	
	void SlopedWest(int x, int y, int z, byte block,AttatchedMesh m)//sloped up towards decreasing Z coord
	{
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y  , ChunkZ + z + 1));
		vertexSlopedAOUp (x, y, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x +1, ChunkY + y-1, ChunkZ + z + 1));
		vertexAOSlopedWest (x + 1, y - 1, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x+1, ChunkY + y-1, ChunkZ + z ));
		vertexAOSlopedWest (x + 1, y - 1, z,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y, ChunkZ + z ));
		vertexSlopedAOUp (x, y, z,m);

		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		Sloped (texturePos,m);
	}
	//Sunken/corner slopes
	void SunkenSlopedNE(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y  , ChunkZ + z + 1));
		vertexSlopedAOUp(x, y, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y, ChunkZ + z ));
		vertexSlopedAOUp(x + 1, y, z,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y - 1, ChunkZ + z ));
		vertexSlopedAOUpOneNE(x, y - 1, z,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);

		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y  , ChunkZ + z + 1));
		vertexCubeAOUp(x, y, z + 1, 0,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y,ChunkZ +  z + 1));
		vertexCubeAOUp (x + 1, y, z + 1, 0,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y,ChunkZ +  z ));
		vertexCubeAOUp(x + 1, y, z, 0,m);

		OffSloped (texturePos,m);
	}
	
	void SunkenSlopedNW(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y  , ChunkZ + z ));
		vertexSlopedAOUp (x, y, z,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y, ChunkZ + z + 1));
		vertexSlopedAOUp (x + 1, y, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z ));
		vertexSlopedAOUpOneNW (x + 1, y - 1, z,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);

		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y  , ChunkZ + z ));
		vertexCubeAOUp (x, y, z, 0,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y, ChunkZ + z + 1));
		vertexCubeAOUp(x, y, z + 1, 0,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y, ChunkZ + z + 1));
		vertexCubeAOUp (x + 1, y, z + 1, 0,m);

		OffSloped  (texturePos,m);
	}
	
	void SunkenSlopedSW(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y  , ChunkZ + z ));
		vertexSlopedAOUp (x + 1, y, z,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y, ChunkZ + z + 1));
		vertexSlopedAOUp(x, y, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z + 1));
		vertexSlopedAOUpOneSW (x + 1, y - 1, z + 1,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped  (texturePos,m);

		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y  , ChunkZ + z ));
		vertexCubeAOUp  (x + 1, y, z, 0,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y, ChunkZ + z ));
		vertexCubeAOUp (x, y, z, 0,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y, ChunkZ + z + 1));
		vertexCubeAOUp (x, y, z + 1, 0,m);

		OffSloped (texturePos,m);
	}
	
	void SunkenSlopedSE(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y  , ChunkZ + z + 1));
		vertexSlopedAOUp (x + 1, y, z + 1,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y, ChunkZ + z ));
		vertexSlopedAOUp(x, y, z,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y - 1, ChunkZ + z + 1));
		vertexSlopedAOUpOneSE (x, y - 1, z + 1,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);

		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y  , ChunkZ + z + 1));
		vertexCubeAOUp (x + 1, y, z + 1, 0,m);
		m.verts.Add (new Vector3(ChunkX + x + 1, ChunkY + y, ChunkZ + z ));
		vertexCubeAOUp (x + 1, y, z, 0,m);
		m.verts.Add (new Vector3(ChunkX + x , ChunkY + y, ChunkZ + z ));
		vertexCubeAOUp(x, y, z, 0,m);

		OffSloped  (texturePos,m);
	}

	
	void CornerSlopedFaceNE(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z));
		vertexSlopedAOUpOneNE(x + 1, y - 1, z,m);
		m.verts.Add (new Vector3 (ChunkX + x, ChunkY + y - 1, ChunkZ + z + 1));
		vertexSlopedAOUpOneNE (x, y - 1, z + 1,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y, ChunkZ + z + 1));
		vertexSlopedAOUp(x + 1, y, z + 1,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void CornerSlopedFaceNW(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z + 1));
		vertexSlopedAOUpOneNW (x + 1, y - 1, z + 1,m);
		m.verts.Add (new Vector3 (ChunkX + x, ChunkY + y - 1, ChunkZ + z ));
		vertexSlopedAOUpOneNW (x, y - 1, z,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y, ChunkZ + z + 1));
		vertexSlopedAOUp (x, y, z + 1,m);

		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void CornerSlopedFaceSE(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z));
		vertexSlopedAOUpOneSE (x, y - 1, z,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z + 1));
		vertexSlopedAOUpOneSE (x + 1, y - 1, z + 1,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y, ChunkZ + z ));
		vertexSlopedAOUp (x + 1, y, z,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void CornerSlopedFaceSW(int x, int y, int z, byte block,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z + 1));
		vertexSlopedAOUpOneSW (x, y - 1, z + 1,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z ));
		vertexSlopedAOUpOneSW (x + 1, y - 1, z,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y, ChunkZ + z ));
		vertexSlopedAOUp (x, y, z,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}

	void TriNorthSlopedEast(int x, int y, int z, byte block,AttatchedMesh m)//this triangle is on the north face and is sloped uptowards the east
	{
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAONorth (x + 1, y - 1, z + 1,0,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y , ChunkZ + z + 1));
		vertexCubeAONorth (x + 1, y, z + 1,0,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAONorth (x, y - 1, z + 1,0,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void TriNorthSlopedWest(int x, int y, int z, byte block,AttatchedMesh m)//this triangle is on the North face and is sloped uptowards the West
	{
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAONorth (x + 1, y - 1, z + 1,0,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y , ChunkZ + z + 1));
		vertexCubeAONorth (x, y, z + 1,0,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAONorth (x, y - 1, z + 1,0,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void TriEastSlopedNorth(int x, int y, int z, byte block,AttatchedMesh m)//this triangle is on the East face and is sloped uptowards the North
	{

		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z ));
		vertexCubeAOEast (x + 1, y - 1, z,0,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y , ChunkZ + z + 1));
		vertexCubeAOEast (x + 1, y, z + 1,0,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAOEast (x + 1, y - 1, z + 1,0,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void TriEastSlopedSouth(int x, int y, int z, byte block,AttatchedMesh m)//this triangle is on the East face and is sloped uptowards the South
	{

		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z ));
		vertexCubeAOEast (x + 1, y - 1, z,0,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y , ChunkZ + z ));
		vertexCubeAOEast (x + 1, y, z,0,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAOEast (x + 1, y - 1, z + 1,0,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void TriSouthSlopedEast(int x, int y, int z, byte block,AttatchedMesh m)//this triangle is on the South face and is sloped uptowards the East
	{

		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z ));
		vertexCubeAOSouth (x, y - 1, z,0,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y , ChunkZ + z ));
		vertexCubeAOSouth (x + 1, y, z,0,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z ));
		vertexCubeAOSouth (x + 1, y - 1, z,0,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void TriSouthSlopedWest(int x, int y, int z, byte block,AttatchedMesh m)//this triangle is on the South face and is sloped uptowards the West
	{

		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z ));
		vertexCubeAOSouth (x, y - 1, z,0,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y , ChunkZ + z ));
		vertexCubeAOSouth (x, y, z,0,m);
		m.verts.Add (new Vector3 (ChunkX + x + 1, ChunkY + y - 1, ChunkZ + z ));
		vertexCubeAOSouth (x + 1, y - 1, z,0,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void TriWestSlopedNorth(int x, int y, int z, byte block,AttatchedMesh m)//this triangle is on the West face and is sloped uptowards the North
	{

		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAOWest (x, y - 1, z + 1,0,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y , ChunkZ + z + 1));
		vertexCubeAOWest (x, y, z + 1,0,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z ));
		vertexCubeAOWest (x, y - 1, z,0,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	void TriWestSlopedSouth(int x, int y, int z, byte block,AttatchedMesh m)//this triangle is on the West face and is sloped uptowards the South
	{

		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z + 1));
		vertexCubeAOWest (x, y - 1, z + 1,0,m);
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y , ChunkZ + z ));
		vertexCubeAOWest (x, y, z,0,m);
		m.verts.Add (new Vector3 (ChunkX + x ,ChunkY +  y - 1, ChunkZ + z ));
		vertexCubeAOWest (x, y - 1, z,0,m);
		
		Vector2 texturePos;
		texturePos = blocks [block].l [data [x, y, z]].UVCoordinates.ToVector2();
		OffSloped (texturePos,m);
	}
	
	
	
	//these two functions are used to draw the triangles and UV's of the different sloped faces
	void Sloped(Vector2 texturePos,AttatchedMesh m)
	{
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 1 ); //2
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount + 3 );
		
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y));
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y));
		
		m.vertCount+=4;
	}
	
	void OffSloped (Vector2 texturePos,AttatchedMesh m)
	{
		m.tris.Add (m.vertCount);
		m.tris.Add (m.vertCount + 1);
		m.tris.Add (m.vertCount + 2);
		
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y));
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y));
		
		m.vertCount+=3;
	}

	void DrawGrassOne(int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3(ChunkX + x,ChunkY + y-1,ChunkZ + z));
		m.verts.Add (new Vector3(ChunkX + x,ChunkY + y,ChunkZ + z));
		m.verts.Add (new Vector3(ChunkX + x+1,ChunkY + y,ChunkZ + z+1));
		m.verts.Add (new Vector3(ChunkX + x+1,ChunkY + y-1,ChunkZ + z+1));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		GrassUV (texturePos,m);
	}
	
	void DrawGrassTwo(int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3(ChunkX + x+1,ChunkY + y-1,ChunkZ + z+1));
		m.verts.Add (new Vector3(ChunkX + x+1,ChunkY + y,ChunkZ + z+1));
		m.verts.Add (new Vector3(ChunkX + x,ChunkY + y,ChunkZ + z));
		m.verts.Add (new Vector3(ChunkX + x,ChunkY + y-1,ChunkZ + z));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		GrassUV (texturePos,m);
	}
	
	void DrawGrassThree(int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3(ChunkX + x+1,ChunkY + y-1,ChunkZ + z));
		m.verts.Add (new Vector3(ChunkX + x+1,ChunkY + y,ChunkZ + z));
		m.verts.Add (new Vector3(ChunkX + x,ChunkY + y,ChunkZ + z+1));
		m.verts.Add (new Vector3(ChunkX + x,ChunkY + y-1,ChunkZ + z+1));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		GrassUV (texturePos,m);
	}
	
	void DrawGrassFour(int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3(ChunkX + x,ChunkY + y-1,ChunkZ + z+1));
		m.verts.Add (new Vector3(ChunkX + x,ChunkY + y,ChunkZ + z+1));
		m.verts.Add (new Vector3(ChunkX + x+1,ChunkY + y,ChunkZ + z));
		m.verts.Add (new Vector3(ChunkX + x+1,ChunkY + y-1,ChunkZ + z));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		GrassUV (texturePos,m);
	}
	
	void DrawBushSWToNE( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x, ChunkY + y - 1, ChunkZ + z));
		m.verts.Add (new Vector3 (ChunkX + x, ChunkY + y + 1, ChunkZ + z));
		m.verts.Add (new Vector3 (ChunkX + x + 2, ChunkY + y + 1, ChunkZ + z + 2));
		m.verts.Add (new Vector3 (ChunkX + x + 2, ChunkY + y - 1, ChunkZ + z + 2));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		BushUV (texturePos,m);
	}
	
	void DrawBushSEToNW( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x + 2, ChunkY + y - 1, ChunkZ + z));
		m.verts.Add (new Vector3 (ChunkX + x + 2,ChunkY +  y + 1, ChunkZ + z));
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y + 1, ChunkZ + z + 2));
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1,ChunkZ +  z + 2));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		BushUV (texturePos,m);
		
	}
	
	void DrawBushNWToSE( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x, ChunkY + y - 1, ChunkZ + z + 2));
		m.verts.Add (new Vector3 (ChunkX + x, ChunkY + y + 1, ChunkZ + z + 2));
		m.verts.Add (new Vector3 (ChunkX + x + 2, ChunkY + y + 1, ChunkZ + z));
		m.verts.Add (new Vector3 (ChunkX + x + 2, ChunkY + y - 1, ChunkZ + z));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		BushUV (texturePos,m);
	}
	
	void DrawBushNEToSW( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x + 2, ChunkY + y - 1, ChunkZ + z + 2));
		m.verts.Add (new Vector3 (ChunkX + x + 2, ChunkY + y + 1, ChunkZ + z + 2));
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y + 1, ChunkZ + z ));
		m.verts.Add (new Vector3 (ChunkX + x , ChunkY + y - 1, ChunkZ + z ));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		BushUV (texturePos,m);
	}
	
	void DrawTreeSToN ( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y - 2, ChunkZ + z - 2));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y + 1, ChunkZ + z - 2));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y + 1, ChunkZ + z + 3));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y - 2, ChunkZ + z + 3));

		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	void DrawTreeNToS(int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y - 2, ChunkZ + z + 3));
		m.verts.Add (new Vector3 (ChunkX + x + .5f,ChunkY +  y + 1, ChunkZ + z + 3));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y + 1, ChunkZ + z - 2));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y - 2, ChunkZ + z - 2));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	void DrawTreeEToW(int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x + 3, ChunkY + y - 2, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x + 3, ChunkY + y + 1, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x - 2, ChunkY + y + 1, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x -2, ChunkY + y - 2, ChunkZ + z + .5f));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	void DrawTreeWToE(int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x - 2, ChunkY + y - 2, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x - 2, ChunkY + y + 1, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x + 3, ChunkY + y + 1, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x + 3, ChunkY + y - 2, ChunkZ + z + .5f));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	void DrawTreeNE( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x - 2, ChunkY + y - 2, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x - 2, ChunkY + y + 1, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x + 3, ChunkY + y + 1, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x + 3, ChunkY + y - 2, ChunkZ + z + .5f));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	void DrawTreeSWToNEDown ( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x - 1.75f, ChunkY + y + 1, ChunkZ + z +.5f));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y - 2, ChunkZ + z + 2.75f));
		m.verts.Add (new Vector3 (ChunkX + x + 2.75f, ChunkY + y - 2, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x +.5f, ChunkY + y + 1, ChunkZ + z - 1.75f));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	void DrawTreeNEToSWDown ( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x + 2.75f, ChunkY + y + 1, ChunkZ + z +.5f));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y - 2, ChunkZ + z - 1.75f));
		m.verts.Add (new Vector3 (ChunkX + x - 1.75f, ChunkY + y - 2, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x +.5f, ChunkY + y + 1, ChunkZ + z + 2.75f));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	void DrawTreeSEToNWDown ( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x +.5f, ChunkY + y + 1, ChunkZ + z - 1.75f));
		m.verts.Add (new Vector3 (ChunkX + x - 1.75f, ChunkY + y - 2, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y - 2, ChunkZ + z + 2.75f));
		m.verts.Add (new Vector3 (ChunkX + x + 2.75f, ChunkY + y + 1, ChunkZ + z + .5f));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	void DrawTreeNWToSEDown ( int x, int y, int z,AttatchedMesh m)
	{
		m.verts.Add (new Vector3 (ChunkX + x +.5f, ChunkY + y + 1, ChunkZ + z + 2.75f));
		m.verts.Add (new Vector3 (ChunkX + x + 2.75f, ChunkY + y - 2, ChunkZ + z + .5f));
		m.verts.Add (new Vector3 (ChunkX + x + .5f, ChunkY + y - 2, ChunkZ + z - 1.75f));
		m.verts.Add (new Vector3 (ChunkX + x - 1.75f, ChunkY + y + 1, ChunkZ + z + .5f));
		
		Vector2 texturePos;
		texturePos = blocks [voxelData[x,y,z]].l [data[x,y,z]].UVCoordinates.ToVector2();
		TreeUV (texturePos,m);
	}
	
	//used in conjuntion with Drawgrass functions
	void GrassUV(Vector2 texturePos,AttatchedMesh m)
	{
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 1 ); //2
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount + 3 );
		
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y));
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y));

		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		
		m.vertCount+=4;
	}
	
	void BushUV(Vector2 texturePos,AttatchedMesh m)
	{
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 1 ); //2
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount + 3 );
		
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y));
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y));

		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		
		m.vertCount+=4;
	}
	
	void TreeUV(Vector2 texturePos,AttatchedMesh m)
	{
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 1 ); //2
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount); //1
		m.tris.Add(m.vertCount + 2 ); //3
		m.tris.Add(m.vertCount + 3 );
		
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y));
		m.uvs.Add(new Vector2 (texturePos.x + m.u, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y + m.u));
		m.uvs.Add(new Vector2 (texturePos.x, texturePos.y));

		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		m.uvs2.Add (Vector2.zero);
		
		m.vertCount+=4;
	}

	//These next functions will be checking for which vertex color value to assign to the voxel terrain shader
	//Each function gets a set of coordinates and corresponds to a certain direction. These functions are called
	//inside of the drawBlock functions that we call in our for loops (for drawing vertices) and will be called for every vertex

	//Only one vertex function can execute at 
	//a time for any given chunk. aOCount is the numeric value (0-3) for the darkness of each vertex. 3 being darkest
	//aOCount is stored in the x value of uv2 and uv2 gets sent to the shader for decoding (and shading !)
	private int aOCount = 0;
	//used to reverse light values, for example: LightVal of 13 gets reveresed to 2, 9 gets reversed to 6, 4 reversed to 11, 3 to 12
	//final 2 light values are special case on the reverse array
	private byte[] reverse = new byte[18]{0,13,11,9,7,5,3,1,1,3,5,7,9,11,13,15,13,2};
	void vertexCubeAOUp(int x, int y, int z, int high,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;
		//if block above us is solid, add darkness
		if (!transparentVoxel(BlockExtended(x,y+1,z,true)))
			aOCount++;
		//otherwise, grab light value
		else{
			lightVal = LightExtended (x, y + 1, z);
			//if this lightvalue is not part of a light beam
			if (GetLightBeamExtended(x,z) > (y+1+ChunkY))
				isBeam = false;
			//it is a part of a light beam
			else
				isBeam = true;
		}
		//each of these if/else works the same as above
		if (!transparentVoxel(BlockExtended(x-1,y+1,z,true)))
			aOCount++;
		//we only grab the first available light value for faster performance
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y + 1, z);
			if (GetLightBeamExtended(x-1,z) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x-1,y+1,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y + 1, z-1);
			if (GetLightBeamExtended(x-1,z-1) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y+1,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y + 1, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		//if we did not find an availble light value (all adjacent blocks are coverered
		//by non transparent blocks, use the input value "high"
		if (lightVal == -1){
			lightVal = high;}

		//if light value is not zero.
		if (lightVal > 0) {
			if (lightVal > 7)
				//we reverse the values because it is easier on the shader end to multiply
				//larger values as darkness
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		//if we have a value of zero and it is not a light beam, max dark
		else{
			if (!isBeam)
				lightVal = 15;
		}

		//if aoCount is out of bounds, resize
		if (aOCount == 4)
			aOCount = 3;

		//Add the vector
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		//reset aOCount
		aOCount = 0;
		//the below functions pretty much follow the same pattern as this one
	}

	void vertexCubeAOBot(int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (!transparentVoxel(BlockExtended(x,y,z,true)))
			aOCount++;
		else {
			lightVal = LightExtended (x, y, z);
			if (GetLightBeamExtended(x,z) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x-1,y,z,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y, z);
			if (GetLightBeamExtended(x-1,z) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x-1,y,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y, z-1);
			if (GetLightBeamExtended(x-1,z-1) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}

		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexCubeAOWest(int x, int y, int z, int high,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;
		
		if (!transparentVoxel(BlockExtended(x-1,y+1,z,true)))
			aOCount++;
		else {
			lightVal = LightExtended (x-1, y+1, z);
			if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x-1,y+1,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y+1, z-1);
			if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x-1,y,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y, z-1);
			if (GetLightBeamExtended(x-1,z-1) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x-1,y,z,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y, z);
			if (GetLightBeamExtended(x-1,z) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}

		if (lightVal == -1){
			lightVal = high;}

		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}

		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexCubeAOEast(int x, int y, int z, int high,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;
		
		if (!transparentVoxel(BlockExtended(x,y+1,z,true)))
			aOCount++;
		else{
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y+1,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y+1, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y, z-1);
			if (GetLightBeamExtended(x,z) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y,z,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y, z);
			if (GetLightBeamExtended(x,z) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}

		if (lightVal == -1){
			lightVal = high;}

		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexCubeAONorth(int x, int y, int z, int high,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;
		
		if (!transparentVoxel(BlockExtended(x-1,y+1,z,true)))
			aOCount++;
		else {
			lightVal = LightExtended (x-1, y+1, z);
			if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y+1,z,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y,z,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y, z);
			if (GetLightBeamExtended(x,z) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x-1,y,z,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y, z);
			if (GetLightBeamExtended(x-1,z) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}

		if (lightVal == -1){
			lightVal = high;}

		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexCubeAOSouth(int x, int y, int z, int high,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;
		
		if (!transparentVoxel(BlockExtended(x-1,y+1,z-1,true)))
			aOCount++;
		else {
			lightVal = LightExtended (x-1, y+1, z-1);
			if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y+1,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y+1, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x,y,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x, y, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (!transparentVoxel(BlockExtended(x-1,y,z-1,true)))
			aOCount++;
		else if (lightVal == -1){
			lightVal = LightExtended (x-1, y, z-1);
			if (GetLightBeamExtended(x-1,z-1) > (y+1+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}

		if (lightVal == -1)
			lightVal = high;

		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexSlopedAOUp(int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x,y+1,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y+1,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y+1,z-1,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y+1,z-1,true)))
			aOCount++;

		if (transparentVoxel(BlockExtended (x , y + 1, z,true))) {
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x, y + 1, z - 1,true))){
			lightVal = LightExtended (x, y+1, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x-1, y + 1, z-1,true))){
				lightVal = LightExtended (x-1, y+1, z-1);
				if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		else if (lightVal == -1){
			if (transparentVoxel(BlockExtended (x - 1, y + 1, z,true))){
				lightVal = LightExtended (x-1, y+1, z);
				if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		
		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexSlopedAOUpOneNE(int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x-1,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y,z-1,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y,z-1,true)))
			aOCount++;

		
		if (transparentVoxel(BlockExtended (x-1 , y, z,true))) {
			lightVal = LightExtended (x-1, y, z);
			if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x-1, y+1, z - 1,true))){
			lightVal = LightExtended (x-1, y+1, z-1);
			if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x, y+1, z-1,true))){
				lightVal = LightExtended (x, y+1, z-1);
				if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		
		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexSlopedAOUpOneNW(int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y,z-1,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y,z-1,true)))
			aOCount++;

		
		if (transparentVoxel(BlockExtended (x , y+1, z,true))) {
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x-1, y+1, z - 1,true))){
			lightVal = LightExtended (x-1, y+1, z-1);
			if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x, y+1, z-1,true))){
				lightVal = LightExtended (x, y+1, z-1);
				if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		
		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexSlopedAOUpOneSE(int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y,z-1,true)))
			aOCount++;

		if (transparentVoxel(BlockExtended (x , y+1, z,true))) {
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x-1, y+1, z,true))){
			lightVal = LightExtended (x-1, y+1, z);
			if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x-1, y+1, z-1,true))){
				lightVal = LightExtended (x-1, y+1, z-1);
				if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}

		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexSlopedAOUpOneSW(int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y,z-1,true)))
			aOCount++;

		if (transparentVoxel(BlockExtended (x , y+1, z,true))) {
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x-1, y+1, z,true))){
			lightVal = LightExtended (x-1, y+1, z);
			if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x, y+1, z-1,true))){
				lightVal = LightExtended (x, y+1, z-1);
				if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		
		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexAOSlopedNorth (int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x-1,y,z-1,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y,z-1,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y+1,z-1,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y+1,z-1,true)))
			aOCount++;

		if (transparentVoxel(BlockExtended (x , y + 1, z,true))) {
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x, y + 1, z - 1,true))){
			lightVal = LightExtended (x, y+1, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x-1, y + 1, z-1,true))){
				lightVal = LightExtended (x-1, y+1, z-1);
				if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		else if (lightVal == -1){
			if (transparentVoxel(BlockExtended (x - 1, y + 1, z,true))){
				lightVal = LightExtended (x-1, y+1, z);
				if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}

		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexAOSlopedEast (int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x-1,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y,z-1,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y+1,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y+1,z-1,true)))
			aOCount++;

		
		if (transparentVoxel(BlockExtended (x , y + 1, z,true))) {
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x, y + 1, z - 1,true))){
			lightVal = LightExtended (x, y+1, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x-1, y + 1, z-1,true))){
				lightVal = LightExtended (x-1, y+1, z-1);
				if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		else if (lightVal == -1){
			if (transparentVoxel(BlockExtended (x - 1, y + 1, z,true))){
				lightVal = LightExtended (x-1, y+1, z);
				if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		
		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexAOSlopedSouth (int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y+1,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x-1,y+1,z,true)))
			aOCount++;

		
		if (transparentVoxel(BlockExtended (x , y + 1, z,true))) {
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x, y + 1, z - 1,true))){
			lightVal = LightExtended (x, y+1, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x-1, y + 1, z-1,true))){
				lightVal = LightExtended (x-1, y+1, z-1);
				if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		else if (lightVal == -1){
			if (transparentVoxel(BlockExtended (x - 1, y + 1, z,true))){
				lightVal = LightExtended (x-1, y+1, z);
				if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		
		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}

		if (aOCount == 4)
			aOCount = 3;
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	void vertexAOSlopedWest (int x, int y, int z,AttatchedMesh m)
	{
		int lightVal = -1;
		bool isBeam = true;

		if (isCube(BlockExtended(x,y,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y,z-1,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y+1,z,true)))
			aOCount++;
		if (isCube(BlockExtended(x,y+1,z-1,true)))
			aOCount++;

		
		if (transparentVoxel(BlockExtended (x , y + 1, z,true))) {
			lightVal = LightExtended (x, y+1, z);
			if (GetLightBeamExtended(x,z) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		else if (transparentVoxel(BlockExtended (x, y + 1, z - 1,true))){
			lightVal = LightExtended (x, y+1, z-1);
			if (GetLightBeamExtended(x,z-1) > (y+2+ChunkY))
				isBeam = false;
			else
				isBeam = true;
		}
		if (lightVal == -1) {
			if (transparentVoxel(BlockExtended (x-1, y + 1, z-1,true))){	
				lightVal = LightExtended (x-1, y+1, z-1);
				if (GetLightBeamExtended(x-1,z-1) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		else if (lightVal == -1){
			if (transparentVoxel(BlockExtended (x - 1, y + 1, z,true))){
				lightVal = LightExtended (x-1, y+1, z);
				if (GetLightBeamExtended(x-1,z) > (y+2+ChunkY))
					isBeam = false;
				else
					isBeam = true;
			}
		}
		
		if (lightVal > 0) {
			if (lightVal > 7)
				lightVal-=reverse[lightVal];
			else
				lightVal+=reverse[lightVal];
		}
		else{
			if (!isBeam)
				lightVal = 15;
		}
		
		m.uvs2.Add (new Vector2(aOCount,lightVal));
		aOCount = 0;
	}

	//makes sure that light points added on the same tile are all close
	//to eachother in value(max difference allowed is 2, there can only ever be one
	//point with a radically different value), if not. This corrects them
	private void verifyLightPoints(AttatchedMesh m){
		int bad = 0, count = m.uvs2.Count;
		//if first point - 2nd point has difference > 2, 2nd point is bad
		if (Math.Abs (m.uvs2 [count - 4].y  - m.uvs2 [count - 3].y ) > 2) {
			bad = 3;
			//if first point - 3rd point has difference > 2, 1st point is bad
			if (Math.Abs (m.uvs2[count-4].y - m.uvs2[count-2].y ) > 2){
				bad = 4;
			}
		}
		//if first point - 3rd point has difference > 2, 3rd point is bad
		else if (Math.Abs (m.uvs2[count-4].y  - m.uvs2[count-2].y ) > 2)
			bad = 2;
		//if first point - 4th has difference > 2, 4th point is bad
		else if (Math.Abs (m.uvs2[count-4].y  - m.uvs2[count-1].y ) > 2)
			bad = 1;

		//if we found a bad point
		if (bad != 0) {
			//create a new Vector value and replace the bad point
			Vector2 newVector = new Vector2();
			newVector.x = m.uvs2[count-bad].x;

			if (bad != 2)
				newVector.y  = m.uvs2[count-2].y ;
			else
				newVector.y  = m.uvs2[count-3].y ;

			m.uvs2[count-bad] = newVector;
		}
	}










	private bool isActive = false;
    /// <summary>
    /// Deletes any Mesh associated with this chunk, and then draws the chunk.
    /// </summary>
    /// <returns></returns>
	public bool Draw(){
		if (!isActive)
        {
            //if air chunk, just return true
            if (regions[regionX, regionZ].GetRealLoadValue(mChunkX, ChunkYChunk, mChunkZ) == RegionData.LoadValues.AirChunk)
            {
                return true;
            }

            //if solid chunk, just check borders
            if (regions[regionX, regionZ].GetRealLoadValue(mChunkX, ChunkYChunk, mChunkZ) == RegionData.LoadValues.SolidChunk)
            {
                drawChunkBorderCheck();
                return true;
            }

            drawChunkInstant();
			return true;
		}
		//return false, failed to draw. ThreadManager will try again later if it receives false
		else
			return false;
	}

    private void drawChunkBorderCheck()
    {
        isActive = true;
        //Temporary list that holds a position for every possible material type
        List<AttatchedMesh>[] textureIndexList = new List<AttatchedMesh>[MOM.texList.Count];
        //current partial mesh being added to
        AttatchedMesh mat;
        //new materials list that will replace the current one
        LinkedList<List<AttatchedMesh>> newList = new LinkedList<List<AttatchedMesh>>();

        //check top & bottom
        for (int x = 0; x < 32; x++)
        {
            for (int z = 0; z < 32; z++)
            {
                //check bottom
                int y = 0;
                //If the desired voxel is not an empty voxel..then draw it
                if (blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType != primitiveBlockType.empty)
                {
                    //Assign material
                    mat = AssignAttatchedMesh(voxelData[x, y, z], data[x, y, z], textureIndexList, newList);
                    drawFunctionsList[(int)(blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType)](x, y, z, mat);
                }

                //check top
                y = 31;
                //If the desired voxel is not an empty voxel..then draw it
                if (blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType != primitiveBlockType.empty)
                {
                    //Assign material
                    mat = AssignAttatchedMesh(voxelData[x, y, z], data[x, y, z], textureIndexList, newList);
                    drawFunctionsList[(int)(blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType)](x, y, z, mat);
                }
            }
        }

        //check North & South
        for (int x = 0; x < 32; x++)
        {
            for (int y = 0; y < 32; y++)
            {
                //check south
                int z = 0;
                //If the desired voxel is not an empty voxel..then draw it
                if (blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType != primitiveBlockType.empty)
                {
                    //Assign material
                    mat = AssignAttatchedMesh(voxelData[x, y, z], data[x, y, z], textureIndexList, newList);
                    drawFunctionsList[(int)(blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType)](x, y, z, mat);
                }

                //check north
                z = 31;
                //If the desired voxel is not an empty voxel..then draw it
                if (blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType != primitiveBlockType.empty)
                {
                    //Assign material
                    mat = AssignAttatchedMesh(voxelData[x, y, z], data[x, y, z], textureIndexList, newList);
                    drawFunctionsList[(int)(blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType)](x, y, z, mat);
                }
            }
        }

        //check North & South
        for (int z = 0; z < 32; z++)
        {
            for (int y = 0; y < 32; y++)
            {
                //check west
                int x = 0;
                //If the desired voxel is not an empty voxel..then draw it
                if (blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType != primitiveBlockType.empty)
                {
                    //Assign material
                    mat = AssignAttatchedMesh(voxelData[x, y, z], data[x, y, z], textureIndexList, newList);
                    drawFunctionsList[(int)(blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType)](x, y, z, mat);
                }

                //check east
                z = 31;
                //If the desired voxel is not an empty voxel..then draw it
                if (blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType != primitiveBlockType.empty)
                {
                    //Assign material
                    mat = AssignAttatchedMesh(voxelData[x, y, z], data[x, y, z], textureIndexList, newList);
                    drawFunctionsList[(int)(blocks[voxelData[x, y, z]].l[data[x, y, z]].blockType)](x, y, z, mat);
                }
            }
        }

        //Remove mesh parts(whatever is left of materials list are materials that no longer exist)
        DestroyMesh();
        //Build mesh
        BuildMesh(newList);
        //reset materials
        materials = newList;

        isActive = false;
    }

	//****This function can only be called once at a time***, draw function enforces it. please use draw
	private void drawChunkInstant()
	{
		isActive = true;
		//Temporary list that holds a position for every possible material type
		List<AttatchedMesh>[] textureIndexList = new List<AttatchedMesh>[MOM.texList.Count];
		//current partial mesh being added to
		AttatchedMesh mat;
		//new materials list that will replace the current one
		LinkedList<List<AttatchedMesh>> newList = new LinkedList<List<AttatchedMesh>>();

		for (int x = 0; x < 32; x++)
		{
			for (int y = 0; y < 32; y++)
			{
				for (int z = 0; z < 32; z++)
				{
					//If the desired voxel is not an empty voxel..then draw it
					if (blocks[voxelData[x,y,z]].l[data[x,y,z]].blockType != primitiveBlockType.empty){
						//Assign material
						mat = AssignAttatchedMesh(voxelData[x,y,z],data[x,y,z],textureIndexList,newList);
						drawFunctionsList[(int)(blocks[voxelData[x,y,z]].l[data[x,y,z]].blockType)](x,y,z,mat);
					}
				}
			}
		}
		//Remove mesh parts(whatever is left of materials list are materials that no longer exist)
		DestroyMesh();
		//Build mesh
		BuildMesh(newList);
		//reset materials
		materials = newList;

		isActive = false;
	}

	//Destroy all materials in the materials array
	private void DestroyMesh(){
		//assign node to the first element
		var node = materials.First;
		//loop through materials
		while (node != null) {
			//loop through each sub list in materials
			for (int i = 0; i < node.Value.Count; i++){
				AttatchedMesh m = node.Value[i];
				//call destroy on the material
				MOM.DestroyRange(m);
				m.OwnedMesh = null;
			}

			var NodeD = node;
			node = node.Next;
			materials.Remove(NodeD);
		}
	}

	//Builds a mesh based on the materials in llist
	private void BuildMesh(LinkedList<List<AttatchedMesh>> llist){

		var node = llist.First;
		Stack<int> incs = new Stack<int> ();

		//loop through llist
		while (node != null) {

			bool removeNode = false;

			//loop through llist node
			for (int i = 0; i < node.Value.Count; i++){

				AttatchedMesh m = node.Value[i];

				//only build a mesh if there is a mesh to build..
				if (m.verts.Count > 0)
					MOM.CreateMeshArrays(m);
				else{
					incs.Push(i);
				}

				//list is empty
				if (node.Value.Count == 0)
					removeNode = true;
				else{
					//remove emptys (AttatchedMesh that was created but has no vertices)
					while (incs.Count > 0){
						node.Value.RemoveAt(incs.Pop());
					}
				}
			}

			if (removeNode){
				var nodeR = node;
				node = node.Next;
				llist.Remove(nodeR);
			}
			else
				node = node.Next;
		}
	}

	//Returns an AttatchedMesh Object based on input blockids, the AttatchedMeshObject is also added to the given lists
	private AttatchedMesh AssignAttatchedMesh(byte blockid1, byte blockid2, List<AttatchedMesh>[] list, LinkedList<List<AttatchedMesh>> llist){

		AttatchedMesh obj;

		if (list [blocks [blockid1].l [blockid2].texArrayPosition] != null) {
			obj = list[blocks[blockid1].l [blockid2].texArrayPosition][list[blocks[blockid1].l [blockid2].texArrayPosition].Count-1];

			//Get length
			int length;
			if (obj.createNewMesh)
				length = 0;
			else
				length = obj.OwnedMesh.nextVertLength;

			//If the block being evaluated needs to be added to a mesh that cant support
			//any more vertices, create a new(identical) material for it
			if (length - obj.verticyLength + obj.verts.Count + MOM.LargestVoxel >= MOM.maxVerticyArraySize){
				//create new AttatchedMesh Object
				AttatchedMesh newMat = new AttatchedMesh(MOM.maxVerticyArraySize, MOM.texList[blocks[blockid1].l[blockid2].texArrayPosition].minBatchSpacePercent);
				newMat.tex = blocks [blockid1].l [blockid2].texArrayPosition;
				newMat.u = blocks[blockid1].l [blockid2].u;
				newMat.b = blocks[blockid1].l [blockid2].textureName;
				newMat.material = blocks[blockid1].l[blockid2].materialPosition;
				newMat.maxSharedChunksPerMesh = MOM.texList[blocks[blockid1].l[blockid2].texArrayPosition].maxSharedChunksPerMesh;
				newMat.cPos = new ChunkVector(ChunkXChunk,ChunkYChunk,ChunkZChunk);

				if (obj.createNewMesh){
					newMat.createNewMesh = true;
				}
				else{
					obj.OwnedMesh.nextVertLength+=obj.verts.Count;
					obj.OwnedMesh.nextTriLength+=obj.tris.Count;
					MOM.DetermineAttatchedMeshPosition(newMat);
				}

				//Add this AttatchedMesh Object to our temporary list
				list[blocks [blockid1].l [blockid2].texArrayPosition].Add (newMat);

				return newMat;
			}
			return obj;
		}
		else {
			//create new list to hold materials of this type
			List<AttatchedMesh> newMatList = new List<AttatchedMesh>();
			//create new AttatchedMesh Object
			AttatchedMesh newMat = new AttatchedMesh(MOM.maxVerticyArraySize, MOM.texList[blocks[blockid1].l[blockid2].texArrayPosition].minBatchSpacePercent);
			newMat.tex = blocks [blockid1].l [blockid2].texArrayPosition;
			newMat.u = blocks[blockid1].l [blockid2].u;
			newMat.b = blocks[blockid1].l [blockid2].textureName;
			newMat.material = blocks[blockid1].l[blockid2].materialPosition;
			newMat.maxSharedChunksPerMesh = MOM.texList[blocks[blockid1].l[blockid2].texArrayPosition].maxSharedChunksPerMesh;
			newMat.cPos = new ChunkVector(ChunkXChunk,ChunkYChunk,ChunkZChunk);
			//Add new object to list
			newMatList.Add(newMat);
			//Addthe list to parent list and linkedlist
			list[blocks [blockid1].l [blockid2].texArrayPosition] = newMatList;
			llist.AddLast(newMatList);
			//Determine some of the mesh characteristics of this material
			MOM.DetermineAttatchedMeshPosition(newMat);
			return newMat;
		}
	}

	//removes any attatched mesh that have null owned mesh
	//will delete the chunk if it has no more materials
	//This function is called from MeshObject in its delete function
	public void NullAllAttatchedMesh(ulong meshID){
		var node = materials.First;
		bool RemovedMaterial = false;

		while (node != null) {

			bool removeNode = false;

			if (node.Value != null){

				//loop through List<AttattchedMesh> and delete any objects with null references
				for (int i = 0; i < node.Value.Count; i++){
					if (node.Value[i].OwnedMesh.GetMeshID() == meshID || node.Value[i].verts.Count == 0){
						node.Value[i].OwnedMesh = null;
						node.Value.RemoveAt(i);
						RemovedMaterial = true;
					}
				}

				//delete list if it is empty
				if (node.Value.Count == 0){
					removeNode = true;
				}
			}
			//delete list if it is non-initialized
			else
				removeNode = true;

			////Material has been removed, redraw chunk
			if (RemovedMaterial)
				IsFullyDrawn = false;

			if (removeNode){
				var nodeD = node;
				node = node.Next;
				materials.Remove(nodeD);
			}
			else
				node = node.Next;
		}

		//if there is no materials, then delete the chunk
		if (materials.Count == 0)
			l.regionData[regionX,regionZ].RemoveChunk(mChunkX,ChunkYChunk,mChunkZ);
			//TM.QueueOnMain(()=>{InitAndLight.Destroy(t);return false;});
	}

    /// <summary>
    /// Returns the number of materials being used on this chunk.
    /// </summary>
    /// <returns></returns>
	public int GetMaterialCount(){
		if (materials == null)
			return 0;
		return materials.Count;
	}

	/// <summary>
    /// Removes this chunk from the voxel data if it is not drawn.
    /// </summary>
	public void NullNonDrawn(){
		if (materials.Count == 0 &&
		    (VerifyMaterials(regionX,regionZ,mChunkX + 1, ChunkYChunk, mChunkZ)) &&
		    (VerifyMaterials(regionX,regionZ,mChunkX - 1, ChunkYChunk, mChunkZ)) &&
		    (VerifyMaterials(regionX,regionZ,mChunkX, ChunkYChunk + 1, mChunkZ)) &&
		    (VerifyMaterials(regionX,regionZ,mChunkX, ChunkYChunk - 1, mChunkZ)) &&
		    (VerifyMaterials(regionX,regionZ,mChunkX, ChunkYChunk, mChunkZ + 1)) &&
		    (VerifyMaterials(regionX,regionZ,mChunkX, ChunkYChunk, mChunkZ - 1))){
			//we may have already nulled out this chunk, but not removed it from deletion list
			if (l.regionData[regionX,regionZ] != null){
				l.regionData[regionX,regionZ].RemoveChunk(mChunkX,ChunkYChunk,mChunkZ);
			}
			//TM.QueueOnMain(()=>{InitAndLight.Destroy(t);return false;});
		}
		else if (materials.Count != 0)
			CheckMaterialsList();
	}

    /// <summary>
    /// check to see if the attatched mesh actually has a mesh, if it doesn't, then clear it
    /// </summary>
    private void CheckMaterialsList(){

		var node = materials.First;

		while (node != null) {

			int nullMeshCount = 0;
			for (int i = 0; i < node.Value.Count; i++){
				if (node.Value[i].OwnedMesh == null)
					nullMeshCount++;
			}

			if (nullMeshCount == node.Value.Count){
				var nodeR = node;
				node = node.Next;
				materials.Remove(nodeR);
			}
			else
				node = node.Next;
		}
	}

    /// <summary>
    /// returns true if an adjacent chunk cannot be drawn or is not drawn
    /// </summary>
    /// <param name="rx"></param>
    /// <param name="rz"></param>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <returns></returns>
    private bool VerifyMaterials(int rx, int rz, int cx, int cy, int cz){

		//modify parameters to the proper array index
		if (cx >= mRegionSize) {
			cx = 0;
			rx++;
		}
		else if (cx < 0) {
			cx = mRegionSize - 1;
			rx--;
		}
		
		else if (cz >= mRegionSize) {
			cz = 0;
			rz++;
		}
		else if (cz < 0) {
			cz = mRegionSize - 1;
			rz--;
		}

		if (cy < 0 || cy >= 8)
			return true;

		if (rx >= l.worldSize || rx < 0 || rz >= l.worldSize || rz < 0)
			return true;

		if (regions [rx, rz] == null)
			return true;

		if (regions [rx, rz].chunks [cx, cy, cz] == null)
			return true;

		if (regions [rx, rz].chunks [cx, cy, cz].GetMaterialCount() == 0)
			return true;

		return false;
	}

    /// <summary>
    /// Clear the stored materials on this chunk.
    /// </summary>
	public void ClearMaterials(){

		var node = materials.First;
		var nodeD = node;
		while (node != null) {

			for (int i = 0; i < node.Value.Count; i++){
				node.Value[i].OwnedMesh = null;
				node.Value.RemoveAt(i);
			}

			nodeD = node;
			node = node.Next;
			materials.Remove(nodeD);
		}
	}

    /// <summary>
    /// Changes The block at the input position. Allows for x,y,z coords to be off by one.
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="blockT"></param>
	public void ChangeBlock(int x, int y, int z, byte blockT)
	{
		
		if( x>31 )
		{
			if (ChunkExists(ChunkXChunk + 1, ChunkYChunk, ChunkZChunk))
			{
				regions [regionX,regionZ].chunks[ChunkXChunk + 1, ChunkYChunk, ChunkZChunk].voxelData[0,y,z] = blockT;
			}

		}
		else if (x < 0)
		{
			if (ChunkExists(ChunkXChunk - 1, ChunkYChunk, ChunkZChunk))
			{
				regions [regionX,regionZ].chunks[ChunkXChunk - 1, ChunkYChunk, ChunkZChunk].voxelData[31,y,z] = blockT;
			}
		}
		else if( y>31 )
		{
			if (ChunkExists(ChunkXChunk, ChunkYChunk + 1, ChunkZChunk))
			{
				regions [regionX,regionZ].chunks[ChunkXChunk , ChunkYChunk + 1, ChunkZChunk].voxelData[x,0,z] = blockT;
			}
		}
		else if (y < 0)
		{
			if (!ChunkExists(ChunkXChunk , ChunkYChunk - 1, ChunkZChunk))
			{
				regions [regionX,regionZ].chunks[ChunkXChunk, ChunkYChunk - 1, ChunkZChunk].voxelData[x,31,z] = blockT;
			}
		}
		else if( z>31)
		{
			if (!ChunkExists(ChunkXChunk , ChunkYChunk, ChunkZChunk + 1))
			{
				regions [regionX,regionZ].chunks[ChunkXChunk, ChunkYChunk, ChunkZChunk + 1].voxelData[x,y,0] = blockT;
			}
		}
		else if (z < 0)
		{
			if (!ChunkExists(ChunkXChunk, ChunkYChunk, ChunkZChunk - 1))
			{
				regions [regionX,regionZ].chunks[ChunkXChunk, ChunkYChunk, ChunkZChunk - 1].voxelData[x,y,31] = blockT;
			}
		}
		else
			voxelData[x,y,z] = blockT;
	}
    /// <summary>
    /// Changes the block at the input position. Allows for the x,y,z coords to be off by 32
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="val"></param>
	public void ChangeBlockExtended(int x, int y, int z, byte val)
	{
		//reset our temp chunks to this chunks position
		int tempChunkX=mChunkX;
		int tempChunkY=ChunkYChunk;
		int tempChunkZ=mChunkZ;
		int tregX=regionX, tregZ=regionZ;
		byte loadval = 0;
		
		if (x >= 32)
		{
			x-=32;
			tempChunkX = mChunkX + 1;
		}
		else if (x < 0)
		{
			x+=32;
			tempChunkX = mChunkX - 1;
		}
		if (y >= 32)
		{
			y-=32;
			tempChunkY = ChunkYChunk + 1;
		}
		else if(y < 0)
		{
			y+=32;
			tempChunkY = ChunkYChunk - 1;
		}
		if (z >= 32)
		{
			z-=32;
			tempChunkZ = mChunkZ + 1;
		}
		else if (z < 0)
		{
			z+=32;
			tempChunkZ = mChunkZ - 1;
		}
		
		loadval = AccessLoadChunkDatPlus (tempChunkX, tempChunkY, tempChunkZ);
		//returns a 1(byte) if chunk that holds the desired block exists
		if (loadval == 1){
			//Change chunk and region coordinates so that they are in bounds
			if (tempChunkX >= mRegionSize){
				tempChunkX = 0;
				tregX++;
			}
			else if (tempChunkX < 0){
				tempChunkX = 7;
				tregX--;
			}
			if (tempChunkZ >= mRegionSize){
				tempChunkZ = 0;
				tregZ++;
			}
			else if (tempChunkZ < 0){
				tempChunkZ = 7;
				tregZ--;
			}
			
			regions[tregX,tregZ].chunks[tempChunkX,tempChunkY,tempChunkZ].voxelData[x,y,z] = val;
		}

	}
	
}
