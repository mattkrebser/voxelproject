﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

/*
TODO
    -Test collisions/raycasts/spherecast/collider casts more thouroughly on world edges
*/

public class PhysicsManager : MonoBehaviour {

    /// <summary>
    /// The collision layer used for characters
    /// </summary>
    public static int CharacterColliderLayer
    {
        get
        {
            return 11;
        }
    }
    /// <summary>
    /// collision layer used for voxels
    /// </summary>
    public static int VoxelColliderLayer
    {
        get
        {
            return 10;
        }
    }
    /// <summary>
    /// collision layer used for non voxel static objects
    /// (like trees)
    /// </summary>
    public static int EnvironmentColliderLayer
    {
        get
        {
            return 12;
        }
    }

    /// <summary>
    /// All of the Colliderscript objects on this world.
    /// </summary>
    Dictionary<int, ColliderScript> active_colliders = 
        new Dictionary<int, ColliderScript>();

    /// <summary>
    /// A list of free box colliders
    /// </summary>
    static Queue<GameObject> free_box_colliders = new Queue<GameObject>();
    static Queue<GameObject> free_ramp_colliders = new Queue<GameObject>();
    static Queue<GameObject> free_ramp_corner_colliders = new Queue<GameObject>();
    static Queue<GameObject> free_ramp_inside_colliders = new Queue<GameObject>();

    /// <summary>
    /// A position of each indivdual collider GameObject in the voxel
    /// coordinate system, and the number of colliderscripts that are using
    /// the collider stored at that location.
    /// </summary>
    Dictionary<Vector3Int32, collider_info> collider_positions = 
        new Dictionary<Vector3Int32, collider_info>();

    /// <summary>
    /// List of colliders that need to be teleported. ho ho ho
    /// </summary>
    Dictionary<int, Tuple<Vector3, ColliderScript>> teleportation_list = 
        new Dictionary<int, Tuple<Vector3, ColliderScript>>();

    /// <summary>
    /// Adds the input collider to physics calculation and returns it
    /// </summary>
    /// <param name="add_this"></param>
    /// <returns></returns>
    public int AddCollider(ColliderScript add_this)
    {
        if (active_colliders.Count > 4900000)
        {
            GameLog.OutputToLog("Error, too many colliders!");
            return 0;
        }

        int new_key = GetKey();
        active_colliders.Add(new_key, add_this);
        return new_key;
    }

    /// <summary>
    /// Remove the collider at key
    /// </summary>
    /// <param name="key"></param>
    public void RemoveCollider(int key, ColliderScript c)
    {
        active_colliders.Remove(key);
        foreach (GameObject g in c.my_colliders.Values)
        {
            g.SetActive(false);
            free_box_colliders.Enqueue(g);
        }
    }
    int GetKey()
    {
        System.Random r = new System.Random();
        int new_key = r.Next(1, int.MaxValue);

        while (active_colliders.ContainsKey(new_key) && new_key != 0)
        {
            new_key = r.Next(1, int.MaxValue);
        }

        return new_key;
    }

    /// <summary>
    /// World to apply physics to
    /// </summary>
    private WorldData world_ref;

    void OnDestroy()
    {
        foreach (GameObject g in free_box_colliders)
        {
            Destroy(g);
        }
        foreach (GameObject g in free_ramp_colliders)
        {
            Destroy(g);
        }
        foreach (GameObject g in free_ramp_corner_colliders)
        {
            Destroy(g);
        }
        foreach (GameObject g in free_ramp_inside_colliders)
        {
            Destroy(g);
        }
        foreach (collider_info c in collider_positions.Values)
        {
            if (c. col != null)
                Destroy(c.col);
        }
        collider_positions.Clear();
        free_box_colliders.Clear();
        free_ramp_colliders.Clear();
        free_ramp_corner_colliders.Clear();
        free_ramp_inside_colliders.Clear();
    }

    void Awake()
    {
        world_ref = GetComponent<WorldData>();
    }

    /// <summary>
    /// Teleport the input colliderscript.
    /// </summary>
    /// <param name="new_position"></param>
    /// <param name="collider_to_tele"></param>
    public void TeleportColliderScript(Vector3 new_position, ColliderScript collider_to_tele)
    {
        if (!teleportation_list.ContainsKey(collider_to_tele.ColliderKey))
        {
            teleportation_list.Add(collider_to_tele.ColliderKey,
                new Tuple<Vector3, ColliderScript>(new_position, collider_to_tele));
        }
        else
        {
            teleportation_list[collider_to_tele.ColliderKey] = 
                new Tuple<Vector3, ColliderScript>(new_position, collider_to_tele);
        }
    }

    /// <summary>
    /// Attempts to teleport the collider. Returns false if no
    /// teleportation occured
    /// </summary>
    /// <param name="c"></param>
    /// <returns></returns>
    private bool handle_teleportation(ColliderScript c)
    {
        if (teleportation_list.Count > 0)
        {
            Tuple<Vector3, ColliderScript> found_this;
            if (teleportation_list.TryGetValue(c.ColliderKey, out found_this))
            {
                if (world_ref.GetBlockAtPosition(found_this.t1.x, found_this.t1.y, found_this.t1.z).Collidable)
                {
                    GameLog.OutputToLog("Failed to teleport, location is occupied.");
                    teleportation_list.Remove(c.ColliderKey);
                    return false;
                }
                found_this.t2.this_RigidBody.transform.position = found_this.t1;
                teleportation_list.Remove(c.ColliderKey);
                return true;
            }
        }
        return false;
    }

    void FixedUpdate()
    {
        foreach (ColliderScript c in active_colliders.Values)
        {
            //handle resizes
            if (c.transform.localScale != c.prevScale)
            {
                c.CalculateColliderExtents();
                c.prevScale = c.transform.localScale;
            }

            //did we teleport?
            bool teleported = !freeze_if_not_initialized(c) && handle_teleportation(c);

            //Get the voxel coordinates of the current and previous positions this collider
            //was found in
            Vector3Int32 curr_voxel = new Vector3Int32((int)c.transform.position.x,
                Mathf.CeilToInt(c.transform.position.y), (int)c.transform.position.z);
            Vector3Int32 prev_voxel = new Vector3Int32((int)c.prev_pos.x,
                Mathf.CeilToInt(c.prev_pos.y), (int)c.prev_pos.z);

            if (!teleported)
                apply_char_movements(c);

            //only need to update voxel physics if the collider has moved into another voxel
            if (curr_voxel != prev_voxel)
            {
                if (!teleported)
                    ResolveMovementCollisions(c, c.transform.position, c.prev_pos);
                RepositionColliders(c);
            }

            c.prev_pos = c.transform.position;
            if (c.movement_vector != Vector3.zero)
                c.this_RigidBody.AddForce(c.movement_vector);
        }
    }

    /// <summary>
    /// freeze the rigidbody if it is in uninitialized voxel space. Returns
    /// true if the rigidbody was frozen.
    /// </summary>
    /// <param name="c"></param>
    private bool freeze_if_not_initialized(ColliderScript c)
    {
        if (!world_ref.coords_in_bounds(transform.position) ||
            world_ref.position_exists(transform.position))
        {
            if (c.this_RigidBody.constraints == RigidbodyConstraints.FreezeAll)
            {
                c.chunk_is_loaded = true;
                c.this_RigidBody.useGravity = true;
                c.this_RigidBody.constraints = RigidbodyConstraints.None;
                c.this_RigidBody.constraints = c.constraints;
                return false;
            }
        }
        //only freeze the position if the chunk the character is in
        //should be initialized and its not
        else
        {
            c.this_RigidBody.constraints = RigidbodyConstraints.FreezeAll;
            return true;
        }
        
        return false;
    }

    /// <summary>
    /// Add character's motion if this rigid body is a character collider
    /// </summary>
    /// <param name="c"></param>
    private void apply_char_movements(ColliderScript c)
    {
        //apply character velocity changes (if this rigid body is a character)
        if (c.CharacterMover != null)
        {
            float extra_gravity = 0.0f;
            Vector3 move_dir = default(Vector3);
            //apply motion vectors for each input movement key
            if (c.CharacterMover.MovingForward)
                move_dir += c.this_RigidBody.rotation * Vector3.forward;
            if (c.CharacterMover.MovingBackward)
                move_dir += c.this_RigidBody.rotation * Vector3.back;
            if (c.CharacterMover.MovingLeft)
                move_dir += c.this_RigidBody.rotation * Vector3.left;
            if (c.CharacterMover.MovingRight)
                move_dir += c.this_RigidBody.rotation * Vector3.right;
            
            //if the character mover is not grounded...
            if (!c.CharacterMover.grounded)
            {
                //Add to the falling time
                if (c.this_RigidBody.velocity.y < 0)
                    c.CharacterMover.total_time_falling += Time.fixedDeltaTime;

                //apply velocity changes
                Vector3 current_v =
                    new Vector3(c.this_RigidBody.velocity.x, 0, c.this_RigidBody.velocity.z);
                //make the in air movement slowly decrease in magnitude over time
                float in_air_time_multiplier = c.CharacterMover.total_time_falling > 3 ?
                    0 : 1 - c.CharacterMover.total_time_falling / 3;
                //Get the additive movement direction vector
                move_dir = ConvertToMagnitude(move_dir, c.CharacterMover.move_speed *
                    c.CharacterMover.air_ctrl * Time.fixedDeltaTime * in_air_time_multiplier);
                //calculate new velocity
                Vector3 new_v = move_dir + current_v;
                //apply new velocity
                c.this_RigidBody.velocity = new Vector3(new_v.x, c.this_RigidBody.velocity.y, new_v.z);

                //Interpolate friction not falling->falling
                if (c.friction > 0.001f)
                {
                    c.friction -= 0.007f;
                    c.friction = Mathf.Abs((float)System.Math.Round((double)c.friction, 2));
                }
                //set grounded if the charmover is within 0.3f of ground
                if (c.CharacterMover.GroundInfo.collider != null)
                {
                    if (Time.realtimeSinceStartup - c.CharacterMover.time_of_last_jump > CharacterMoverScript.Jump_Time_Limit)
                    {
                        c.CharacterMover.grounded = true;
                        return;
                    }
                }

                //apply gravity amplifications (since we are in the air)
                if (c.CharacterMover.gravity_mult < 1.0f)
                {
                    extra_gravity += (1 - c.CharacterMover.gravity_mult) * -9.8f;
                    c.this_RigidBody.velocity = new Vector3(c.this_RigidBody.velocity.x,
                        c.this_RigidBody.velocity.y + extra_gravity * Time.fixedDeltaTime,
                        c.this_RigidBody.velocity.z);
                }
                else if (c.CharacterMover.gravity_mult > 1.0f && !c.CharacterMover.grounded)
                {
                    extra_gravity += (c.CharacterMover.gravity_mult - 1) * -9.8f;
                    c.this_RigidBody.velocity = new Vector3(c.this_RigidBody.velocity.x,
                        c.this_RigidBody.velocity.y + extra_gravity * Time.fixedDeltaTime,
                        c.this_RigidBody.velocity.z);
                }
            }
            else
            {
                RaycastHit ground_info = default(RaycastHit);
                if ((ground_info = c.CharacterMover.GroundInfo).collider == null)
                {
                    c.CharacterMover.grounded = false;
                }

                //reset falling time
                c.CharacterMover.total_time_falling = 0;

                //If we just changed to a non-grounded state
                if (!c.CharacterMover.grounded)
                {
                    //Apply velocity changes
                    move_dir = ConvertToMagnitude(move_dir, c.CharacterMover.move_speed);
                    c.this_RigidBody.velocity = new Vector3(move_dir.x, c.this_RigidBody.velocity.y, move_dir.z);
                }
                //otherwise, 
                else
                {
                    //Get the surface normal
                    Vector3 surface_normal = ground_info.normal;
                    //Project our xz direction vector onto the plane representing the surface below the
                    //character mover
                    Vector3 updated_direction = Vector3.ProjectOnPlane(move_dir, surface_normal).normalized;
                    //Make the direction vector have a magnitude of the character mover's speed
                    move_dir = ConvertToMagnitude(updated_direction, c.CharacterMover.move_speed);
                    c.this_RigidBody.velocity = move_dir;
                }

                //Interpolate friction from faling->not falling friction
                if (c.friction < 0.4f)
                {
                    c.friction += 0.007f;
                    c.friction = (float)System.Math.Round((double)c.friction, 2);
                }
            }
        }
    }

    /// <summary>
    /// Position one of our colliders at the input coordinates
    /// </summary>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    private GameObject position_box_collider_at(int gx, int gy, int gz, Block b_type)
    {
        GameObject use_this_collider = null;
        if (b_type.blockType == primitiveBlockType.cube)
        {
            if (free_box_colliders.Count == 0)
            {
                use_this_collider = Shapes.GetCube();
                use_this_collider.transform.SetParent(world_ref.collider_parent.transform);
                Shapes.voxel_place_block(use_this_collider, gx, gy, gz);
                collider_positions.Add(new Vector3Int32(gx, gy, gz), new collider_info(use_this_collider, 1, 0));
            }
            else
            {
                use_this_collider = free_box_colliders.Dequeue();
                if (!use_this_collider.activeSelf)
                    use_this_collider.SetActive(true);
                Shapes.voxel_place_block(use_this_collider, gx, gy, gz);
                collider_positions.Add(new Vector3Int32(gx, gy, gz), new collider_info(use_this_collider, 1, 0));
            }
        }
        else if (b_type.blockType == primitiveBlockType.slope)
        {
            if (free_ramp_colliders.Count == 0)
            {
                use_this_collider = Shapes.GetRamp();
                use_this_collider.transform.SetParent(world_ref.collider_parent.transform);
                Shapes.voxel_place_ramp(use_this_collider, gx, gy, gz, b_type.Rotation);
                collider_positions.Add(new Vector3Int32(gx, gy, gz), new collider_info(use_this_collider, 1, b_type.Rotation));
            }
            else
            {
                use_this_collider = free_ramp_colliders.Dequeue();
                if (!use_this_collider.activeSelf)
                    use_this_collider.SetActive(true);
                Shapes.voxel_place_ramp(use_this_collider, gx, gy, gz, b_type.Rotation);
                collider_positions.Add(new Vector3Int32(gx, gy, gz), new collider_info(use_this_collider, 1, b_type.Rotation));
            }
        }
        else if (b_type.blockType == primitiveBlockType.cornerSlope)
        {
            if (free_ramp_corner_colliders.Count == 0)
            {
                use_this_collider = Shapes.GetRampCorner();
                use_this_collider.transform.SetParent(world_ref.collider_parent.transform);
                Shapes.voxel_place_corner_ramp(use_this_collider, gx, gy, gz, b_type.Rotation);
                collider_positions.Add(new Vector3Int32(gx, gy, gz), new collider_info(use_this_collider, 1, b_type.Rotation));
            }
            else
            {
                use_this_collider = free_ramp_corner_colliders.Dequeue();
                if (!use_this_collider.activeSelf)
                    use_this_collider.SetActive(true);
                Shapes.voxel_place_corner_ramp(use_this_collider, gx, gy, gz, b_type.Rotation);
                collider_positions.Add(new Vector3Int32(gx, gy, gz), new collider_info(use_this_collider, 1, b_type.Rotation));
            }
        }
        else if (b_type.blockType == primitiveBlockType.sunkenCornerSlope)
        {
            if (free_ramp_inside_colliders.Count == 0)
            {
                use_this_collider = Shapes.GetRampInside();
                use_this_collider.transform.SetParent(world_ref.collider_parent.transform);
                Shapes.voxel_place_inside_ramp(use_this_collider, gx, gy, gz, b_type.Rotation);
                collider_positions.Add(new Vector3Int32(gx, gy, gz), new collider_info(use_this_collider, 1, b_type.Rotation));
            }
            else
            {
                use_this_collider = free_ramp_inside_colliders.Dequeue();
                if (!use_this_collider.activeSelf)
                    use_this_collider.SetActive(true);
                Shapes.voxel_place_inside_ramp(use_this_collider, gx, gy, gz, b_type.Rotation);
                collider_positions.Add(new Vector3Int32(gx, gy, gz), new collider_info(use_this_collider, 1, b_type.Rotation));
            }
        }
        else
        {
            Debug.Log("nope");
        }
        
        return use_this_collider;
    }

    /// <summary>
    /// adds the input object to a free list
    /// </summary>
    /// <param name="g"></param>
    private void add_col_free_list(GameObject g)
    {
        if (g.name == Shapes.BoxCollider)
        {
            free_box_colliders.Enqueue(g);
        }
        else if (g.name == Shapes.RampCollider)
        {
            free_ramp_colliders.Enqueue(g);
        }
        else if (g.name == Shapes.RampInsideCollider)
        {
            free_ramp_inside_colliders.Enqueue(g);
        }
        else if (g.name == Shapes.RampCornerCollider)
        {
            free_ramp_corner_colliders.Enqueue(g);
        }
        else
        {
            GameLog.Out("Error, unkown collider");
        }
    }

    /// <summary>
    /// Used for Removing colliders. We only need one because
    /// this is all sequential.
    /// </summary>
    private Stack<Vector3Int32> global_removal_stack = new Stack<Vector3Int32>();
    private Stack<Vector3Int32> local_removal_stack = new Stack<Vector3Int32>();
    /// <summary>
    /// Repositions colliders surrounding the input collider object
    /// </summary>
    /// <param name="c"></param>
    private void RepositionColliders(ColliderScript c)
    {
        int sx = c.VoxelExtent_SWB.x + (int)c.transform.position.x,
            sy = c.VoxelExtent_SWB.y + Mathf.CeilToInt(c.transform.position.y),
            sz = c.VoxelExtent_SWB.z + (int)c.transform.position.z;
        int stopx = c.VoxelExtent_NEU.x + (int)c.transform.position.x, 
            stopy = c.VoxelExtent_NEU.y + Mathf.CeilToInt(c.transform.position.y),
            stopz = c.VoxelExtent_NEU.z + (int)c.transform.position.z;

        //sometime the collider parent can move if we use excesive force
        //and drag colliders inside of other colliders
        if (world_ref.collider_parent.transform.position != Vector3.zero)
            world_ref.collider_parent.transform.position = Vector3.zero;

        //check on old colliders, and remove/modify them when necessary
        foreach (KeyValuePair<Vector3Int32, GameObject> my_col in c.my_colliders)
        {
            Block col_block = world_ref.GetBlockAtPosition(my_col.Key.x, my_col.Key.y, my_col.Key.z);

            collider_info found_col = default(collider_info);
            if (!collider_positions.TryGetValue(my_col.Key, out found_col))
            {
                GameLog.Out("Collider should exist");
                local_removal_stack.Push(my_col.Key);
                continue;
            }

            //The terrain is dynamic, se we have to check if a collider
            //position is still valid
            if (!col_block.Collidable)
            {
                //remove the collider from the global list and sleep it
                global_removal_stack.Push(my_col.Key);
                local_removal_stack.Push(my_col.Key);
                my_col.Value.SetActive(false);
                add_col_free_list(my_col.Value);
            }
            else if (found_col.col.name != Shapes.block_type_to_collider_name(col_block.blockType))
            {
                Debug.Log("block type changed");
            }
            //check if this collider is out of our colliderscript bounds
            else if (my_col.Key.x < sx || my_col.Key.x >= stopx ||
                my_col.Key.y < sy || my_col.Key.y >= stopy ||
                my_col.Key.z < sz || my_col.Key.z >= stopz)
            {
                //if the use_count > 1, then multiple colliderscripts
                //are using this collider
                if (found_col.use_count > 1)
                {
                    found_col.use_count--;
                    collider_positions[my_col.Key] = found_col;
                    local_removal_stack.Push(my_col.Key);
                }
                //this colliderscript is the only user
                else
                {
                    //remove the collider from the global list and sleep it
                    global_removal_stack.Push(my_col.Key);
                    local_removal_stack.Push(my_col.Key);
                    my_col.Value.SetActive(false);
                    add_col_free_list(my_col.Value);
                }
            }
        }

        //remove anything we dont need (can't do this in the foreach loop)
        while (global_removal_stack.Count > 0)
        {
            collider_positions.Remove(global_removal_stack.Pop());
        }
        while (local_removal_stack.Count > 0)
        {
            c.my_colliders.Remove(local_removal_stack.Pop());
        }

        //check close positions and add colliders where appropriate
        for (int ix = sx; ix <= stopx; ix++)
        {
            for (int iy = sy; iy <= stopy; iy++)
            {
                for (int iz = sz; iz <= stopz; iz++)
                {
                    //only add a collider if the block at this position is collidable
                    //and not 'buried' (surrounded by collidable voxels)
                    Block b_ref;
                    if ((b_ref = world_ref.GetBlockAtPosition(ix, iy, iz)).Collidable &&
                        !world_ref.position_is_buried(ix, iy, iz))
                    {
                        //Determine if a collider already exists at this position
                        collider_info collider_i;
                        if (!collider_positions.TryGetValue(new Vector3Int32(ix, iy, iz), out collider_i))
                        {
                            //if we don't already have a collider here
                            if (!c.my_colliders.ContainsKey(new Vector3Int32(ix, iy, iz)))
                                c.my_colliders.Add(new Vector3Int32(ix, iy, iz),
                                    position_box_collider_at(ix, iy, iz, b_ref));
                        }
                        //there is already a collider placed, here. Just increment the count.
                        else
                        {
                            if (!c.my_colliders.ContainsKey(new Vector3Int32(ix, iy, iz)))
                            {
                                collider_i.use_count++;
                                collider_positions[new Vector3Int32(ix, iy, iz)] = collider_i;
                            }
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Repositions a collider object to where it should be if it moves to quickly
    /// </summary>
    /// <param name="c"></param>
    /// <param name="curr_pos"></param>
    /// <param name="old_pos"></param>
    private void ResolveMovementCollisions(ColliderScript c, Vector3 curr_pos, Vector3 old_pos)
    {
        int x_dist = Mathf.Abs((int)(old_pos.x - curr_pos.x));
        int y_dist = Mathf.Abs((int)(old_pos.y - curr_pos.y));
        int z_dist = Mathf.Abs((int)(old_pos.z - curr_pos.z));
        //if we moved fast (more than 2 voxels in a single update)
        if ((x_dist >= 2 || y_dist >= 2 || z_dist >= 2) ||
            point_is_colliding(curr_pos, c.colliding_with))
        {
            int max_dist = Mathf.Max(Mathf.Max(x_dist, y_dist), z_dist) * 2;
            Vector3 new_pos;
            if (correct_movement(old_pos, curr_pos, max_dist, c, out new_pos))
            {
                c.this_RigidBody.transform.position = new_pos;
                c.this_RigidBody.constraints = RigidbodyConstraints.FreezeAll;
            }
        }
    }

    /// <summary>
    /// Returns true if the input position is colliding with voxel terrain.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="b_here"></param>
    /// <returns></returns>
    public static bool point_is_colliding(Vector3 position, WorldData world)
    {
        Block b_here = world.GetBlockAtPosition(position.x, position.y, position.z);

        if (b_here.Collidable)
        {
            if (b_here.blockType == primitiveBlockType.cube)
                return true;
            else if (b_here.blockType == primitiveBlockType.slope)
            {
                float px = position.x - (int)position.x;
                float py = position.y - (int)position.y == 0.0f ? 1.0f : position.y - (int)position.y;
                float pz = position.z - (int)position.z;

                if (b_here.Rotation == 1)
                {
                    if (py > pz)
                        return false;
                    return true;
                }
                else if (b_here.Rotation == 2)
                {
                    if (py > px)
                        return false;
                    return true;
                }
                else if (b_here.Rotation == 3)
                {
                    if (py + pz > 1)
                        return false;
                    return true;
                }
                else if (b_here.Rotation == 4)
                {
                    if (py + px > 1)
                        return false;
                    return true;
                }
                else
                {
                    GameLog.Out("Cannot handle rotation " + b_here.Rotation.ToString());
                    return false;
                }
            }
            else if (b_here.blockType == primitiveBlockType.cornerSlope)
            {
                Vector3Int32 pos = new Vector3Int32((int)position.x, Mathf.CeilToInt(position.y),
                    (int)position.z);
                if (b_here.Rotation == 1)
                {
                    pos.x += 1;
                    pos.z += 1;
                    Plane p = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 2)
                {
                    pos.z += 1;
                    Plane p = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 3)
                {
                    pos.x += 1;
                    Plane p = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 4)
                {
                    Plane p = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else
                {
                    GameLog.Out("Unsupported rotation " + b_here.Rotation + ", " + b_here.itemName);
                }
            }
            else if (b_here.blockType == primitiveBlockType.sunkenCornerSlope)
            {
                Vector3Int32 pos = new Vector3Int32((int)position.x, Mathf.CeilToInt(position.y),
                    (int)position.z);
                pos.y += -1;
                if (b_here.Rotation == 1)
                {
                    Plane p = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 2)
                {
                    pos.x += 1;
                    Plane p = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 3)
                {
                    pos.z += 1;
                    Plane p = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 4)
                {
                    pos.x += 1;
                    pos.z += 1;
                    Plane p = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else
                {
                    GameLog.Out("Unsupported rotation " + b_here.Rotation + ", " + b_here.itemName);
                }
            }
            else
            {
                GameLog.Out("Cannot handle " + b_here.blockType.ToString() + " block types");
                return false;
            }
        }
        return false;
    }

    /// <summary>
    /// Returns true if the input position is colliding with voxel terrain.
    /// </summary>
    /// <param name="position"></param>
    /// <param name="b_here"></param>
    /// <returns></returns>
    public static bool point_is_colliding(Vector3 position, WorldData world, out Block collided_with)
    {
        Block b_here = world.GetBlockAtPosition(position.x, position.y, position.z);
        collided_with = b_here;

        if (b_here.Collidable)
        {
            if (b_here.blockType == primitiveBlockType.cube)
                return true;
            else if (b_here.blockType == primitiveBlockType.slope)
            {
                float px = position.x - (int)position.x;
                float py = position.y - (int)position.y == 0.0f ? 1.0f : position.y - (int)position.y;
                float pz = position.z - (int)position.z;

                if (b_here.Rotation == 1)
                {
                    if (py > pz)
                        return false;
                    return true;
                }
                else if (b_here.Rotation == 2)
                {
                    if (py > px)
                        return false;
                    return true;
                }
                else if (b_here.Rotation == 3)
                {
                    if (py + pz > 1)
                        return false;
                    return true;
                }
                else if (b_here.Rotation == 4)
                {
                    if (py + px > 1)
                        return false;
                    return true;
                }
                else
                {
                    GameLog.Out("Cannot handle rotation " + b_here.Rotation.ToString());
                    return false;
                }
            }
            else if (b_here.blockType == primitiveBlockType.cornerSlope)
            {
                Vector3Int32 pos = new Vector3Int32((int)position.x, Mathf.CeilToInt(position.y),
                    (int)position.z);
                if (b_here.Rotation == 1)
                {
                    pos.x += 1;
                    pos.z += 1;
                    Plane p = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 2)
                {
                    pos.z += 1;
                    Plane p = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 3)
                {
                    pos.x += 1;
                    Plane p = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 4)
                {
                    Plane p = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else
                {
                    GameLog.Out("Unsupported rotation " + b_here.Rotation + ", " + b_here.itemName);
                }
            }
            else if (b_here.blockType == primitiveBlockType.sunkenCornerSlope)
            {
                Vector3Int32 pos = new Vector3Int32((int)position.x, Mathf.CeilToInt(position.y),
                    (int)position.z);
                pos.y += -1;
                if (b_here.Rotation == 1)
                {
                    Plane p = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 2)
                {
                    pos.x += 1;
                    Plane p = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 3)
                {
                    pos.z += 1;
                    Plane p = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else if (b_here.Rotation == 4)
                {
                    pos.x += 1;
                    pos.z += 1;
                    Plane p = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                        pos.ToVector3());
                    return !p.GetSide(position);
                }
                else
                {
                    GameLog.Out("Unsupported rotation " + b_here.Rotation + ", " + b_here.itemName);
                }
            }
            else
            {
                GameLog.Out("Cannot handle " + b_here.blockType.ToString() + " block types");
                return false;
            }
        }
        return false;
    }

    /// <summary>
    /// Tests for and Resolves collisions for the input Collider (the collider is assumed to have moved).
    /// Can only resolve collisions that are less than 1 block deep.
    /// </summary>
    /// <param name="c"></param>
    /// <param name="new_position"></param>
    /// <returns></returns>
    static bool resolve_collisions(Vector3 curr_pos, Vector3 prev_pos, Vector3 dir,
        ColliderScript col, List<pairx4> vertices, out Vector3 col_pos)
    {
        //special case:
        //  -triangles that fit inside the voxel: get furthest vertice from prev_position, move transform
        //   accordingly
        col_pos = curr_pos;
        bool ret_val = false;

        //position that gets modified
        Vector3 t_pos = curr_pos;

        //**Note please ensure Triangle normals are already normalized
        //**Please ensure the 'dir' vector is also normalized

        //every three vertices returned is a triangle
        foreach (pairx4 pair in vertices)
        {
            pairx4 v = pair;

            //used for iterating through the voxel data this triangle intersects
            Vector3Int32 vmin;
            Vector3Int32 vmax;

            //transform vertice position to current position
            v.v1 += t_pos - col.transform.position;
            v.v2 += t_pos - col.transform.position;
            v.v3 += t_pos - col.transform.position;
            //transform rotation of the plane normal
            v.n = col.transform.rotation * v.n;

            //assign iteration coords
            vmin.x = (int)Mathf.Min(v.v1.x, Mathf.Min(v.v2.x, v.v3.x));
            vmin.y = Mathf.CeilToInt(Mathf.Min(v.v1.y, Mathf.Min(v.v2.y, v.v3.y)));
            vmin.z = (int)Mathf.Min(v.v1.z, Mathf.Min(v.v2.z, v.v3.z));
            vmax.x = (int)Mathf.Max(v.v1.x, Mathf.Max(v.v2.x, v.v3.x));
            vmax.y = Mathf.CeilToInt(Mathf.Max(v.v1.y, Mathf.Max(v.v2.y, v.v3.y)));
            vmax.z = (int)Mathf.Max(v.v1.z, Mathf.Max(v.v2.z, v.v3.z));

            //VERTICE IS COLLIDING TEST
            //we have to do this because a triangle could be completely
            //encapsulated by a voxel, therefore no faces would collided with eachother
            //also, this is a fast + accurate collision test
            Block block_at_vert;
            if (point_is_colliding(v.v1, col.colliding_with, out block_at_vert))
            {
                //get a position where the vertice is not colliding with the voxel
                //add a small amount to the tpos, so it is not intersecting (colliding) with the voxel face
                t_pos = GetVerticeCollisionResolution(v.v1, t_pos, dir, block_at_vert) + (-0.01f * dir);
                //assign the collision position to the value we will output
                col_pos = t_pos;
                //indicate a collision has occured
                ret_val = true;

                //update all vertices on this triangle
                v = pair;
                v.v1 += t_pos - col.transform.position;
                v.v2 += t_pos - col.transform.position;
                v.v3 += t_pos - col.transform.position;
            }
            if (point_is_colliding(v.v2, col.colliding_with, out block_at_vert))
            {
                t_pos = GetVerticeCollisionResolution(v.v2, t_pos, dir, block_at_vert) + (-0.01f * dir);
                col_pos = t_pos;
                ret_val = true;
                v = pair;
                v.v1 += t_pos - col.transform.position;
                v.v2 += t_pos - col.transform.position;
                v.v3 += t_pos - col.transform.position;
            }
            if (point_is_colliding(v.v3, col.colliding_with, out block_at_vert))
            {
                t_pos = GetVerticeCollisionResolution(v.v3, t_pos, dir, block_at_vert) + (-0.01f * dir);
                col_pos = t_pos;
                ret_val = true;
                v = pair;
                v.v1 += t_pos - col.transform.position;
                v.v2 += t_pos - col.transform.position;
                v.v3 += t_pos - col.transform.position;
            }

            if (ret_val)
            {
                goto collided;
            }

            //iterate through whatever blocks this triangle might intersect
            for (int ix = vmin.x; ix <= vmax.x; ix++)
            {
                for (int iy = vmin.y; iy <= vmax.y; iy++)
                {
                    for (int iz = vmin.z; iz <= vmax.z; iz++)
                    {
                        Block curr_block;
                        if ((curr_block = col.colliding_with.GetBlockAtPosition(ix, iy, iz)).Collidable)
                        {
                            //Detect if a triangle face is intersect a voxel
                            //We currently aren't calculating exact collision positions of triangle faces
                            //and voxels, instead we are just approximating by gradually moving the object closer
                            //to a collision and checking if it is colliding
                            if (TriVoxelIntersection(v, curr_block, new Vector3Int32(ix, iy, iz)))
                            {
                                //collision was detected, break so the solver can solve it
                                ret_val = true;
                                goto collided;
                            }
                        }
                    }
                }
            }
        }

        collided:

        //if a collision was detected
        if (ret_val)
            collision_solver(t_pos, prev_pos, dir, col, out col_pos);

#if UNITY_EDITOR
        if (GameLog.VISUALIZE_VOXEL_CASTS)
            //this is good for seeing everything that was checked
            foreach (pairx4 pair in col.ColliderVertices)
            {
                pairx4 v = pair;

                //transform vertice position to current position
                v.v1 += col_pos - col.transform.position;
                v.v2 += col_pos - col.transform.position;
                v.v3 += col_pos - col.transform.position;
                //transform rotation of the plane normal
                v.n = col.transform.rotation * v.n;

                Debug.DrawLine(v.v1, v.v2, Color.white, 30.0f);
                Debug.DrawLine(v.v1, v.v3, Color.white, 30.0f);
                Debug.DrawLine(v.v3, v.v2, Color.white, 30.0f);
            }
#endif

        return ret_val;
    }

    /// <summary>
    /// Iterativly test and solve a collision.
    /// </summary>
    /// <param name="curr_pos"></param>
    /// <param name="dir"></param>
    /// <param name="col"></param>
    /// <param name="tried_distance"></param>
    /// <param name="prev_resultout"></param>
    /// <param name="col_pos"></param>
    static void collision_solver(Vector3 curr_pos, Vector3 prev_pos, Vector3 dir,
        ColliderScript col, out Vector3 col_pos)
    {
        Vector3 t_pos = curr_pos;
        col_pos = prev_pos;
        //result of collision test
        bool ret_val = true;

        dir = -dir;

        //**Note please ensure Triangle normals are already normalized
        //**Please ensure the 'dir' vector is also normalized

        //the amount of distance from the current position to test for collisions
        float test_dist = 1.0f;

        //Usually ends at 4 iterations unless the object is already within 0.1f of the voxel
        for (int i = 0; i < 6; i++)
        {
            //If we did not collide last pass.
            if (!ret_val)
            {
                col_pos = t_pos;
                t_pos -= dir * test_dist;
                test_dist = test_dist / 2;
            }

            //Using collision accuracy of 0.1f
            if (Mathf.Abs(test_dist) < 0.1f)
            {
                return;
            }

            //increment position by the test distance variable along the move direction
            t_pos += dir * test_dist;

            //reset collision state
            ret_val = false;

            //every three vertices returned is a triangle
            foreach (pairx4 pair in col.ColliderVertices)
            {
                pairx4 v = pair;

                //used for iterating through the voxel data this triangle intersects
                Vector3Int32 vmin;
                Vector3Int32 vmax;

                //transform vertice position to current position
                v.v1 += t_pos - col.transform.position;
                v.v2 += t_pos - col.transform.position;
                v.v3 += t_pos - col.transform.position;
                //transform rotation of the plane normal
                v.n = col.transform.rotation * v.n;

                //assign iteration coords
                vmin.x = (int)Mathf.Min(v.v1.x, Mathf.Min(v.v2.x, v.v3.x));
                vmin.y = Mathf.CeilToInt(Mathf.Min(v.v1.y, Mathf.Min(v.v2.y, v.v3.y)));
                vmin.z = (int)Mathf.Min(v.v1.z, Mathf.Min(v.v2.z, v.v3.z));
                vmax.x = (int)Mathf.Max(v.v1.x, Mathf.Max(v.v2.x, v.v3.x));
                vmax.y = Mathf.CeilToInt(Mathf.Max(v.v1.y, Mathf.Max(v.v2.y, v.v3.y)));
                vmax.z = (int)Mathf.Max(v.v1.z, Mathf.Max(v.v2.z, v.v3.z));

                //VERTICE IS COLLIDING TEST
                //we have to do this because a triangle could be completely
                //encapsulated by a voxel, therefore no faces would collided with eachother
                //also, this is a fast + accurate collision test
                Block block_at_vert;
                if (point_is_colliding(v.v1, col.colliding_with, out block_at_vert))
                {
                    //get a position where the vertice is not colliding with the voxel
                    //add a small amount to the tpos, so it is not intersecting (colliding) with the voxel face
                    t_pos = GetVerticeCollisionResolution(v.v1, t_pos, dir, block_at_vert) + (-0.01f * dir);
                    //indicate a collision has occured
                    ret_val = true;

                    //update all vertices on this triangle
                    v = pair;
                    v.v1 += t_pos - col.transform.position;
                    v.v2 += t_pos - col.transform.position;
                    v.v3 += t_pos - col.transform.position;
                }
                if (point_is_colliding(v.v2, col.colliding_with, out block_at_vert))
                {
                    t_pos = GetVerticeCollisionResolution(v.v2, t_pos, dir, block_at_vert) + (-0.01f * dir);
                    ret_val = true;
                    v = pair;
                    v.v1 += t_pos - col.transform.position;
                    v.v2 += t_pos - col.transform.position;
                    v.v3 += t_pos - col.transform.position;
                }
                if (point_is_colliding(v.v3, col.colliding_with, out block_at_vert))
                {
                    t_pos = GetVerticeCollisionResolution(v.v3, t_pos, dir, block_at_vert) + (-0.01f * dir);
                    ret_val = true;
                    v = pair;
                    v.v1 += t_pos - col.transform.position;
                    v.v2 += t_pos - col.transform.position;
                    v.v3 += t_pos - col.transform.position;
                }

                if (ret_val)
                {
                    //assign iteration coords, the position of the object has changed,
                    //so the iteration coordinates need to be chanegd aswell
                    goto collided;
                }

                //iterate through whatever blocks this triangle might intersect
                for (int ix = vmin.x; ix <= vmax.x; ix++)
                {
                    for (int iy = vmin.y; iy <= vmax.y; iy++)
                    {
                        for (int iz = vmin.z; iz <= vmax.z; iz++)
                        {
                            Block curr_block;
                            if ((curr_block = col.colliding_with.GetBlockAtPosition(ix, iy, iz)).Collidable)
                            {
                                if (TriVoxelIntersection(v, curr_block, new Vector3Int32(ix, iy, iz)))
                                {
                                    ret_val = true;
                                    goto collided;
                                }
                            }
                        }
                    }
                }
            }

            collided:
            ;
        }
    }

    /// <summary>
    /// Only use for fastcollision function***, Corrects movement, if necessary.
    /// </summary>
    /// <param name="c"></param>
    /// <param name="col"></param>
    /// <param name="curr_pos"></param>
    /// <param name="old_pos"></param>
    /// <param name="dist"></param>
    /// <returns></returns>
    private bool correct_movement(Vector3 curr_pos_f, Vector3 old_pos_f, int block_search_dist,
        ColliderScript c, out Vector3 col_position)
    {
        col_position = curr_pos_f;
        Vector3 dir_vec = (c.transform.position - c.prev_pos).normalized;

        //apply corrections if standing perfectly on a coordinate
        if (dir_vec.z < 0 && (int)curr_pos_f.z == curr_pos_f.z)
            curr_pos_f.z -= 0.01f;
        else if (dir_vec.z > 0 && (int)curr_pos_f.z == curr_pos_f.z)
            curr_pos_f.z += 0.01f;
        if (dir_vec.x < 0 && (int)curr_pos_f.x == curr_pos_f.x)
            curr_pos_f.x -= 0.01f;
        else if (dir_vec.x > 0 && (int)curr_pos_f.x == curr_pos_f.x)
            curr_pos_f.x += 0.01f;
        if (dir_vec.y < 0 && (int)curr_pos_f.y == curr_pos_f.y)
            curr_pos_f.y -= 0.01f;
        else if (dir_vec.y > 0 && (int)curr_pos_f.y == curr_pos_f.y)
            curr_pos_f.y += 0.01f;

        Vector3Int32 curr_pos = new Vector3Int32((int)curr_pos_f.x, Mathf.CeilToInt(curr_pos_f.y), (int)curr_pos_f.z);
        Vector3Int32 old_pos = new Vector3Int32((int)old_pos_f.x, Mathf.CeilToInt(old_pos_f.y), (int)old_pos_f.z);

        //Direction to increment x,y,z when stepping.
        int stepX = dir_sign(dir_vec.x);
        int stepY = dir_sign(dir_vec.y);
        int stepZ = dir_sign(dir_vec.z);
        //tMaxX is the distance along the ray until our x-axis voxel position changes
        float tMaxX = intbound(curr_pos_f.x, dir_vec.x);
        float tMaxY = intbound(curr_pos_f.y, dir_vec.y);
        float tMaxZ = intbound(curr_pos_f.z, dir_vec.z);
        //The change in t when taking a step
        float tDeltaX = stepX / dir_vec.x;
        float tDeltaY = stepY / dir_vec.y;
        float tDeltaZ = stepZ / dir_vec.z;

        //no direction vector, so just return
        if (dir_vec.x == 0 && dir_vec.y == 0 && dir_vec.z == 0)
            return false;

        //world max coordinates
        int wx = c.colliding_with.regionSize * 32, wz = c.colliding_with.regionSize * 32;
        int wy = 256;

        int checked_sum = 0;

        //used for getting the next location of the collider
        Vector3 intersection = curr_pos_f;
        Vector3 prev_intersection = intersection;

        //List of vertices for this collider
        List<pairx4> verts = c.ColliderVertices;

        //A Vector that represents which face was hit.
        //Eg 0,1,0 is the top face (y+ axis). 0,0,-1 is the face at z- axis
        //1,0,0 is the face in the x+ direction from the center of the cube
        Vector3Int32 face = default(Vector3Int32);

        float sqrd_dist = (c.transform.position - c.prev_pos).sqrMagnitude;

        while ((stepX > 0 ? curr_pos.x < wx : curr_pos.x >= 0) && (stepY > 0 ? curr_pos.y < wy : curr_pos.y >= 0) &&
               (stepZ > 0 ? curr_pos.z < wz : curr_pos.z >= 0))
        {

            if (checked_sum > block_search_dist)
                return false;
            if (curr_pos == old_pos)
                return false;

            if (face.x < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.left,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.z < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.back,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.x > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.right,
                    new Vector3(curr_pos.x + 1, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.z > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.forward,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z + 1)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.y < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.down,
                    new Vector3(curr_pos.x, curr_pos.y - 1, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.y > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.up,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (checked_sum > 0)
            {
                GameLog.Out("Error checking voxel collider.");
                return true;
            }

            if (((intersection - c.prev_pos).sqrMagnitude - 0.25f) > sqrd_dist)
                return false;

            Vector3 new_pos;
            if (resolve_collisions(intersection, prev_intersection, dir_vec, c, verts, out new_pos))
            {
                col_position = new_pos;
                return true;
            }

            prev_intersection = intersection;

            if (tMaxX < tMaxY)
            {
                if (tMaxX < tMaxZ)
                {
                    if (!c.colliding_with.xz_coord_in_bounds(tMaxX))
                        return false;
                    curr_pos.x += stepX;
                    tMaxX += tDeltaX;
                    checked_sum++;
                    face.x = -stepX;
                    face.y = 0;
                    face.z = 0;
                }
                else
                {
                    if (!c.colliding_with.xz_coord_in_bounds(tMaxZ))
                        return false;
                    curr_pos.z += stepZ;
                    tMaxZ += tDeltaZ;
                    checked_sum++;
                    face.x = 0;
                    face.y = 0;
                    face.z = -stepZ;
                }
            }
            else
            {
                if (tMaxY < tMaxZ)
                {
                    if (!c.colliding_with.xz_coord_in_bounds(tMaxY))
                        return false;
                    curr_pos.y += stepY;
                    tMaxY += tDeltaY;
                    checked_sum++;
                    face.x = 0;
                    face.y = -stepY;
                    face.z = 0;
                }
                else
                {
                    if (!c.colliding_with.xz_coord_in_bounds(tMaxZ))
                        return false;
                    curr_pos.z += stepZ;
                    tMaxZ += tDeltaZ;
                    checked_sum++;
                    face.x = 0;
                    face.y = 0;
                    face.z = -stepZ;
                }
            }

            checked_sum++;
        }
        return false;
    }
    static float intbound(float pos, float dir)
    {
        if (dir < 0)
        {
            return intbound(-pos, -dir);
        }
        else
        {
            pos = mod_this(pos, 1);
            return (1 - pos) / dir;
        }
    }
    static int dir_sign(float x)
    {
        return x > 0 ? 1 : x < 0 ? -1 : 0;
    }
    static float mod_this(float val, int mod)
    {
        return (val % mod + mod) % mod;
    }

    /// <summary>
    /// Returns exact hit positions for cube types only.
    /// </summary>
    /// <param name="collided_with"></param>
    /// <param name="face_collided_with"></param>
    /// <param name="starting_pos"></param>
    /// <returns></returns>
    static Vector3 GetExactPos(Vector3Int32 collided_with, Vector3Int32 face_collided_with, Vector3 starting_pos,
        Vector3 direction)
    {
        Plane plane = default(Plane);
        if (face_collided_with.x > 0)
        {
            collided_with.x += face_collided_with.x;
            plane = new Plane(Vector3.right, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.x < 0)
        {
            plane = new Plane(Vector3.left, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.y < 0)
        {
            collided_with.y += face_collided_with.y;
            plane = new Plane(Vector3.down, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.y > 0)
        {
            plane = new Plane(Vector3.up, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.z > 0)
        {
            collided_with.z += face_collided_with.z;
            plane = new Plane(Vector3.forward, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.z < 0)
        {
            plane = new Plane(Vector3.back, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else
        {
            return starting_pos;
        }

        float dist;
        
        Ray ray = new Ray(starting_pos, direction);
        plane.Raycast(ray, out dist);

        return ray.GetPoint(dist);
    }

    /// <summary>
    /// Draws a plane
    /// </summary>
    /// <param name="position"></param>
    /// <param name="normal"></param>
    public static void DrawPlane(Vector3 position, Vector3 normal, float duration)
    {
        if (normal == Vector3.zero)
        {
            return;
        }

        Vector3 v3;

        if (normal.normalized != Vector3.forward)
            v3 = Vector3.Cross(normal, Vector3.forward).normalized * normal.magnitude;
        else
            v3 = Vector3.Cross(normal, Vector3.up).normalized * normal.magnitude; ;

        var corner0 = position + v3;
        var corner2 = position - v3;
        var q = Quaternion.AngleAxis(90.0f, normal);
        v3 = q * v3;
        var corner1 = position + v3;
        var corner3 = position - v3;

        Debug.DrawLine(corner0, corner2, Color.green, duration);
        Debug.DrawLine(corner1, corner3, Color.green, duration);
        Debug.DrawLine(corner0, corner1, Color.green, duration);
        Debug.DrawLine(corner1, corner2, Color.green, duration);
        Debug.DrawLine(corner2, corner3, Color.green, duration);
        Debug.DrawLine(corner3, corner0, Color.green, duration);
        Debug.DrawRay(position, normal, Color.red, duration);
    }

    /// <summary>
    /// Draws a box. Used for debug purposes.
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="extents"></param>
    /// <param name="color"></param>
    /// <param name="duration"></param>
    public static void DrawBox(Vector3 pos, Vector3 extents, Color color, float duration = 30.0f)
    {
        Debug.DrawLine(pos, new Vector3(pos.x, pos.y, pos.z + extents.z), color, duration);
        Debug.DrawLine(pos, new Vector3(pos.x + extents.x, pos.y, pos.z), color, duration);
        Debug.DrawLine(pos, new Vector3(pos.x, pos.y + extents.y, pos.z), color, duration);
        Vector3 p1 = new Vector3(pos.x, pos.y + extents.y, pos.z);
        Debug.DrawLine(p1, new Vector3(pos.x, pos.y + extents.y, pos.z + extents.z), color, duration);
        Debug.DrawLine(p1, new Vector3(pos.x + extents.x, pos.y + extents.y, pos.z), color, duration);
        Vector3 p2 = pos + extents;
        Debug.DrawLine(p2, new Vector3(p2.x, p2.y, p2.z - extents.z), color, duration);
        Debug.DrawLine(p2, new Vector3(p2.x - extents.x, p2.y, p2.z), color, duration);
        Debug.DrawLine(p2, new Vector3(p2.x, p2.y - extents.y, p2.z), color, duration);
        Vector3 p3 = new Vector3(p2.x, p2.y - extents.y, p2.z);
        Debug.DrawLine(p3, new Vector3(p2.x, p2.y - extents.y, p2.z - extents.z), color, duration);
        Debug.DrawLine(p3, new Vector3(p2.x - extents.x, p2.y - extents.y, p2.z), color, duration);
        Debug.DrawLine(new Vector3(pos.x + extents.x, pos.y, pos.z),
            new Vector3(pos.x + extents.x, pos.y + extents.y, pos.z), color, duration);
        Debug.DrawLine(new Vector3(pos.x, pos.y, pos.z + extents.z),
            new Vector3(pos.x, pos.y + extents.y, pos.z + extents.z), color, duration);
    }

    /// <summary>
    /// Draws a sphere (slow)
    /// </summary>
    /// <param name="position"></param>
    /// <param name="duration"></param>
    public static void DrawSphere(Vector3 position, float radius, Color color, float duration = 30.0f)
    {
        GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(obj.GetComponent<SphereCollider>());
        obj.AddComponent<DestroyAfterSeconds>().StartTimer(duration);
        obj.GetComponent<MeshRenderer>().material = Resources.Load("Materials/StandardShader") as Material;
        obj.GetComponent<MeshRenderer>().material.color = color;
        obj.transform.position = position;
        obj.transform.localScale = new Vector3(radius * 2, radius * 2, radius * 2);
    }

    /// <summary>
    /// Voxel cast in the input world. Returns true if collided
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="curr_pos_f"></param>
    /// <param name="block_search_dist"></param>
    /// <param name="result"></param>
    /// <param name="world_ref"></param>
    /// <returns></returns>
    public static bool voxel_cast(Vector3 direction, Vector3 curr_pos_f, float dist, out collision_info result,
        WorldData world_ref)
    {
        Vector3 dir_vec = direction.normalized;
        result = default(collision_info);

        //apply corrections if standing perfectly on a coordinate
        if (dir_vec.z < 0 && (int)curr_pos_f.z == curr_pos_f.z)
            curr_pos_f.z -= 0.01f;
        else if (dir_vec.z > 0 && (int)curr_pos_f.z == curr_pos_f.z)
            curr_pos_f.z += 0.01f;
        if (dir_vec.x < 0 && (int)curr_pos_f.x == curr_pos_f.x)
            curr_pos_f.x -= 0.01f;
        else if (dir_vec.x > 0 && (int)curr_pos_f.x == curr_pos_f.x)
            curr_pos_f.x += 0.01f;
        if (dir_vec.y < 0 && (int)curr_pos_f.y == curr_pos_f.y)
            curr_pos_f.y -= 0.01f;
        else if (dir_vec.y > 0 && (int)curr_pos_f.y == curr_pos_f.y)
            curr_pos_f.y += 0.01f;

        Vector3Int32 curr_pos = new Vector3Int32((int)curr_pos_f.x, Mathf.CeilToInt(curr_pos_f.y), (int)curr_pos_f.z);
        Vector3Int32 prev_pos = curr_pos;
        //Direction to increment x,y,z when stepping.
        int stepX = dir_sign(dir_vec.x);
        int stepY = dir_sign(dir_vec.y);
        int stepZ = dir_sign(dir_vec.z);
        //tMaxX is the distance along the ray until our x-axis voxel position changes
        float tMaxX = intbound(curr_pos_f.x, dir_vec.x);
        float tMaxY = intbound(curr_pos_f.y, dir_vec.y);
        float tMaxZ = intbound(curr_pos_f.z, dir_vec.z);
        //The change in t when taking a step
        float tDeltaX = stepX / dir_vec.x;
        float tDeltaY = stepY / dir_vec.y;
        float tDeltaZ = stepZ / dir_vec.z;

        //no direction vector, so just return
        if (dir_vec.x == 0 && dir_vec.y == 0 && dir_vec.z == 0)
            return false;

        //world max coordinates
        int wx = world_ref.regionSize * 32, wz = world_ref.regionSize * 32;
        int wy = 256;

        //distance squared, 1.733f is approx. longest distance across a 1x1x1 voxel (from upper corner to lower corner)
        float distsqrd = dist * dist + 1.733f;

        //A Vector that represents which face was hit.
        //Eg 0,1,0 is the top face (y+ axis). 0,0,-1 is the face at z- axis
        //1,0,0 is the face in the x+ direction from the center of the cube
        Vector3Int32 face = default(Vector3Int32);

        while ((stepX > 0 ? curr_pos.x < wx : curr_pos.x >= 0) && (stepY > 0 ? curr_pos.y < wy : curr_pos.y >= 0) &&
               (stepZ > 0 ? curr_pos.z < wz : curr_pos.z >= 0))
        {
            if ((curr_pos.ToVector3() - curr_pos_f).sqrMagnitude > distsqrd)
                return false;

#if UNITY_EDITOR
            if (GameLog.VISUALIZE_VOXEL_CASTS)
                DrawBox(curr_pos.ToVector3(), new Vector3(1, -1, 1), Color.yellow, 50.0f);
#endif

            //check for a collision
            Block curr_block;
            if ((curr_block = world_ref.GetBlockAtPosition(curr_pos.x, curr_pos.y, curr_pos.z)).Collidable)
            {
                if (curr_block.blockType == primitiveBlockType.cube)
                {
                    result = new collision_info(curr_block,
                        GetExactPos(curr_pos, face, curr_pos_f, dir_vec),
                        new Vector3Int32(curr_pos.x, curr_pos.y, curr_pos.z), face, face.ToVector3(), prev_pos);

#if UNITY_EDITOR
                    if (GameLog.VISUALIZE_VOXEL_CASTS)
                        Debug.DrawLine(curr_pos_f, result.exact_hit_position, Color.blue, 50.0f);
#endif
                    //Ensure that the distance the collision occured at is less than the input max distance
                    if (!((result.exact_hit_position - curr_pos_f).sqrMagnitude > dist * dist))
                        return true;
                }
                else
                {
                    Vector3 hit_pos = default(Vector3), hit_normal;
                    if (face == new Vector3Int32(0, 0, 0))
                    {
                        if (NoFaceCollision(curr_pos, new Vector3Int32(0, 0, 0), curr_pos_f, dir_vec, curr_block,
                            out hit_pos, out hit_normal))
                        {
                            result = new collision_info(curr_block, hit_pos,
                                  new Vector3Int32(curr_pos.x, curr_pos.y, curr_pos.z), 
                                  new Vector3Int32(-1, -1, -1), hit_normal, prev_pos);

#if UNITY_EDITOR
                            if (GameLog.VISUALIZE_VOXEL_CASTS)
                                Debug.DrawLine(curr_pos_f, result.exact_hit_position, Color.blue, 50.0f);
#endif
                            //Ensure that the distance the collision occured at is less than the input max distance
                            if (!((result.exact_hit_position - curr_pos_f).sqrMagnitude > dist * dist))
                                return true;
                        }
                    }
                    else if (IregularShapeCollisionHit(curr_pos, face, curr_pos_f, dir_vec,
                        curr_block, out hit_pos, out hit_normal))
                    {
                        result = new collision_info(curr_block,
                            hit_pos,
                            new Vector3Int32(curr_pos.x, curr_pos.y, curr_pos.z), face, hit_normal, prev_pos);

#if UNITY_EDITOR
                        if (GameLog.VISUALIZE_VOXEL_CASTS)
                            Debug.DrawLine(curr_pos_f, result.exact_hit_position, Color.blue, 50.0f);
#endif
                        //Ensure that the distance the collision occured at is less than the input max distance
                        if (!((result.exact_hit_position - curr_pos_f).sqrMagnitude > dist * dist))
                            return true;
                    }
                }
            }

            prev_pos = curr_pos;

            if (tMaxX < tMaxY)
            {
                if (tMaxX < tMaxZ)
                {
                    if (!world_ref.xz_coord_in_bounds(tMaxX))
                        return false;
                    curr_pos.x += stepX;
                    tMaxX += tDeltaX;
                    face.x = -stepX;
                    face.y = 0;
                    face.z = 0;
                }
                else
                {
                    if (!world_ref.xz_coord_in_bounds(tMaxZ))
                        return false;
                    curr_pos.z += stepZ;
                    tMaxZ += tDeltaZ;
                    face.x = 0;
                    face.y = 0;
                    face.z = -stepZ;
                }
            }
            else
            {
                if (tMaxY < tMaxZ)
                {
                    if (!world_ref.xz_coord_in_bounds(tMaxY))
                        return false;
                    curr_pos.y += stepY;
                    tMaxY += tDeltaY;
                    face.x = 0;
                    face.y = -stepY;
                    face.z = 0;
                }
                else
                {
                    if (!world_ref.xz_coord_in_bounds(tMaxZ))
                        return false;
                    curr_pos.z += stepZ;
                    tMaxZ += tDeltaZ;
                    face.x = 0;
                    face.y = 0;
                    face.z = -stepZ;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// Cast a collider against the voxel terrain. Returns true if collided and outputs the position of the collider transform
    /// at the collision location.
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="curr_pos_f"></param>
    /// <param name="block_search_dist"></param>
    /// <param name="result"></param>
    /// <param name="world_ref"></param>
    /// <returns></returns>
    public static bool voxel_cast_collider(Vector3 direction, Vector3 curr_pos_f, float dist,
        ColliderScript c, out Vector3 col_position)
    {
        col_position = curr_pos_f;
        Vector3 dir_vec = direction.normalized;

        //apply corrections if standing perfectly on a coordinate
        if (dir_vec.z < 0 && (int)curr_pos_f.z == curr_pos_f.z)
            curr_pos_f.z -= 0.01f;
        else if (dir_vec.z > 0 && (int)curr_pos_f.z == curr_pos_f.z)
            curr_pos_f.z += 0.01f;
        if (dir_vec.x < 0 && (int)curr_pos_f.x == curr_pos_f.x)
            curr_pos_f.x -= 0.01f;
        else if (dir_vec.x > 0 && (int)curr_pos_f.x == curr_pos_f.x)
            curr_pos_f.x += 0.01f;
        if (dir_vec.y < 0 && (int)curr_pos_f.y == curr_pos_f.y)
            curr_pos_f.y -= 0.01f;
        else if (dir_vec.y > 0 && (int)curr_pos_f.y == curr_pos_f.y)
            curr_pos_f.y += 0.01f;

        Vector3Int32 curr_pos = new Vector3Int32((int)curr_pos_f.x, Mathf.CeilToInt(curr_pos_f.y), (int)curr_pos_f.z);

        //Direction to increment x,y,z when stepping.
        int stepX = dir_sign(dir_vec.x);
        int stepY = dir_sign(dir_vec.y);
        int stepZ = dir_sign(dir_vec.z);
        //tMaxX is the distance along the ray until our x-axis voxel position changes
        float tMaxX = intbound(curr_pos_f.x, dir_vec.x);
        float tMaxY = intbound(curr_pos_f.y, dir_vec.y);
        float tMaxZ = intbound(curr_pos_f.z, dir_vec.z);
        //The change in t when taking a step
        float tDeltaX = stepX / dir_vec.x;
        float tDeltaY = stepY / dir_vec.y;
        float tDeltaZ = stepZ / dir_vec.z;

        //no direction vector, so just return
        if (dir_vec.x == 0 && dir_vec.y == 0 && dir_vec.z == 0)
            return false;

        //world max coordinates
        int wx = c.colliding_with.regionSize * 32, wz = c.colliding_with.regionSize * 32;
        int wy = 256;

        int checked_sum = 0;

        //used for getting the next location of the collider
        Vector3 intersection = curr_pos_f;
        Vector3 prev_intersection = intersection;

        //List of vertices for this collider
        List<pairx4> verts = c.ColliderVertices;

        //A Vector that represents which face was hit.
        //Eg 0,1,0 is the top face (y+ axis). 0,0,-1 is the face at z- axis
        //1,0,0 is the face in the x+ direction from the center of the cube
        Vector3Int32 face = default(Vector3Int32);

        float sqrd_dist = dist * dist;

        while ((stepX > 0 ? curr_pos.x < wx : curr_pos.x >= 0) && (stepY > 0 ? curr_pos.y < wy : curr_pos.y >= 0) &&
               (stepZ > 0 ? curr_pos.z < wz : curr_pos.z >= 0))
        {
            if (face.x < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.left,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.z < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.back,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.x > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.right,
                    new Vector3(curr_pos.x + 1, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.z > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.forward,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z + 1)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.y < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.down,
                    new Vector3(curr_pos.x, curr_pos.y - 1, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.y > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.up,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (checked_sum > 0)
            {
                GameLog.Out("Error checking voxel collider.");
                return true;
            }

            if (((intersection - curr_pos_f).sqrMagnitude -0.25f) > sqrd_dist)
                return false;

            Vector3 new_pos;
            if (resolve_collisions(intersection, prev_intersection, dir_vec, c, verts, out new_pos))
            {
                col_position = new_pos;
                return true;
            }

            prev_intersection = intersection;

            if (tMaxX < tMaxY)
            {
                if (tMaxX < tMaxZ)
                {
                    if (!c.colliding_with.xz_coord_in_bounds(tMaxX))
                        return false;
                    curr_pos.x += stepX;
                    tMaxX += tDeltaX;
                    checked_sum++;
                    face.x = -stepX;
                    face.y = 0;
                    face.z = 0;
                }
                else
                {
                    if (!c.colliding_with.xz_coord_in_bounds(tMaxZ))
                        return false;
                    curr_pos.z += stepZ;
                    tMaxZ += tDeltaZ;
                    checked_sum++;
                    face.x = 0;
                    face.y = 0;
                    face.z = -stepZ;
                }
            }
            else
            {
                if (tMaxY < tMaxZ)
                {
                    if (!c.colliding_with.xz_coord_in_bounds(tMaxY))
                        return false;
                    curr_pos.y += stepY;
                    tMaxY += tDeltaY;
                    checked_sum++;
                    face.x = 0;
                    face.y = -stepY;
                    face.z = 0;
                }
                else
                {
                    if (!c.colliding_with.xz_coord_in_bounds(tMaxZ))
                        return false;
                    curr_pos.z += stepZ;
                    tMaxZ += tDeltaZ;
                    checked_sum++;
                    face.x = 0;
                    face.y = 0;
                    face.z = -stepZ;
                }
            }

            checked_sum++;
        }
        return false;
    }

    /// <summary>
    /// Cast a sphere against the voxel terrain, Returns true if collided and outs the collision position (as defined by the center
    /// of the sphere)
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="curr_pos_f"></param>
    /// <param name="dist"></param>
    /// <param name="radius"></param>
    /// <param name="col_position"></param>
    /// <returns></returns>
    public static bool voxel_cast_sphere(Vector3 direction, Vector3 curr_pos_f, float dist, float radius,
        WorldData world, out Vector3 col_position)
    {

        col_position = curr_pos_f;
        Vector3 dir_vec = direction.normalized;

        //apply corrections if standing perfectly on a coordinate
        if (dir_vec.z < 0 && (int)curr_pos_f.z == curr_pos_f.z)
            curr_pos_f.z -= 0.01f;
        else if (dir_vec.z > 0 && (int)curr_pos_f.z == curr_pos_f.z)
            curr_pos_f.z += 0.01f;
        if (dir_vec.x < 0 && (int)curr_pos_f.x == curr_pos_f.x)
            curr_pos_f.x -= 0.01f;
        else if (dir_vec.x > 0 && (int)curr_pos_f.x == curr_pos_f.x)
            curr_pos_f.x += 0.01f;
        if (dir_vec.y < 0 && (int)curr_pos_f.y == curr_pos_f.y)
            curr_pos_f.y -= 0.01f;
        else if (dir_vec.y > 0 && (int)curr_pos_f.y == curr_pos_f.y)
            curr_pos_f.y += 0.01f;

        Vector3Int32 curr_pos = new Vector3Int32((int)curr_pos_f.x, Mathf.CeilToInt(curr_pos_f.y), (int)curr_pos_f.z);

        //Direction to increment x,y,z when stepping.
        int stepX = dir_sign(dir_vec.x);
        int stepY = dir_sign(dir_vec.y);
        int stepZ = dir_sign(dir_vec.z);
        //tMaxX is the distance along the ray until our x-axis voxel position changes
        float tMaxX = intbound(curr_pos_f.x, dir_vec.x);
        float tMaxY = intbound(curr_pos_f.y, dir_vec.y);
        float tMaxZ = intbound(curr_pos_f.z, dir_vec.z);
        //The change in t when taking a step
        float tDeltaX = stepX / dir_vec.x;
        float tDeltaY = stepY / dir_vec.y;
        float tDeltaZ = stepZ / dir_vec.z;

        //no direction vector, so just return
        if (dir_vec.x == 0 && dir_vec.y == 0 && dir_vec.z == 0)
            return false;

        //world max coordinates
        int wx = world.regionSize * 32, wz = world.regionSize * 32;
        int wy = 256;

        int checked_sum = 0;

        //used for getting the next location of the collider
        Vector3 intersection = curr_pos_f;
        Vector3 prev_intersection = intersection;

        //A Vector that represents which face was hit.
        //Eg 0,1,0 is the top face (y+ axis). 0,0,-1 is the face at z- axis
        //1,0,0 is the face in the x+ direction from the center of the cube
        Vector3Int32 face = default(Vector3Int32);

        float sqrd_dist = dist * dist;

        while ((stepX > 0 ? curr_pos.x < wx : curr_pos.x >= 0) && (stepY > 0 ? curr_pos.y < wy : curr_pos.y >= 0) &&
               (stepZ > 0 ? curr_pos.z < wz : curr_pos.z >= 0))
        {
            if (face.x < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.left,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.z < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.back,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.x > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.right,
                    new Vector3(curr_pos.x + 1, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.z > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.forward,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z + 1)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.y < 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.down,
                    new Vector3(curr_pos.x, curr_pos.y - 1, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (face.y > 0)
            {
                if (!LinePlaneIntersection(out intersection, curr_pos_f, dir_vec, Vector3.up,
                    new Vector3(curr_pos.x, curr_pos.y, curr_pos.z)))
                {
                    GameLog.Out("Error checking voxel collider."); return true;
                }
            }
            else if (checked_sum > 0)
            {
                GameLog.Out("Error checking voxel collider.");
                return true;
            }

            if (((intersection - curr_pos_f).sqrMagnitude - radius * 2) > sqrd_dist)
                return false;

#if UNITY_EDITOR
            if (GameLog.VISUALIZE_VOXEL_CASTS)
                DrawSphere(intersection, radius, Color.white);
#endif

            Vector3 new_pos;
            if (check_sphere_collision(prev_intersection, intersection, dir_vec, radius, world, out new_pos))
            {
                col_position = new_pos;
                //We raycast the input distance + the diameter of the collider, if the collision that was found
                //occurs past the input distance, then there was no collision
                if ((col_position - curr_pos_f).sqrMagnitude > sqrd_dist)
                    return false;

#if UNITY_EDITOR
                if (GameLog.VISUALIZE_VOXEL_CASTS)
                    DrawSphere(col_position, radius, Color.red);
#endif

                return true;
            }

            prev_intersection = intersection;

            if (tMaxX < tMaxY)
            {
                if (tMaxX < tMaxZ)
                {
                    if (!world.xz_coord_in_bounds(tMaxX))
                        return false;
                    curr_pos.x += stepX;
                    tMaxX += tDeltaX;
                    checked_sum++;
                    face.x = -stepX;
                    face.y = 0;
                    face.z = 0;
                }
                else
                {
                    if (!world.xz_coord_in_bounds(tMaxZ))
                        return false;
                    curr_pos.z += stepZ;
                    tMaxZ += tDeltaZ;
                    checked_sum++;
                    face.x = 0;
                    face.y = 0;
                    face.z = -stepZ;
                }
            }
            else
            {
                if (tMaxY < tMaxZ)
                {
                    if (!world.xz_coord_in_bounds(tMaxY))
                        return false;
                    curr_pos.y += stepY;
                    tMaxY += tDeltaY;
                    checked_sum++;
                    face.x = 0;
                    face.y = -stepY;
                    face.z = 0;
                }
                else
                {
                    if (!world.xz_coord_in_bounds(tMaxZ))
                        return false;
                    curr_pos.z += stepZ;
                    tMaxZ += tDeltaZ;
                    checked_sum++;
                    face.x = 0;
                    face.y = 0;
                    face.z = -stepZ;
                }
            }

            checked_sum++;
        }
        return false;
    }

    /// <summary>
    /// Converts the input vector to have the input magnitude
    /// </summary>
    /// <param name="v"></param>
    /// <param name="target_magnitude"></param>
    /// <returns></returns>
    public static Vector3 ConvertToMagnitude(Vector3 v, float target_magnitude)
    {
        return v == Vector3.zero ? v :
            v * Mathf.Sqrt((target_magnitude * target_magnitude) / (v.x * v.x + v.y * v.y + v.z * v.z));
    }

    /// <summary>
    /// Returns true if the input ray is colliding with the block at 'collided_with' position.
    /// </summary>
    /// <param name="collided_with"></param>
    /// <param name="face_collided_with"></param>
    /// <param name="starting_pos"></param>
    /// <param name="direction"></param>
    /// <param name="b_type"></param>
    /// <returns></returns>
    static bool IregularShapeCollisionHit(Vector3Int32 collided_with, Vector3Int32 face_collided_with, Vector3 starting_pos,
        Vector3 direction, Block b, out Vector3 hit_pos, out Vector3 hit_normal)
    {
        //Plane representing the non sloped face of the hit block.
        Plane plane = default(Plane);
        //Plane Representing the secondary face of the block. (the sloped face)
        Plane secondary_plane = default(Plane);
        //used for positioning the secondary_plane
        Vector3Int32 s2_pos = collided_with;
        Vector3Int32 orig_pos = collided_with;

        hit_normal = face_collided_with.ToVector3();

        //This really big if/else tree will create a plane
        //that represents the two faces of the block that we are checking

        //See Block.cs to see rotations

        //get the face of the block we are hitting
        //(even if this is not a cube voxel, just think of the sourrounding cube as
        //a bounds we are collecting)
        if (face_collided_with.x > 0)
        {
            collided_with.x += face_collided_with.x;
            plane = new Plane(Vector3.right, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.x < 0)
        {
            plane = new Plane(Vector3.left, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.y < 0)
        {
            collided_with.y += face_collided_with.y;
            plane = new Plane(Vector3.down, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.y > 0)
        {
            plane = new Plane(Vector3.up, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.z > 0)
        {
            collided_with.z += face_collided_with.z;
            plane = new Plane(Vector3.forward, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else if (face_collided_with.z < 0)
        {
            plane = new Plane(Vector3.back, new Vector3(collided_with.x, collided_with.y, collided_with.z));
        }
        else
        {
            GameLog.Out("Face collision error");
            hit_pos = collided_with.ToVector3();
            return true;
        }

        //make a plane representing the extra face
        if (b.blockType == primitiveBlockType.slope)
        {
            s2_pos.y += -1;
            if (b.Rotation == 1)
            {
                secondary_plane = new Plane(new Vector3(0, 0.7071067811865475f, -0.7071067811865475f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 2)
            {
                secondary_plane = new Plane(new Vector3(-0.7071067811865475f, 0.7071067811865475f, 0),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 3)
            {
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(0, 0.7071067811865475f, 0.7071067811865475f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 4)
            {
                s2_pos.x += 1;
                secondary_plane = new Plane(new Vector3(0.7071067811865475f, 0.7071067811865475f, 0),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
            }
        }
        else if (b.blockType == primitiveBlockType.cornerSlope)
        {
            if (b.Rotation == 1)
            {
                s2_pos.x += 1;
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 2)
            {
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 3)
            {
                s2_pos.x += 1;
                secondary_plane = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 4)
            {
                secondary_plane = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
            }
        }
        else if (b.blockType == primitiveBlockType.sunkenCornerSlope)
        {
            s2_pos.y += -1;
            if (b.Rotation == 1)
            {
                secondary_plane = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 2)
            {
                s2_pos.x += 1;
                secondary_plane = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 3)
            {
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 4)
            {
                s2_pos.x += 1;
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
            }
        }
        else
        {
            GameLog.Out("Error, unsupported block type " + b.blockType.ToString());
        }

        float dist;
        Ray ray = new Ray(starting_pos, direction);
        plane.Raycast(ray, out dist);
        hit_pos = ray.GetPoint(dist);

        //If we collided with a non sloped face, then return true.
        if (tri_secondary_hit(face_collided_with, hit_pos, b))
        {
            hit_pos = GetExactPos(orig_pos, face_collided_with, starting_pos, direction);
            return true;
        }
        else
        {
            dist = 0.0f;
            //raycast onto sloped plane
            if (!secondary_plane.Raycast(ray, out dist))
                return false;
            else
            {
                //get the point and see if it is inside the current voxel bounds
                hit_pos = ray.GetPoint(dist);
                if (hit_pos.x <= orig_pos.x + 1 && hit_pos.x >= orig_pos.x &&
                    hit_pos.y <= orig_pos.y && hit_pos.y >= orig_pos.y - 1 &&
                    hit_pos.z <= orig_pos.z + 1 && hit_pos.z >= orig_pos.z)
                {
                    return true;
                }
                else
                    return false;
            }
        }
    }

    /// <summary>
    /// Resolves collisions that occur in the same block from when they were cast
    /// </summary>
    /// <param name="collided_with"></param>
    /// <param name="face_collided_with"></param>
    /// <param name="starting_pos"></param>
    /// <param name="direction"></param>
    /// <param name="b"></param>
    /// <param name="hit_pos"></param>
    /// <returns></returns>
    static bool NoFaceCollision(Vector3Int32 collided_with, Vector3Int32 face_collided_with, Vector3 starting_pos,
        Vector3 direction, Block b, out Vector3 hit_pos, out Vector3 hit_normal)
    {
        //Plane Representing the secondary face of the block. (the sloped face)
        Plane secondary_plane = default(Plane);
        //used for positioning the secondary_plane
        Vector3Int32 s2_pos = collided_with;
        Vector3Int32 orig_pos = collided_with;

        //make a plane representing the extra face
        if (b.blockType == primitiveBlockType.slope)
        {
            s2_pos.y += -1;
            if (b.Rotation == 1)
            {
                secondary_plane = new Plane(new Vector3(0, 0.7071067811865475f, -0.7071067811865475f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 2)
            {
                secondary_plane = new Plane(new Vector3(-0.7071067811865475f, 0.7071067811865475f, 0),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 3)
            {
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(0, 0.7071067811865475f, 0.7071067811865475f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 4)
            {
                s2_pos.x += 1;
                secondary_plane = new Plane(new Vector3(0.7071067811865475f, 0.7071067811865475f, 0),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
            }
        }
        else if (b.blockType == primitiveBlockType.cornerSlope)
        {
            if (b.Rotation == 1)
            {
                s2_pos.x += 1;
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 2)
            {
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 3)
            {
                s2_pos.x += 1;
                secondary_plane = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 4)
            {
                secondary_plane = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
            }
        }
        else if (b.blockType == primitiveBlockType.sunkenCornerSlope)
        {
            s2_pos.y += -1;
            if (b.Rotation == 1)
            {
                secondary_plane = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 2)
            {
                s2_pos.x += 1;
                secondary_plane = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 3)
            {
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else if (b.Rotation == 4)
            {
                s2_pos.x += 1;
                s2_pos.z += 1;
                secondary_plane = new Plane(new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(s2_pos.x, s2_pos.y, s2_pos.z));
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
            }
        }
        else
        {
            GameLog.Out("Error, unsupported block type " + b.blockType.ToString());
        }

        float dist;
        Ray ray = new Ray(starting_pos, direction);

        hit_normal = secondary_plane.normal;

            dist = 0.0f;
        //raycast onto sloped plane
        if (!secondary_plane.Raycast(ray, out dist))
        {
            hit_pos = default(Vector3);
            return false;
        }
        else
        {
            //get the point and see if it is inside the current voxel bounds
            hit_pos = ray.GetPoint(dist);
            if (hit_pos.x <= orig_pos.x + 1 && hit_pos.x >= orig_pos.x &&
                hit_pos.y <= orig_pos.y && hit_pos.y >= orig_pos.y - 1 &&
                hit_pos.z <= orig_pos.z + 1 && hit_pos.z >= orig_pos.z)
            {
                
                return true;
            }
            else
                return false;
        }
        
    }

    /// <summary>
    /// Checks if a sphere is colliding and returns true if it is along with the hit_position
    /// </summary>
    /// <param name="collided_with"></param>
    /// <param name="current_position"></param>
    /// <param name="direction"></param>
    /// <param name="radius"></param>
    /// <param name="world"></param>
    /// <param name="hit_pos"></param>
    /// <returns></returns>
    static bool check_sphere_collision(Vector3 prev_pos, Vector3 current_position,
        Vector3 direction, float radius, WorldData world, out Vector3 hit_pos)
    {
        //get the bounds of the sphere
        Vector3Int32 lb_ext =
            new Vector3Int32((int)(current_position.x - radius), Mathf.CeilToInt(current_position.y - radius),
            (int)(current_position.z - radius));
        Vector3Int32 ru_ext =
            new Vector3Int32((int)(current_position.x + radius), Mathf.CeilToInt(current_position.y + radius),
            (int)(current_position.z + radius));

        bool collided = false;

        //Test if the center of the sphere intersects a voxel
        hit_pos = current_position;
        Block cntr_block;
        if (point_is_colliding(current_position, world, out cntr_block))
        {
            // If the sphere is Inside the voxel, then GetVerticeCollisionResolution will solve the collision
            //for us perfectly
            hit_pos = GetVerticeCollisionResolution(current_position, current_position, direction, cntr_block);
            hit_pos += (-1 * direction) * (radius + 0.01f);
            current_position = hit_pos;
            collided = true;
            goto collided;
        }

        //If the sphere past the above test, then the center point of the sphere
        //is guarenteed to not be inside the voxel

        //Iterate through voxels that intersect the spheres bounding box
        for (int x = lb_ext.x; x <= ru_ext.x; x++)
        {
            for (int y = lb_ext.y; y <= ru_ext.y; y++)
            {
                for (int z = lb_ext.z; z <= ru_ext.z; z++)
                {
                    Block curr_block;
                    if ((curr_block = world.GetBlockAtPosition(x, y, z)).Collidable)
                    {
                        if (IsSphereColliding(current_position, radius, curr_block, new Vector3Int32(x, y, z), direction))
                        {
                            collided = true;
                            goto collided;
                        }
                    }
                }
            }
        }

        collided:

        if (collided)
            sphere_collision_solver(prev_pos, current_position, radius, direction, world, out hit_pos);

        return collided;
    }

    /// <summary>
    /// Iterativly solve the input collision scenario
    /// </summary>
    /// <param name="prev_pos"></param>
    /// <param name="cur_pos"></param>
    /// <param name="radius"></param>
    /// <param name="dir"></param>
    /// <param name="world"></param>
    /// <param name="new_pos"></param>
    static void sphere_collision_solver(Vector3 prev_pos, Vector3 cur_pos, float radius, Vector3 dir,
        WorldData world, out Vector3 new_pos)
    {
        new_pos = prev_pos;
        //result of collision test
        bool ret_val = true;

        dir = -dir;

        //**Note please ensure Triangle normals are already normalized
        //**Please ensure the 'dir' vector is also normalized

        //the amount of distance from the current position to test for collisions
        float test_dist = 1.0f;

        //Usually ends it 4-5 iterations
        for (int i = 0; i < 6; i++)
        {
            //If we did not collide last pass.
            if (!ret_val)
            {
                new_pos = cur_pos;
                cur_pos -= dir * test_dist;
                test_dist = test_dist / 2;
                //getting within 0.1f of collision.
            }

            //collision accuracy of 0.5f
            if (Mathf.Abs(test_dist) < 0.1f)
            {
                return;
            }

            //increment position by the test distance variable along the move direction
            cur_pos += dir * test_dist;

            //reset collision state
            ret_val = false;

            //get the bounds of the sphere
            Vector3Int32 lb_ext =
                new Vector3Int32((int)(cur_pos.x - radius), Mathf.CeilToInt(cur_pos.y - radius),
                (int)(cur_pos.z - radius));
            Vector3Int32 ru_ext =
                new Vector3Int32((int)(cur_pos.x + radius), Mathf.CeilToInt(cur_pos.y + radius),
                (int)(cur_pos.z + radius));

            Block cntr_block;
            if (point_is_colliding(cur_pos, world, out cntr_block))
            {
                cur_pos = GetVerticeCollisionResolution(cur_pos, cur_pos, dir, cntr_block);
                cur_pos += (-1 * dir) * radius;
                ret_val = true;
                goto collided;
            }

            //If the sphere past the above test, then the center point of the sphere
            //is guarenteed to not be inside the voxel

            //Iterate through voxels that intersect the spheres bounding box
            for (int x = lb_ext.x; x <= ru_ext.x; x++)
            {
                for (int y = lb_ext.y; y <= ru_ext.y; y++)
                {
                    for (int z = lb_ext.z; z <= ru_ext.z; z++)
                    {
                        Block curr_block;
                        if ((curr_block = world.GetBlockAtPosition(x, y, z)).Collidable)
                        {
                            if (IsSphereColliding(cur_pos, radius, curr_block, new Vector3Int32(x, y, z), dir))
                            {
                                ret_val = true;
                                goto collided;
                            }
                        }
                    }
                }
            }

            collided:
            ;
        }
    }

    /// <summary>
    /// Returns true if we hit the non sloped face of a sloped block or slope variant
    /// </summary>
    /// <param name="face"></param>
    /// <param name="hit_pos"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    static bool tri_secondary_hit(Vector3Int32 face, Vector3 hit_pos, Block b)
    {
        if (face.x == 0 && face.y == 0 && face.z == 0)
        {
            GameLog.Out("Error, improper face value.");
            return true;
        }

        if (face.y > 0 && b.blockType != primitiveBlockType.sunkenCornerSlope)
            return false;

        float dx = hit_pos.x - (int)hit_pos.x;
        float dy = hit_pos.y - (int)hit_pos.y == 0.0f ? 1.0f : hit_pos.y - (int)hit_pos.y;
        float dz = hit_pos.z - (int)hit_pos.z;

        if (face.y < 0 && b.blockType != primitiveBlockType.cornerSlope)
            return true;
        else if (face.y < 0 && b.blockType == primitiveBlockType.cornerSlope)
        {
            //sloped ne
            if (b.Rotation == 1)
            {
                if (dx + dz < 1)
                    return false;
                return true;
            }
            //sloped nw
            else if (b.Rotation == 2)
            {
                if (dx > dz)
                    return false;
                return true;
            }
            //sloped southe
            else if (b.Rotation == 3)
            {
                if (dz > dx)
                    return false;
                return true;
            }
            //sloped swest
            else if (b.Rotation == 4)
            {
                if (dx + dz > 1)
                    return false;
                return true;
            }
            else
            {
                GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
            }
        }

        if (b.blockType == primitiveBlockType.slope)
        {
            //hit the east face
            if (face.x > 0 || face.x < 0)
            {
                //sloped north
                if (b.Rotation == 1)
                {
                    if (dy > dz)
                        return false;
                    return true;
                }
                //sloped east
                else if (b.Rotation == 2)
                {
                    if (face.x > 0)
                        return true;
                    return false;
                }
                //sloped south
                else if (b.Rotation == 3)
                {
                    if (dy + dz > 1)
                        return false;
                    return true;
                }
                //sloped west
                else if (b.Rotation == 4)
                {
                    if (face.x > 0)
                        return false;
                    return true;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
            else if (face.z > 0 || face.z < 0)
            {
                //sloped north
                if (b.Rotation == 1)
                {
                    if (face.z < 0)
                        return false;
                    return true;
                }
                //sloped east
                else if (b.Rotation == 2)
                {
                    if (dy > dx)
                        return false;
                    return true;
                }
                //sloped south
                else if (b.Rotation == 3)
                {
                    if (face.z < 0)
                        return true;
                    return false;
                }
                //sloped west
                else if (b.Rotation == 4)
                {
                    if (dy + dx > 1)
                        return false;
                    return true;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
        }
        else if (b.blockType == primitiveBlockType.cornerSlope)
        {
            //hit the east face
            if (face.x > 0)
            {
                if (b.Rotation == 1)
                {
                    if (dy > dz)
                        return false;
                    return true;
                }
                else if (b.Rotation == 2)
                {
                    return false;
                }
                else if (b.Rotation == 3)
                {
                    if (dz + dy > 1)
                        return false;
                    return true;
                }
                else if (b.Rotation == 4)
                {
                    return false;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
            //west face
            else if (face.x < 0)
            {
                if (b.Rotation == 1)
                {
                    return false;
                }
                else if (b.Rotation == 2)
                {
                    if (dy > dz)
                        return false;
                    return true;
                }
                else if (b.Rotation == 3)
                {
                    return false;
                }
                else if (b.Rotation == 4)
                {
                    if (dy + dz > 1)
                        return false;
                    return true;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
            //hit the north face
            if (face.z > 0)
            {
                if (b.Rotation == 1)
                {
                    if (dy > dx)
                        return false;
                    return true;
                }
                else if (b.Rotation == 2)
                {
                    if (dy + dx > 1)
                        return false;
                    return true;
                }
                else if (b.Rotation == 3)
                {
                    return false;
                }
                else if (b.Rotation == 4)
                {
                    return false;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
            else if (face.z < 0)
            {
                if (b.Rotation == 1)
                {
                    return false;
                }
                else if (b.Rotation == 2)
                {
                    return false;
                }
                else if (b.Rotation == 3)
                {
                    if (dy > dx)
                        return false;
                    return true;
                }
                else if (b.Rotation == 4)
                {
                    if (dy + dx > 1)
                        return false;
                    return true;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
        }
        else if (b.blockType == primitiveBlockType.sunkenCornerSlope)
        {
            //hit the east face
            if (face.x > 0)
            {
                if (b.Rotation == 1)
                {
                    return true;
                }
                else if (b.Rotation == 2)
                {
                    if (dy > dz)
                        return false;
                    return true;
                }
                else if (b.Rotation == 3)
                {
                    return true;
                }
                else if (b.Rotation == 4)
                {
                    if (dy + dz > 1)
                        return false;
                    return true;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
            //west face
            else if (face.x < 0)
            {
                if (b.Rotation == 1)
                {
                    if (dy > dz)
                        return false;
                    return true;
                }
                else if (b.Rotation == 2)
                {
                    return true;
                }
                else if (b.Rotation == 3)
                {
                    if (dy + dz > 1)
                        return false;
                    return true;
                }
                else if (b.Rotation == 4)
                {
                    return true;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
            //hit the north face
            if (face.z > 0)
            {
                if (b.Rotation == 1)
                {
                    return true;
                }
                else if (b.Rotation == 2)
                {
                    return true;
                }
                else if (b.Rotation == 3)
                {
                    if (dy > dx)
                        return false;
                    return true;
                }
                else if (b.Rotation == 4)
                {
                    if (dy + dx > 1)
                        return false;
                    return true;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
            else if (face.z < 0)
            {
                if (b.Rotation == 1)
                {
                    if (dy > dx)
                        return false;
                    return true;
                }
                else if (b.Rotation == 2)
                {
                    if (dy + dx > 1)
                        return false;
                    return true;
                }
                else if (b.Rotation == 3)
                {
                    return true;
                }
                else if (b.Rotation == 4)
                {
                    return true;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
            else if (face.y > 0)
            {
                if (b.Rotation == 1)
                {
                    if (dx + dz < 1)
                        return false;
                    return true;
                }
                else if (b.Rotation == 2)
                {
                    if (dx > dz)
                        return false;
                    return true;
                }
                else if (b.Rotation == 3)
                {
                    if (dz > dx)
                        return false;
                    return true;
                }
                else if (b.Rotation == 4)
                {
                    if (dx + dz > 1)
                        return true;
                    return false;
                }
                else
                {
                    GameLog.Out("Error, unsupported sloped block rotation " + b.Rotation.ToString());
                }
            }
        }

        return true;
    }

    /// <summary>
    /// Finds the intersection between 2 planes. Returns false if they are parallel.
    /// </summary>
    /// <param name="linePoint"></param>
    /// <param name="lineVec"></param>
    /// <param name="plane1"></param>
    /// <param name="plane2"></param>
    public static bool PlanePlaneIntersection(out Vector3 linePoint, out Vector3 lineDir, Plane_ex plane1, Plane_ex plane2)
    {
        linePoint = new Vector3(0, 0, 0);

        //Get the normals of the planes.
        Vector3 plane1Normal = plane1.plane.normal;
        Vector3 plane2Normal = plane2.plane.normal;

        lineDir = Vector3.Cross(plane1Normal, plane2Normal);
        //Determine if the cross product yielded (0,0,0) - parallel
        if (lineDir.x < 0.00001f && lineDir.x > -0.00001f &&
            lineDir.y < 0.00001f && lineDir.y > -0.00001f &&
            lineDir.z < 0.00001f && lineDir.z > -0.00001f)
        {
            linePoint = new Vector3(0, 0, 0);
            return false;
        }

        //absolute values of the normal
        float ax = (lineDir.x >= 0 ? lineDir.x : -lineDir.x);
        float ay = (lineDir.y >= 0 ? lineDir.y : -lineDir.y);
        float az = (lineDir.z >= 0 ? lineDir.z : -lineDir.z);

        //Determine which coordinate is the largest from 0
        //x is biggest => maxc = 1, y is biggest => maxc = 2, z => maxc = 3
        //we use this to solve the plane equations
        int maxc;
        if (ax > ay)
        {
            if (ax > az)
                maxc = 1;
            else maxc = 3;
        }
        else
        {
            if (ay > az)
                maxc = 2;
            else maxc = 3;
        }
                
        float d1, d2;            
        d1 = -Vector3.Dot(plane1.plane.normal, plane1.v1);  
        d2 = -Vector3.Dot(plane2.plane.normal, plane2.v1);  

        switch (maxc)
        {           
            case 1:
                linePoint.x = 0;
                linePoint.y = (d2 * plane1.plane.normal.z - d1 * plane2.plane.normal.z) / lineDir.x;
                linePoint.z = (d1 * plane2.plane.normal.y - d2 * plane1.plane.normal.y) / lineDir.x;
                break;
            case 2:
                linePoint.x = (d1 * plane2.plane.normal.z - d2 * plane1.plane.normal.z) / lineDir.y;
                linePoint.y = 0;
                linePoint.z = (d2 * plane1.plane.normal.x - d1 * plane2.plane.normal.x) / lineDir.y;
                break;
            case 3:
                linePoint.x = (d2 * plane1.plane.normal.y - d1 * plane2.plane.normal.y) / lineDir.z;
                linePoint.y = (d1 * plane2.plane.normal.x - d2 * plane1.plane.normal.x) / lineDir.z;
                linePoint.z = 0;
                break;
        }
        return true;
    }

    /// <summary>
    /// Calculate line intersection. Returns false if skew. Ignores Coinciding lines
    /// </summary>
    /// <param name="intersection"></param>
    /// <param name="linePoint1"></param>
    /// <param name="lineVec1"></param>
    /// <param name="linePoint2"></param>
    /// <param name="lineVec2"></param>
    /// <returns></returns>
    public static bool LineLineIntersection(out Vector3 intersection, Vector3 linePoint1, Vector3 lineDir1, Vector3 linePoint2, Vector3 lineDir2)
    {
        intersection = new Vector3(0, 0, 0);

        Vector3 lineDir3 = linePoint2 - linePoint1;
        Vector3 crossVec1and2 = Vector3.Cross(lineDir1, lineDir2);
        Vector3 crossVec3and2 = Vector3.Cross(lineDir3, lineDir2);

        float planarFactor = Vector3.Dot(lineDir3, crossVec1and2);
        //Lines are not coplanar. Take into account rounding errors.
        if ((planarFactor >= 0.00001f) || (planarFactor <= -0.00001f))
        {
            return false;
        }

        //Note: sqrMagnitude does x*x+y*y+z*z on the input vector.
        float s = Vector3.Dot(crossVec3and2, crossVec1and2) / crossVec1and2.sqrMagnitude;

        if ((s >= 0.0f) && (s <= 1.0f))
        {
            intersection = linePoint1 + (lineDir1 * s);
            return true;
        }
        else
        {
            //we aren't using coincidence :P
           /* //check for coincidence
            if (linePoint1 == linePoint2)
                return true;
            Vector3 norm21 = (linePoint2 - linePoint1).normalized;
            Vector3 norm12 = (linePoint1 - linePoint2).normalized;
            Vector3 dir_norm = lineDir1.normalized;

            //check if dir_norm == norm21 or == dir_norm == norm12
            if ((norm21.x < dir_norm.x + 0.00001f && norm21.y < dir_norm.y + 0.00001f && norm21.z < dir_norm.z + 0.00001f
                && norm21.x > dir_norm.x - 0.00001f && norm21.y > dir_norm.y - 0.00001f && norm21.z > dir_norm.z - 0.00001f) ||
                (norm12.x < dir_norm.x + 0.00001f && norm12.y < dir_norm.y + 0.00001f && norm12.z < dir_norm.z + 0.00001f
                && norm12.x > dir_norm.x - 0.00001f && norm12.y > dir_norm.y - 0.00001f && norm12.z > dir_norm.z - 0.00001f))
                return true;
            */

            return false;
        }
    }

    /// <summary>
    /// Intersection of a line and plane. Returns false if parallel, otherwise true.
    /// </summary>
    /// <param name="intersection"></param>
    /// <param name="linePoint"></param>
    /// <param name="lineVec"></param>
    /// <param name="planeNormal"></param>
    /// <param name="planePoint"></param>
    /// <returns></returns>
    public static bool LinePlaneIntersection(out Vector3 intersection, Vector3 linePoint, Vector3 lineVec, Vector3 planeNormal, Vector3 planePoint)
    {
        float length;
        float dotNumerator;
        float dotDenominator;
        Vector3 vector;

        //calculate the distance between the linePoint and the line-plane intersection point
        Vector3 planept_linept = planePoint - linePoint;
        dotNumerator = planept_linept.x * planeNormal.x + planept_linept.y * planeNormal.y +
            planept_linept.z * planeNormal.z;
        dotDenominator = lineVec.x * planeNormal.x + lineVec.y * planeNormal.y +
            lineVec.z * planeNormal.z;
    
        //line and plane are not parallel
        if (dotDenominator != 0.0f)
        {
            length = dotNumerator / dotDenominator;

            //create a vector from the linePoint to the intersection point
            vector = lineVec.normalized * length;

            //get the coordinates of the line-plane intersection point
            intersection = linePoint + vector;

            return true;
        }

        //output not valid
        else
        {
            intersection = new Vector3(0, 0, 0);
            return false;
        }
    }

    /// <summary>
    /// Returns true if the point 'p' is inside the triangle defined by (t1,t2,t3).
    /// Assumes triangle and point are in the same plane!
    /// </summary>
    /// <param name="t1"></param>
    /// <param name="t2"></param>
    /// <param name="t3"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static bool PointInsideTriangle(Vector3 t1, Vector3 t2, Vector3 t3, Vector3 p)
    {
        //Compute vectors        
        Vector3 v0 = t3 - t1;
        Vector3 v1 = t2 - t1;
        Vector3 v2 = p - t1;

        //Compute dot products
        float dot00 = v0.x * v0.x + v0.y * v0.y + v0.z * v0.z;
        float dot01 = v0.x * v1.x + v0.y * v1.y + v0.z * v1.z;
        float dot02 = v0.x * v2.x + v0.y * v2.y + v0.z * v2.z;
        float dot11 = v1.x * v1.x + v1.y * v1.y + v1.z * v1.z;
        float dot12 = v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;

        //Compute barycentric coordinates
        float denom = 1 / (dot00 * dot11 - dot01 * dot01);
        float u = (dot11 * dot02 - dot01 * dot12) * denom;
        float v = (dot00 * dot12 - dot01 * dot02) * denom;

        //Check if point is in triangle
        return (u >= -0.0001f) && (v >= -0.0001f) && (u + v < 1.0001f);
    }

    /// <summary>
    /// Returns the closest distance from a line segment and a point
    /// </summary>
    /// <param name="v"></param>
    /// <param name="w"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static float LineSegmentPointDistance(Vector3 v, Vector3 w, Vector3 p)
    {
        // Return minimum distance between line segment vw and point p
        float l2 = (w - v).sqrMagnitude;  //length of segment
        if (l2 == 0.0)
            return (v - p).magnitude;   // segment length of zero, return distance of two points

        // Consider the line extending the segment, parameterized as v + t (w - v).
        // We find projection of point p onto the line. 
        // It falls where t = [(p-v) . (w-v)] / |w-v|^2
        float t = Vector3.Dot(p - v, w - v) / l2;
        if (t < 0.0)
            return (v - p).magnitude;       // Beyond the 'v' end of the segment
        else if (t > 1.0)
            return (w - p).magnitude;  // Beyond the 'w' end of the segment

        Vector3 projection = v + t * (w - v);
        return (projection - p).magnitude;
    }

    /// <summary>
    /// Returns the closest squared distance from a line segment and a point
    /// </summary>
    /// <param name="v"></param>
    /// <param name="w"></param>
    /// <param name="p"></param>
    /// <returns></returns>
    public static float LineSegmentPointDistanceSqrd(Vector3 v, Vector3 w, Vector3 p)
    {
        // Return minimum distance between line segment vw and point p
        float l2 = (w - v).sqrMagnitude;  //length of segment
        if (l2 == 0.0)
            return (v - p).sqrMagnitude;   // segment length of zero, return distance of two points

        // Consider the line extending the segment, parameterized as v + t (w - v).
        // We find projection of point p onto the line. 
        // It falls where t = [(p-v) . (w-v)] / |w-v|^2
        float t = Vector3.Dot(p - v, w - v) / l2;
        if (t < 0.0)
            return (v - p).sqrMagnitude;       // Beyond the 'v' end of the segment
        else if (t > 1.0)
            return (w - p).sqrMagnitude;  // Beyond the 'w' end of the segment

        Vector3 projection = v + t * (w - v);
        return (projection - p).sqrMagnitude;
    }

    /// <summary>
    /// Tests for and resolves collisions for the input triangle. This tests if any of the voxel
    /// edges intersect the triangle.
    /// </summary>
    /// <param name="p">Plane to test for intersection</param>
    /// <param name="b">Block testing against</param>
    /// <param name="block_pos">Position of the block being tested</param>
    /// <param name="c">Collider that the input triangle belongs to</param>
    /// <returns></returns>
    private static bool TriVoxelIntersection(pairx4 tri, Block b, Vector3Int32 block_pos)
    {
        //plane representing this triangle
        Plane tri_face = new Plane(tri.n, tri.v1);

        //for all block types, the first thing we do is create a plane for each face on the voxel

        //then we test to see which planes the triangle intersects
        if (b.blockType == primitiveBlockType.cube)
        {
            Vector3 bpos = new Vector3(block_pos.x, block_pos.y, block_pos.z);
            Vector3 bpos101 = new Vector3(block_pos.x + 1, block_pos.y, block_pos.z + 1);
            Vector3 bpos1s11 = new Vector3(block_pos.x + 1, block_pos.y - 1, block_pos.z + 1);
            Vector3 bpos0s10 = new Vector3(block_pos.x, block_pos.y - 1, block_pos.z);
            
            //currently being tested intersection
            Vector3 intersection;

            /*cube points
            3-------4
           /|      /|
          / |     / |
         1--|----2  |
         |  7----|--8
         | /     | /
         5-------6
            */
            //testing cube against triangle.
            bool p1 = tri_face.GetSide(bpos),
                 //each point is a vertex on the cube
                 p2 = tri_face.GetSide(new Vector3(bpos.x + 1, bpos.y, bpos.z)),
                 p3 = tri_face.GetSide(new Vector3(bpos.x, bpos.y, bpos.z + 1)),
                 p4 = tri_face.GetSide(new Vector3(bpos.x + 1, bpos.y, bpos.z + 1));
            bool p5 = tri_face.GetSide(new Vector3(bpos.x, bpos.y - 1, bpos.z)),
                 p6 = tri_face.GetSide(new Vector3(bpos.x + 1, bpos.y - 1, bpos.z)),
                 p7 = tri_face.GetSide(new Vector3(bpos.x, bpos.y - 1, bpos.z + 1)),
                 p8 = tri_face.GetSide(new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1));

            //test all 12 edges of the cube against the triangle
            //Note** because we do the vertice test and resolution before calling this function,
            //we do not have to check the triangle against the voxel

            //intersect the edge of the cube with the plane of the triangle
            if (p1 != p2 && LinePlaneIntersection(out intersection, bpos, Vector3.right, tri.n, tri.v1))
            {
                //determine if the intersection is inside the triangle
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p1 != p3 && LinePlaneIntersection(out intersection, bpos, Vector3.forward, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p1 != p5 && LinePlaneIntersection(out intersection, bpos, Vector3.down, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p4 != p3 && LinePlaneIntersection(out intersection, bpos101, Vector3.left, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p4 != p2 && LinePlaneIntersection(out intersection, bpos101, Vector3.back, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }

            if (p4 != p8 && LinePlaneIntersection(out intersection, bpos101, Vector3.down, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p5 != p7 && LinePlaneIntersection(out intersection, bpos0s10, Vector3.down, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p5 != p6 && LinePlaneIntersection(out intersection, bpos101, Vector3.right, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p8 != p7 && LinePlaneIntersection(out intersection, bpos1s11, Vector3.left, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p8 != p6 && LinePlaneIntersection(out intersection, bpos1s11, Vector3.back, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p2 != p6 && LinePlaneIntersection(out intersection, new Vector3(bpos.x + 1, bpos.y, bpos.z),
                Vector3.down, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p3 != p7 && LinePlaneIntersection(out intersection, new Vector3(bpos.x, bpos.y, bpos.z + 1),
                Vector3.down, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            return false;
        }
        else if (b.blockType == primitiveBlockType.slope)
        {
            //common points
            Vector3 bpos = new Vector3(block_pos.x, block_pos.y, block_pos.z);

            /*forall rotations, ramp points
            3-------4
           /|      /|
          / 5     / 6
         1--------2
            */
            //negative = false/ positive = true side of plane
            bool p1, p2, p3, p4, p5, p6;
            //point position
            Vector3 v1, v2, v3, v4, v5, v6;
            //direction of edges
            Vector3 d1to3, d1to2, d1to5, d4to3, d4to6, d4to2, d6to5, d6to2, d3to5;

            //assign variables based on the rotation of this sloped block
            if (b.Rotation == 1)
            {
                v1 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v2 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z); 
                v3 = new Vector3(bpos.x, bpos.y, bpos.z + 1);      
                v4 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);  
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                d1to3 = new Vector3(0, 0.7071067811865475f, 0.7071067811865475f);
                d1to2 = Vector3.right;
                d1to5 = Vector3.forward;
                d4to3 = Vector3.left;
                d4to2 = new Vector3(0, -0.7071067811865475f, -0.7071067811865475f);
                d4to6 = Vector3.down;
                d6to5 = Vector3.left;
                d6to2 = Vector3.back;
                d3to5 = Vector3.down;                     
            }
            else if (b.Rotation == 2)
            {
                v1 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);  
                v2 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v3 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y, bpos.z);      
                v5 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                d1to3 = new Vector3(0.7071067811865475f, 0.7071067811865475f, 0);
                d1to2 = Vector3.back;
                d1to5 = Vector3.right;
                d4to3 = Vector3.forward;
                d4to2 = new Vector3(-0.7071067811865475f, -0.7071067811865475f, 0);
                d4to6 = Vector3.down;
                d6to5 = Vector3.forward;
                d6to2 = Vector3.left;
                d3to5 = Vector3.down;
            }
            else if (b.Rotation == 3)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v2 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v3 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v4 = bpos;
                v5 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v6 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                d1to3 = new Vector3(0, 0.7071067811865475f, -0.7071067811865475f);
                d1to2 = Vector3.left;
                d1to5 = Vector3.back;
                d4to3 = Vector3.right;
                d4to2 = new Vector3(0, -0.7071067811865475f, 0.7071067811865475f);
                d4to6 = Vector3.down;
                d6to5 = Vector3.right;
                d6to2 = Vector3.forward;
                d3to5 = Vector3.down;
            }
            else if (b.Rotation == 4)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v2 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v3 = bpos;
                v4 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v6 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                d1to3 = new Vector3(-0.7071067811865475f, 0.7071067811865475f, 0);
                d1to2 = Vector3.forward;
                d1to5 = Vector3.left;
                d4to3 = Vector3.back;
                d4to2 = new Vector3(0.7071067811865475f, -0.7071067811865475f, 0);
                d4to6 = Vector3.down;
                d6to5 = Vector3.back;
                d6to2 = Vector3.right;
                d3to5 = Vector3.down;
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
                return false;
            }

            //get the side of each point
            p1 = tri_face.GetSide(v1);
            p2 = tri_face.GetSide(v2);
            p3 = tri_face.GetSide(v3);
            p4 = tri_face.GetSide(v4);
            p5 = tri_face.GetSide(v5);
            p6 = tri_face.GetSide(v6);

            //determine if the sloped block intersects the triangle
            Vector3 intersection;
            if (p1 != p3 && LinePlaneIntersection(out intersection, v1, d1to3, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p1 != p2 && LinePlaneIntersection(out intersection, v1, d1to2, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p1 != p5 && LinePlaneIntersection(out intersection, v1, d1to5, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p4 != p6 && LinePlaneIntersection(out intersection, v4, d4to6, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p4 != p2 && LinePlaneIntersection(out intersection, v4, d4to2, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p4 != p3 && LinePlaneIntersection(out intersection, v4, d4to3, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p6 != p2 && LinePlaneIntersection(out intersection, v6, d6to2, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p6 != p5 && LinePlaneIntersection(out intersection, v6, d6to5, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p3 != p5 && LinePlaneIntersection(out intersection, v3, d3to5, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            return false;
        }
        else if (b.blockType == primitiveBlockType.cornerSlope)
        {
            Vector3 bpos = new Vector3(block_pos.x, block_pos.y, block_pos.z);

            bool p1, p2, p3, p4;
            Vector3 v1, v2, v3, v4;
            Vector3 d3to1, d3to4, d3to2, d1to4, d1to2, d2to4;

            /*forall rotations, corner ramp points
                     2
                   / |
                 /   |
               /     |
             1-------3
              `\    /
                 ` 4
            */

            if (b.Rotation == 1)
            {
                v1 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v3 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                d3to1 = Vector3.left;
                d3to4 = Vector3.back;
                d3to2 = Vector3.up;
                d1to4 = new Vector3(0.7071067811865475f, 0, -0.7071067811865475f);
                d1to2 = new Vector3(0.7071067811865475f, 0.7071067811865475f, 0);
                d2to4 = new Vector3(0, -0.7071067811865475f, -0.7071067811865475f);
            }
            else if (b.Rotation == 2)
            {
                v1 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v2 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v3 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                d3to1 = Vector3.back;
                d3to4 = Vector3.right;
                d3to2 = Vector3.up;
                d1to4 = new Vector3(0.7071067811865475f, 0, 0.7071067811865475f);
                d1to2 = new Vector3(0, 0.7071067811865475f, 0.7071067811865475f);
                d2to4 = new Vector3(0.7071067811865475f, -0.7071067811865475f, 0);
            }
            else if (b.Rotation == 3)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v3 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v4 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                d3to1 = Vector3.forward;
                d3to4 = Vector3.left;
                d3to2 = Vector3.up;
                d1to4 = new Vector3(-0.7071067811865475f, 0, -0.7071067811865475f);
                d1to2 = new Vector3(0, 0.7071067811865475f, -0.7071067811865475f);
                d2to4 = new Vector3(-0.7071067811865475f, -0.7071067811865475f, 0);
            }
            else if (b.Rotation == 4)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v2 = bpos;
                v3 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v4 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                d3to1 = Vector3.right;
                d3to4 = Vector3.forward;
                d3to2 = Vector3.up;
                d1to4 = new Vector3(-0.7071067811865475f, 0, 0.7071067811865475f);
                d1to2 = new Vector3(-0.7071067811865475f, 0.7071067811865475f, 0);
                d2to4 = new Vector3(0, -0.7071067811865475f, 0.7071067811865475f);
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
                return false;
            }

            //get the side of each point
            p1 = tri_face.GetSide(v1);
            p2 = tri_face.GetSide(v2);
            p3 = tri_face.GetSide(v3);
            p4 = tri_face.GetSide(v4);

            //test for intersections
            Vector3 intersection;
            if (p3 != p1 && LinePlaneIntersection(out intersection, v3, d3to1, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p3 != p4 && LinePlaneIntersection(out intersection, v3, d3to4, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p3 != p2 && LinePlaneIntersection(out intersection, v3, d3to2, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p1 != p4 && LinePlaneIntersection(out intersection, v1, d1to4, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p1 != p2 && LinePlaneIntersection(out intersection, v1, d1to2, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p2 != p4 && LinePlaneIntersection(out intersection, v2, d2to4, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }

            return false;
        }
        else if (b.blockType == primitiveBlockType.sunkenCornerSlope)
        {
            Vector3 bpos = new Vector3(block_pos.x, block_pos.y, block_pos.z);

            /*forall rotations, inside ramp points
            3-------1
            | \    /|
            |   \ / |
            |    2  |
            6----|--7
           /     | /
          5------4
            */

            bool p1, p2, p3, p4, p5, p6, p7;
            Vector3 v1, v2, v3, v4, v5, v6, v7;
            Vector3 d1to2, d1to3, d1to7, d6to7, d6to3, d6to5, d4to2, d4to5, d4to7, d3to2, d3to5, d2to5;

            if (b.Rotation == 1)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v3 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v6 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v7 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                d1to2 = Vector3.back;
                d1to3 = Vector3.left;
                d1to7 = Vector3.down;
                d6to7 = Vector3.right;
                d6to3 = Vector3.up;
                d6to5 = Vector3.back;
                d4to2 = Vector3.up;
                d4to5 = Vector3.left;
                d4to7 = Vector3.forward;
                d3to2 = new Vector3(0.7071067811865475f, 0, -0.7071067811865475f);
                d3to5 = new Vector3(0, -0.7071067811865475f, -0.7071067811865475f);
                d2to5 = new Vector3(-0.7071067811865475f, -0.7071067811865475f, 0);
            }
            else if (b.Rotation == 2)
            {
                v1 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v3 = bpos;
                v4 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v5 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v6 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v7 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                d1to2 = Vector3.right;
                d1to3 = Vector3.back;
                d1to7 = Vector3.down;
                d6to7 = Vector3.forward;
                d6to3 = Vector3.up;
                d6to5 = Vector3.right;
                d4to2 = Vector3.up;
                d4to5 = Vector3.back;
                d4to7 = Vector3.left;
                d3to2 = new Vector3(0.7071067811865475f, 0, 0.7071067811865475f);
                d3to5 = new Vector3(0.7071067811865475f, -0.7071067811865475f, 0);
                d2to5 = new Vector3(0, -0.7071067811865475f, -0.7071067811865475f);
            }
            else if (b.Rotation == 3)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v2 = bpos;
                v3 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v4 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v7 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                d1to2 = Vector3.left;
                d1to3 = Vector3.forward;
                d1to7 = Vector3.down;
                d6to7 = Vector3.back;
                d6to3 = Vector3.up;
                d6to5 = Vector3.left;
                d4to2 = Vector3.up;
                d4to5 = Vector3.forward;
                d4to7 = Vector3.right;
                d3to2 = new Vector3(-0.7071067811865475f, 0, -0.7071067811865475f);
                d3to5 = new Vector3(-0.7071067811865475f, -0.7071067811865475f, 0);
                d2to5 = new Vector3(0, -0.7071067811865475f, 0.7071067811865475f);
            }
            else if (b.Rotation == 4)
            {
                v1 = bpos;
                v2 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v3 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v4 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v5 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v7 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                d1to2 = Vector3.forward;
                d1to3 = Vector3.right;
                d1to7 = Vector3.down;
                d6to7 = Vector3.left;
                d6to3 = Vector3.up;
                d6to5 = Vector3.forward;
                d4to2 = Vector3.up;
                d4to5 = Vector3.right;
                d4to7 = Vector3.back;
                d3to2 = new Vector3(-0.7071067811865475f, 0, 0.7071067811865475f);
                d3to5 = new Vector3(0, -0.7071067811865475f, 0.7071067811865475f);
                d2to5 = new Vector3(0.7071067811865475f, -0.7071067811865475f, 0);
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
                return false;
            }

            //get the side of each point
            p1 = tri_face.GetSide(v1);
            p2 = tri_face.GetSide(v2);
            p3 = tri_face.GetSide(v3);
            p4 = tri_face.GetSide(v4);
            p5 = tri_face.GetSide(v5);
            p6 = tri_face.GetSide(v6);
            p7 = tri_face.GetSide(v7);

            Vector3 intersection;
            if (p1 != p2 && LinePlaneIntersection(out intersection, v1, d1to2, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p1 != p3 && LinePlaneIntersection(out intersection, v1, d1to3, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p1 != p7 && LinePlaneIntersection(out intersection, v1, d1to7, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p6 != p7 && LinePlaneIntersection(out intersection, v6, d6to7, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p6 != p3 && LinePlaneIntersection(out intersection, v6, d6to3, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p6 != p5 && LinePlaneIntersection(out intersection, v6, d6to5, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p4 != p7 && LinePlaneIntersection(out intersection, v4, d4to7, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p4 != p5 && LinePlaneIntersection(out intersection, v4, d4to5, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p4 != p2 && LinePlaneIntersection(out intersection, v4, d4to2, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p3 != p2 && LinePlaneIntersection(out intersection, v3, d3to2, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p3 != p5 && LinePlaneIntersection(out intersection, v3, d3to5, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }
            if (p2 != p5 && LinePlaneIntersection(out intersection, v2, d2to5, tri.n, tri.v1))
            {
                if (PointInsideTriangle(tri.v1, tri.v2, tri.v3, intersection))
                {
                    return true;
                }
            }

            return false;
        }
        else
        {
            GameLog.Out("Error, unsupported block type. Cannot test for and resolve collisions.");
            return false;
        }
    }

    /// <summary>
    /// Returns a non colliding position (regarding this vertice only) for the collider. The vertice is assumed to be somewhere
    /// inside of the space that the voxel occupies.
    /// </summary>
    /// <param name="vert"></param>
    /// <param name="c"></param>
    /// <param name="block"></param>
    /// <returns></returns>
    static Vector3 GetVerticeCollisionResolution(Vector3 vert, Vector3 transform_pos, Vector3 move_direction, Block block)
    {
        //the direction is the opposite of the direction the object moved (because
        //we want to move the object backwards if it is colliding)
        Vector3 direction = -1 * move_direction;
        //coordinates of the block being checked
        int x = (int)vert.x, y = Mathf.CeilToInt(vert.y), z = (int)vert.z;

        if (block.blockType == primitiveBlockType.cube)
        {
            //points that define each plane we test against (p1 = point, p1 = normal, p2 = point, p2n = normal, etc...)
            Vector3 p1, p1n, p2, p2n, p3, p3n;

            //this if/else tree determines which faces of the voxel will be tested
            //to get the new position
            if (direction.x < 0)
            {
                p1 = new Vector3(x, y, z);
                p1n = Vector3.right;
            }
            else
            {
                p1 = new Vector3(x + 1, y, z);
                p1n = Vector3.left;
            }
            if (direction.y < 0)
            {
                p2 = new Vector3(x, y - 1, z);
                p2n = Vector3.up;
            }
            else
            {
                p2 = new Vector3(x, y, z);
                p2n = Vector3.down;
            }
            if (direction.z < 0)
            {
                p3 = new Vector3(x, y, z);
                p3n = Vector3.forward;
            }
            else
            {
                p3 = new Vector3(x, y, z + 1);
                p3n = Vector3.back;
            }

            //do line/plane intersection to get the depth of this vertice into the voxel
            Vector3 intersection;
            if (LinePlaneIntersection(out intersection, vert, direction, p1n, p1))
            {
                //ensure that the intersection occured inside the voxel
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }
            if (LinePlaneIntersection(out intersection, vert, direction, p2n, p2))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }
            if (LinePlaneIntersection(out intersection, vert, direction, p3n, p3))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }

            GameLog.Out("Error resolving vertice collision.");
            return transform_pos;
        }
        else if (block.blockType == primitiveBlockType.slope)
        {
            Ray dir = new Ray(vert, direction);
            Plane p;

            Vector3 intersection;

            Vector3 p1 = default(Vector3), p1n = default(Vector3),
                p2 = default(Vector3), p2n = default(Vector3),
                p3 = default(Vector3), p3n = default(Vector3);
            bool d1 = false, d2 = false, d3 = false;

            int py = y - 1;
            if (block.Rotation == 1)
            {
                p = new Plane(new Vector3(0, -0.7071067811865475f, 0.7071067811865475f),
                    new Vector3(x, py, z));
                if (direction.y < 0)
                {
                    d1 = true;
                    p1 = new Vector3(x, py, z);
                    p1n = Vector3.up;
                }
                if (direction.x >= 0)
                {
                    d2 = true;
                    //(the points dont have to be on the triangle, they just need to be on the plane)
                    p2 = new Vector3(x + 1, y, z);
                    p2n = Vector3.left;
                }
                else
                {
                    d2 = true;
                    p2 = new Vector3(x, y, z);
                    p2n = Vector3.right;
                }
                if (direction.z > 0)
                {
                    d3 = true;
                    p3 = new Vector3(x, y, z + 1);
                    p3n = Vector3.back;
                }
            }
            else if (block.Rotation == 2)
            {
                p = new Plane(new Vector3(0.7071067811865475f, -0.7071067811865475f, 0),
                    new Vector3(x, py, z));
                if (direction.y < 0)
                {
                    d1 = true;
                    p1 = new Vector3(x, py, z);
                    p1n = Vector3.up;
                }
                if (direction.z >= 0)
                {
                    d2 = true;
                    p2 = new Vector3(x, y, z + 1);
                    p2n = Vector3.back;
                }
                else
                {
                    d2 = true;
                    p2 = new Vector3(x, y, z);
                    p2n = Vector3.forward;
                }
                if (direction.x > 0)
                {
                    d3 = true;
                    p3 = new Vector3(x + 1, y, z + 1);
                    p3n = Vector3.left;
                }
            }
            else if (block.Rotation == 3)
            {
                p = new Plane(new Vector3(0, -0.7071067811865475f, -0.7071067811865475f),
                    new Vector3(x, py, z + 1));
                if (direction.y < 0)
                {
                    d1 = true;
                    p1 = new Vector3(x, py, z);
                    p1n = Vector3.up;
                }
                if (direction.x >= 0)
                {
                    d2 = true;
                    p2 = new Vector3(x + 1, y, z);
                    p2n = Vector3.left;
                }
                else
                {
                    d2 = true;
                    p2 = new Vector3(x, y, z);
                    p2n = Vector3.right;
                }
                if (direction.z < 0)
                {
                    d3 = true;
                    p3 = new Vector3(x, y, z);
                    p3n = Vector3.forward;
                }
            }
            else if (block.Rotation == 4)
            {
                p = new Plane(new Vector3(-0.7071067811865475f, -0.7071067811865475f, 0),
                    new Vector3(x + 1, py, z));
                if (direction.y < 0)
                {
                    d1 = true;
                    p1 = new Vector3(x, py, z);
                    p1n = Vector3.up;
                }
                if (direction.z >= 0)
                {
                    d2 = true;
                    p2 = new Vector3(x, y, z + 1);
                    p2n = Vector3.back;
                }
                else
                {
                    d2 = true;
                    p2 = new Vector3(x, y, z);
                    p2n = Vector3.forward;
                }
                if (direction.x < 0)
                {
                    d3 = true;
                    p3 = new Vector3(x, y, z);
                    p3n = Vector3.right;
                }
            }
            else
            {
                GameLog.Out("Unsupported rotation " + block.Rotation + ", " + block.itemName);
                return transform_pos;
            }

            //test for the point of intersection, which is also where we determine the
            //new (non colliding) position to be.

            float dist;
            if (p.Raycast(dir, out dist))
            {
                Vector3 ray_intersection = dir.GetPoint(dist);
                if (ray_intersection.x < x + 1.01f && ray_intersection.x > x - 0.01f &&
                     ray_intersection.y < y + 0.01f && ray_intersection.y > y - 1.01f &&
                     ray_intersection.z < z + 1.01f && ray_intersection.z > z - 0.01f)
                {
                    return (ray_intersection - vert).magnitude * direction + transform_pos;
                }
            }

            if (d1 && LinePlaneIntersection(out intersection, vert, direction, p1n, p1))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }
            if (d2 && LinePlaneIntersection(out intersection, vert, direction, p2n, p2))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }
            if (d3 && LinePlaneIntersection(out intersection, vert, direction, p3n, p3))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }

            GameLog.Out("Error resolving vertice collision.");
            return transform_pos;
        }
        else if (block.blockType == primitiveBlockType.cornerSlope)
        {
            Ray dir = new Ray(vert, direction);
            Plane p;

            Vector3 intersection;
            Vector3 p1 = default(Vector3), p1n = default(Vector3),
                p2 = default(Vector3), p2n = default(Vector3),
                p3 = default(Vector3), p3n = default(Vector3);

            //gets set to true if we can intersect with the corresponding planes (d1 for p1 and p1n)
            // d2 for p2 and p2n etc..
            bool d1 = false, d2 = false, d3 = false;

            if (block.Rotation == 1)
            {
                int px = x + 1;
                int pz = z + 1;
                p = new Plane(new Vector3(0.5773502691896258f, -0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(px, y, pz));
                if (direction.y < 0)
                {
                    d1 = true;
                    p1 = new Vector3(x, y - 1, z); p1n = Vector3.up;
                    p1n = Vector3.up;
                }
                if (direction.z > 0)
                {
                    d2 = true;
                    p2 = new Vector3(x + 1, y, z + 1); p2n = Vector3.back;
                    p2n = Vector3.back;
                }
                if (direction.x > 0)
                {
                    d3 = true;
                    p3 = new Vector3(x + 1, y, z + 1); p3n = Vector3.left;
                    p3n = Vector3.left;
                }
            }
            else if (block.Rotation == 2)
            {
                int pz = z + 1;
                p = new Plane(new Vector3(-0.5773502691896258f, -0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(x, y, pz));
                if (direction.y < 0)
                {
                    d1 = true;
                    p1 = new Vector3(x, y - 1, z); p1n = Vector3.up;
                    p1n = Vector3.up;
                }
                if (direction.x < 0)
                {
                    d2 = true;
                    p2 = new Vector3(x, y, z + 1); p2n = Vector3.right;
                    p2n = Vector3.right;
                }
                if (direction.z > 0)
                {
                    d3 = true;
                    p3 = new Vector3(x, y, z + 1); p3n = Vector3.back;
                    p3n = Vector3.back;
                }
            }
            else if (block.Rotation == 3)
            {
                int px = x + 1;
                p = new Plane(new Vector3(0.5773502691896258f, -0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(px, y, z));
                if (direction.y < 0)
                {
                    d1 = true;
                    p1 = new Vector3(x, y - 1, z); p1n = Vector3.up;
                    p1n = Vector3.up;
                }
                if (direction.x > 0)
                {
                    d2 = true;
                    p2 = new Vector3(x + 1, y, z); p2n = Vector3.left;
                    p2n = Vector3.left;
                }
                if (direction.z < 0)
                {
                    d3 = true;
                    p3 = new Vector3(x + 1, y, z); p3n = Vector3.forward;
                    p3n = Vector3.forward;
                }
            }
            else if (block.Rotation == 4)
            {
                p = new Plane(new Vector3(-0.5773502691896258f, -0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(x, y, z));
                if (direction.y < 0)
                {
                    d1 = true;
                    p1 = new Vector3(x, y - 1, z); p1n = Vector3.up;
                    p1n = Vector3.up;
                }
                if (direction.x < 0)
                {
                    d2 = true;
                    p2 = new Vector3(x, y, z); p2n = Vector3.right;
                    p2n = Vector3.right;
                }
                if (direction.z < 0)
                {
                    d3 = true;
                    p3 = new Vector3(x, y, z); p3n = Vector3.forward;
                    p3n = Vector3.forward;
                }
            }
            else
            {
                GameLog.Out("Unsupported rotation " + block.Rotation + ", " + block.itemName);
                return transform_pos;
            }

            //test for the point of intersection, which is also where we determine the
            //new (non colliding) position to be.

            float dist;
            if (p.Raycast(dir, out dist))
            {
                Vector3 ray_intersection = dir.GetPoint(dist);
                if (ray_intersection.x < x + 1.01f && ray_intersection.x > x - 0.01f &&
                     ray_intersection.y < y + 0.01f && ray_intersection.y > y - 1.01f &&
                     ray_intersection.z < z + 1.01f && ray_intersection.z > z - 0.01f)
                {
                    return (ray_intersection - vert).magnitude * direction + transform_pos;
                }
            }

            if (d1 && LinePlaneIntersection(out intersection, vert, direction, p1n, p1))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }
            if (d2 && LinePlaneIntersection(out intersection, vert, direction, p2n, p2))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }
            if (d3 && LinePlaneIntersection(out intersection, vert, direction, p3n, p3))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }

            GameLog.Out("Error resolving vertice collision.");
            return transform_pos;
        }
        else if (block.blockType == primitiveBlockType.sunkenCornerSlope)
        {
            Ray dir = new Ray(vert, direction);
            Plane p;

            Vector3 intersection;
            Vector3 p1 = default(Vector3), p1n = default(Vector3),
                p2 = default(Vector3), p2n = default(Vector3),
                p3 = default(Vector3), p3n = default(Vector3);

            int py = y - 1;
            if (block.Rotation == 1)
            {
                p = new Plane(new Vector3(0.5773502691896258f, -0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(x, py, z));
                if (direction.x < 0)
                {
                    p1 = new Vector3(x, y, z);
                    p1n = Vector3.right;
                }
                else
                {
                    p1 = new Vector3(x + 1, y, z);
                    p1n = Vector3.left;
                }
                if (direction.y < 0)
                {
                    p2 = new Vector3(x, y - 1, z);
                    p2n = Vector3.up;
                }
                else
                {
                    p2 = new Vector3(x, y, z);
                    p2n = Vector3.down;
                }
                if (direction.z < 0)
                {
                    p3 = new Vector3(x, y, z);
                    p3n = Vector3.forward;
                }
                else
                {
                    p3 = new Vector3(x, y, z + 1);
                    p3n = Vector3.back;
                }
            }
            else if (block.Rotation == 2)
            {
                int px = x + 1;
                p = new Plane(new Vector3(-0.5773502691896258f, -0.5773502691896258f, 0.5773502691896258f),
                    new Vector3(px, py, z));
                if (direction.x < 0)
                {
                    p1 = new Vector3(x, y, z);
                    p1n = Vector3.right;
                }
                else
                {
                    p1 = new Vector3(x + 1, y, z);
                    p1n = Vector3.left;
                }
                if (direction.y < 0)
                {
                    p2 = new Vector3(x, y - 1, z);
                    p2n = Vector3.up;
                }
                else
                {
                    p2 = new Vector3(x, y, z);
                    p2n = Vector3.down;
                }
                if (direction.z < 0)
                {
                    p3 = new Vector3(x, y, z);
                    p3n = Vector3.forward;
                }
                else
                {
                    p3 = new Vector3(x, y, z + 1);
                    p3n = Vector3.back;
                }
            }
            else if (block.Rotation == 3)
            {
                int pz = z + 1;
                p = new Plane(new Vector3(0.5773502691896258f, -0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(x, py, pz));
                if (direction.x < 0)
                {
                    p1 = new Vector3(x, y, z);
                    p1n = Vector3.right;
                }
                else
                {
                    p1 = new Vector3(x + 1, y, z);
                    p1n = Vector3.left;
                }
                if (direction.y < 0)
                {
                    p2 = new Vector3(x, y - 1, z);
                    p2n = Vector3.up;
                }
                else
                {
                    p2 = new Vector3(x, y, z);
                    p2n = Vector3.down;
                }
                if (direction.z < 0)
                {
                    p3 = new Vector3(x, y, z);
                    p3n = Vector3.forward;
                }
                else
                {
                    p3 = new Vector3(x, y, z + 1);
                    p3n = Vector3.back;
                }
            }
            else if (block.Rotation == 4)
            {
                int px = x + 1;
                int pz = z + 1;
                p = new Plane(new Vector3(-0.5773502691896258f, -0.5773502691896258f, -0.5773502691896258f),
                    new Vector3(px, py, pz));
                if (direction.x < 0)
                {
                    p1 = new Vector3(x, y, z);
                    p1n = Vector3.right;
                }
                else
                {
                    p1 = new Vector3(x + 1, y, z);
                    p1n = Vector3.left;
                }
                if (direction.y < 0)
                {
                    p2 = new Vector3(x, y - 1, z);
                    p2n = Vector3.up;
                }
                else
                {
                    p2 = new Vector3(x, y, z);
                    p2n = Vector3.down;
                }
                if (direction.z < 0)
                {
                    p3 = new Vector3(x, y, z);
                    p3n = Vector3.forward;
                }
                else
                {
                    p3 = new Vector3(x, y, z + 1);
                    p3n = Vector3.back;
                }
            }
            else
            {
                GameLog.Out("Unsupported rotation " + block.Rotation + ", " + block.itemName);
                return transform_pos;
            }

            float dist;
            if (p.Raycast(dir, out dist))
            {
                Vector3 ray_intersection = dir.GetPoint(dist);
                if (ray_intersection.x < x + 1.01f && ray_intersection.x > x - 0.01f &&
                     ray_intersection.y < y + 0.01f && ray_intersection.y > y - 1.01f &&
                     ray_intersection.z < z + 1.01f && ray_intersection.z > z - 0.01f)
                {
                    return (ray_intersection - vert).magnitude * direction + transform_pos;
                }
            }

            if (LinePlaneIntersection(out intersection, vert, direction, p1n, p1))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }
            if (LinePlaneIntersection(out intersection, vert, direction, p2n, p2))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }
            if (LinePlaneIntersection(out intersection, vert, direction, p3n, p3))
            {
                if (intersection.x < x + 1.01f && intersection.x > x - 0.01f &&
                    intersection.y < y + 0.01f && intersection.y > y - 1.01f &&
                    intersection.z < z + 1.01f && intersection.z > z - 0.01f)
                {
                    return (intersection - vert).magnitude * direction + transform_pos;
                }
            }

            GameLog.Out("Error resolving vertice collision.");
            return transform_pos;
        }
        else
        {
            GameLog.Out("Cannot handle " + block.blockType.ToString() + " block types");
            return transform_pos;
        }
    }

    /// <summary>
    /// Returns true if the voxel and sphere are colliding. Outputs a position opposite the move direction
    /// to resolve the collision.
    /// </summary>
    /// <param name="sphere_position"></param>
    /// <param name="radius"></param>
    /// <param name="block"></param>
    /// <param name="block_pos"></param>
    /// <returns></returns>
    static bool IsSphereColliding(Vector3 sphere_position, float radius, Block b,
        Vector3Int32 block_pos, Vector3 move_direction)
    {
        //This function only works if the center of the sphere is not inside the voxel.

        //radius squared
        float radsqrd = radius * radius;

        if (b.blockType == primitiveBlockType.cube)
        {
            Vector3 bpos = new Vector3(block_pos.x, block_pos.y, block_pos.z);
            Vector3
                v1 = bpos,
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z),
                v3 = new Vector3(bpos.x, bpos.y, bpos.z + 1),
                v4 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1),
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z),
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z),
                v7 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1),
                v8 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);

            /*cube points
            3-------4
           /|      /|
          / |     / |
         1--|----2  |
         |  7----|--8
         | /     | /
         5-------6
            */

            //sphere plane intersection tests, with the restriction that the spheres
            //are outside of the cube
            //east square face
            if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
            {
                if (sphere_position.x < radius + bpos.x + 1) return true;
                else return false;
            }
            //west face
            if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
            {
                if (sphere_position.x > bpos.x - radius) return true;
                else return false;
            }
            //north face
            if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x)
            {
                if (sphere_position.z < radius + bpos.z + 1) return true;
                else return false;
            }
            //south face
            if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x)
            {
                if (sphere_position.z > bpos.z - radius) return true;
                else return false;
            }
            //up face
            if (sphere_position.y > bpos.y && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
            {
                if (sphere_position.y < bpos.y + radius) return true;
                else return false;
            }
            //down face
            if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
            {
                if (sphere_position.y > bpos.y - 1 - radius) return true;
                else return false;
            }

            //we don't have to consider vertices because they are considered as endpoints in the line segment test

            //If the minimum distance from the line segment to the center of the sphere is less than the radius...
            //Then there is an intersection
            if (LineSegmentPointDistanceSqrd(v1,v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v1, v3, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v1, v5, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v4, v3, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v4, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v4, v8, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v6, v5, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v6, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v6, v8, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v7, v5, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v7, v3, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v7, v8, sphere_position) < radsqrd)
                return true;

            return false;
        }
        else if (b.blockType == primitiveBlockType.slope)
        {
            //common points
            Vector3 bpos = new Vector3(block_pos.x, block_pos.y, block_pos.z);

            /*forall rotations, ramp points
            3-------4
           /|      /|
          / 5     / 6
         1--------2
            */

            //point position
            Vector3 v1, v2, v3, v4, v5, v6;

            Vector3 n = b.TopFaceNormal, p1;

            float rx = sphere_position.x - (int)sphere_position.x;
            float ry = sphere_position.y - (int)sphere_position.y == 0.0f ? 1.0f : sphere_position.y - (int)sphere_position.y;
            float rz = sphere_position.z - (int)sphere_position.z;

            //assign variables based on the rotation of this sloped block
            if (b.Rotation == 1)
            {
                v1 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v2 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v3 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                p1 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                //bottom face
                if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.y > bpos.y - 1 - radius) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    ry < rz)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                        ry < rz)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
            }
            else if (b.Rotation == 2)
            {
                v1 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v2 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v3 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v5 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                p1 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                //bottom face
                if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.y > bpos.y - 1 - radius) return true;
                    else return false;
                }
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry < rx)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry < rx)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
            }
            else if (b.Rotation == 3)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v2 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v3 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v4 = bpos;
                v5 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v6 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                p1 = bpos;
                //bottom face
                if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.y > bpos.y - 1 - radius) return true;
                    else return false;
                }
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    ry + rz < 1)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                        ry + rz < 1)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
            }
            else if (b.Rotation == 4)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v2 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v3 = bpos;
                v4 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v6 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                p1 = bpos;
                //bottom face
                if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.y > bpos.y - 1 - radius) return true;
                    else return false;
                }
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry + rx < 1)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry + rx < 1)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
                return false;
            }

            //up face
            Vector3 intersection;
            if (LinePlaneIntersection(out intersection, sphere_position, -n, n, p1))
            {
                if (PointInsideTriangle(v1, v3, v2, intersection) || PointInsideTriangle(v2, v3, v4, intersection))
                {
                    if ((intersection - sphere_position).sqrMagnitude < radsqrd)
                        return true;
                }
            }

            if (LineSegmentPointDistanceSqrd(v1, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v1, v3, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v1, v5, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v4, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v4, v3, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v4, v6, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v6, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v6, v5, sphere_position) < radsqrd)
                return true;


            if (LineSegmentPointDistanceSqrd(v3, v5, sphere_position) < radsqrd)
                return true;

            return false;
        }
        else if (b.blockType == primitiveBlockType.cornerSlope)
        {
            Vector3 bpos = new Vector3(block_pos.x, block_pos.y, block_pos.z);

            Vector3 v1, v2, v3, v4;

            //Plane representing the slanted face
            Vector3 n = b.TopFaceNormal, p1;

            float rx = sphere_position.x - (int)sphere_position.x;
            float ry = sphere_position.y - (int)sphere_position.y == 0.0f ? 1.0f : sphere_position.y - (int)sphere_position.y;
            float rz = sphere_position.z - (int)sphere_position.z;

            /*forall rotations, corner ramp points
                     2
                   / |
                 /   |
               /     |
             1-------3
              `.    /
                 ` 4
            */

            if (b.Rotation == 1)
            {
                v1 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v3 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                p1 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                //bottom face
                if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z
                    && rx + rz > 1)
                {
                    if (sphere_position.y > bpos.y - 1 - radius) return true;
                    else return false;
                }
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    ry < rz)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry < rx)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
            }
            else if (b.Rotation == 2)
            {
                v1 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v2 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v3 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                p1 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                //bottom face
                if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z
                    && rx < rz)
                {
                    if (sphere_position.y > bpos.y - 1 - radius) return true;
                    else return false;
                }
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                        ry < rz)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry + rx < 1)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
            }
            else if (b.Rotation == 3)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v3 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v4 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                p1 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                //bottom face
                if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z
                    && rz < rx)
                {
                    if (sphere_position.y > bpos.y - 1 - radius) return true;
                    else return false;
                }
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    ry + rz < 1)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry < rx)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
            }
            else if (b.Rotation == 4)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v2 = bpos;
                v3 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v4 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                p1 = new Vector3(bpos.x, bpos.y, bpos.z);
                //bottom face
                if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z
                    && rz + rx < 1)
                {
                    if (sphere_position.y > bpos.y - 1 - radius) return true;
                    else return false;
                }
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                        ry + rz < 1)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry + rz < 1)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
                return false;
            }

            //up face
            Vector3 intersection;
            if (LinePlaneIntersection(out intersection, sphere_position, -n, n, p1))
            {
                if (PointInsideTriangle(v4, v1, v2, intersection))
                {
                    if ((intersection - sphere_position).sqrMagnitude < radsqrd)
                    { return true; }
                }
            }

            if (LineSegmentPointDistanceSqrd(v3, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v3, v1, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v3, v4, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v2, v1, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v2, v4, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v1, v4, sphere_position) < radsqrd)
                return true;

            return false;
        }
        else if (b.blockType == primitiveBlockType.sunkenCornerSlope)
        {
            Vector3 bpos = new Vector3(block_pos.x, block_pos.y, block_pos.z);

            /*forall rotations, inside ramp points
            3-------1
            | \    /|
            |   \ / |
            |    2  |
            6----|--7
           /     | /
          5------4
            */

            Vector3 v1, v2, v3, v4, v5, v6, v7;
            Vector3 n = b.TopFaceNormal, p1;

            float rx = sphere_position.x - (int)sphere_position.x;
            float ry = sphere_position.y - (int)sphere_position.y == 0.0f ? 1.0f : sphere_position.y - (int)sphere_position.y;
            float rz = sphere_position.z - (int)sphere_position.z;

            if (b.Rotation == 1)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v3 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v4 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v6 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v7 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                p1 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                        ry < rz)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry < rx)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
                //up face
                if (sphere_position.y > bpos.y && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    rx + rz > 1)
                {
                    if (sphere_position.y < bpos.y + radius) { return true; }
                    else return false;
                }
            }
            else if (b.Rotation == 2)
            {
                v1 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v2 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v3 = bpos;
                v4 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v5 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v6 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v7 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                p1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry + rx < 1)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    ry < rz)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //up face
                if (sphere_position.y > bpos.y && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    rx < rz)
                {
                    if (sphere_position.y < bpos.y + radius) { return true; }
                    else return false;
                }
            }
            else if (b.Rotation == 3)
            {
                v1 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v2 = bpos;
                v3 = new Vector3(bpos.x + 1, bpos.y, bpos.z + 1);
                v4 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                v5 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v7 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                p1 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                        ry + rz < 1)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    ry < rx)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
                //up face
                if (sphere_position.y > bpos.y && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    rz < rx)
                {
                    if (sphere_position.y < bpos.y + radius) { return true; }
                    else return false;
                }
            }
            else if (b.Rotation == 4)
            {
                v1 = bpos;
                v2 = new Vector3(bpos.x, bpos.y, bpos.z + 1);
                v3 = new Vector3(bpos.x + 1, bpos.y, bpos.z);
                v4 = new Vector3(bpos.x, bpos.y - 1, bpos.z + 1);
                v5 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                v6 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z);
                v7 = new Vector3(bpos.x, bpos.y - 1, bpos.z);
                p1 = new Vector3(bpos.x + 1, bpos.y - 1, bpos.z + 1);
                //west face
                if (sphere_position.x < bpos.x && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                        sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
                {
                    if (sphere_position.x > bpos.x - radius) return true;
                    else return false;
                }
                //south face
                if (sphere_position.z < bpos.z && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x)
                {
                    if (sphere_position.z > bpos.z - radius) return true;
                    else return false;
                }
                //east square face
                if (sphere_position.x > bpos.x + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    ry + rz < 1)
                {
                    if (sphere_position.x < radius + bpos.x + 1) return true;
                    else return false;
                }
                //north face
                if (sphere_position.z > bpos.z + 1 && sphere_position.y < bpos.y && sphere_position.y > bpos.y - 1 &&
                    sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    rx + ry < 1)
                {
                    if (sphere_position.z < radius + bpos.z + 1) return true;
                    else return false;
                }
                //up face
                if (sphere_position.y > bpos.y && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                    sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z &&
                    rx + rz < 1)
                {
                    if (sphere_position.y < bpos.y + radius) {return true; }
                    else return false;
                }
            }
            else
            {
                GameLog.Out("Unsupported rotation " + b.Rotation + ", " + b.itemName);
                return false;
            }

            //down face
            if (sphere_position.y < bpos.y - 1 && sphere_position.x < bpos.x + 1 && sphere_position.x > bpos.x &&
                sphere_position.z < bpos.z + 1 && sphere_position.z > bpos.z)
            {
                if (sphere_position.y > bpos.y - 1 - radius) return true;
                else return false;
            }

            //slanted up face
            Vector3 intersection;
            if (LinePlaneIntersection(out intersection, sphere_position, -n, n, p1))
            {
                if (PointInsideTriangle(v5, v3, v2, intersection))
                {
                    if ((intersection - sphere_position).sqrMagnitude < radsqrd)
                        {return true; }
                    
                }
            }

            if (LineSegmentPointDistanceSqrd(v1, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v1, v3, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v1, v7, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v4, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v4, v5, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v4, v7, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v6, v3, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v6, v5, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v6, v7, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v5, v2, sphere_position) < radsqrd)
                return true;
            if (LineSegmentPointDistanceSqrd(v5, v3, sphere_position) < radsqrd)
                return true;

            if (LineSegmentPointDistanceSqrd(v3, v2, sphere_position) < radsqrd)
                return true;

            return false;
        }
        else
        {
            GameLog.Out("Error, unsupported block type. Cannot test for and resolve collisions.");
            return false;
        }
    }

    /// <summary>
    /// Cast against the voxel terrain and unity physics objects
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="origin"></param>
    /// <param name="dist"></param>
    /// <param name="col_hit"></param>
    /// <param name="world"></param>
    /// <param name="layermask"></param>
    /// <returns></returns>
    public static bool VoxPhysRayCast(Vector3 direction, Vector3 origin, float dist,
        out combined_collision_info col_hit, WorldData world, int layermask)
    {
        //variables
        col_hit = default(combined_collision_info);
        collision_info col; RaycastHit rayc; float vox_distsq = 0.0f, phys_distsq = 0.0f;

        //voxel cast
        if (voxel_cast(direction, origin, dist, out col, world))
            vox_distsq = (origin - col.exact_hit_position).sqrMagnitude;

        //unity raycast
        if (Physics.Raycast(origin, direction.normalized, out rayc, dist, layermask))
            phys_distsq = (origin - rayc.point).sqrMagnitude;

        //neither hit
        if (vox_distsq == 0 && phys_distsq == 0)
            return false;

        //unity physics hit first
        if (phys_distsq < vox_distsq)
        {
            col_hit = new combined_collision_info(false, true, default(collision_info), rayc);
        }
        //voxel physics hit first
        else
        {
            col_hit = new combined_collision_info(true, false, col, default(RaycastHit));
        }

        return true;
    }
}

/// <summary>
/// Information pertaining to a voxel cast collision hit
/// </summary>
public struct collision_info
{
    /// <summary>
    /// the block hit
    /// </summary>
    public Block block_hit;
    /// <summary>
    /// The exact position hit
    /// </summary>
    public Vector3 exact_hit_position;
    /// <summary>
    /// voxel coordinates of hit
    /// </summary>
    public Vector3Int32 voxel_hit_position;
    /// <summary>
    /// Which face of the voxel was hit. **Extra note: A face value of (-1,-1,-1) indicates the collision
    /// occured in the same block from when the raycast was casted
    /// </summary>
    public Vector3Int32 face_hit;
    /// <summary>
    /// The hit normal.
    /// </summary>
    public Vector3 hit_normal;
    /// <summary>
    /// The previous voxel (empty) before this voxel was hit. 
    /// </summary>
    public Vector3Int32 prev_voxel_position;

    public collision_info(Block b, Vector3 hit_pos, Vector3Int32 v_pos,Vector3Int32 f_hit, Vector3 hit_n, Vector3Int32 prev_vox)
    {
        block_hit = b;
        exact_hit_position = hit_pos;
        voxel_hit_position = v_pos;
        face_hit = f_hit;
        hit_normal = hit_n;
        prev_voxel_position = prev_vox;
    }
}

internal struct collider_info
{
    public GameObject col;
    public int use_count;

    public int rotation_number;

    public collider_info(GameObject obj, int count, int rotation)
    {
        col = obj;
        use_count = count;
        rotation_number = rotation;
    }
}

/// <summary>
/// Extended plane.
/// </summary>
public struct Plane_ex
{
    /// <summary>
    /// Contained plane
    /// </summary>
    public Plane plane;
    /// <summary>
    /// Some point on the plane.
    /// </summary>
    public Vector3 v1;

    public Plane_ex(Plane p, Vector3 p1)
    {
        plane = p;
        v1 = p1;
    }
}

/// <summary>
/// RayCast voxel and unity colliders result
/// </summary>
public struct combined_collision_info
{
    /// <summary>
    /// Was this a unity physics collision? (Collision with a unity collider)
    /// </summary>
    public bool isPhysicsCollision;
    /// <summary>
    /// Was this a voxel collision? Otherwise it is a unity physics collision
    /// </summary>
    public bool isVoxelCollision;
    /// <summary>
    /// Voxel collision info
    /// </summary>
    public collision_info c;
    /// <summary>
    /// Unity RayCast hit info
    /// </summary>
    public RaycastHit r;

    public combined_collision_info(bool vox, bool uphys, collision_info ci, RaycastHit rh)
    {
        isVoxelCollision = vox;
        c = ci;
        isPhysicsCollision = uphys;
        r = rh;
    }
}