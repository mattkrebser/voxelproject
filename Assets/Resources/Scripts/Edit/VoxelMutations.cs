using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Threading;

public class VoxelMutations: MonoBehaviour
{
    //references for convienience
    /// <summary>
    /// World being modified
    /// </summary>
    WorldData this_world;
    /// <summary>
    /// Voxel Data Reference
    /// </summary>
    RegionData[,] voxels;
    /// <summary>
    /// Blocks contained in the world 'this_world'
    /// </summary>
    List<L> blocks;
    /// <summary>
    /// Reference to this world's chunk manager
    /// </summary>
    ChunkManager cm;

    /// <summary>
    /// Represents a Change in the Voxel Terrain that will take place.
    /// </summary>
    public class VoxelMutation
    {
        public Block block;
        public Vector3Int32 position;
        public Action<VoxelMutation> function;
        public blockID block_id;

        public VoxelMutation(Block b, Vector3Int32 v, Action<VoxelMutation> f, blockID b_id)
        {
            block = b;
            position = v;
            function = f;
            block_id = b_id;
        }
    }

    /// <summary>
    /// Initialize.
    /// </summary>
    /// <param name="w"></param>
    public void Init(WorldData w)
    {
        this_world = w;
        voxels = this_world.regionData;
        blocks = w.blocks;
        cm = GetComponent<ChunkManager>();

        if (blocks != null) { }
    }

    /// <summary>
    /// Update the voxel at the position with the input block
    /// </summary>
    /// <param name="add_this"></param>
    /// <param name="position"></param>
    public void AddBlock(Block add_this, Vector3Int32 position, blockID b)
    {
        Action<VoxelMutation> f = VoxelUpdateSingleBlock;
        cm.AddVoxelUpdate(new VoxelMutation(add_this, position, f, b));
    }

    //*****Please Note******
    /*All of the functions that Modify the Voxel Data are Meant to be called on the Draw Thread,
      Please do not call them on the main thread.*/
    /*Further More, the functions assume that all of the chunks being updated exist and have
      been drawn.*/

    /// <summary>
    /// Apply a single voxel Change.
    /// </summary>
    /// <param name="v"></param>
    private void VoxelUpdateSingleBlock(VoxelMutation v)
    {
        int rx = RegionData.GlobalCoordToRegionCoord(v.position.x);
        int rz = RegionData.GlobalCoordToRegionCoord(v.position.z);

        int cx = RegionData.GlobalCoordToRegionChunkCoord(v.position.x);
        int cy = RegionData.GlobalCoordToRegionChunkCoord(v.position.y);
        int cz = RegionData.GlobalCoordToRegionChunkCoord(v.position.z);

        int x = RegionData.GlobalCoordToLocalCoord(v.position.x);
        int y = RegionData.GlobalCoordToLocalCoord(v.position.y);
        int z = RegionData.GlobalCoordToLocalCoord(v.position.z);

        if (x < 0 || y < 0 || z < 0)
            return;

        //get the chunk
        DrawChunks d = voxels[rx, rz].chunks[cx, cy, cz];
        //set the voxel if there is no collision issue
        if (!voxels[rx, rz].IsColliding(v.position.x, v.position.y, v.position.z, this_world))
        {
            d.SetVoxel(v.block_id, x, y, z);
            //redraw
            d.Draw();
        }
    }
}
