using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RegionData
{
	/// <summary>
    /// Holds chunk positions and sizes on the disk. (The disk table)
    /// </summary>
	public byte[] diskBuffer;
    /// <summary>
    /// Voxel data stored in a one dimensial array and in compacted format.
    /// This may not be the most recent data.
    /// </summary>
    public byte[] diskVoxelData;

	/// <summary>
    /// a 8x8x32x32 array to represent the height
    /// of each 'light beam' source in the region.
    /// </summary>
	public byte[,,,] naturalLight;


	//each DrawChunk class is held in here
	/********************************************************************/
	//Do not modify chunks, use AddChunk or RemoveChunk. It is public so we can access it
	//easier without making modifications
	/********************************************************************/
	private DrawChunks[,,] _chunks;
    /// <summary>
    /// Each chunk has a DrawChunks Object that will draw the chunk in question
    /// </summary>
    public DrawChunks[,,] chunks
    {
        get
        {
            return _chunks;
        }
    }

	/// <summary>
    /// Size (in chunks) of this region
    /// </summary>
	private int regionSize;

    /// <summary>
    /// The 'loadValue' for each chunk. 0 means the chunk consists entirely of air,
    /// 1 means the chunk contains air and solid voxels, > 1 means the chunk is completely solid
    /// </summary>
	private byte[,,] loadValues;

    /// <summary>
    /// Common Values used in the LoadValues Array for determined how to use a chunk
    /// </summary>
    public enum LoadValues : byte
    {
        UnInitialized = 0,
        AirChunk = 255,
        HasNonEmptyVoxels = 1,
        SolidChunk = 2
    }

    /// <summary>
    /// Returns 0 if LoadValue.AirChunk, or LoadValue.UnInitialized, returns 1 otherwise
    /// </summary>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <returns></returns>
    public byte GetLoadValue(int cx, int cy, int cz)
    {
        return loadValues[cx, cy, cz] == 255 || loadValues[cx, cy, cz] == 0 ? (byte)0 : (byte)1;
    }

    /// <summary>
    /// Return enum load value representation
    /// </summary>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <returns></returns>
    public LoadValues GetRealLoadValue(int cx, int cy, int cz)
    {
        return (LoadValues)loadValues[cx, cy, cz];
    }

    /// <summary>
    /// Set a load value at the input position
    /// </summary>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <param name="val"></param>
    public void SetLoadValue(int cx, int cy, int cz, byte val)
    {
        loadValues[cx, cy, cz] = val == 0 ? (byte)255 : val;
    }

    /// <summary>
    /// Set a load value at the input position
    /// </summary>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <param name="val"></param>
    public void SetRealLoadValue(int cx, int cy, int cz, LoadValues val)
    {
        loadValues[cx, cy, cz] = (byte)val;
    }

    /// <summary>
    /// Length of the loadvalues array
    /// </summary>
    public int LoadValLength
    {
        get
        {
            return loadValues.Length;
        }
    }
    /// <summary>
    /// Returns the actual load value at the input position
    /// </summary>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <returns></returns>
    public byte GetActualLoadValue(int cx, int cy, int cz)
    {
        return loadValues[cx, cy, cz];
    }


#if UNITY_EDITOR
    public GameObject g;
#endif

    //number of chunks loaded in this region
    private int chunkCount = 0;
	
	//initialize new region (size is always < 8 and >= 0)
	public RegionData(int size, ThreadManager TM, WorldData l, int rx, int rz){
		naturalLight = new byte[size,size,32,32];
		_chunks = new DrawChunks[size, 8, size];
		loadValues = new byte[size, 8, size];
		regionSize = size;

        if (regionSize > 8)
            GameLog.OutputToWarningLog("Warning, created a region that is too big");
#if UNITY_EDITOR
        if (GameLog.VISUALIZE_CHUNK_ARRAYS)
            TM.QueueOnMain (() => {g = (GameObject)WorldData.Instantiate
                (l.gameObject.GetComponent<ChunkManager>().test1,
                new Vector3(rx*8*32 + 128,-5,rz*8*32 + 128),Quaternion.identity);return false;}, l.worldName, false);
#endif
    }

	/// <summary>
    /// Initializes the loadValues array, the function is called
    /// after the diskbuffer has already been filled
    /// </summary>
	public void initLoadValues(){
		for (int x = 0; x < regionSize; x++) {
			for (int y = 0; y < 8; y++){
				for (int z = 0; z < regionSize; z++){
                    byte b;
					loadValues[x,y,z] = (b = diskBuffer[(x*8*regionSize + z*8 + y) * 4]) == 0 ? (byte)255 : b;
				}
			}
		}
	}

	public void delete(ThreadManager tm, string worldName){
#if UNITY_EDITOR
        if (GameLog.VISUALIZE_CHUNK_ARRAYS)
            tm.QueueOnMain(()=>{WorldData.Destroy(g);return false;},worldName,false);
#endif
    }

    /// <summary>
    /// Add the input DrawChunks object to this RegionSet
    /// </summary>
    /// <param name="d"></param>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <returns></returns>
	public bool AddChunk(DrawChunks d, int cx, int cy, int cz){

		if (chunks [cx, cy, cz] != null || cx < 0 || cy < 0 || cz < 0 || cy >= 8 || cx >= d.mRegionSize || cz >= d.mRegionSize){
            GameLog.VoxelStatusOutput("Failed to add a chunk " + cx.ToString() + ", " + cy.ToString() + ", " + cz.ToString());
            return false;
		}
		else{
			_chunks[cx,cy,cz] = d;
			chunkCount++;
			return true;
		}

	}

    /// <summary>
    /// Remove the chunk at the input region-relative coordinates
    /// </summary>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <returns></returns>
	public bool RemoveChunk(int cx, int cy, int cz){

		if (chunks [cx, cy, cz] == null || cx < 0 || cy < 0 || cz < 0 || cy >= 8 || cx >= regionSize || cz >= regionSize){
            GameLog.VoxelStatusOutput("Failed to remove a chunk " + cx.ToString() + ", " + cy.ToString() + ", " + cz.ToString());
			return false;
		}
		else{
			_chunks[cx,cy,cz] = null;
			chunkCount--;
			return true;
		}
	}

    /// <summary>
    /// Returns the number of chunks that currently exist in this region
    /// </summary>
    /// <returns></returns>
	public int GetCount(){
		return chunkCount;
	}

    /// <summary>
    /// Determine if there is a voxel or object conflict at the input global coordinates.
    /// Only good for intiger coordinates. This is for voxel allocation and should not be used for physics.
    /// </summary>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <returns></returns>
    public bool IsColliding(int gx, int gy, int gz, WorldData world)
    {
        int rx = GlobalCoordToRegionCoord(gx);
        int rz = GlobalCoordToRegionCoord(gz);

        int cx = GlobalCoordToRegionChunkCoord(gx);
        int cy = GlobalCoordToRegionChunkCoord(gy);
        int cz = GlobalCoordToRegionChunkCoord(gz);

        int x = GlobalCoordToLocalCoord(gx);
        int y = GlobalCoordToLocalCoord(gy);
        int z = GlobalCoordToLocalCoord(gz);

        if (!IsExistingChunkCoordinate(rx, rz, cx, cy, cz, world))
            return false;

        byte id1 = world.regionData[rx, rz].chunks[cx, cy, cz].voxelData[x, y, z];
        byte id2 = world.regionData[rx, rz].chunks[cx, cy, cz].data[x, y, z];

        if (world.blocks[id1].l[id2].blockType != primitiveBlockType.empty)
            return false;

        return false;
    }

    /// <summary>
    /// Returns true if the input coordinate is in bounds.
    /// </summary>
    /// <param name="rx"></param>
    /// <param name="rz"></param>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <param name="world"></param>
    /// <returns></returns>
    public static bool IsValidChunkCoordinate(int rx, int rz, int cx, int cy, int cz, WorldData world)
    {
        if (rx >= world.worldSize || rz >= world.worldSize || cx >= world.minimizedRegionSize ||
            cy >= 8 || cz >= world.minimizedRegionSize || rx < 0 || rz < 0 || cx < 0 || cy < 0 || cz < 0)
            return false;
        return true;
    }

    /// <summary>
    /// Returns true if a chunk exists at the input chunk coordinates
    /// </summary>
    /// <param name="rx"></param>
    /// <param name="rz"></param>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <param name="world"></param>
    /// <returns></returns>
    public static bool IsExistingChunkCoordinate(int rx, int rz, int cx, int cy, int cz, WorldData world)
    {
        if (!IsValidChunkCoordinate(rx, rz, cx, cy, cz, world))
            return false;
        if (world.regionData != null && world.regionData[rx, rz] != null &&
            (world.regionData[rx, rz].chunks[cx, cy, cz] != null))
            return true;
        return false;
    }

    /// <summary>
    /// Returns true if the input chunk coordinate is a null non-air chunk.
    /// If the input region is not loaded, then an error could occur until the proper load values are updated
    /// </summary>
    /// <param name="rx"></param>
    /// <param name="rz"></param>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <param name="world"></param>
    /// <returns></returns>
    public static bool IsNullNonAirChunk(int rx, int rz, int cx, int cy, int cz, WorldData world)
    {
        if (!IsValidChunkCoordinate(rx, rz, cx, cy, cz, world))
            return false;
        if (world.regionData == null || world.regionData[rx,rz] == null || world.regionData[rx,rz].chunks == null ||
            world.regionData[rx, rz].loadValues == null) return true;
        if (world.regionData[rx, rz].chunks[cx, cy, cz] == null && world.regionData[rx,rz].loadValues[cx,cy,cz] != (byte)LoadValues.AirChunk) return true;
        return false;
    }

    /// <summary>
    /// Inputs the size of the world in chunks, and outputs the size of the world
    /// in regions.
    /// </summary>
    /// <param name="chunk_length"></param>
    /// <returns></returns>
    public static int WorldLengthInChunksToWorldLengthInRegions(int chunk_length)
    {
        return (chunk_length - 1) / 8 + 1;
    }

    /// <summary>
    /// Inputs a single global coordinate, and outputs a region relative chunk coordinate.
    /// ie (0 to 7)
    /// </summary>
    /// <param name="val"></param>
    /// <returns></returns>
    public static int GlobalCoordToRegionChunkCoord(int val)
    {
        return (val / 32) % 8;
    }

    /// <summary>
    /// Inuts a single global coordinate and outputs a coordinate relative to a chunk.
    /// ie 0 to 31
    /// </summary>
    /// <param name="val"></param>
    /// <returns></returns>
    public static int GlobalCoordToLocalCoord(int val)
    {
        return val % 32;
    }

    /// <summary>
    /// Inputs a global coordinate and outputs the coordinate as a region coordinate
    /// </summary>
    /// <param name="val"></param>
    /// <returns></returns>
    public static int GlobalCoordToRegionCoord(int val)
    {
        return val / 256;
    }

    /// <summary>
    /// Return the Chunk at the input global coordinates. Returns null if it doesn't exist.
    /// </summary>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <param name="world"></param>
    /// <returns></returns>
    internal static DrawChunks GetChunk(int gx, int gy, int gz, WorldData world)
    {
        int rx = GlobalCoordToRegionCoord(gx);
        int rz = GlobalCoordToRegionCoord(gz);

        int cx = GlobalCoordToRegionChunkCoord(gx);
        int cy = GlobalCoordToRegionChunkCoord(gy);
        int cz = GlobalCoordToRegionChunkCoord(gz);

        if (IsExistingChunkCoordinate(rx, rz, cx, cy, cz, world))
        {
            return world.regionData[rx, rz].chunks[cx, cy, cz];
        }

        return null;
    }
}
















