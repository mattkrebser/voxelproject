using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class ChunkColumnLoad
{
    /// <summary>
    /// WorldData ia a data holding object that contains almost all of the voxel
    /// and other relevent data for a world
    /// </summary>
	private WorldData l;
    //Chunk Position in chunk coordinates, relative to world
	private int chunkX = 0;
    //Position in chunk coordinates, relative to world
	private int chunkZ = 0;
    //Load a ChunkColumn somewhere around this point
	private Vector3 ls;
    //set to true once a suitable column of chunks can be loaded in
	private bool foundColumn = false;
	ChunkManager cm;
	MeshObjectManager MOM;
	ThreadManager TM;
    /// <summary>
    /// list of blocks
    /// </summary>
	List<L> b;
    /// <summary>
    /// list of load source positions
    /// </summary>
	List<Vector3> LSs;
    /// <summary>
    /// current load source transform that is loading this column
    /// </summary>
	Transform LSD;
    /// <summary>
    /// world name
    /// </summary>
    string worldName;

	private static int NoFoundChunk = 0;

	//most of the variables here just need to be passed through to DrawChunks
	public ChunkColumnLoad( WorldData lc, Vector3 LS, ChunkManager CM, MeshObjectManager m, List<L> ll, 
	                       ThreadManager tm, List<Vector3> lss, Transform lsd, string wn){
		l = lc;
		ls = LS;
		cm = CM;
		MOM = m;
		b = ll;
		TM = tm;
		LSs = lss;
		LSD = lsd;
        worldName = wn;
	}

	public bool loadChunks()
	{
		ls.x = (int)(ls.x / 32);
		ls.z = (int)(ls.z / 32);

		int cI = l.chunkPos.Length-1;
			
		int x, z;

		if (NoFoundChunk > 0) {
			NoFoundChunk--;
		}
		else
			{
			//loops through sorrounding chunks in a spiral until a close one(that hasn't been initialized) is found
			//This For loop starts at previous found chunk unless the load source(player) moves to a different chunk
			for (int i = cI; i > l.renderDistIncrement[l.renderDistance]; i--){
				//assign x and z chunk coordinates of the next chunk to check
				x = (int)(ls.x + l.chunkPos[cI].x);z = (int)(ls.z + l.chunkPos[cI].y);
				cI--;
				//If the chunk coordinates are valid
				if (x > -1 && x < l.regionSize && z > -1 && z < l.regionSize)
				{

					for (int y = 0; y < 8; y++){
						//If the region exists and the chunk at this location does not exist
						if (l.regionData.Length != 0 && !System.Object.ReferenceEquals(l.regionData[x/8,z/8], null) && 
						    System.Object.ReferenceEquals(l.regionData[x/8,z/8].chunks[x%8,y,z%8],null) && 
						    l.regionData[x/8,z/8].GetActualLoadValue(x%8,y,z%8) != 255 && 
						    cm.WithinDistanceOfLoadSource((int)ls.x,(int)ls.z,LSs)){

							chunkX = x;chunkZ = z;
							foundColumn = true;
						}
						//if the region exists and the chunk at this location already exists
						else if (l.regionData.Length != 0 && !System.Object.ReferenceEquals(l.regionData[x/8,z/8],null) && 
						        !System.Object.ReferenceEquals( l.regionData[x/8,z/8].chunks[x%8,y,z%8],null)){

							continue;
						}
						else if (cm.WithinDistanceOfLoadSource((int)ls.x,(int)ls.z,LSs) &&
						         !System.Object.ReferenceEquals(l.regionData[x/8,z/8], null) &&
						         l.regionData[x/8,z/8].GetLoadValue(x %8,y,z%8) != 1){

							continue;
						}
						else if (cm.WithinDistanceOfLoadSource((int)ls.x,(int)ls.z,LSs) &&
						         System.Object.ReferenceEquals(l.regionData[x/8,z/8], null)){

							chunkX = x;chunkZ = z;
							foundColumn = true;
						}
					}

					if (foundColumn)
						break;
				}
			}
		}

		//make this wait 40 frames to load
		if (cI == l.renderDistIncrement[l.renderDistance]){
			NoFoundChunk = 40;
		}
			
		LoadChunkColumn();
		return false;
	}

	private bool LoadChunkColumn(){

        //If no column was found, just return
        if (!foundColumn)
			return foundColumn;

        //If set to true, we will generate all chunks in this column instead of trying to load
        //them from the disk
        bool region_not_saved = false;

        //load values for each chunk.
        byte[] load_value = new byte[8];
		
		//used to get the X and Z Coordinates of the region the chunk column to be loaded is in
		int RX = chunkX / 8;
		int RZ = chunkZ / 8;

        int diskBufferPos = 0, cx = chunkX, cz = chunkZ;
		byte[][,,] dataHolder = new byte[24][,,];

        ChunkColumnGen chunk_gen_ref = null;
		
		//Adjust chunkX and chunkZ coordinates to their region-Relative positions
		if (RX != 0)
			chunkX = chunkX % 8;
		if (RZ != 0)
			chunkZ = chunkZ % 8;

		
		//If we have not created this region yet, initialize it
		if (l.regionData[RX,RZ] == null){
            //Path that the filestream will open on
            string columnDataPath;

            //Get columndataPath, which is the location of the data of the column we will load
            columnDataPath = FileClass.GetRegionDataPath(l.worldName, RX, RZ, l.regionSize);

            //Make a new region
            l.regionData[RX, RZ] = new RegionData(l.minimizedRegionSize, TM, l, RX, RZ);

            if (!File.Exists(columnDataPath))
            {
                region_not_saved = true;
            }
            else
            {
                //open new file stream/binary reader
                FileStream getChunkData = new FileStream(columnDataPath, FileMode.Open, FileAccess.Read);
                BinaryReader columnReader = new BinaryReader(getChunkData);
                l.regionData[RX, RZ].diskBuffer = new byte[l.minimizedRegionSize * l.minimizedRegionSize * 32];
                l.regionData[RX, RZ].diskBuffer = columnReader.ReadBytes(l.regionData[RX, RZ].diskBuffer.Length);
                l.regionData[RX, RZ].initLoadValues();
                l.regionData[RX, RZ].diskVoxelData = columnReader.ReadBytes((int)columnReader.BaseStream.Length -
                    (int)columnReader.BaseStream.Position);
                //close
                columnReader.Close();
                getChunkData.Close();
            }
        }
        else if (l.regionData[RX,RZ].diskBuffer == null && l.regionData[RX,RZ].diskVoxelData == null)
        {
            region_not_saved = true;
        }

		
		//For every chunk in the column
		for (int i = 0; i < 8; i++)
        {
			//global coordinate of chunk in y-direction
			int ChunkYGlobal = i * 32;

            byte[] chunk_data = l.regionData[RX, RZ].diskVoxelData;

            //position of chunk in the look up table
            diskBufferPos = chunkX*8*l.minimizedRegionSize*4+chunkZ*8*4+i*4;
            //If the loadValue stored for this chunk is equal to one (the chunk voxels are a mixture of 0 and nonzero)
            if (region_not_saved || l.regionData[RX,RZ].diskBuffer[diskBufferPos] == 1)
            {
                //get the offset of the chunk (in bytes) on the disk
                int chunkOffset = 0, chunkLength = 0;

                if (!region_not_saved)
                {
                    if (diskBufferPos != 0)
                        chunkOffset = getInt(0, l.regionData[RX, RZ].diskBuffer[diskBufferPos - 3],
                            l.regionData[RX, RZ].diskBuffer[diskBufferPos - 2],
                            l.regionData[RX, RZ].diskBuffer[diskBufferPos - 1]);
                    //Get Length of the chunk Data Array on the disk
                    //The length is equal to [next offset] - [current offset]
                    if ((diskBufferPos + 4) < l.regionData[RX, RZ].diskBuffer.Length)
                        chunkLength = getInt(0, l.regionData[RX, RZ].diskBuffer[diskBufferPos + 1],
                            l.regionData[RX, RZ].diskBuffer[diskBufferPos + 2],
                            l.regionData[RX, RZ].diskBuffer[diskBufferPos + 3]) - chunkOffset;
                }

                //the chunklength is equal to zero when the chunk has never been written to the disk
                if (chunkLength == 0 || region_not_saved)
                {
                    //chunk data structures
                    byte[,,] chunkdat = new byte[32, 32, 32];
                    byte[,,] secondarydat = new byte[32, 32, 32];

                    if (chunk_gen_ref == null)
                        chunk_gen_ref = new ChunkColumnGen(chunkX, chunkZ, l.regionSize, l.ActiveBiome, l.blocks);
                    //generate the chunk
                    load_value[i] = chunk_gen_ref.FillDataArraysAtChunk(cx * 32, i * 32, cz * 32, out chunkdat, out secondarydat);

                    //if the chunk generated was not an air chunk
                    if (load_value[i] != 0)
                    {
                        //hold references to data inside dataHolder
                        dataHolder[i * 2] = chunkdat;
                        dataHolder[i * 2 + 1] = secondarydat;
                    }
                }
                else
                {
                    //chunk data structures, holds good stuff
                    byte[,,] chunkdat = new byte[32, 32, 32];
                    byte[,,] secondarydat = new byte[32, 32, 32];

                    //hold references to data inside dataHolder
                    dataHolder[i * 2] = chunkdat;
                    dataHolder[i * 2 + 1] = secondarydat;

                    //assign load value
                    load_value[i] = 1;

                    //Seek to position on disk, might need to switch to a streamwriter/reader
                    //if issues occur
                    int start = chunkOffset;

                    //used for algorithm below
                    byte blockID = 0, secondId = 0;
                    int chunkBufferPos = start, n = 0;

                    //loop through the data >:)
                    for (int y = 31; y > -1; y--)
                    {
                        for (int x = 0; x < 32; x++)
                        {
                            for (int z = 0; z < 32; z++)
                            {
                                //assign ID's
                                if (n <= 0)
                                {
                                    if (chunkBufferPos < chunkLength + start)
                                    {
                                        blockID = chunk_data[chunkBufferPos];
                                        secondId = chunk_data[chunkBufferPos + 3];
                                        n = getInt(0, 0, chunk_data[chunkBufferPos + 1], chunk_data[chunkBufferPos + 2]);
                                        chunkBufferPos += 4;
                                    }
                                }

                                //ignore zeros which is about 2/3 of the typical chunk
                                if (blockID != 0)
                                {
                                    chunkdat[x, y, z] = blockID;
                                    secondarydat[x, y, z] = secondId;

                                    //Fill natural light array
                                    if (!l.blocks[blockID].l[secondId].isTransparent)
                                    {
                                        if (1 + y + ChunkYGlobal > l.regionData[RX, RZ].naturalLight[chunkX, chunkZ, x, z])
                                            l.regionData[RX, RZ].naturalLight[chunkX, chunkZ, x, z] = (byte)(y + ChunkYGlobal + 1);
                                    }
                                }

                                n--;

                            }//endfor
                        }//end for
                    }//end for
                }//end else

			}//end if loadvalue == 1

		}//end for
        for (int n = 0; n < 8; n++)
        {
            l.regionData[RX, RZ].SetLoadValue(cx % 8, n, cz % 8, load_value[n]);
        }
        //Create drawchunks objects
        for (int n = 0; n < 16; n += 2)
        {
            if (dataHolder[n] != null && dataHolder[n + 1] != null)
            {

                //make new chunk script
                DrawChunks newscript = new DrawChunks(dataHolder[n], dataHolder[n + 1], null,
                                                    cx, n / 2, cz, l.regionSize, l, MOM, b);
                l.regionData[RX, RZ].AddChunk(newscript, chunkX, n / 2, chunkZ);

                //Add this chunk to disposal lists
                TM.AddToDrawThread(() =>
                {
                    LoadSourceData loadSD = cm.FindLoadSourceData(LSD);
                    if (loadSD != null && LSD != null)
                    {
                        loadSD.AddChunk(newscript);
                    }
                    else
                    {
                        l.regionData[RX, RZ].RemoveChunk(chunkX, n / 2, chunkZ);
                    }
                    return false;
                }, worldName, false);
            }
        }

		return false;
	}

    //this function returns ints stored as bytes
    int getInt(byte byte0, byte byte1, byte byte2, byte byte3)
    {
        int zero, one, two, three;

        zero = (byte0 << 24);
        one = (byte1 << 16);
        two = (byte2 << 8);
        three = (byte3);

        return zero | one | two | three;
    }
}

