using UnityEngine;
using System.Collections;

[System.Serializable]
public class BlockMaterial
{
	public Texture2D texture;
	[Tooltip("Min space in vertices to allow another batch")]
	public float minBatchSpacePercent = 0.5f;
	[Tooltip("Chunks allowed per Batch")]
	public int maxSharedChunksPerMesh = 5;
	[Tooltip("Batching Distance in chunks")]
	public int DistanceBias = 1;
	[Tooltip("Don't return or add Spaces at the end")]
	public string texture_name;
	[Tooltip("texture is inside resources folder")]
	public bool BaseTexture = false;

	public BlockMaterial(float minB, int maxShared, int distanceB, string tex_name, bool baseTex){
		minBatchSpacePercent = minB;
		maxSharedChunksPerMesh = maxShared;
		DistanceBias = distanceB;
		texture_name = tex_name;
		BaseTexture = baseTex;
	}

	public BlockMaterial(){}
}

