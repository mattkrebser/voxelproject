﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Threading;

/*
    Notes on some terminology and coordinates sizes:

        chunk:  32 x 32 x 32 voxel spaces
        region: 8 x 8 x 8 chunks
        world:  ## x ## regions

        
        examples:
        -Global : exact intiger coordinates relative to a specific world.
        -Region : The intiger coordinates of a region relative to the current world
            and number of regions. Regions are 256x256 voxels. A region coordinate of
            3,3 is at 3 x 256, 3 x 256 in global coordinates.
        -Global Chunk : Intiger coordinates of a chunk relative to the current world
            and number of chunks. A Chunk at 4,3,1 is at
            4 x 32, 3 x 32, 1 x 32 in global coordinates.
        -Local Chunk : Intiger coordinates of a chunk relative to its parent region.
            A chunk located at 1,1,1 is at:
            1 x 32 + global_region_coord, 1 x 32 + global_region_coord, 1 x 32 + global_region_coord
        -Local : Intiger coordinates relative to voxels inside a single chunk.
            31,31,31 is : 31 + global_chunk_coord, 31 + global_chunk_coord, 31 + global_chunk_coord

    *Note on indivdual voxel rendering:
        a voxel intiger coord x,y,z makes a box with (x=0,y=0,z=0) as the origin
        to the opposite corner (x + 1, y - 1, z + 1)

*/


/// <summary>
/// World Initializer and mahor data container for voxel information.
/// </summary>
public class WorldData: MonoBehaviour {
	//directory path (it's to a specific file in the directory, but we chop that part off)
	private string _datapath;
	private int _regionSize, _minimizedRegionSize, _worldSize;

    /// <summary>
    /// LoadingCircle object, holds data tables. Please see LoadingCircle.cs
    /// </summary>
	[System.NonSerialized]
	public LoadingCircle LoadingCoords;
    /// <summary>
    /// see LoadingCircle.cs
    /// </summary>
    public Vector2[] chunkPos;
    /// <summary>
    /// Holds 32 values where each different render increment is held... See LoadingCircle.cs
    /// </summary>
    public int[] renderDistIncrement;
	//Data path for voxel data, name of world being used
	private string _worldName;
	/// <summary>
    /// Array containing highest resolution voxel data for this world.
    /// </summary>
	public RegionData[,] regionData;

	[Tooltip("Radius(In Chunks) around load Transforms to load chunks")]
	//render distance
	public int renderDistance = 8;

    /// <summary>
    /// List of blocks being used on this world
    /// </summary>
	public List<L> blocks;

    /// <summary>
    /// Reference to the world manager
    /// </summary>
	internal WorldManager w;

    /// <summary>
    /// Colliders get parented to this gameobject
    /// </summary>
    [System.NonSerialized]
    public GameObject collider_parent;
    /// <summary>
    /// Chunks Get parented to this gameobject
    /// </summary>
    [System.NonSerialized]
    public GameObject chunk_parent;

    /// <summary>
    /// Current Biome being used to generate chunks.
    /// </summary>
    [System.NonSerialized]
    public WorldBiomes ActiveBiome;

    /// <summary>
    /// Size of the world in chunks
    /// </summary>
    public int regionSize
    {
        get
        {
            return _regionSize;
        }
    }
    /// <summary>
    /// Size of the world in regions
    /// </summary>
    public int worldSize
    {
        get
        {
            return _worldSize;
        }
    }
    /// <summary>
    /// x and z size per region in chunks. Y axis is always 8 chunks long
    /// </summary>
    public int minimizedRegionSize
    {
        get
        {
            return _minimizedRegionSize;
        }
    }
    /// <summary>
    /// Path to this world in our Saves folder
    /// </summary>
    public string datapath
    {
        get
        {
            return _datapath;
        }
    }
    /// <summary>
    /// Name of this world.
    /// </summary>
    public string worldName
    {
        get
        {
            return _worldName;
        }
    }

    /// <summary>
    /// First out of bounds x or z coordinate. Largest acceptable is maxX - 1 or maxZ - 1.
    /// Minimum acceptable x or z is 0 
    /// </summary>
    public int max_xzcoord
    {
        get
        {
            return regionSize * 32;
        }
    }
    /// <summary>
    /// First out of bounds y coordinate. The largest acceptable is max_ycoord - 1.
    /// Minimum acceptable y is 0.
    /// </summary>
    public int max_ycoord
    {
        get
        {
            return 256;
        }
    }

    private VoxelMutations voxel_mod;
    /// <summary>
    /// Object that has methods to modify the voxel terrain
    /// </summary>
    public VoxelMutations ModifyVoxels
    {
        get
        {
            return voxel_mod;
        }
    }

    private ItemDatabase world_items;
    /// <summary>
    /// Reference to the ItemDataBase which holds all items in this world.
    /// </summary>
    public ItemDatabase Items
    {
        get
        {
            return world_items;
        }
    }

    public override bool Equals(object obj)
    {
        return obj is WorldData ? Equals((WorldData)obj) : false;
    }
    public bool Equals(WorldData world)
    {
        return world == null ? false : world.worldName == worldName;
    }
    public override int GetHashCode()
    {
        return worldName == null ? 0 : worldName.GetHashCode();
    }

    // Use this for initialization
    void Awake () 
	{

		renderDistance = 10;
		//reset quality level
		QualitySettings.SetQualityLevel (3,true);
	}

	public void initiateLoading(string worldname){

        collider_parent = new GameObject("Colliders");
        collider_parent.transform.SetParent(transform);

        chunk_parent = new GameObject("Chunks");
        chunk_parent.transform.SetParent(transform);

        _worldName = worldname;
		
		//retreive block database
		blocks = ((world_items = GetComponent<ItemDatabase> ()).blockList = FileClass.LoadBlockLibrary(worldName));
        //retreive Item database

        //retreive biome settings
        ActiveBiome = ConvertBiome.ConvertFromSerializable(FileClass.LoadWorldSettings(worldName));
        ActiveBiome.InitializeWorldBiomeData(blocks);

        _regionSize = ActiveBiome.worldSize;
		_datapath = FileClass.GetWorldDataPath (worldName);
		
		//Create LoadingCoords object and initialize tVox
		LoadingCoords = new LoadingCircle();
		chunkPos = LoadingCoords.chunksToRender;
		renderDistIncrement = LoadingCoords.renderPositions;
		
		//Initialize minimizedRegionSize
		if (regionSize > 8)
			_minimizedRegionSize = 8;
		else
			_minimizedRegionSize = regionSize;

        //Size of entire world in regions, region Size is size of world in chunks
        _worldSize = RegionData.WorldLengthInChunksToWorldLengthInRegions(regionSize);
		
		//initialize regionData array to the number of regions in this world
		regionData = new RegionData[worldSize,worldSize];

		GetComponent<MeshObjectManager>().Init(worldName);
        (voxel_mod = GetComponent<VoxelMutations>()).Init(this);
		GetComponent<ChunkManager> ().Init (worldName);

        GameObject g = new GameObject("LOAD HERE!!!");
        g.transform.position = new Vector3(0, 0, 0);

        GetComponent<ChunkManager>().AddLoadSource(g.transform);
	}

    /// <summary>
    /// Returns a block type at the input position.
    /// </summary>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <param name="world"></param>
    public Block GetBlockAtPosition(float gx, float gy, float gz)
    {
        if (!y_coord_in_bounds(gy) || !xz_coord_in_bounds(gx) || !xz_coord_in_bounds(gz))
        {
            return blocks[0].l[0];
        }

        int _gx = (int)gx;
        int _gy = Mathf.CeilToInt(gy);
        int _gz = (int)gz;

        int rx = RegionData.GlobalCoordToRegionCoord(_gx);
        int rz = RegionData.GlobalCoordToRegionCoord(_gz);

        int cx = RegionData.GlobalCoordToRegionChunkCoord(_gx);
        int cy = RegionData.GlobalCoordToRegionChunkCoord(_gy);
        int cz = RegionData.GlobalCoordToRegionChunkCoord(_gz);

        int x = RegionData.GlobalCoordToLocalCoord(_gx);
        int y = RegionData.GlobalCoordToLocalCoord(_gy);
        int z = RegionData.GlobalCoordToLocalCoord(_gz);

        if (!RegionData.IsExistingChunkCoordinate(rx, rz, cx, cy, cz, this))
            return blocks[0].l[0];

        if (x < 0 || z < 0 || y < 0)
            return blocks[0].l[0];

        byte id1 = regionData[rx, rz].chunks[cx, cy, cz].voxelData[x, y, z];
        byte id2 = regionData[rx, rz].chunks[cx, cy, cz].data[x, y, z];

        return blocks[id1].l[id2];
    }
    /// <summary>
    /// Returns the block at position. Returns an Air voxel if no block was found.
    /// </summary>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <returns></returns>
    public Block GetBlockAtPosition(int _gx, int _gy, int _gz)
    {
        if (!y_coord_in_bounds(_gy) || !xz_coord_in_bounds(_gx) || !xz_coord_in_bounds(_gz))
        {
            return blocks[0].l[0];
        }

        int rx = RegionData.GlobalCoordToRegionCoord(_gx);
        int rz = RegionData.GlobalCoordToRegionCoord(_gz);

        int cx = RegionData.GlobalCoordToRegionChunkCoord(_gx);
        int cy = RegionData.GlobalCoordToRegionChunkCoord(_gy);
        int cz = RegionData.GlobalCoordToRegionChunkCoord(_gz);

        int x = RegionData.GlobalCoordToLocalCoord(_gx);
        int y = RegionData.GlobalCoordToLocalCoord(_gy);
        int z = RegionData.GlobalCoordToLocalCoord(_gz);

        if (!RegionData.IsExistingChunkCoordinate(rx, rz, cx, cy, cz, this))
            return blocks[0].l[0];

        if (x < 0 || z < 0 || y < 0)
            return blocks[0].l[0];

        byte id1 = regionData[rx, rz].chunks[cx, cy, cz].voxelData[x, y, z];
        byte id2 = regionData[rx, rz].chunks[cx, cy, cz].data[x, y, z];

        return blocks[id1].l[id2];
    }

    /// <summary>
    /// Returns true if the x-coord or z-coord is in bounds
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public bool xz_coord_in_bounds(float x)
    {
        if ( x < 0 || x >= regionSize * 32)
            return false;
        return true;
    }
    /// <summary>
    /// Returns true if the input y-coord is in bounds
    /// </summary>
    /// <param name="y"></param>
    /// <returns></returns>
    public bool y_coord_in_bounds(float y)
    {
        if (y >= 255 || y < 0)
            return false;
        return true;
    }
    /// <summary>
    /// Returns true if the input Vector3 coordinate is inside this world
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public bool coords_in_bounds(Vector3 pos)
    {
        return xz_coord_in_bounds(pos.x) && xz_coord_in_bounds(pos.z) && y_coord_in_bounds(pos.y);
    }
    /// <summary>
    /// Returns true if the input position exists on this world
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public bool position_exists(Vector3 pos)
    {
        if (pos.y < 0 || pos.y >= 255)
            return false;

        int _gx = (int)pos.x;
        int _gy = Mathf.CeilToInt(pos.y);
        int _gz = (int)pos.z;

        int rx = RegionData.GlobalCoordToRegionCoord(_gx);
        int rz = RegionData.GlobalCoordToRegionCoord(_gz);

        int cx = RegionData.GlobalCoordToRegionChunkCoord(_gx);
        int cy = RegionData.GlobalCoordToRegionChunkCoord(_gy);
        int cz = RegionData.GlobalCoordToRegionChunkCoord(_gz);

        int x = RegionData.GlobalCoordToLocalCoord(_gx);
        int y = RegionData.GlobalCoordToLocalCoord(_gy);
        int z = RegionData.GlobalCoordToLocalCoord(_gz);

        if (!RegionData.IsExistingChunkCoordinate(rx, rz, cx, cy, cz, this))
            return false;

        if (x < 0 || z < 0 || y < 0 || x > 31 || z > 31 || y > 31)
            return false;

        return true;
    }

    /// <summary>
    /// Returns true if the input position exists on this world or is empty space
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public bool position_exists_or_air(Vector3 pos)
    {
        int _gx = (int)pos.x;
        int _gy = Mathf.CeilToInt(pos.y);
        int _gz = (int)pos.z;

        int rx = RegionData.GlobalCoordToRegionCoord(_gx);
        int rz = RegionData.GlobalCoordToRegionCoord(_gz);

        int cx = RegionData.GlobalCoordToRegionChunkCoord(_gx);
        int cy = RegionData.GlobalCoordToRegionChunkCoord(_gy);
        int cz = RegionData.GlobalCoordToRegionChunkCoord(_gz);

        return !RegionData.IsNullNonAirChunk(rx, rz, cx, cy, cz, this);
    }

    /// <summary>
    /// Returns wether or not a input position is sorrounded by collidable voxels
    /// on all 6 sides. 
    /// </summary>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <returns></returns>
    public bool position_is_buried(int gx, int gy, int gz)
    {
        Block c_b = null;
        if (!(c_b = GetBlockAtPosition(gx + 1, gy, gz)).Collidable)
            return false;
        else if (c_b.Rotation > 0)
            return false;
        if (!(c_b = GetBlockAtPosition(gx - 1, gy, gz)).Collidable)
            return false;
        else if (c_b.Rotation > 0)
            return false;
        if (!(c_b = GetBlockAtPosition(gx, gy + 1, gz)).Collidable)
            return false;
        else if (c_b.Rotation > 0 && c_b.blockType != primitiveBlockType.sunkenCornerSlope &&
            c_b.blockType != primitiveBlockType.slope)
            return false;
        if (!(c_b = GetBlockAtPosition(gx, gy - 1, gz)).Collidable)
            return false;
        else if (c_b.Rotation > 0)
            return false;
        if (!(c_b = GetBlockAtPosition(gx, gy, gz + 1)).Collidable)
            return false;
        else if (c_b.Rotation > 0)
            return false;
        if (!(c_b = GetBlockAtPosition(gx, gy, gz - 1)).Collidable)
            return false;
        else if (c_b.Rotation > 0)
            return false;
        return true;
    }
}









	