using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChunkColumnGen 
{
    /// <summary>
    /// Max Height of Each voxel for the landscape
    /// </summary>
	public int[,] heightMap;
    /// <summary>
    /// Which Biome is being used at every x,z point on the chunk
    /// </summary>
	public BiomeRef[,] biomesTable;
    /// <summary>
    /// The World Biome Settings
    /// </summary>
	public WorldBiomes w;
    /// <summary>
    /// Noise Generator
    /// </summary>
	public OpenSimplexNoise n;

	//chunkx, chunkz
	//regionsize
	//chunkarray
	public int cx, cz;
	public int rs;
	public Chunk[,,] ca;

	//Has the height map been calculated yet?
	public bool CalculatedHeightMap = false;

	public List<L> blocksl;

    /// <summary>
    /// Generate an entire column, for sequential generation.
    /// </summary>
	void makeColumn(){
        if (ca == null)
        {
            GameLog.Out("Chunk Error");
            return;
        }
		for (int i = 0; i < 8; i++) {
			Chunk chunk = new Chunk(cx*32,i*32,cz*32,rs,n,w,this,blocksl);
			ca[cx%8,i,cz%8] = chunk;
		}
	}

    /// <summary>
    /// Generates a single chunk using this chunk column height map. Fills the input arrays with
    /// the resulting voxel data.
    /// </summary>
    /// <param name="gcx"></param>
    /// <param name="gcy"></param>
    /// <param name="gcz"></param>
    /// <param name="dat"></param>
    /// <param name="dat2"></param>
    public byte FillDataArraysAtChunk(int gcx, int gcy, int gcz, out byte[,,] dat, out byte[,,] dat2)
    {
        Chunk c = new Chunk(gcx, gcy, gcz, rs, n, w, this, blocksl);
        dat = c.chunkBlockData;
        dat2 = c.secondaryID;
        return c.loadChunk;
    }

    /// <summary>
    /// Return the height value at the x,z position for this ChunkColumn
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <returns></returns>
	public int GetHeight(int x, int z){
		if (x > 31) {
			if (z > 31)
				return NECorner;
			else if (z < 0)
				return SECorner;
			else
				return east[z];
		}
		else if (x < 0){
			if (z > 31)
				return NWCorner;
			else if (z < 0)
				return SWCorner;
			else
				return west[z];
		}
		else if (z > 31){
			if (x > 31)
				return NECorner;
			else if (x < 0)
				return NWCorner;
			else
				return north[x];
		}
		else if (z < 0){
			if (x > 31)
				return SECorner;
			else if (x < 0)
				return SWCorner;
			else
				return south[x];
		}
		else
			return heightMap[x,z];
	}

	/// <summary>
    /// Initialize for Sequential Chunk Generation
    /// </summary>
    /// <param name="chx"></param>
    /// <param name="chz"></param>
    /// <param name="rgs"></param>
    /// <param name="ns"></param>
    /// <param name="wb"></param>
    /// <param name="c"></param>
    /// <param name="bb"></param>
	public ChunkColumnGen(int chx, int chz, int rgs, OpenSimplexNoise ns, WorldBiomes wb, Chunk[,,] c, List<L> bb){
		cx = chx;
		cz = chz;
		rs = rgs;
		n = ns;
		w = wb;
		ca = c;
		blocksl = bb;
		heightMap = new int[32,32];
		biomesTable = new BiomeRef[32,32];
		west = new int[32];
		east = new int[32];
		north = new int[32];
		south = new int[32];
		makeColumn ();
	}

    /// <summary>
    /// Initialization for random Generation.
    /// </summary>
    /// <param name="cgx"></param>
    /// <param name="cgz"></param>
    /// <param name="rgs"></param>
    /// <param name="ns"></param>
    /// <param name="wb"></param>
    /// <param name="bb"></param>
    public ChunkColumnGen(int cgx, int cgz, int rgs, WorldBiomes wb,  List<L> bl)
    {
        cx = cgx;
        cz = cgz;
        rs = rgs;
        n = new OpenSimplexNoise(wb.seed);
        w = wb;
        blocksl = bl;
        heightMap = new int[32, 32];
        biomesTable = new BiomeRef[32, 32];
        west = new int[32];
        east = new int[32];
        north = new int[32];
        south = new int[32];
    }

	//Hold height map values for +1 all the way around chunk borders
	public int[] west;
	public int[] east;
	public int[] north;
	public int[] south;
	public int NWCorner;
	public int NECorner;
	public int SWCorner;
	public int SECorner;

}

