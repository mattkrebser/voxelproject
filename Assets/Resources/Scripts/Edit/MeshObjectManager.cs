﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[System.Serializable]
public class MeshObjectManager : MonoBehaviour {
	//holds textures of all of our lovely blocks
	public List<BlockMaterial> texList = new List<BlockMaterial>();
	//list of all of the mesh object, sorted by texture number
	
	private List<MeshObject>[] meshObjects;

	[Tooltip("Add Materials Here")]
	//Material we want to add to the object
	public List<Material> materialsList = new List<Material> ();

	//65536 is max size of vertices array that unity supports
	//There is no verification for this number, I am assuming we all know what
	//to not set it to (:
	[Tooltip("Max mesh Size")]
	public int maxVerticyArraySize = 65535;

	[Tooltip("See MeshObject.cs")]
	//look at Meshobject function 'DetectChain'
	public int chainLimit = 2;

	//Verticy count of voxel with the most vertices
	private int largestVoxel = 50;

	[Tooltip("Parent All Created Mesh Objects to..")]
	//Object that all the mesh gets parented to
	public GameObject world;

	private ThreadManager TM;
	private WorldData l;

	private ulong MeshObjectCount = 0;

	//name of the world.
	private string worldName;

	public int LargestVoxel
	{
		get {return largestVoxel;}
	}

	public void Init(string wn){
		TM = GameObject.Find ("WorldManager").GetComponent<ThreadManager> ();
		l = GetComponent<WorldData> ();
		worldName = wn;
		InitializeTextureArray ();
		meshObjects = new List<MeshObject>[texList.Count];
	}

    /// <summary>
    /// Remove circular references
    /// </summary>
	public void nullRefs(){
		TM = null;
		l = null;
	}

	/// <summary>
    /// Initialize the TextureArray with unique Identifiers
    /// </summary>
	private void InitializeTextureArray(){
        List<L> blocks = l.blocks;
		Dictionary<string,Block> defaults = new Dictionary<string,Block>();
		Dictionary<string,Block> nonDefaults = new Dictionary<string,Block>();
		texList = new List<BlockMaterial> ();
		l.blocks = blocks;

		if (blocks == null){
			GameLog.OutputToLog("Failed to load Library, library is null");
			return;
		}

		for (int i = 1; i < blocks.Count; i++){

			if (blocks[i] == null)
				blocks[i] = new L();

			for (int n = 0; n < blocks[i].l.Count; n++){
				if (blocks[i].l[n].blockType != primitiveBlockType.empty){
					//handle default textures
					if (blocks[i].l[n].BaseTexture){
						if (defaults.ContainsKey(blocks[i].l[n].textureName)){
							Block b = null;
							defaults.TryGetValue(blocks[i].l[n].textureName,out b);
							blocks[i].l[n].texArrayPosition = b.texArrayPosition;
                        }
						else{
							defaults.Add (blocks[i].l[n].textureName,blocks[i].l[n]);
							blocks[i].l[n].texArrayPosition = texList.Count;
							texList.Add (new BlockMaterial(0.5f,6,1,blocks[i].l[n].textureName,blocks[i].l[n].BaseTexture));
						}
					}
					//handle other textures
					else{
						if (nonDefaults.ContainsKey(blocks[i].l[n].textureName)){
							Block b = null;
							nonDefaults.TryGetValue(blocks[i].l[n].textureName,out b);
							blocks[i].l[n].texArrayPosition = b.texArrayPosition;
                        }
						else{
                            nonDefaults.Add (blocks[i].l[n].textureName,blocks[i].l[n]);
							blocks[i].l[n].texArrayPosition = texList.Count;
							texList.Add (new BlockMaterial(0.5f,6,1,blocks[i].l[n].textureName,blocks[i].l[n].BaseTexture));
						}
					}
				}
			}
		}

        GetComponent<ItemDatabase>().blockList = blocks;
	}

	/// <summary>
    /// Create a Unity Mesh for the Input AttatchedMesh Object
    /// </summary>
    /// <param name="m"></param>
	public void CreateMeshArrays(AttatchedMesh m){

		Vector3[] v;
		Vector2[] u;
		Vector2[] u2;
		int[] t;

		m.vertCount = 0;

		//All of the vert/uv/int arrays passed to this function + the existing mesh arrays must be guarenteed
		//to have a max size of less than max VerticyArraySize
		if (!m.createNewMesh && m.OwnedMesh != null){
			//create array with capacity of mesh.vertexCount - OldVertexCount of desired material + new Vertex
			//Count of desired material, the other lists follow this same pattern
			v = new Vector3[(m.OwnedMesh.vertCopy.Length - m.verticyLength + m.verts.Count)];
			u = new Vector2[(m.OwnedMesh.vertCopy.Length  - m.verticyLength + m.verts.Count)];
			u2 = new Vector2[(m.OwnedMesh.vertCopy.Length  - m.verticyLength + m.verts.Count)];
			t = new int[(m.OwnedMesh.triCopy.Length - m.triLength + m.tris.Count)];

			FillMeshArrays(v,u,u2,t,m);

			//clear lists
			m.verts = new List<Vector3>();
			m.tris = new List<int>();
			m.uvs = new List<Vector2>();
			m.uvs2 = new List<Vector2>();

			//update mesh
			m.OwnedMesh.UpdateMeshCopies(v,u,u2,t);
			TM.QueueOnMain(()=>{
				//return false if the meshobject or its components dont exist

				m.OwnedMesh.updateMesh();
				return false;
			},worldName,false);
		}
		//fall back, make a new mesh(sometimes this is what we want)
		else{
			//create array of capacity
			v = m.verts.ToArray();
			u = m.uvs.ToArray();
			u2 = m.uvs2.ToArray();
			t = m.tris.ToArray();

			//Assign AttatchedMesh properties
			m.verticyLength = m.verts.Count;
			m.triLength = m.tris.Count;

			//clear lists, initialize for future use
			m.verts = new List<Vector3>();
			m.tris = new List<int>();
			m.uvs = new List<Vector2>();
			m.uvs2 = new List<Vector2>();
			m.createNewMesh = false;

			CreateMeshObject(v,u,u2,t,m);
		}
	}

	/// <summary>
    /// Create a New MeshObject with the supplied Mesh Data
    /// </summary>
    /// <param name="verts"></param>
    /// <param name="uvs"></param>
    /// <param name="uvs2"></param>
    /// <param name="tris"></param>
    /// <param name="m"></param>
	private void CreateMeshObject(Vector3[] verts, Vector2[] uvs, Vector2[] uvs2, int[] tris, AttatchedMesh m){
		//create new GameObject
		MeshObject newMeshObject = new MeshObject(MeshObjectCount);
		MeshObjectCount++;
			
		//Assign m.ownedMesh to AttatchedMesh Object
		m.OwnedMesh = newMeshObject;
		m.OwnedMesh.cPos = m.cPos;
			
		//if this material doesnt exist yet, add a new list to hold it
		if (meshObjects[m.tex] == null)
			meshObjects[m.tex] = new List<MeshObject>();
		meshObjects[m.tex].Add (m.OwnedMesh);
			
		//initialize AttatchedMesh properties
		//m.meshObjectPos = meshObjects [m.tex].Count - 1;
		m.OwnedMesh.referencedAttatchedMesh.Add (m.cPos);
			
		//update mesh
		m.OwnedMesh.UpdateMeshCopies (verts, uvs, uvs2, tris);
		int mp = m.material;
		int tp = m.tex;
		string b = m.b;
		TM.QueueOnMain(()=>{
			GameObject newMeshObj = new GameObject();
			newMeshObj.AddComponent<MeshContainer>();
			m.OwnedMesh.updateMesh (newMeshObj,texList,materialsList,l,mp,tp,l.worldName,b);
			return false;
		},worldName,false);
	}

	/// <summary>
    /// Will set the input Attatched Mesh MeshObject Reference to
    /// a desirable exsiting MeshObject. If no such Object is found, a new empty
    /// MeshObject gets created
    /// </summary>
    /// <param name="m"></param>
    /// <returns></returns>
	public bool DetermineAttatchedMeshPosition(AttatchedMesh m){
		//If a material of this type already exists on a mesh somewhere
		if (meshObjects[m.tex] != null){
			MeshObject obj;
			//loop through existing materials, try to find a small enough mesh
			for (int n = 0; n < meshObjects[m.tex].Count; n++) {
				obj = meshObjects[m.tex][n];
				if (maxVerticyArraySize - obj.nextVertLength > m.requiredFreeVerts && m.maxSharedChunksPerMesh > obj.numChunks && ChunkIsCloseEnough(obj,m)){
					m.verticyOffset = obj.nextVertLength;
					m.triOffset = obj.nextTriLength;
					m.vertCount = obj.nextVertLength;
					m.OwnedMesh = obj;
					m.OwnedMesh.numChunks++;
				//	m.meshObjectPos = n;
					m.createNewMesh = false;
					//Assign the found meshObject to the AttatchedMesh object
					if (m.refChunkPos == -1) {
						m.OwnedMesh.referencedAttatchedMesh.Add(m.cPos);
					}
					return true;
				}
			}
		}
		//Could not find suitable, mesh. Create new mesh.
		m.createNewMesh = true;
		return false;
	}

	/// <summary>
    /// Returns true if the input AttatchedMesh is Close enough to the Input MeshObject
    /// </summary>
    /// <param name="o"></param>
    /// <param name="m"></param>
    /// <returns></returns>
	private bool ChunkIsCloseEnough(MeshObject o, AttatchedMesh m){
		int xd = Math.Abs (o.cPos.cx - m.cPos.cx);
		int zd = Math.Abs (o.cPos.cz - m.cPos.cz);
		//commented out code make is so batching doesnt batch diagonal chunks
		//increases draw calls, but decreases number of verts/tris
		//if ((xd + zd) / 2 < m.DistanceBias)
			if (xd <= m.DistanceBias)
				if (Math.Abs(o.cPos.cy-m.cPos.cy) <= m.DistanceBias)
					if (zd <= m.DistanceBias)
						return true;
		return false;
	}

	/// <summary>
    /// Updates a MeshObject by removing the input AttatchedMesh from it
    /// </summary>
    /// <param name="m"></param>
	public void DestroyRange(AttatchedMesh m){
		Vector3[] v;
		Vector2[] u;
		Vector2[] u2;
		int[] t;

		//If the mesh is the same size as the material being deleted
		if (m.verticyLength == m.OwnedMesh.vertCopy.Length) {
			m.OwnedMesh.DestroyMeshObjectAndChunks(TM,worldName);
			RemoveMesh(m.tex,m.OwnedMesh.GetMeshID());
		}
		else{
			//creates new mesh data that excludes the data on AttatchedMesh
			//This erases any mesh data associated with AttatchedMesh
			v = new Vector3[m.OwnedMesh.vertCopy.Length - m.verticyLength];
			u = new Vector2[m.OwnedMesh.uvCopy.Length - m.verticyLength];
			u2 = new Vector2[m.OwnedMesh.uv2Copy.Length - m.verticyLength];
			t = new int[m.OwnedMesh.triCopy.Length - m.triLength];

			ClearMeshArrays(v,u,u2,t,m);
			m.OwnedMesh.UpdateMeshCopies(v,u,u2,t);
			MeshObject mO = m.OwnedMesh;
			m.OwnedMesh.RemoveRef(m.cPos);

			TM.QueueOnMain(()=>{
				//return false if the meshobject or its components dont exist
				mO.updateMesh();
				return false;
			},worldName,false);
		}
	}

	/// <summary>
    /// Remove a mesh
    /// </summary>
    /// <param name="t"></param>
    /// <param name="meshID"></param>
	private void RemoveMesh(int t, ulong meshID){

		if (t < meshObjects.Length && meshObjects[t] != null){
			for (int i = 0; i < meshObjects[t].Count; i++) {
				if (meshObjects[t][i].GetMeshID() == meshID){
					meshObjects[t].RemoveAt(i);
					break;
				}
			}
		}
	}

	//Gets rid of AttatchedMesh m data from Meshobject
	private void ClearMeshArrays(Vector3[] v, Vector2[] u, Vector2[] u2, int[] t, AttatchedMesh m){
		int n = 0;
		//Even if verticy/triangle length is zero, we still need to copy due to different array sizes
		//copy mesh data that doesn't belong to the AttachedMesh into the array first
		for (int i = 0; i < m.OwnedMesh.vertCopy.Length-m.verticyLength; i++) {
			if (n == m.verticyOffset){
				n+=m.verticyLength;
				if (n >= m.OwnedMesh.vertCopy.Length)
					break;
			}
			v[i] = m.OwnedMesh.vertCopy[n];
			u[i] = m.OwnedMesh.uvCopy[n];
			u2[i]=m.OwnedMesh.uv2Copy[n];
			n++;
		}
		n = 0;
		//Triangle Array is not the same size as verticy and uv arrays, it must be done seperatly here
		for (int i = 0; i < m.OwnedMesh.triCopy.Length-m.triLength; i++) {
			if (n == m.triOffset){
				n+=m.triLength;
				if (n >= m.OwnedMesh.triCopy.Length)
					break;
			}
			t[i] = m.OwnedMesh.triCopy[n];
			n++;
		}
	}

	//Fills the given Arrays with current mesh Data belonging to other chunks - old data from this chunk
	// + new data from this chunk
	private void FillMeshArrays(Vector3[] v, Vector2[] u, Vector2[] u2, int[] t, AttatchedMesh m){
		int count = 0, n = 0;
		//Even if verticy/triangle length is zero, we still need to copy due to different array sizes
		//copy mesh data that doesn't belong to the AttachedMesh into the array first
		for (int i = 0; i < m.OwnedMesh.vertCopy.Length-m.verticyLength; i++) {
			if (n == m.verticyOffset){
				n+=m.verticyLength;
				if (n >= m.OwnedMesh.vertCopy.Length)
					break;
			}
			v[i] = m.OwnedMesh.vertCopy[n];
			u[i] = m.OwnedMesh.uvCopy[n];
			u2[i]=m.OwnedMesh.uv2Copy[n];
			n++;
		}
		m.verticyOffset = m.OwnedMesh.vertCopy.Length-m.verticyLength;
		count = m.verticyOffset;
		//Append AttatchedMesh data
		for (int i = 0; i < m.verts.Count; i++) {
			v[count] = m.verts[i];
			u[count] = m.uvs[i];
			u2[count]= m.uvs2[i];
			count++;
		}
		count = 0; n = 0;
		//Triangle Array is not the same size as verticy and uv arrays, it must be done seperatly here
		for (int i = 0; i < m.OwnedMesh.triCopy.Length-m.triLength; i++) {
			if (n == m.triOffset){
				n+=m.triLength;
				if (n >= m.OwnedMesh.triCopy.Length)
					break;
			}
			t[i] = m.OwnedMesh.triCopy[n];
			count++;
			n++;
		}
		m.triOffset = m.OwnedMesh.triCopy.Length-m.triLength;
		count = m.triOffset;
		//Append AttatchedMesh data
		for (int i = 0; i < m.tris.Count; i++) {
			t[count] = m.tris[i];
			count++;
		}

		m.verticyLength = m.verts.Count;
		m.triLength = m.tris.Count;
	}

	/// <summary>
    /// Iterate through all Meshes and try to dispose of them if they are
    /// far away from the input loadPos.
    /// </summary>
    /// <param name="v">Starting position in Mesh Array to iterate from</param>
    /// <param name="vel">Velocity to iterate</param>
    /// <param name="loadPos">World Position to evaluate distances from</param>
	public void MeshDisposal(Vector2Int32 v, double vel, Vector3 loadPos){

		int maxCount = (int)(2 * vel) + 20;
		int count = 0;

		//loop through meshObjects array
		for (int i = v.x; i < meshObjects.Length; i++) {

			if (meshObjects[i] != null){
				for (int n = v.y; n < meshObjects[i].Count; n++){

					//Candelete returns true if the mesh at (i,n) conatins only chunks
					//who are queued to be destroyed
					if (meshObjects[i][n].CanDelete(loadPos,l.renderDistance)){
						v.x = i;
						v.y = n;
						meshObjects[i][n].DestroyMeshObjectAndChunks(l,TM,worldName);
						meshObjects[i].RemoveAt(n);
						MeshObjectCount--;
						if (meshObjects[i].Count == 0)
							meshObjects[i] = null;
						goto done;
					}

					count++;

					//run the loop 2 times max
					if (count > maxCount){
						v.x = i;
						v.y = n;
						goto done;
					}

					if (n + 1 >= meshObjects[i].Count){
						v.y = 0;
						if (i + 1 >= meshObjects.Length && n + 1 >= meshObjects[i].Count){
							v.x = 0;
						}
					}
				}
			}
			else if (i + 1 >= meshObjects.Length && meshObjects[i] == null){
				v.x = 0;
				v.y = 0;
			}
		}

		done:
			return;
	}

    /// <summary>
    /// Destroy All Meshobjects
    /// </summary>
	public void DestroyAll(){
		for (int i = 0; i < meshObjects.Length; i++){
			if (meshObjects[i] != null){
				for (int n = 0; n < meshObjects[i].Count; n++){
					if (meshObjects[i][n] != null){
						meshObjects[i][n].DestroyOnlyThis();
						meshObjects[i][n] = null;
					}
					else
						meshObjects[i].RemoveAt(n);
				}
			}
		}
	}
}

























