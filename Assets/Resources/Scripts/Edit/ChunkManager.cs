using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChunkManager : MonoBehaviour
{
#if UNITY_EDITOR
    public GameObject test;
	public GameObject test1;
	public GameObject test2;
#endif


    /// <summary>
    /// Reference to the ThreadManager
    /// </summary>
    private ThreadManager TM;
	/// <summary>
    /// Reference to this world
    /// </summary>
	private WorldData l;

	/// <summary>
    /// List of LoadSources (their positions)
    /// </summary>
	private List<Vector3> loadSources = new List<Vector3>();
    /// <summary>
    /// List of LoadsSource Transforms
    /// </summary>
	private List<Transform> ts = new List<Transform>();
	/// <summary>
    /// List of LoadSources, this list is specifically synced with the drawing thread
    /// </summary>
	private List<LoadSourceData> LSD = new List<LoadSourceData> ();

    /// <summary>
    /// Queue of Voxel Changes that will occur
    /// </summary>
    private Queue<VoxelMutations.VoxelMutation> vox_changes = new Queue<VoxelMutations.VoxelMutation>();

	/// <summary>
    /// Reference to the mesh manager for this world
    /// </summary>
	private MeshObjectManager MOM;
    /// <summary>
    /// list of blocks contained on this world
    /// </summary>
	private List<L> blocks;

	/// <summary>
    /// All loops on the ChunkManager will stop if this variable is set to false
    /// </summary>
	private bool contin = true;

    private bool move_flag;

    /// <summary>
    /// Name of the world (in the file system) that this manager belongs to
    /// </summary>
    string worldName;

#if UNITY_EDITOR
    private GameObject[,,,,] tg;
#endif

    public void Init(string wn){
		TM = GameObject.Find ("WorldManager").GetComponent<ThreadManager> ();
		l = GetComponent<WorldData> ();
		MOM = GetComponent<MeshObjectManager> ();
		blocks = GetComponent<ItemDatabase> ().blockList;
        worldName = wn;
        StartCoroutine(ColumnLoading());
        StartCoroutine(DrawTasking());
    }

	void OnApplicationQuit(){
		contin = false;
	}

	/// <summary>
	/// Deletes all chunks. Is called last on any thread for any task
	/// </summary>
	/// <returns><c>true</c>, if all chunks was deleted, <c>false</c> otherwise.</returns>
	public bool DeleteAllChunks(){

		if (l != null)
			GameLog.OutputToLog ("Deleting world " + l.worldName);

		contin = false;

		if (TM != null)
			TM.StopFutureTasksOnWorld (worldName);

		if (TM != null)
		TM.QueueOnMain(()=>{
			//loop through loadSourceData list
			for (int i = 0; i < LSD.Count; i++) {
			
				var node = LSD[i].ContainedChunks.First;

				//delete each chunk in each list
				while (node != null){
					node.Value.ClearMaterials();
					if (l.regionData[node.Value.regionX,node.Value.regionZ] != null)
						l.regionData[node.Value.regionX,node.Value.regionZ].chunks[node.Value.mChunkX,node.Value.ChunkYChunk,node.Value.mChunkZ] = null;
					var nodeD = node;
					node = node.Next;
					LSD[i].ContainedChunks.Remove(nodeD);
				}

				//set the current ref to null as well
				if (LSD[i].current != null){
					LSD[i].current.Value.ClearMaterials();
					LSD[i].current = null;
				}
			}

			LSD.Clear ();
			loadSources.Clear ();
			ts = null;

			//destroy all mesh
			if (MOM != null){
				MOM.DestroyAll ();
			}

			TM.ResumeTaskingOnWorld (worldName);
			NullRefs ();

			return true;

		},worldName,true);

		return false;
	}

    /// <summary>
    /// Explicitly remove circular references
    /// </summary>
	private void NullRefs(){
		MOM.nullRefs ();
		TM = null;
		MOM = null;
		l = null;
		blocks = null;
	}

    /// <summary>
    /// Add a loadSource (ie, something that loads voxel chunks around it) to this ChunkManager
    /// </summary>
    /// <param name="ls"></param>
	public void AddLoadSource(Transform ls){
		loadSources.Add (ls.localPosition);
		ts.Add (ls);

		TM.AddToDrawThread(()=>{
			LSD.Add (new LoadSourceData ());
			return false;
		},worldName,false);
	}

	/// <summary>
    /// Remove a LoadSource from the ChunkManager. This will stop loading chunks around
    /// the input transform.
    /// </summary>
    /// <param name="t"></param>
	public void RemoveLoadSource(Transform t){
        bool found = false;
		//iterate through transforms list
		for (int n = 0; n < ts.Count; n++) {
			//if the transform to be removed is the transform given, then remove the load source
			if (ts[n] == t){
				loadSources.RemoveAt(n);
				ts.RemoveAt(n);
                found = true;
				TM.AddToDrawThread(()=>{
					LSD[n].Destruct();
					LSD.RemoveAt(n);
					return false;
				},worldName,false);
			}
		}

        if (!found)
            GameLog.Out("Failed to remove a load Source! Reason: Not found.");
	}

    /// <summary>
    /// Returns true if the input loadSource exists on this chunk manager
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    public bool Contains_LoadSource(Transform t)
    {
        for (int i = 0; i < ts.Count; i++)
        {
            if (t == ts[i])
            {
                return true;
            }
        }
        return false;
    }

	/// <summary>
    /// Copies the list 'ts' positions into the list 'loadSources'. This has to be done because
    /// Unity does not allow Transforms to be used on other threads.
    /// </summary>
	private void UpdateLoadSourceVectors(){
        for (int i = 0; i < ts.Count; i++)
        {
            if (loadSources[i] != ts[i].position)
            {
                move_flag = true;
            }
        }

        if (move_flag)
        {
            loadSources.Clear();
            for (int i = 0; i < ts.Count; i++)
            {
                loadSources.Add(ts[i].localPosition);
            }
        }
	}

    /// <summary>
    /// Adds a voxel update to the queue
    /// </summary>
    /// <param name="new_block"></param>
    /// <param name="position"></param>
    public void AddVoxelUpdate(VoxelMutations.VoxelMutation v)
    {
        if (!l.position_exists(v.position.ToVector3())){
            GameLog.Out("Cannot update non-existing position. The position may be out of bounds, or the chunk is not loaded.");
            return;}
        if (v.block == null) { GameLog.Out("Cannot Add a null block to the mesh."); return; }
        vox_changes.Enqueue(v);
    }

	/// <summary>
    /// Loads in voxel data around any load sources (players)
    /// </summary>
    /// <returns></returns>
	private IEnumerator ColumnLoading(){

		while (Application.isPlaying && contin)
        {

            //if the loading thread is backed up...
            if (TM.LoadThreadTaskCount > 10)
            {
                //finish all tasks before continuing...
                while (TM.LoadThreadTaskCount > 0)
                {
                    UpdateLoadSourceVectors();
                    yield return 0;
                }
                UpdateLoadSourceVectors();
            }
            else
                UpdateLoadSourceVectors();

            //loop through all load sources
            for (int i = 0; i < loadSources.Count; i++)
            {
					
				Vector3 loadSource = loadSources[i];

                if (ts == null)
                    break;

				if (ts[i] != null){

					//create new column object (only exists during load process)
					ChunkColumnLoad cL = new ChunkColumnLoad (l, loadSource,this,MOM,blocks,TM,loadSources,ts[i],worldName);
					//Assign the task
					DoTask t = cL.loadChunks;

					//add task to loading thread
					TM.AddToLoadThread (t,worldName,false);
				}
				else{
					//the load source is null, so get rid of it
					loadSources.RemoveAt(i);
					ts.RemoveAt(i);
				}

				yield return 0;

				//reset and restart (at the beginning of the load source list)
				if (i + 1 >= loadSources.Count)
					i = 0;
	
			}

			yield return 0;
		}
	}

	/// <summary>
    /// Iterativly (I can't spell, deal with it) Draw Chunks
    /// </summary>
    /// <returns></returns>
	private IEnumerator DrawTasking(){

		//used to keep track of disposal progress
		RegionChunkVector rV = new RegionChunkVector (0, 0, 0, 0, 0);rV.v = 0; rV.v1 = 0;
		Vector2Int32 v = new Vector2Int32 (0, 0);

		while (Application.isPlaying && contin) {

			bool cont = false;

            //Represent chunks/regions, used for debugging
#if UNITY_EDITOR
            if (GameLog.VISUALIZE_CHUNK_ARRAYS)
                RegionClear(rV, 5);
#endif
            //If the Thread Queue is backed up..
            if (TM.DrawThreadTaskCount > 40)
            {
                //Finish all tasks before starting a new one
                while (TM.DrawThreadTaskCount > 0)
                    yield return 0;
            }

			//Copy load Source vectors into a new list
			List<Vector3> LSs = new List<Vector3>(loadSources);

            //Adds a task to the thread that:
            //  -Updates Voxel terrain (from player editing)
            //  -determines which chunk column needs to be drawn (from render distance), and then draws it
            TM.AddToDrawThread(()=>{

                //Do any voxel edits first
                UpdateVoxels(LSs);

                //loop for every load source
				for (int n = 0; n < LSD.Count; n++){

					//Initialize variables used to find the next chunk
					Vector3 ls = LSs[n];
					ls.x = (int)(ls.x / 32);
					ls.z = (int)(ls.z / 32);
					int x ,z;

					//Set the current increment, reset it if the loadSource has moved into a different chunk
					//Also resets every 15 frames
					if (((int)(LSD[n].PreviousPosition.x / 32)) != ((int)(LSs[n].x / 32)) ||
					    ((int)(LSD[n].PreviousPosition.z / 32)) != ((int)(LSs[n].z / 32)) ||
					    LSD[n].GuarenteedDrawCheck > 13){
						LSD[n].GuarenteedDrawCheck = 0;
						LSD[n].cI = l.chunkPos.Length-1;
						LSD[n].ForceResetCount = false;
					}

					LSD[n].GuarenteedDrawCheck++;

					//Determine velocity of loadSource, this is used to speed up/slow down chunk and mesh disposal
					double vel = 0.0d;

					if (LSD[n].LastAccess == 0.0d || System.DateTime.Now.TimeOfDay.TotalSeconds - LSD[n].LastAccess < 0.01d)
						vel = 1.0d;
					else
						vel = (1 / (System.DateTime.Now.TimeOfDay.TotalSeconds - LSD[n].LastAccess)) * DistanceXZ (LSs[n] , LSD[n].PreviousPosition);

					if (vel < 1.0d)
						vel = 1.0d;

					//Set loadSourceData variables for the next drawTask tick
					LSD[n].LastAccess = System.DateTime.Now.TimeOfDay.TotalSeconds;
                    LSD[n].PreviousPosition = LSs[n];

                    //Run disposal tasks
                    ChunkDisposal(LSs,vel,n);
					MOM.MeshDisposal(v,vel,LSs[n]);

					//Wait until loadSource moves to start loading again
					if (LSD[n].WaitFrames > 2){
						LSD[n].WaitFrames--;
						continue;
					}

                    //Picks the closest non drawn column of chunks and draws them
                    for (int i = LSD[n].cI; i > l.renderDistIncrement[l.renderDistance]; i--){
						bool drewChunk = false;
						//assign x and z chunk coordinates of the next chunk to check
						x = (int)(ls.x + l.chunkPos[LSD[n].cI].x);z = (int)(ls.z + l.chunkPos[LSD[n].cI].y);
						LSD[n].cI--;

						if (x > -1 && x < l.regionSize && z > -1 && z < l.regionSize){
							//loop through column
							for (int y = 0; y < 8; y++){
								//make sure region + chunk is not null
								if (l.regionData[x/8,z/8] != null)
									if (l.regionData[x/8,z/8].chunks[x%8,y,z%8] != null){
										DrawChunks d = l.regionData[x/8,z/8].chunks[x%8,y,z%8];
										//Call CanDraw function, true means draw the chunk
										if (CanDraw(l.regionData,d,LSs) && !d.IsFullyDrawn)
                                        { 
                                            drewChunk = true;
											d.Draw();
											d.IsFullyDrawn = true;
										}
									}
							}
						}
						//If a chunk was drawn, break out of this loop
						if (drewChunk)
							break;
					}

					//Force a reset if max incrment is reached
					if (LSD[n].cI <= l.renderDistIncrement[l.renderDistance])
						LSD[n].ForceResetCount = true;
					
					//if consecutive force resets, make the draw task wait
					if (LSD[n].ForceResetCount)
						LSD[n].WaitFrames++;
				}

				//set cont = true so main thread can continue
				cont = true;
					
				//return true so the draw thread can continue
				return true;
			
			},worldName,false);

			//waiting for draw task above to finish
			while (!cont)
				yield return 0;

			yield return 0;
		}
	}

    /// <summary>
    /// Do all voxel updates.
    /// </summary>
    private void UpdateVoxels(List<Vector3> LSs)
    {
        while (vox_changes.Count > 0)
        {
            VoxelMutations.VoxelMutation v;
            v = vox_changes.Dequeue();
            //get the chunk that is being updated
            DrawChunks d = RegionData.GetChunk(v.position.x, v.position.y, v.position.z, l);
            if (d != null)
            {
                //Ensure that the chunk is in a position to be drawn, otherwise it will not be drawn
                //and no voxel change will occur
                if (CanDraw(l.regionData, d, LSs))
                    v.function(v);
            }
        }
    }

	/// <summary>
	/// Not used, but kept for debugging. Use it to display where chunks/regions are being drawn.
	/// </summary>
	/// <param name="rV">R v.</param>
	/// <param name="vel">Vel.</param>
	private void RegionClear(RegionChunkVector rV, double vel){
		int maxCount = (int)(l.renderDistance * vel * l.renderDistance);

		if (tg == null)
			tg = new GameObject[l.worldSize, l.worldSize, 8, 8, 8];
		int count = 0;
		//loop through regions
		for (int i = rV.rx; i < l.worldSize; i++) {
			for (int n = rV.rz; n < l.worldSize; n++){

				//set rx,rz
				rV.rx = i;
				rV.rz = n;

				//if the region at rx,rz isn ot null
				if (l.regionData[rV.rx,rV.rz] != null){

					//loop through contained chunks
					for (int x = rV.cx; x < l.minimizedRegionSize; x++){
						for (int z = rV.cz; z < l.minimizedRegionSize; z++){
							for (int y = rV.cy; y < 8; y++){

								//set current chunk position
								rV.cx = x; rV.cy = y; rV.cz = z; rV.v++; count++;

								//increment counter if the chunk is null
								if (l.regionData[rV.rx,rV.rz].chunks[rV.cx,rV.cy,rV.cz] == null){
									rV.v1++;
									WorldData.Destroy(tg[rV.rx,rV.rz,rV.cx,rV.cy,rV.cz]);
								}
								else if (tg[rV.rx,rV.rz,rV.cx,rV.cy,rV.cz] == null)
									tg[rV.rx,rV.rz,rV.cx,rV.cy,rV.cz] = (GameObject)WorldData.Instantiate(test,
									new Vector3(l.regionData[rV.rx,rV.rz].chunks[rV.cx,rV.cy,rV.cz].ChunkX + 16,
                                    l.regionData[rV.rx,rV.rz].chunks[rV.cx,rV.cy,rV.cz].ChunkY * -1,
                                    l.regionData[rV.rx,rV.rz].chunks[rV.cx,rV.cy,rV.cz].ChunkZ + 16)
									 ,Quaternion.identity);

								//only allows 35 check per frame
								if (count > maxCount)
									goto done;

								if (y + 1 >= 8){
									rV.cy = 0;
									if (z + 1 >= l.minimizedRegionSize){
										rV.cz = 0;
										if (x + 1 >= l.minimizedRegionSize){
											rV.cx = 0;

											//If the counter equals the total amount of chunks in a region, null the region
											if (rV.v == rV.v1){
												l.regionData[rV.rx,rV.rz].delete(TM, worldName);
											}
											rV.v = 0;
											rV.v1 = 0;
											rV.rz++;
											if (rV.rz >= l.worldSize){
												rV.rz = 0; rV.rx++;
												if (rV.rx >= l.worldSize){
													rV.rx = 0;
												}
											}

											goto done;
										}
									}
								}
							}
						}
					}
				}
				else{
					for (int x = 0; x < l.minimizedRegionSize; x++){
						for (int z = 0; z < l.minimizedRegionSize; z++){
							for (int y = 0; y < 8; y++){
								if (tg[rV.rx,rV.rz,x,y,z] != null)
									WorldData.Destroy(tg[rV.rx,rV.rz,x,y,z]);
							}
						}
					}
				}

				count++;

				if (count > maxCount)
					goto done;

				if (n + 1 >= l.worldSize){
					rV.cx = 0; rV.cy = 0; rV.cz = 0; rV.rz = 0;
					if (i + 1 >= l.worldSize){
						rV.rx = 0;
					}
				}
			}
		}

		//return false because this is a light task
		done:
			return;

	}
    /// <summary>
    /// Dispose of chunks that are far away from loadsources.
    /// </summary>
    /// <param name="LSs">List of loadsources</param>
    /// <param name="vel">speed at which to clear</param>
    /// <param name="i">Which LoadSource (LSD[i]) is being used</param>
	private void ChunkDisposal(List<Vector3> LSs, double vel, int i){

		//get current position in search
		if (LSD[i].current == null)
			LSD[i].current = LSD[i].ContainedChunks.First;

		//max increments
		int count = (int)(vel * 1.5f) + 18;
		//increments up to count
		int inc = 0;

		//no point in huge count
		if (count > LSD[i].ContainedChunks.Count / 2)
			count = LSD[i].ContainedChunks.Count / 2;

		while ( LSD[i].current != null){

			DrawChunks d = LSD[i].current.Value;
			var node = LSD[i].current;
			LSD[i].current = LSD[i].current.Next;

			if (d!= null){

				//null regions if they contain no chunks
				if (l.regionData[d.regionX,d.regionZ] != null && l.regionData[d.regionX,d.regionZ].GetCount() == 0){
#if UNITY_EDITOR
                    if (GameLog.VISUALIZE_CHUNK_ARRAYS)
                        l.regionData[d.regionX,d.regionZ].delete(TM, worldName);
#endif
                    l.regionData[d.regionX,d.regionZ] = null;
				}

				//Remove chunks from deletion list if they do not exist in the data arrays
				if (l.regionData[d.regionX,d.regionZ] == null)
					LSD[i].ContainedChunks.Remove(node);
					
				if (l.regionData[d.regionX,d.regionZ] != null)
					if (l.regionData[d.regionX,d.regionZ].chunks[d.mChunkX,d.ChunkYChunk,d.mChunkZ] == null)
						LSD[i].ContainedChunks.Remove(node);

				//if this chunk is far away from the load source...
				if (!WithinDistanceOfLoadSourcePlus10(d.ChunkXChunk,d.ChunkZChunk,LSs)){
					d.NullNonDrawn();
                }
			}
			//This chunk doesn't exist, get rid of it
			else{
				LSD[i].ContainedChunks.Remove(node);
			}

			inc++;

			if (inc > count)
				break;
		}
	}

	/// <summary>
    /// Returns true if the input chunk d, can be drawn
    /// </summary>
    /// <param name="r"></param>
    /// <param name="d"></param>
    /// <param name="LSs"></param>
    /// <returns></returns>
	private bool CanDraw(RegionData[,] r, DrawChunks d, List<Vector3> LSs){
	
		//In order to be drawn, a chunk needs to be encompassed with loaded chunks (or adjacent to a
		//spot that won't load in a chunk)
		if (AdequateData (d.regionX,d.regionZ,d.mChunkX + 1, d.ChunkYChunk, d.mChunkZ, r, d) &&
		    AdequateData (d.regionX,d.regionZ,d.mChunkX - 1, d.ChunkYChunk, d.mChunkZ, r, d) &&
		    AdequateData (d.regionX,d.regionZ,d.mChunkX, d.ChunkYChunk + 1, d.mChunkZ, r, d) &&
		    AdequateData (d.regionX,d.regionZ,d.mChunkX, d.ChunkYChunk - 1, d.mChunkZ, r, d) &&
		    AdequateData (d.regionX,d.regionZ,d.mChunkX, d.ChunkYChunk, d.mChunkZ + 1, r, d) &&
		    AdequateData (d.regionX,d.regionZ,d.mChunkX, d.ChunkYChunk, d.mChunkZ - 1, r, d)
		    ){

			bool withinDistance = false;

			//usually just one load source
			for (int i = 0; i < LSs.Count; i++){
				//if this chunk is within render distance of any loadSource, then draw it!
				if (WithinDistance(LSs[i],d.ChunkX,d.ChunkZ,l.renderDistance * 32))
					withinDistance = true;
			}

			//Finish off any draws
			if (LSs.Count == 0)
				return true;

			//Remove the draw task if it is out of bounds, otherwise, draw it
			if (!withinDistance)
				return false;
			else
				return true;
		}

		return false;

	}
    /// <summary>
    /// Returns true if the chunk at the input coordinates contains enough information
    /// so that an adjacent chunk can use it to be drawn
    /// </summary>
    /// <param name="rx"></param>
    /// <param name="rz"></param>
    /// <param name="cx"></param>
    /// <param name="cy"></param>
    /// <param name="cz"></param>
    /// <param name="r"></param>
    /// <param name="d"></param>
    /// <returns></returns>
	private bool AdequateData( int rx, int rz, int cx, int cy, int cz, RegionData[,] r, DrawChunks d){

		bool empty = false;

		//modify parameters to the proper array index
		if (cx >= d.mRegionSize) {
			cx = 0;
			rx++;
		}
		else if (cx < 0) {
			cx = d.mRegionSize - 1;
			rx--;
		}

		else if (cz >= d.mRegionSize) {
			cz = 0;
			rz++;
		}
		else if (cz < 0) {
			cz = d.mRegionSize - 1;
			rz--;
		}

		else if (cy >= 8) {
			empty = true;
		}
		else if (cy < 0) {
			empty = true;
		}

		//returning tru helps to get the chunk drawn, false guarentees it won't
		if (empty)
			return true;

		if (rx >= l.worldSize || rx < 0 || rz >= l.worldSize || rz < 0)
			return true;

		if (r[rx,rz] == null)
			return false;

		if (r[rx,rz].chunks == null)
			return false;

		if (r[rx,rz].chunks[cx,cy,cz] != null)
			return true;

		if (r[rx,rz].GetActualLoadValue(cx,cy,cz) == (byte)RegionData.LoadValues.AirChunk)
			return true;

		return false;
	}

	/// <summary>
    /// Returns true if the distance from LS to (x,z) is less than 'd'
    /// </summary>
    /// <param name="LS"></param>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <param name="d"></param>
    /// <returns></returns>
	private bool WithinDistance(Vector3 LS, int x, int z, int d){
		int x1 = (int)LS.x;
		int z1 = (int)LS.z;
		
		float d1 = System.Math.Abs (x1 - x);
		float d2 = System.Math.Abs (z1 - z);
		
		if (( d1 * d1 + d2 * d2 ) > ( d * d ))
			return false;
		return true;
	}
    /// <summary>
    /// Returns true if the input (x,z) position is within render distance of any load source
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <param name="LSs"></param>
    /// <returns></returns>
	public bool WithinDistanceOfLoadSource(int x, int z, List<Vector3> LSs){
		bool withinDistance = false;
		
		//usually just one load source
		for (int i = 0; i < LSs.Count; i++){
			//if this chunk is within render distance of any loadSource, then draw it!
			if (WithinDistance(LSs[i],x * 32,z * 32,l.renderDistance * 32))
				withinDistance = true;
		}

		//don't delete anything if we aren't loading anything
		if (LSs.Count == 0){
			return true;
		}

		return withinDistance;

	}
    /// <summary>
    /// Returns true if the input (x,z) position is within (render distance + 10 units) of any load source
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <param name="LSs"></param>
    /// <returns></returns>
	public bool WithinDistanceOfLoadSourcePlus10(int x, int z, List<Vector3> LSs){
		bool withinDistance = false;
		
		//usually just one load source
		for (int i = 0; i < LSs.Count; i++){

			//if this chunk is within render distance of any loadSource, then draw it!
			if (WithinDistance(LSs[i],x * 32,z * 32,l.renderDistance * 32 + 47))
				withinDistance = true;
		}
		
		//don't delete anything if we aren't loading anything
		if (LSs.Count == 0){
			return true;
		}
		
		return withinDistance;
		
	}
    /// <summary>
    /// Computes distance from v1 to v2
    /// </summary>
    /// <param name="v1"></param>
    /// <param name="v2"></param>
    /// <returns></returns>
	private float DistanceXZ(Vector3 v1, Vector3 v2){

		float d1 = System.Math.Abs (v1.x - v2.x);
		float d2 = System.Math.Abs (v1.z - v2.z);

		return (float)System.Math.Sqrt (d1 * d1 + d2 * d2);
	}

	/// <summary>
	/// Finds the load source data with transform t. LoadSourceData holds variables that are used to determine
	/// where to load chunks. It also regulates the speed of chunk loading
	/// </summary>
	/// <returns>The load source data.</returns>
	/// <param name="t">T.</param>
	public LoadSourceData FindLoadSourceData(Transform t){

		if (!contin)
			return null;

		for (int i = 0; i < ts.Count; i++) {
			if (t == ts[i] && t != null && ts[i] != null){
				if (LSD.Count > i && LSD[i] != null)
					return LSD[i];
			}
		}

		return null;
	}

}

public class LoadSourceData{
	//list of chunk objects connected to this loadsource
	public LinkedList<DrawChunks> ContainedChunks = new LinkedList<DrawChunks>();
	
	//increment for drawing spiral
	public int cI = 0;
	//force reset cI (increment)
	public bool ForceResetCount = false;
	//frames to wait before attempting to draw
	public int WaitFrames = 0;
	//current node in chunk disposal
	public LinkedListNode<DrawChunks> current;

	//is this loadSourceDestroyed
	public bool destroyed = false;

	//Time since last access of previous position vector
	public double LastAccess = 0.0d;

	//Try to make a draw every x frames regardless of count
	public int GuarenteedDrawCheck = 60;

	//previous local position of loadsource
	public Vector3 PreviousPosition = Vector3.zero;
	
	//probably need to copy containedchunks for no errors
	public void Destruct(){
		destroyed = true;
	}
	//if (destroyed), immediatly delete and null reference
	public void AddChunk(DrawChunks d){
		ContainedChunks.AddLast(d);
	}
}

















