﻿using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// Manages which worlds are loaded and whatnot
/// </summary>
public sealed class WorldManager : MonoBehaviour {

    //World Manager holds references to some objects that are used by static classes

    public GameObject worldPrefab;

    private static WorldManager _instance;
    private static WorldManager instance
    {
        get
        {
            return _instance;
        }
        set
        {
            if (_instance == null)
                _instance = value;
        }
    }

    private static WorldData _activeWorld;
    public static WorldData ActiveWorld
    {
        get
        {
            return _activeWorld;
        }
    }

	void Awake(){

        //WorldManager Initializes the static classes

		FileClass.Init ();
        _instance = this;
    }

    internal static void LoadWorld(string worldname, Vector3 position, Quaternion rotation, bool editorMode = false)
    {
        if (instance == null)
        {
            GameLog.OutputToLog("Error, WorldManager not Initialized yet, please wait.");
            return;
        }
        instance.LoadThisWorld(worldname, position, rotation, editorMode);
    }

    internal static void UnLoadWorld()
    {
        if (instance == null)
        {
            GameLog.OutputToLog("Error, WorldManager not Initialized yet, please wait.");
            return;
        }
        instance.UnLoadActiveWorld();
    }

    /// <summary>
    /// Load a world GameObject
    /// </summary>
    /// <param name="worldName"></param>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    /// <param name="EditorIsOn"></param>
    private void LoadThisWorld(string worldName, Vector3 position, Quaternion rotation, bool editorMode)
    {
        UnLoadActiveWorld();
        if (!FileClass.WorldExists(worldName))
        {
            GameLog.OutputToLog("World does not exist in Saves Folder: " + worldName);
            return;
        
        }

        try {
            GameObject g = Instantiate(worldPrefab, position, rotation) as GameObject;
            WorldData wD = g.GetComponentInChildren<WorldData>();
            wD.w = this;
            GetComponent<ThreadManager>().ResumeTaskingOnWorld(worldName);

            wD.initiateLoading(worldName);

            if (editorMode)
            {
                //do something
            }

            _activeWorld = wD;

        }
        catch (Exception e)
        {
            GameLog.OutputToLog(e);
        }
    }

    internal void UnLoadActiveWorld()
    {
        if (ActiveWorld != null)
        {
            GameObject g = ActiveWorld.gameObject;
            _activeWorld = null;
            ChunkManager cm = g.GetComponentInChildren<ChunkManager>();
            cm.DeleteAllChunks();
            Destroy(g);
        }
    }
}
