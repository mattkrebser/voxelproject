using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Block : Item
{
	/// <summary>
    /// Position in the texture Array. This value gets automatically filled at runtime
    /// </summary>
	[Tooltip("Position in LoadChunks/MeshObjectManager/texList")]
	public int texArrayPosition;
    /// <summary>
    /// Position in the material array for the material being used by this block
    /// </summary>
	[Tooltip("Position in Loadchunks/MeshObjectManager/materialsList")]
	public int materialPosition;
    /// <summary>
    /// Position of the texture slice being used
    /// </summary>
	[Tooltip("x Pixel / texture width, y Pixel / texture Height")]
	public Vector2S UVCoordinates;
    /// <summary>
    /// width of texture in tile texture, desiredPixelWidth/textureWidth
    /// </summary>
	[Tooltip("width of texture in tile texture, desiredPixelWidth/textureWidth")]
	public float u = 1.0f;
    /// <summary>
    /// height of texture in tile texture, desiredPixelHeight/textureHeight
    /// </summary>
	[Tooltip("height of texture in tile texture, desiredPixelHeight/textureHeight")]
	public float uy = 1.0f;
    /// <summary>
    /// Set to true if this block allows light to pass through it.
    /// </summary>
	[Tooltip("Light Passes through the block")]
	public bool isTransparent = true;
    /// <summary>
    /// Set to true if this block does not interact with the physics system.
    /// </summary>
	[Tooltip("Block is not added to mesh collider")]
	public bool physicsDisabled = true;

    /// <summary>
    /// What type of block is this?
    /// </summary>
	[Tooltip("Cube, sloped, imported mesh, etc..")]
	public primitiveBlockType blockType;

    /// <summary>
    /// Some blocks have 'rotations'
    /// </summary>
	[Tooltip("0 means no rotation variants, rotations are 1 to 16, look at the top of..")]
	//used for more accuratly placing vertices on the default sloped blocks
	public int Rotation = 0;

    /// <summary>
    /// The name of the texture (image path). This texture is the actual texture that is used in Game
    /// to display the 3D voxel
    /// </summary>
	[Tooltip("Name of texture")]
	public string textureName;
    /// <summary>
    /// Set to true if the texture for this block is found in the Resources folder.
    /// </summary>
	[Tooltip("True if the texture file is stored in the Resources folder")]
	public bool BaseTexture = true;

    /// <summary>
    /// Returns true if the block can be collided with
    /// </summary>
    public bool Collidable
    {
        get
        {
            return !(blockType == primitiveBlockType.empty || physicsDisabled);
        }
    }

    /// <summary>
    /// Possible block rotations
    /// </summary>
    public enum Rotations
    {
        None = 0,
        /// <summary>
        /// North for everything but corner and inside ramps. Corner and inside
        /// ramps point towards the NorthEast. North is in the z+ direction
        /// </summary>
        North_or_NE = 1,
        /// <summary>
        /// East for everything but corner and inside ramps. Corner and inside
        /// ramps point towards the NorthWest. East is in the x+ direction.
        /// </summary>
        East_or_NW = 2,
        /// <summary>
        /// South for everything but corner and inside ramps. Corner and inside
        /// ramps point towards the SouthEast. South is in the z- direction.
        /// </summary>
        South_or_SE = 3,
        /// <summary>
        /// West for everything but corner and inside ramps. Corner and inside
        /// ramps point towards the SouthWest. West is in the x- direction.
        /// </summary>
        West_or_SW = 4
    }

    /// <summary>
    /// Returns the Normal of the top face of the voxel.
    /// </summary>
    public Vector3 TopFaceNormal
    {
        get
        {
            if (blockType == primitiveBlockType.cube)
            {
                return Vector3.up;
            }
            else if (blockType == primitiveBlockType.slope)
            {
                if (Rotation == 1)
                {
                    return new Vector3(0, 0.7071067811865475f, -0.7071067811865475f);
                }
                else if (Rotation == 2)
                {
                    return new Vector3(-0.7071067811865475f, 0.7071067811865475f, 0);
                }
                else if (Rotation == 3)
                {
                    return new Vector3(0, 0.7071067811865475f, 0.7071067811865475f);
                }
                else if (Rotation == 4)
                {
                    return new Vector3(0.7071067811865475f, 0.7071067811865475f, 0);
                }
                else
                {
                    GameLog.Out("Unsupported rotation.");
                    return Vector3.up;
                }
            }
            else if (blockType == primitiveBlockType.sunkenCornerSlope)
            {
                if (Rotation == 1)
                {
                    return new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f);
                }
                else if (Rotation == 2)
                {
                    return new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f);
                }
                else if (Rotation == 3)
                {
                    return new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f);
                }
                else if (Rotation == 4)
                {
                    return new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f);
                }
                else
                {
                    GameLog.Out("Unsupported rotation.");
                    return Vector3.up;
                }
            }
            else if (blockType == primitiveBlockType.cornerSlope)
            {
                if (Rotation == 1)
                {
                    return new Vector3(-0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f);
                }
                else if (Rotation == 2)
                {
                    return new Vector3(0.5773502691896258f, 0.5773502691896258f, -0.5773502691896258f);
                }
                else if (Rotation == 3)
                {
                    return new Vector3(-0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f);
                }
                else if (Rotation == 4)
                {
                    return new Vector3(0.5773502691896258f, 0.5773502691896258f, 0.5773502691896258f);
                }
                else
                {
                    GameLog.Out("Unsupported rotation.");
                    return Vector3.up;
                }
            }
            else
            {
                GameLog.Out("Unsupported block type.");
                return Vector3.up;
            }
        }
    }

    public static blockID intToblockID(int block_id)
    {
        return new blockID((byte)(block_id >> 8), (byte)block_id);
    }
    public static int blockIDToInt(blockID block_id)
    {
        return (block_id.blockID1 << 8) | (block_id.blockID2);
    }
}

[System.Serializable]
public class L{
	public List<Block> l = new List<Block> ();
}




public struct BlockCompare : IEquatable<BlockCompare>
{
	string texName;
	bool baseTex;
	int rot;
	bool phys;
	bool trans;
	float u;
	float uy;
	Vector2SH uv;
	string itemName;

	public BlockCompare(string textureName, bool BaseTexture, int rotation, bool physics, bool transparent, 
	                    float uu, float uuy, Vector2SH vec, string itemNamee)
	{
		texName = textureName;
		baseTex = BaseTexture;
		rot = rotation;
		phys = physics;
		trans = transparent;
		u = uu;
		uy = uuy;
		uv = vec;
		itemName = itemNamee;
	}

	public override bool Equals (object obj)
	{
		return obj is BlockCompare ? Equals ((BlockCompare)obj) : false;
	}

	public bool Equals(BlockCompare b){
		if (b.texName == texName && b.baseTex == baseTex && b.rot == rot && b.phys == phys &&
						b.trans == trans && b.u == u && b.uy == uy && b.uv.Equals (uv) && b.itemName == itemName)
			return true;
		return false;
	}

	public override int GetHashCode ()
	{
		unchecked
		{
			int hash = 17;
			hash = hash * 23 + texName.GetHashCode();
			hash = hash * 23 + baseTex.GetHashCode();
			hash = hash * 23 + rot.GetHashCode();
			hash = hash * 23 + phys.GetHashCode();
			hash = hash * 23 + trans.GetHashCode();
			hash = hash * 23 + u.GetHashCode();
			hash = hash * 23 + uy.GetHashCode();
			hash = hash * 23 + itemName.GetHashCode();
			hash = hash * 23 + uv.GetHashCode();
			return hash;
		}
	}
}



public struct Vector2SH : IEquatable<Vector2SH>{
	public float x;
	public float y;
	
	public Vector2SH(float X, float Y){
		x = X;
		y = Y;
	}

	public override bool Equals (object obj)
	{
		return obj is Vector2SH ? Equals ((Vector2SH)obj) : false;
	}
	
	public bool Equals(Vector2SH v){
		if (v.x == x && v.y == y)
			return true;
		return false;
	}

	public override int GetHashCode ()
	{
		unchecked
		{
			int hash = 17;
			hash = hash * 23 + x.GetHashCode();
			hash = hash * 23 + y.GetHashCode();
			return hash;
		}
	}
}


















