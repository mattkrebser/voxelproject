using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class VoxelGroup : Item
{
	//dimensions of furthest pertruding voxels
	public Dimensions dimensions;
	//list of voxelStructures, each voxelStructure in here gets treated like a single chunk
	//if there is more than one voxelStructure
	public List<VoxelStructure> voxelGroup;
	//Used in creating the world, if the structure leans off an edge or in the water
	//It will fill transparent voxels with baseBlock until it hits non transparent voxels
	public blockID baseBlock;

	public int ArrayPosition = 0;
}

public class VoxelStructure
{
	public byte[,,] voxelData1;
	public byte[,,] voxelData2;
	//list of contained generic objects..
}





















