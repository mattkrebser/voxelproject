using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Each MeshObject Corresponds to a different Unity GameObject. Each Meshobject
/// is a different Material.
/// </summary>
public class MeshObject
{
	[System.NonSerialized]
	//Holds a reference to each attatchedmesh object that owns part of the mesh on this object
	public List<ChunkVector> referencedAttatchedMesh = new List<ChunkVector> ();

	public Material material;

	[System.NonSerialized]
	public MeshContainer MC;

	//Number of Chunks that own part of the mesh
	[System.NonSerialized]
	public int numChunks = 0;

	//Copy of mesh data
	[System.NonSerialized]
	public Vector3[] vertCopy;
	[System.NonSerialized]
	public Vector2[] uvCopy;
	[System.NonSerialized]
	public Vector2[] uv2Copy;
	[System.NonSerialized]
	public int[] triCopy;

	//Length that the mesh vertices and triangles arrays will be after
	//The attatchedMesh updates it
	[System.NonSerialized]
	public int nextVertLength = 0;
	[System.NonSerialized]
	public int nextTriLength = 0;

	private ulong meshID = 0;

	//Position of the first chunk to be added to this mesh
	//is the position of the mesh. Other chunks can only batch themselves to this mesh
	//if they are close enough to Cpos
	public ChunkVector cPos;

	//update cached mesh data
	public void UpdateMeshCopies(Vector3[] vertices,Vector2[] uvs,Vector2[] uvs2,int[] triss){
		nextVertLength = vertices.Length;
		nextTriLength = triss.Length;
		vertCopy = vertices;
		uvCopy = uvs;
		uv2Copy = uvs2;
		triCopy = triss;
	}

	public ulong GetMeshID(){
		return meshID;
	}

	public static int count = 0;
	public MeshObject(ulong t){
		meshID = t;
		count++;
		//Debug.Log ("mesh up " + count);
	}

	~MeshObject(){
		count--;
		GameLog.VoxelStatusOutput ("MeshFinal " + count.ToString());
	}

	//returns true if all of the attatched mesh for this meshobject needs to be deleted
	public bool CanDelete(Vector3 loadPos, int minDist){

		if (VerifiedDistance(loadPos,minDist))
			return true;
		else
			return false;

	}

	//make sure that all chunk positions in deletion list are far away from the load source
	private bool VerifiedDistance(Vector3 lP, int minDist){

		int count = 0;

		//loop through deletion list
		for (int i = 0; i < referencedAttatchedMesh.Count; i++) {
			//if the chunk in question is further away than the render distance, increment count
			if (!WithinDistance1(lP,referencedAttatchedMesh[i].cx * 32,referencedAttatchedMesh[i].cz * 32,minDist * 32 + 47)){
				count++;
			}
			//otherwise, remove it from deletion list
			else{
				break;
			}
		}

		//all chunks were too far away
		if (count == referencedAttatchedMesh.Count)
			return true;

		return false;
	}

	//if the distance between Ls(x,z) and (x,z) is less than d, return true
	private bool WithinDistance1(Vector3 LS, int x, int z, int d){
		int x1 = (int)LS.x;
		int z1 = (int)LS.z;
		
		float d1 = System.Math.Abs (x1 - x);
		float d2 = System.Math.Abs (z1 - z);
		
		if (( d1 * d1 + d2 * d2 ) > ( d * d ))
			return false;
		return true;
	}

	//update mesh
	public void updateMesh(GameObject g, List<BlockMaterial> tL, List<Material> mL, WorldData W, int matPos, int texPos,string wn,string b){

		MC = g.GetComponent<MeshContainer> ();

		MC.UpdateMesh (vertCopy,uvCopy,uv2Copy,triCopy,g,tL,mL,W,matPos,texPos,wn,b);
	}

	public void updateMesh(){
		if (MC != null){
			MC.UpdateMesh (vertCopy, uvCopy, uv2Copy, triCopy);
		}
	}

	//Destroys mesh and chunk data if needed, also destroys this
	public void DestroyMeshObjectAndChunks(WorldData l, ThreadManager tm, string worldName){

		//Call Null All attatchedMesh for each AttatchedMesh
		for (int i = 0; i < referencedAttatchedMesh.Count; i++) {
			ChunkVector am = referencedAttatchedMesh[i];
			if (l.regionData [am.cx / 8, am.cz / 8] != null)
				if (l.regionData [am.cx / 8, am.cz / 8].chunks [am.cx % 8, am.cy % 8, am.cz % 8] != null)
					l.regionData [am.cx / 8, am.cz / 8].chunks [am.cx % 8, am.cy % 8, am.cz % 8].NullAllAttatchedMesh (meshID);

		}

		MeshContainer m = MC;
		//Destroy MeshContainer
		tm.QueueOnMain(()=>{
			if (m != null){
				m.Destroy ();
			}
			return false;
		},worldName,false);
	}

	//Destroys mesh.
	public void DestroyMeshObjectAndChunks(ThreadManager tm, string worldName){
		MeshContainer mco = MC;
		tm.QueueOnMain(()=>{
			if (mco != null){
				mco.Destroy ();
			}
			return false;
		},worldName,false);
	}

	public void DestroyOnlyThis(){
		if (MC != null) {
			MC.Destroy();
		}
	}

	//Removing an item from referencedAttatchedMesh requires that all items
	//following have their positions updated
	public void RemoveRef(ChunkVector cPos){
		for (int i = 0; i < referencedAttatchedMesh.Count; i++){
			if (referencedAttatchedMesh[i].Equals(cPos)){
				referencedAttatchedMesh.RemoveAt(i);
				break;
			}
		}
	}
}


























