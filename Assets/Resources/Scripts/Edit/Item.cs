﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Base class for all Items.
/// </summary>
[System.Serializable]
public class Item 
{
    /// <summary>
    /// What is this item called?
    /// </summary>
	public string itemName;
    /// <summary>
    /// Path to the image that this item holds. This image is usually a 2D
    /// representation for a 3D object
    /// </summary>
	public string imagePath;

    /// <summary>
    /// What type of item is this?
    /// </summary>
	public itemType type;

    /// <summary>
    /// Every Item has an ID. Blocks take IDs 0 to 65535.
    /// Items are 2D representations of Components and Blocks.
    /// </summary>
    public int itemID;
}

[System.Serializable]
public enum itemType
{
	blockType,
	voxelStructType
};

/// <summary>
/// Necessary information to identify any unique item.
/// </summary>
public struct ItemKey
{
    public int ID;
    public itemType type;
}