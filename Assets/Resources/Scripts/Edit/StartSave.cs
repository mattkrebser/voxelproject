﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;

/*Save Formatting

[REGION]
region.a.b
a == Size of region length (in chunks), regions are perfect cubes.
b == region number ( 1 through number of regions)

[CHUNK] 
in region.a.b, there is a^3 chunks
	[CHUNK LOOKUP TABLE]
	chunks Are ordered xyz, Example: Chunk: x0y0z0,x0y1z0,020,030,040,050,060,070,001,011,021,031,etc..
	| 1 byte, load Value | 3 bytes, Position of chunk in File ||->next chunk->|| load value | position | etc..

	[CHUNK ENCODING]
	| 1 byte, BlockID | 2 bytes, number of consecutive blocks | 1 byte, secondary ID ||->nextblock->|| blockID | numBlocks | secondID | etc...


    -Please Note, values greater than bytes
        (like intigers) are stored in big
        endian format
*/

public class StartSave : MonoBehaviour {
	
	public string worldName;
	private BinaryWriter binWriter;
	public int regionSize;
	public byte[] lookUp;
	[System.NonSerialized]
	public bool lastSave = false;
	private GameObject tempRegion;
	private Region tempRegionScript;
	[System.NonSerialized]
	public int regionX = 0;
	[System.NonSerialized]
	public int regionZ = 0;
	[System.NonSerialized]
	public int trueSize = 0;
	//the length of all data buffers added together
	private int len = 0;

	//position in the file of the previously saved chunk
	private int prevOffset = 0;

	[System.NonSerialized]
	public Chunk[,,] chunkArray;
	
	// Use this for initialization
	void Start () 
	{
		FileClass.MakeFullDirectory (worldName);
		StartCoroutine(Task());
	}
	
	IEnumerator Task()
	{
		yield return StartCoroutine(SaveRegion ());
		//If this is the last time we create a save object for the world being generated..
		if (lastSave)
			tempRegionScript.savingDone = true;
		//Self destruct in 3..2...1....byebye
		Destroy (this);
		DestroyStartSave ();
	}

	void DestroyStartSave(){
		GameObject temp = GameObject.FindWithTag ("StartSave");
		Destroy (temp);
	}
	//Look up table position is(by using chunk x,y,z) x*regionSize*32 + y * 4 + z * 32
	// 4 bytes per chunk in look up table
	//regionSize is diamter of map in chunk coords, so max is 32

	
	IEnumerator SaveRegion()
	{
		List<byte> regiondata = new List<byte> ();
		//Incase regionSize is out of bounds.
		if (regionSize > 8)
			regionSize = 8;

		string filePath = FileClass.GetRegionDataPath (worldName, regionX,regionZ, trueSize);

		//Create a file and open filestream
		FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite);
		//New binary writer
		binWriter = new BinaryWriter (fs);
		//Find the Region object and get the Region script
		tempRegion = GameObject.Find ("Region(Clone)");
		tempRegionScript = (Region)tempRegion.GetComponent<Region> ();
		int size = regionSize;
		lookUp = new byte[size * size * 8 * 4];
		
		//advances stream by size of the lookup table (number of chunks * bytes per chunk)
		binWriter.BaseStream.Seek(size*size*8*4, SeekOrigin.Begin);
		
		for(int x = 0; x < size; x++)
		{
			for (int z = 0; z < size; z++)
			{
				for (int y = 0; y < 8 ; y++)
				{
					//write the chunk to output buffers then delete it
					Chunk chunk = chunkArray[x,y,z];
					if (chunk != null)
						SaveChunkToDisk(chunk,chunk.chunkx*32*size+ chunk.chunkz*32+chunk.chunky*4,regiondata);

					//continue on next frame every x frames
					if ((x*size*8 + z*8 + y +320) % 320 == 0)
						yield return 0;

					//last save was written, write extra data
					if (x + 1 == size && z + 1 == size && y + 1 == 8 && lastSave)
					{
						//make path
						string filePathWorldDat = FileClass.GetWorldDat(worldName);

						//new file stream/binary writer
						FileStream fs1 = new FileStream(filePathWorldDat, FileMode.Create, FileAccess.ReadWrite);
						BinaryWriter worldWriter = new BinaryWriter(fs1);
						//write stuff
						worldWriter.Write (trueSize*16);
						worldWriter.Write(260);
						worldWriter.Write (trueSize*16);
						fs1.Close ();
					}
				}
			}
		}

		//write region data and lookup table
		binWriter.Write (regiondata.ToArray ());
		fs.Position = 0;
		binWriter.Write (lookUp);
		fs.Close();
	}

    /// <summary>
    /// Put a chunk's byte information inside the input regiondata list.
    /// 'b' is the position of this chunk in the lookup table
    /// </summary>
    /// <param name="chunk"></param>
    /// <param name="b"></param>
    /// <param name="regiondata"></param>
    void SaveChunkToDisk(Chunk chunk, int b, List<byte> regiondata)
    {
        //Block is the type of block(blockID) of the current block, secondary is the secondary ID(data array in DrawChunks)
        byte block = 0, secondary = 0;
        //Test block is the next block while iterating through the data, s2 is the next secondary ID
        byte testBlock = 0, s2 = 0;
        //Load is the 'load value' 1 == load, all other values don't load
        byte load;

        load = chunk.loadChunk;
        //Incrementers
        //<count> number of blocks consecutivley found of the same type
        //<countTotal> Total iterations of 'z'
        int count = 0, countTotal = 0;

        if (load != 0)
        {
            //We decrement y instead of incrementing because for most cases, the surface of a chunk is somewhat flat
            //This way the file size is smaller
            for (int y = 31; y > -1; y--)
            {
                for (int x = 0; x < 32; x++)
                {
                    for (int z = 0; z < 32; z++)
                    {
                        //CountTotal is the total iterations of the loops
                        countTotal++;
                        //assign testBlock
                        testBlock = chunk.chunkBlockData[x, y, z];
                        secondary = chunk.secondaryID[x, y, z];

                        //IF this is the first iteration,
                        if (countTotal == 1)
                        {
                            block = testBlock; s2 = secondary;
                        }

                        //If this is the final iteration,
                        if (countTotal >= 32768)
                        {
                            //If the next block is the same as our current block,
                            //Increment count and add the values to the buffer
                            if (testBlock == block && s2 == secondary)
                            {
                                count++;
                                regiondata.Add(block);
                                regiondata.Add((byte)(count >> 8));
                                regiondata.Add((byte)(count));
                                regiondata.Add(s2);
                            }
                            //Otherwise, write the values of the current block to the buffer, then aquire values of the next
                            //block and write them to the buffer
                            else
                            {
                                regiondata.Add(block);
                                regiondata.Add((byte)(count >> 8));
                                regiondata.Add((byte)(count));
                                regiondata.Add(s2);
                                count = 1;
                                regiondata.Add(testBlock);
                                regiondata.Add((byte)(count >> 8));
                                regiondata.Add((byte)(count));
                                regiondata.Add(secondary);
                            }
                            break;
                        }

                        //If the next block is the same as our current block, increase count and continue
                        if (s2 == secondary && testBlock == block)
                            count++;
                        //otherwise, Add the blockID, the number of blocks of this type we counted, and the secondary ID
                        //to the buffer and reset count to '1' and set block equal to testblock
                        else
                        {
                            regiondata.Add(block);
                            regiondata.Add((byte)(count >> 8));
                            regiondata.Add((byte)(count));
                            regiondata.Add(s2);
                            block = testBlock;
                            s2 = secondary;
                            count = 1;
                        }
                    }
                }
            }
        }
        else
        {
            int num = (int)32768;
            regiondata.Add((byte)(0));
            regiondata.Add((byte)(num >> 8));
            regiondata.Add((byte)(num));
            regiondata.Add((byte)(0));
        }

        //Add the length of our buffer to 'len'
        len = regiondata.Count;

        // "len" is the offset saved for each chunk. However, the chunk data on the disk does not
        //start at "len", it ends at "len". The chunk before the chunk(on the disk) we wish to load holds the offset
        //for the chunk we wish to load
        //In the event that Region.cs does not create every chunk in the region (regions hold 8x8x8 chunks, a world size of
        //10x8x10 chunks would have regions with 2x8x8 and 8x8x2 chunks..so not every possible chunk gets made)
        //this little if statement adds data where it is needed.
        if (b != 0)
        {
            if (lookUp[b - 3] == 0 && lookUp[b - 2] == 0 && lookUp[b - 1] == 0 && lookUp[b - 4] == 0)
            {
                lookUp[b - 3] = (byte)(prevOffset >> 16);
                lookUp[b - 2] = (byte)(prevOffset >> 8);
                lookUp[b - 1] = (byte)(prevOffset);
            }
        }

        //Add values to lookUp, which is a buffer that gets added before all the chunkData
        //It is the lookup table
        lookUp[b] = load;
        lookUp[b + 1] = (byte)(len >> 16);
        lookUp[b + 2] = (byte)(len >> 8);
        lookUp[b + 3] = (byte)(len);

        prevOffset = len;
    }

}
























