using UnityEngine;
using System.Collections;

public class BiomeRef 
{
	//referenced biome
	public Biome biome;
	//index of median, not the median
	public float median;
	//UQuartile (UpperQuartile) between median and UBound (Upper Bound)
	public float UQuartile;
	//LQuartile between median and LBound
	public float LQuartile;
	//Lower bound
	public float LBound;
	//upper bound
	public float UBound;

    //A Biome found in the range 34->35
    //34 34.1 34.2 34.3 34.5 34.6 34.7 34.8 34.9 35
    //|lbound   |lquartile     rquartile|   rbound|

	//adjacent biomes
	public BiomeRef UBiomeRef;
	public BiomeRef LBiomeRef;

	//used to determine which biome is which. Same for all biomeRefs with identical biome references
	public int pos;

	public BiomeRef(Biome b, float m, float lq, float uq, float lb, float ub, int ps){
		biome = b;
		median = m;
		UQuartile = uq;
		LQuartile = lq;
		LBound = lb;
		UBound = ub;
		pos = ps;
	}

	public BiomeRef(Biome b, float m, int ps){
		biome = b;
		LBound = m;
		pos = ps;
	}
}

