﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour 
{
    //The database(s) Gets initialized on WorldData.cs

	//itemList is blocks
	public List<L> blockList = new List<L>();
	public List<VoxelGroup> voxelStructs = new List<VoxelGroup>();

	//Search through items using a string name
	public static List<Item> Search(string item, ItemDatabase d){
		List<Item> returnList = new List<Item>();

		for (int i = 0; i < d.blockList.Count; i++) {
			for (int n = 0; n < d.blockList[i].l.Count; n++){
				if (item.Length <= d.blockList[i].l[n].itemName.Length)
					if (item == d.blockList[i].l[n].itemName.Substring(0,item.Length))
						returnList.Add (d.blockList[i].l[n]);
			}
		}

		for (int i = 0; i < d.voxelStructs.Count; i++) {
			if (item.Length <= d.voxelStructs[i].itemName.Length)
				if (item == d.voxelStructs[i].itemName.Substring(0,item.Length))
					returnList.Add(d.voxelStructs[i]);
		}

		return returnList;
	}

    /// <summary>
    /// Gets All blocks in the data base. Specifically, only one empty block can be returned in the list,
    /// Only a single variant of any slope block gets returned (rotation 1). If a block has a rotation
    /// of 1 or 0 and is non empty, then it will always be added to the returned list.
    /// </summary>
    /// <returns></returns>
    public List<blockitem> GetAllUniqueBlocks()
    {
        List<blockitem> returnList = new List<blockitem>();
        for (int i = 0; i < blockList.Count; i++)
        {
            for (int n = 0; n < blockList[i].l.Count; n++)
            {
                if (blockList[i].l[n].blockType == primitiveBlockType.empty)
                {
                    if (i == 0 && n == 0)
                        returnList.Add(new blockitem(blockList[i].l[n], (byte)i, (byte)n));
                    else
                        continue;
                }
                else
                {
                    if (blockList[i].l[n].Rotation == 0 || blockList[i].l[n].Rotation == 1)
                        returnList.Add(new blockitem(blockList[i].l[n], (byte)i, (byte)n));
                }
            }
        }
        return returnList;
    }
	/// <summary>
	/// Adds intput List<L> to input itemDataBase d. Does not copy duplicate
	/// values. Items from bList will be placed anywhere there is space in the input database d
	/// </summary>
	/// <param name="bList">B list.</param>
	/// <param name="d">D.</param>
	public static void AddToBlockDatabase(List<L> bList, ItemDatabase d){
		Dictionary<BlockCompare,Block> dictionary = new Dictionary<BlockCompare, Block> ();
		List<Block> blocksToAdd = new List<Block> ();

		for (int i = 1; i < d.blockList.Count; i++){
			if (d.blockList[i] != null && d.blockList[i].l != null){
				for (int n = 0; n < d.blockList[i].l.Count; n++){
					if (d.blockList[i].l[n].blockType != primitiveBlockType.empty){
						Block b = d.blockList[i].l[n];
						dictionary.Add(new BlockCompare(b.textureName,b.BaseTexture,b.Rotation,b.physicsDisabled,b.isTransparent,
				                               	    b.u,b.uy,new Vector2SH(b.UVCoordinates.x,b.UVCoordinates.y),b.itemName),b);
					}
				}
			}
		}

		for (int i = 1; i < bList.Count; i++){

			if (bList[i] != null && bList[i].l != null){
				for (int n = 0; n < bList[i].l.Count; n++){
					if (bList[i].l[n].blockType != primitiveBlockType.empty){
						Block b = bList[i].l[n];
						BlockCompare bc = new BlockCompare(b.textureName,b.BaseTexture,b.Rotation,b.physicsDisabled,b.isTransparent,
					                                   b.u,b.uy,new Vector2SH(b.UVCoordinates.x,b.UVCoordinates.y),b.itemName);
						if (!dictionary.ContainsKey(bc)){
							blocksToAdd.Add(b);
						}
					}
				}
			}
		}

		int p = 0;
		for (int i = 1; i < d.blockList.Count; i++){

			if (d.blockList[i] == null)
				d.blockList[i] = new L();

			if (d.blockList[i].l == null)
				d.blockList[i].l = new List<Block>();

			for (int n = 0; n < 256; n++){
				if (n >= d.blockList[i].l.Count || d.blockList[i].l[n].blockType == primitiveBlockType.empty ){
					if (p >= blocksToAdd.Count)
						goto done;
					d.blockList[i].l.Add(blocksToAdd[p]);
					GameLog.OutputToLog("Added a block, " + blocksToAdd[p].itemName);
					p++;
				}
			}
		}

		done:
			return;
	}

	//search through items, return different type
	public static List<ItemExtended> SearchExtended(string item, ItemDatabase d){
		List<ItemExtended> returnList = new List<ItemExtended>();
		
		for (int i = 0; i < d.blockList.Count; i++) {
			for (int n = 0; n < d.blockList[i].l.Count; n++){
				if (item.Length <= d.blockList[i].l[n].itemName.Length)
					if (item == d.blockList[i].l[n].itemName.Substring(0,item.Length))
						returnList.Add (new ItemExtended(d.blockList[i].l[n],(byte)(i),(byte)(n)));
			}
		}

		for (int i = 0; i < d.voxelStructs.Count; i++) {
			if (item.Length <= d.voxelStructs[i].itemName.Length)
				if (item == d.voxelStructs[i].itemName.Substring(0,item.Length))
					returnList.Add(new ItemExtended(d.voxelStructs[i],i));
		}
		
		return returnList;
	}

	/// <summary>
    /// Contains extra positional information that the default item type does not contain
    /// </summary>
	public class ItemExtended : Item 
	{
		public blockID block;
		public int pos;

		public ItemExtended(Item item, byte i1, byte i2)
		{
			this.itemName = item.itemName;
			this.type = item.type;
			block.blockID1 = i1;
			block.blockID2 = i2;
		}

		public ItemExtended(Item item, int i)
		{
			this.itemName = item.itemName;
			this.type = item.type;
			pos = i;
		}
	}
}










