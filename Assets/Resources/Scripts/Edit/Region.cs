using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Region : MonoBehaviour
{
	public GameObject chunk;
	public GameObject chunkColumn;
	public GUIStyle style;
	[System.NonSerialized]
	public int chunkSize = 32;
	[System.NonSerialized]
	public int regionSize = 32;
	[System.NonSerialized]
	public int regionY = 8;
	
	private Texture2D tex ;
	//Values to be passed besides regionsize and chunk coords

	[System.NonSerialized]
	public int treeDens;
	[System.NonSerialized]
	public int grassDens;

	//i is chunk count
	private int i = 0;
	//number of chunks created per frame
	private int num;
	//number of chunks
	private int totalChunks;
	//time since start
	private float timeInit;
	private bool cancled = false;
	
	[System.NonSerialized]
	public bool savingDone = false;
	
	[System.NonSerialized]
	public WorldBiomes worldData;

	[System.NonSerialized]
	public string worldName = " ";
	
	//noisy
	private OpenSimplexNoise noiseObj;

	private List<L> blocksList;
	
	void Start()
	{
		blocksList = GameObject.FindWithTag ("Chunky").GetComponent<ItemDatabase> ().blockList;
		InitializeWorldBiomeData ();
		//Destroy menu objects
		DestroyAllObjects ();
		totalChunks = regionSize * regionSize * 8;
		noiseObj = new OpenSimplexNoise (127368777712);
		//get background of GUI textures
		tex = Resources.Load ("Materials/transparent") as Texture2D;
		//start generating chunks.
		StartCoroutine(createChunkArray ());
	}
	
	void OnGUI()
	{
		if (GUI.Toggle (new Rect(Screen.width/60 + 160,Screen.height/1.06f-19,80,20),false,"CANCEL",style)){
			cancled = true;
		}
		//Show progress of world gen
		if ( Event.current.type == EventType.Repaint)
		{
			GUI.skin.box.normal.background = tex;
			GUI.skin.box.alignment = 0;
			GUI.Box (new Rect (Screen.width/60,Screen.height/1.06f-36,Screen.width/4,Screen.height/20), "Building The World...?");
			GUI.Box (new Rect (Screen.width/60,Screen.height/1.06f-24,Screen.width/4,Screen.height/20), "Elapsed Time: " + (time - timeInit));
			GUI.Box (new Rect (Screen.width/60,Screen.height/1.06f-12,Screen.width/4,Screen.height/20), i + "/" + totalChunks + " Chunks Made");
			GUI.Box (new Rect(Screen.width/60,Screen.height/1.06f,400,Screen.height/20),"About "+ timeTot/60 + " minutes " + timeTot%60 + " Seconds Left");
		}
	}

	//Chunk Generation is not an asynchronous task, all chunk generation needs to occur
	//on the same thread. (It doesn't have to be unity's main thread, though)
	public IEnumerator createChunkArray()
	{
		
		//Set timeInit to the exact time when we start generating chunks
		timeInit = Time.time;
		
		//set the number of regions, each region is 8x8x8 chunks
		int numRegions = (Mathf.CeilToInt (((float)(regionSize)) / 8));
		
		if (numRegions == 0)
			numRegions = 1;
		
		int rSize = 0;
		if (regionSize > 8)
			rSize = 8;
		else
			rSize = regionSize;
		//int a is the regionNum
		for (int a =0; a < numRegions; a++)
		{
			for(int b = 0; b < numRegions;b++)
			{
				//num is used to speed up or slow down generation
				num = 2;
				Chunk[,,] region = new Chunk[rSize,8,rSize];
				
				//iterate through every chunk
				for (int x = a*rSize; x < a*rSize+rSize; x++)
				{
					for (int z = b*rSize; z < b*rSize+rSize; z++)
					{
						for ( int y = 0; y < regionY; y+=8)
						{
							//regulate call to creating a new chunk based on fps(regulates amount of new chunks
							//created per frame by increasing/decreasing num)
							if (y%num == 0)
								yield return 0;
							if (fps > 20)
								num+=2;
							else if (num > 2)
								num--;
							
							//If we pressed the cancel button..
							if (cancled)
								goto cancel;
							//create and initialize chunk if it is in bounds
							if (x < regionSize && z < regionSize)
							{
								//keeps track of chunk gen progress
								i+=8;
								//new chunk weeeee
								new ChunkColumnGen(x,z,regionSize,noiseObj,worldData,region,blocksList);
							}
						}
					}
				}
				//start savin, instantiate and initialize save object
				GameObject starter = Instantiate(Resources.Load("StartSave"),new Vector3(0,0,0), Quaternion.identity) as GameObject;
				StartSave newScript = (StartSave)starter.GetComponent<StartSave>();
				newScript.worldName = worldName;
				newScript.regionSize = regionSize;
				newScript.regionX = a;
				newScript.regionZ = b;
				newScript.trueSize = regionSize;
				newScript.chunkArray = region;
				//If we are on the last iteration of this for loop
				if (a+1 >= numRegions && b+1 >= numRegions){
					newScript.lastSave = true;
				}
			}
		}
		
	cancel:
		if (cancled){
			DestroyAllChunkObjects();
			//load level select screen
			Instantiate(Resources.Load("LevelSelectPrefab"),new Vector3(0,0,0),Quaternion.identity);
		}
		
		//StartSave Object will set savingDone to true when saving has finished
		while(!savingDone)
			yield return 0;
		
		//World save is completed, destroy this and load menu
		DestroyAllChunkObjects ();
		Instantiate (Resources.Load ("UIPrefabs/loadlevel"), new Vector3 (0, 0, 0), Quaternion.identity);
	}

	//dis function will give you a headache
	void InitializeWorldBiomeData(){
		//If they didn't bother to make anything...
		if (worldData.biomesList.Count == 0)
			worldData.biomesList.Add(new Biome());

		//Verify each biome has atleast one layer, if it doesn't, then make one
		//max of around 300 operations per biome
		for (int i = 0; i < worldData.biomesList.Count; i++) {
			//if there is no layers, make a new one and add grass block to it
			if (worldData.biomesList[i].layers.Count == 0){
				worldData.biomesList[i].layers.Add(new BlockLayer());
				//grass cube is base block, please don't delete it from ItemDataBase
				worldData.biomesList[i].layers[0].layers[0].Add(new BlockIDInt32(108,0));
			}
			else{
				for (int n = 0; n < worldData.biomesList[i].layers.Count; n++){
					//If the first block in the layer doesnt exist
					if (worldData.biomesList[i].layers[n].layers[0].Count == 0)
						worldData.biomesList[i].layers[0].layers[0].Add(new BlockIDInt32(0,0));
					//for each layer, fill out it's height in the biome's layerRef's
					for (int p = worldData.biomesList[i].layers[n].layerPlacementHeightMin;
					     p <= worldData.biomesList[i].layers[n].layerPlacementHeightMax; p++){
						worldData.biomesList[i].layerRefs[p] = n;
					}

					//sort any sloped type blocks on the top leveled layer (1)
					RotationTask(0, worldData.biomesList[i].layers[n]);
				}
			}

			//Fill layer arrays on sub biomes, same process as above but for each sub biome
			for (int a = 0; a < worldData.biomesList[i].SubBiomes.Count; a++){
				if (worldData.biomesList[i].SubBiomes[a].layers.Count == 0){
					worldData.biomesList[i].SubBiomes[a].layers.Add(new BlockLayer());
					//grass cube is base block, please don't delete it from ItemDataBase (blockitem(108,0))
					worldData.biomesList[i].SubBiomes[a].layers[0].layers[0].Add(new BlockIDInt32(108,0));
				}
				else{
					for (int b = 0; b < worldData.biomesList[i].SubBiomes[a].layers.Count; b++){
						if (worldData.biomesList[i].SubBiomes[a].layers[b].layers[0].Count == 0)
							worldData.biomesList[i].SubBiomes[a].layers[0].layers[0].Add(new BlockIDInt32(0,0));
						//iterate from min to max for each layer
						for (int c = worldData.biomesList[i].SubBiomes[a].layers[b].layerPlacementHeightMin;
						     c < worldData.biomesList[i].SubBiomes[a].layers[b].layerPlacementHeightMax; c++){
							worldData.biomesList[i].SubBiomes[a].layerRefs[c] = b;
						}

						//sort any sloped type blocks on the top leveled layer (1)
						RotationTask(0, worldData.biomesList[i].SubBiomes[a].layers[b]);
					}
				}
			}

			//Fill biome selection array, located on WorldBiomes.cs
			//iterate from min to max
			int min = (int)worldData.biomesList[i].biomeSelectParams[0].min;
			int max = (int)worldData.biomesList[i].biomeSelectParams[0].max;
			bool started = false;
			BiomeRef bbRef = null;
			//for min to max..
			for (int n = min; n < max; n++){
				//start to fill in a new section of biome
				if (worldData.biomes[n] == null && !started){
					bbRef = new BiomeRef(worldData.biomesList[i],n,i);
					worldData.biomes[n] = bbRef;
					started = true;
				}
				//continue filling in
				else if (worldData.biomes[n] == null && started){
					//if this is the last increment of the for loop, initialize the bime
					if (n + 1 >= max){
						if (bbRef != null){
							bbRef.UBound = n;
							float length = bbRef.UBound - bbRef.LBound;
							bbRef.median = bbRef.LBound + length * 0.5f;
							bbRef.LQuartile = bbRef.LBound + length * worldData.BiomeBlendPercent;
							bbRef.UQuartile = bbRef.LBound + length * (1 - worldData.BiomeBlendPercent);
							started = false;
							worldData.biomes[n] = bbRef;
						}
					}
					//otherwise continue filling references
					else
						worldData.biomes[n] = bbRef;
				}
				//if this position is already filled, end the current section and initialize the biome
				else if (worldData.biomes[n] != null && started){
					if (bbRef != null){
						bbRef.UBound = n-1;
						float length = bbRef.UBound - bbRef.LBound;
						bbRef.median = bbRef.LBound + length * 0.5f;
						bbRef.LQuartile = bbRef.LBound + length * worldData.BiomeBlendPercent;
						bbRef.UQuartile = bbRef.LBound + length * (1 - worldData.BiomeBlendPercent);
						started = false;
					}
				}
				else
					started = false;
			}

			//Fill sub biome selection arrays on each biome
			for (int m = 0; m < worldData.biomesList[i].SubBiomes.Count; m++){
				int smin = (int)worldData.biomesList[i].SubBiomes[m].biomeSelectParams[0].min;
				int smax = (int)worldData.biomesList[i].SubBiomes[m].biomeSelectParams[0].max;
				bool sstarted = false;
				BiomeRef sRef = null;

				for (int k = smin; k < smax; k++){
					//start to fill in a new section of biome
					if (worldData.biomesList[i].NearSubBiomes[k] == null && !sstarted){
						sRef = new BiomeRef(worldData.biomesList[i].SubBiomes[m],k,i);
						worldData.biomesList[i].NearSubBiomes[k] = sRef;
						sstarted = true;
					}
					//continue filling in
					else if (worldData.biomesList[i].NearSubBiomes[k] == null && sstarted){
						//if this is the last increment of the for loop, initialize the bime
						if (k + 1 >= (int)smax){
							if (sRef != null){
								sRef.UBound = k;
								float slength = sRef.UBound - sRef.LBound;
								sRef.median = sRef.LBound + slength * 0.5f;
								sRef.LQuartile = sRef.LBound + slength * worldData.BiomeBlendPercent;
								sRef.UQuartile = sRef.LBound + slength * (1 - worldData.BiomeBlendPercent);
								sstarted = false;
								worldData.biomesList[i].NearSubBiomes[k] = sRef;
							}
						}
						//otherwise continue filling references
						else
							worldData.biomesList[i].NearSubBiomes[k] = sRef;
					}
					//if this position is already filled, end the current section and initialize the biome
					else if (worldData.biomesList[i].NearSubBiomes[k] != null && sstarted){
						if (sRef != null){
							sRef.UBound = k-1;
							float slength = sRef.UBound - sRef.LBound;
							sRef.median = sRef.LBound + slength * 0.5f;
							sRef.LQuartile = sRef.LBound + slength * worldData.BiomeBlendPercent;
							sRef.UQuartile = sRef.LBound + slength * (1 - worldData.BiomeBlendPercent);
							sstarted = false;
						}
					}
					else
						sstarted = false;
				}
			}

			/*for (int j = 0; j < 100; j++) {
				if (worldData.biomesList[i].NearSubBiomes[j] != null)
					Debug.Log("- Current - " + j + " Q's: "  + worldData.biomesList[i].NearSubBiomes[j].biome.biomeName + " " + worldData.biomesList[i].NearSubBiomes[j].LQuartile + "  " + worldData.biomesList[i].NearSubBiomes[j].UQuartile + " L,U " + worldData.biomesList[i].NearSubBiomes[j].LBound + " " + worldData.biomesList[i].NearSubBiomes[j].UBound +"   *****************************  ");	
			}*/

			//Assign lower and upper adjacent biomes in NearSubBiomes array
			//Assign Upper and lower Biomes for each BiomeRef
			bool newBiome = true;
			BiomeRef cB = null;
			BiomeRef pB = null;
			//for the length of NearSubBiomes array
			for (int p = 0; p < 100; p++){
				//If the array is null at this position, make a new reference assign it in the array
				if (worldData.biomesList[i].NearSubBiomes[p] == null && newBiome){
					newBiome = false;

					int fsmin = 0;
					if (pB != null)
						fsmin = (int)pB.UBound;
					cB = new BiomeRef(worldData.biomesList[i],fsmin,-1);

					worldData.biomesList[i].NearSubBiomes[p] = cB;
				}
				//if the position in the array is null and we just made a new biomeRef
				else if (worldData.biomesList[i].NearSubBiomes[p] == null && !newBiome){
					worldData.biomesList[i].NearSubBiomes[p] = cB;
				}
				//if the position in the array is not null
				else if (worldData.biomesList[i].NearSubBiomes[p] != null){
					if (!newBiome){
						cB.UBound = worldData.biomesList[i].NearSubBiomes[p].LBound;
						float fslength = cB.UBound - cB.LBound;
						cB.median = cB.LBound + fslength * 0.5f;
						cB.LQuartile = cB.LBound + fslength * worldData.BiomeBlendPercent;
						cB.UQuartile = cB.LBound + fslength * (1 - worldData.BiomeBlendPercent);
					}
					newBiome = true;
					cB = worldData.biomesList[i].NearSubBiomes[p];
				}
				if ( p + 1 >= 100 && worldData.biomesList[i].NearSubBiomes[p].pos == -1){
					cB.UBound = 100;
					float fslength = cB.UBound - cB.LBound;
					cB.median = cB.LBound + fslength * 0.5f;
					cB.LQuartile = cB.LBound + fslength * worldData.BiomeBlendPercent;
					cB.UQuartile = cB.LBound + fslength * (1 - worldData.BiomeBlendPercent);
				}
				//if this position is different than the previous one
				if (pB != null && pB.LBound != cB.LBound ){
					pB.UBiomeRef = cB;
					cB.LBiomeRef = pB;
				}
				pB = cB;
			}

			/*for (int j = 0; j < 100; j++) {
				Debug.Log("------------ Current ---------" + worldData.biomesList[i].NearSubBiomes[j].biome.biomeName + " " + worldData.biomesList[i].NearSubBiomes[j].LQuartile + " " + worldData.biomesList[i].NearSubBiomes[j].UQuartile + " " + worldData.biomesList[i].NearSubBiomes[j].LBound+ " " + worldData.biomesList[i].NearSubBiomes[j].UBound +"   -----------------------------------------------------   ");	
			if (worldData.biomesList[i].NearSubBiomes[j].UBiomeRef != null)
					Debug.Log ("Upper " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.UBound + " " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.LBound + " q's " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.UQuartile +" " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.LQuartile + " " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.biome.biomeName);
			if (worldData.biomesList[i].NearSubBiomes[j].LBiomeRef != null)
					Debug.Log (" Lower " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.UBound + " " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.LBound + " q's " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.UQuartile +" " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.LQuartile + " " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.biome.biomeName);
		}*/
		}

		/*for (int j = 0; j < 100; j++) {
			if (worldData.biomes[j] != null)
			Debug.Log("- Current - " + j + " Q's: "  + worldData.biomes[j].biome.biomeName + " " + worldData.biomes[j].LQuartile + "  " + worldData.biomes[j].UQuartile + " L,U " + worldData.biomes[j].LBound + " " + worldData.biomes[j].UBound +"   *****************************  ");	
		}*/

		//Assign Upper and lower Biomes for each BiomeRef, works same as above
		bool bnewBiome = true;
		BiomeRef bcB = null;
		BiomeRef bpB = null;
		for (int p = 0; p < 100; p++){
			//array at [p] is empty, fill it with base biome
			if (worldData.biomes[p] == null && bnewBiome){
				//no new initialization is needed
				bnewBiome = false;

				int bfsmin = 0;

				if (bpB != null)
					bfsmin = (int)bpB.UBound;

				bcB = new BiomeRef(worldData.biomesList[0],bfsmin,0);

				worldData.biomes[p] = bcB;
			}
			//array at [p] is empty, base biome reference is already initialized
			else if (worldData.biomes[p] == null && !bnewBiome){
				worldData.biomes[p] = bcB;
				
			}
			//if the array at [p] is not empty ( a biome has already been placed there)
			else if (worldData.biomes[p] != null){
				//if the base biome has already been initialized
				if (!bnewBiome){
					bcB.UBound = worldData.biomes[p].LBound;
					float bfslength = bcB.UBound - bcB.LBound;
					bcB.median = bcB.LBound + bfslength * 0.5f;
					bcB.LQuartile = bcB.LBound + bfslength * worldData.BiomeBlendPercent;
					bcB.UQuartile = bcB.LBound + bfslength * (1 - worldData.BiomeBlendPercent);
				}
				//set base biome initialization boolean to true, allows for the creation of another biome
				bnewBiome = true;
				bcB = worldData.biomes[p];
			}
			if ( p + 1 >= 100 && worldData.biomes[p].pos == 0){
				bcB.UBound = 100;
				float bfslength = bcB.UBound - bcB.LBound;
				bcB.median = bcB.LBound + bfslength * 0.5f;
				bcB.LQuartile = bcB.LBound + bfslength * worldData.BiomeBlendPercent;
				bcB.UQuartile = bcB.LBound + bfslength * (1 - worldData.BiomeBlendPercent);
			}
			//if the current biome at [p] is different than the previous one
			if (bpB != null && bpB.LBound != bcB.LBound){
				bpB.UBiomeRef = bcB;
				bcB.LBiomeRef = bpB;
			}
			bpB = bcB;
		}

		/*for (int j = 0; j < 100; j++) {
			Debug.Log("------------ Current ---------" + worldData.biomes[j].biome.biomeName + " " + worldData.biomes[j].LQuartile + " " + worldData.biomes[j].UQuartile + "   -----------------------------------------------------   ");	
			if (worldData.biomes[j].UBiomeRef != null)
				Debug.Log ("Upper " + worldData.biomes[j].UBiomeRef.UBound + " " + worldData.biomes[j].UBiomeRef.LBound + " q's " + worldData.biomes[j].UBiomeRef.UQuartile +" " + worldData.biomes[j].UBiomeRef.LQuartile + " " + worldData.biomes[j].UBiomeRef.biome.biomeName);
			if (worldData.biomes[j].LBiomeRef != null)
				Debug.Log (" Lower " + worldData.biomes[j].LBiomeRef.UBound + " " + worldData.biomes[j].LBiomeRef.LBound + " q's " + worldData.biomes[j].LBiomeRef.UQuartile +" " + worldData.biomes[j].LBiomeRef.LQuartile + " " + worldData.biomes[j].LBiomeRef.biome.biomeName);
		}*/
	}


	/// <summary>
	/// Reformats blocks listed in a layer so that a cube type is at the front
	/// followed by slope types, and so that each slope type is also alloted into a seperate list
	/// </summary>
	/// <param name="i">The index.</param>
	/// <param name="layer">Layer.</param>
	void RotationTask(int i, BlockLayer layer){

		int pos = -1;
		if (blocksList[layer.layers[i][0].blockID1].l[layer.layers[i][0].blockID2].blockType != primitiveBlockType.cube){
			for (int b = 0; b < layer.layers[i].Count; b++){
				if (blocksList[layer.layers[i][b].blockID1].l[layer.layers[i][b].blockID2].blockType == primitiveBlockType.cube)
					pos = b;
			}
		}
		else
			pos = 0;

		if (pos != -1){
			//swap cube type with first element
			BlockIDInt32 b = layer.layers[i][pos];
			layer.layers[i][pos] = layer.layers[i][0];
			layer.layers[i][0] = b;
		}
		else{
            //add first element to back
			layer.layers[i].Add(layer.layers[i][0]);
			//place default grass as first
			layer.layers[i][0] = new BlockIDInt32(108,0);
		}

        //Initialize if not initialized
        if (layer.Cslopes == null)
            layer.Cslopes = new BlockIDInt32[3][];
        if (layer.Sslopes == null)
            layer.Sslopes = new BlockIDInt32[3][];
        if (layer.slopes == null)
            layer.slopes = new BlockIDInt32[3][];

        //initalize if not initialized
        if (layer.Cslopes[i] == null)
            layer.Cslopes[i] = new BlockIDInt32[25];
        if (layer.Sslopes[i] == null)
            layer.Sslopes[i] = new BlockIDInt32[25];
        if (layer.slopes[i] == null)
            layer.slopes[i] = new BlockIDInt32[25];

        //usually n will go up to around 4 or so
        for (int n = 1; n < layer.layers[i].Count; n++){
			if (blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].blockType == primitiveBlockType.slope){
				layer.slopes[i][blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].Rotation] = layer.layers[i][n];
			}
			else if (blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].blockType == primitiveBlockType.cornerSlope)
				layer.Cslopes[i][blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].Rotation] = layer.layers[i][n];
			else if (blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].blockType == primitiveBlockType.sunkenCornerSlope)
				layer.Sslopes[i][blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].Rotation] = layer.layers[i][n];
		}
	}
	
	GameObject[] gameObjects;
	
	//finds and destroys menu obgjects
	void DestroyAllObjects()
	{
		gameObjects = GameObject.FindGameObjectsWithTag ("Menu1");
		
		for(var i = 0 ; i < gameObjects.Length ; i ++)
		{
			Destroy(gameObjects[i]);
		}
	}
	
	//finds and destroys chunk objects
	void DestroyAllChunkObjects()
	{
		gameObjects = GameObject.FindGameObjectsWithTag ("Chunky1");
		
		for(var i = 0 ; i < gameObjects.Length ; i ++)
		{
			Destroy(gameObjects[i]);
		}
		
		gameObjects = GameObject.FindGameObjectsWithTag ("Chunk");
		for(var i = 0 ; i < gameObjects.Length ; i ++)
		{
			Destroy(gameObjects[i]);
		}
	}
	
	//fps
	float deltaTime = 0.0f;
	float fps = 0.0f;
	int timeTot;
	float time=0.0f;
	void Update()
	{
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		fps = 1.0f / deltaTime;
		if (i != totalChunks && !cancled)
		{
			//timeTot = (int)(((totalChunks - i) / (i / (Time.time - timeInit)))*(1.4f-(float)(i/totalChunks)));
			timeTot = (int)((totalChunks - i) / (i / (Time.time - timeInit)));
			time = Time.time;
		}
	}
	
	public void setWorldName(string name){
		worldName = name;
	}
}