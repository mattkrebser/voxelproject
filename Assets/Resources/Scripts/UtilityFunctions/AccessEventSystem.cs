using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public sealed class AccessEventSystem : MonoBehaviour
{

	private AccessStandalone accessStandalone;

	void Awake(){
		accessStandalone = GetComponent<AccessStandalone> ();
	}

	/// <summary>
	/// Returns pointer event data
	/// </summary>
	/// <returns>The last pointer enter object.</returns>
	public PointerEventData GetPointerEventData(){
		PointerEventData standAlone = accessStandalone.GetLastPointerEventDataPublic (-1);
		return standAlone;
	}

	/// <summary>
	/// Gets the GameObject that the pointer is above
	/// </summary>
	/// <returns>The last pointer enter object.</returns>
	public GameObject GetPointerEnter(){

		if (GetPointerEventData () == null)
			return null;

		return GetPointerEventData ().pointerEnter;
	}

	/// <summary>
	/// Gets the pointer position
	/// </summary>
	/// <returns>The pointer.</returns>
	public Vector2 GetPointer(){
		return GetPointerEventData ().position;
	}

}

