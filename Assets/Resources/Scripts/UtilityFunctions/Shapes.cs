﻿using UnityEngine;
using System.Collections;

public static class Shapes {

    //we have short names for the colliders because
    //the physics manager compares them based on names

    /// <summary>
    /// BoxColliderName
    /// </summary>
    public static string BoxCollider
    {
        get
        {
            return "B";
        }
    }
    /// <summary>
    /// SlopedColliderName
    /// </summary>
    public static string RampCollider
    {
        get
        {
            return "S";
        }
    }
    /// <summary>
    /// SunkenSloped Collider Name
    /// </summary>
    public static string RampInsideCollider
    {
        get
        {
            return "K";
        }
    }
    /// <summary>
    /// Corner Sloped Collider Name
    /// </summary>
    public static string RampCornerCollider
    {
        get
        {
            return "C";
        }
    }
    /// <summary>
    /// Name for diamond shaped colliders
    /// </summary>
    public static string Diamond
    {
        get
        {
            return "D";
        }
    }


    /// <summary>
    /// Gets a collider gameObject name for the input type.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static string block_type_to_collider_name(primitiveBlockType type)
    {
        if (type == primitiveBlockType.cube)
        {
            return BoxCollider;
        }
        else if (type == primitiveBlockType.slope)
        {
            return RampCollider;
        }
        else if (type == primitiveBlockType.sunkenCornerSlope)
        {
            return RampInsideCollider;
        }
        else if (type == primitiveBlockType.cornerSlope)
        {
            return RampCornerCollider;
        }
        else
        {
            return BoxCollider;
        }
    }


    /// <summary>
    /// Places the input object in alignment with the voxel grid
    /// </summary>
    /// <param name="g"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    public static void voxel_place_block(GameObject g, int gx, int gy, int gz)
    {
        g.transform.localPosition = new Vector3(gx + 0.5f, gy - 0.5f, gz + 0.5f);
    }
    /// <summary>
    /// Places a ramp with the input rotation. (See Block.Rotations) in the voxel grid.
    /// </summary>
    /// <param name="g"></param>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <param name="rotation_num"></param>
    public static void voxel_place_ramp(GameObject g, int gx, int gy, int gz, int rotation_num)
    {
        if (rotation_num == 1)
        {
            g.transform.localEulerAngles = new Vector3(0, 0, 0);
            g.transform.localPosition = new Vector3(gx, gy, gz);
        }
        else if (rotation_num == 2)
        {
            g.transform.localEulerAngles = new Vector3(0, 90, 0);
            g.transform.localPosition = new Vector3(gx, gy, gz + 1);
        }
        else if (rotation_num == 3)
        {
            g.transform.localEulerAngles = new Vector3(0, 180, 0);
            g.transform.localPosition = new Vector3(gx + 1, gy, gz + 1);
        }
        else if (rotation_num == 4)
        {
            g.transform.localEulerAngles = new Vector3(0, -90, 0);
            g.transform.localPosition = new Vector3(gx + 1, gy, gz);
        }
        else
        {
            GameLog.Out("Cannot handle a rotation " + rotation_num.ToString());
        }
    }
    /// <summary>
    /// Places a corner ramp with the input rotation. (See Block.Rotations) in the voxel grid.
    /// </summary>
    /// <param name="g"></param>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <param name="rotation_num"></param>
    public static void voxel_place_corner_ramp(GameObject g, int gx, int gy, int gz, int rotation_num)
    {
        if (rotation_num == 1)
        {
            g.transform.localEulerAngles = new Vector3(0, 0, 0);
            g.transform.localPosition = new Vector3(gx, gy, gz);
        }
        else if (rotation_num == 2)
        {
            g.transform.localEulerAngles = new Vector3(0, -90, 0);
            g.transform.localPosition = new Vector3(gx + 1, gy, gz);
        }
        else if (rotation_num == 3)
        {
            g.transform.localEulerAngles = new Vector3(0, 90, 0);
            g.transform.localPosition = new Vector3(gx, gy, gz + 1);
        }
        else if (rotation_num == 4)
        {
            g.transform.localEulerAngles = new Vector3(0, 180, 0);
            g.transform.localPosition = new Vector3(gx + 1, gy, gz + 1);
        }
        else
        {
            GameLog.Out("Cannot handle a rotation " + rotation_num.ToString());
        }
    }
    /// <summary>
    /// Places a inside ramp with the input rotation. (See Block.Rotations) in the voxel grid.
    /// </summary>
    /// <param name="g"></param>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <param name="rotation_num"></param>
    public static void voxel_place_inside_ramp(GameObject g, int gx, int gy, int gz, int rotation_num)
    {
        if (rotation_num == 1)
        {
            g.transform.localEulerAngles = new Vector3(0, 0, 0);
            g.transform.localPosition = new Vector3(gx, gy, gz);
        }
        else if (rotation_num == 2)
        {
            g.transform.localEulerAngles = new Vector3(0, -90, 0);
            g.transform.localPosition = new Vector3(gx + 1, gy, gz);
        }
        else if (rotation_num == 3)
        {
            g.transform.localEulerAngles = new Vector3(0, 90, 0);
            g.transform.localPosition = new Vector3(gx, gy, gz + 1);
        }
        else if (rotation_num == 4)
        {
            g.transform.localEulerAngles = new Vector3(0, 180, 0);
            g.transform.localPosition = new Vector3(gx + 1, gy, gz + 1);
        }
        else
        {
            GameLog.Out("Cannot handle a rotation " + rotation_num.ToString());
        }
    }


    /// <summary>
    /// Return a collider shaped like a standard voxel cube
    /// </summary>
    /// <returns></returns>
    public static GameObject GetCube()
    {
        GameObject new_obj = new GameObject(BoxCollider);
        new_obj.AddComponent<BoxCollider>().size = new Vector3(1, 1, 1);
        new_obj.layer = PhysicsManager.VoxelColliderLayer;
        return new_obj;
    }
    /// <summary>
    /// Returns a convex triangular prism mesh GameObject
    /// </summary>
    /// <returns></returns>
    public static GameObject GetRamp()
    {
        GameObject new_obj = new GameObject(RampCollider);
        MeshCollider mc;
        (mc = new_obj.AddComponent<MeshCollider>()).convex = true;
        Mesh new_mesh = new Mesh();
        new_mesh.vertices = Ramp;
        new_mesh.triangles = RampTris;
        mc.sharedMesh = new_mesh;
        new_obj.layer = PhysicsManager.VoxelColliderLayer;
        return new_obj;
    }
    /// <summary>
    /// Returns convex mesh for RampCorner
    /// </summary>
    /// <returns></returns>
    public static GameObject GetRampCorner()
    {
        GameObject new_obj = new GameObject(RampCornerCollider);
        MeshCollider mc;
        (mc = new_obj.AddComponent<MeshCollider>()).convex = true;
        Mesh new_mesh = new Mesh();
        new_mesh.vertices = RampCorner;
        new_mesh.triangles = RampCornerTris;
        mc.sharedMesh = new_mesh;
        new_obj.layer = PhysicsManager.VoxelColliderLayer;
        return new_obj;
    }
    /// <summary>
    /// Returns Convex Mesh for Ramp Inside
    /// </summary>
    /// <returns></returns>
    public static GameObject GetRampInside()
    {
        GameObject new_obj = new GameObject(RampInsideCollider);
        MeshCollider mc;
        (mc = new_obj.AddComponent<MeshCollider>()).convex = true;
        Mesh new_mesh = new Mesh();
        new_mesh.vertices = RampInside;
        new_mesh.triangles = RampInsideTris;
        mc.sharedMesh = new_mesh;
        new_obj.layer = PhysicsManager.VoxelColliderLayer;
        return new_obj;
    }
    /// <summary>
    /// Get Diamond Shaped collider
    /// </summary>
    /// <returns></returns>
    public static GameObject GetDiamond()
    {
        GameObject new_obj = new GameObject(Diamond);
        MeshCollider mc;
        (mc = new_obj.AddComponent<MeshCollider>()).convex = true;
        Mesh new_mesh = new Mesh();
        new_obj.layer = PhysicsManager.CharacterColliderLayer;
        new_mesh.vertices = DiamondVertices;
        new_mesh.triangles = DiamondTris;
        mc.sharedMesh = new_mesh;
        return new_obj;
    }

    private static readonly Vector3[] Ramp =
    {    
        new Vector3(0,0,1), //0
        new Vector3(0,-1,1), //1
        new Vector3(0,-1,0), //2
        new Vector3(1,0,1), //3
        new Vector3(1,-1,1), //4
        new Vector3(1,-1,0) //5
    };
    private static readonly int[] RampTris =
    {
        0,2,1,
        4,5,3,
        3,2,0,
        3,5,0,
        3,1,4,
        3,0,4,
        5,4,1,
        2,4,1
    };

    private static readonly Vector3[] RampCorner =
    {
        new Vector3(1,-1,0),//0
        new Vector3(1,-1,1),//1
        new Vector3(1,0,1),//2
        new Vector3(0,-1,1)//3
    };

    private static readonly int[] RampCornerTris =
    {
        1,0,2,
        0,3,1,
        3,1,1,
        3,0,2
    };

    private static readonly Vector3[] RampInside =
    {
        new Vector3(0,0,1), //0
        new Vector3(0,-1,1), //1
        new Vector3(0,-1,0), //2
        new Vector3(1,0,1), //3
        new Vector3(1,-1,1), //4
        new Vector3(1,-1,0), //5
        new Vector3(1,0,0)//6
    };
    private static readonly int[] RampInsideTris =
    {
        0,2,1,
        3,1,4,
        3,0,4,
        5,4,1,
        2,4,1,
        4,5,3,
        5,6,3,
        2,6,5,
        2,0,6
    };

    public static readonly Vector3[] DiamondVertices =
    {
        new Vector3(0,-0.5f,0),  //0
        new Vector3(0, 0, 0.5f), //1
        new Vector3(0.5f, 0, 0), //2
        new Vector3(0, 0, -0.5f),//3
        new Vector3(-.5f, 0, 0), //4
        new Vector3(0, 0.5f, 0)  //5
    };

    public static readonly int[] DiamondTris =
    {
        0,4,3,
        0,3,2,
        0,1,4,
        0,2,1,
        5,4,3,
        5,3,2,
        5,1,4,
        5,2,1
    };

    /// <summary>
    /// Pleae Note*** These are per triangle normals, and not per vertice normals!
    /// </summary>
    public static readonly Vector3[] DiamondNormals =
    {
        (Vector3.down + Vector3.left + Vector3.back).normalized,
        (Vector3.down + Vector3.right + Vector3.back).normalized,
        (Vector3.down + Vector3.left + Vector3.forward).normalized,
        (Vector3.down + Vector3.right + Vector3.forward).normalized,
        (Vector3.up + Vector3.left + Vector3.back).normalized,
        (Vector3.up + Vector3.right + Vector3.back).normalized,
        (Vector3.up + Vector3.left + Vector3.forward).normalized,
        (Vector3.up + Vector3.right + Vector3.forward).normalized
    };
}
