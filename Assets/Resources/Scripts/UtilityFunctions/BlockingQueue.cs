﻿using UnityEngine;
using System.Collections.Generic;

public class BlockingQueue<T>
{
    private Queue<T> queue;

    public BlockingQueue()
    {
        queue = new Queue<T>();
    }
	
    public void Enqueue(T item)
    {
        lock (this)
        {
            queue.Enqueue(item);
        }
    }

    public T Dequeue()
    {
        lock (this)
        {
            return queue.Dequeue();
        }
    }

    public int Count
    {
        get
        {
            lock (this)
            {
                return queue.Count;
            }
        }
    }

    public void Clear()
    {
        lock (this)
        {
            queue.Clear();
        }
    }
}
