using UnityEngine;
using System.Collections;

public static class GameLog{

    public static bool DISPLAY_VOXEL_STATUS_LOGS = false;
	public static bool DISPLAY_WARNING_LOGS = true;
	public static bool DISPLAY_ERROR_LOGS = true;
    public static bool DISPLAY_OBJECT_STATUS_LOGS = false;
    public static bool DISPLAY_TASKING_STATUS_LOGS = false;
    public static bool DISPLAY_EVENT_STATUS_LOG = false;
    public static bool DISPLAY_INPUT_LOG = false;
    public static bool DISPLAY_FILE_STATUS_LOG = false;

    /// <summary>
    /// Will create gameObjects to represent where chunks currently exist
    /// </summary>
    public static bool VISUALIZE_CHUNK_ARRAYS = false;
    /// <summary>
    /// If true, voxel casts will be drawn in editor mode
    /// </summary>
    public static bool VISUALIZE_VOXEL_CASTS = false;

    public static bool DEBUG_MODE = true;

    private static int Logs_this_frame = 0;
    private static int MAX_LOGS_PER_FRAME = 100;

    private static bool Can_log
    {
        get
        {
            return Logs_this_frame < MAX_LOGS_PER_FRAME;
        }
    }

    internal static void GameLogFunction()
    {
        if (DEBUG_MODE)
        {
            if (Logs_this_frame > MAX_LOGS_PER_FRAME)
            {
                Debug.Log("Exceeded max logs per frame!");
            }
            Logs_this_frame = 0;
        }
    }

    public static bool OutputToLog(string s){
        if (DEBUG_MODE && Can_log && DISPLAY_ERROR_LOGS)
        {
            Debug.Log(s);
            Logs_this_frame++;
            return true;
        }
		return false;
	}

    /// <summary>
    /// Unlike the other output functions, Out will always send output to
    /// the main log.
    /// </summary>
    /// <param name="s"></param>
    public static void Out(string s)
    {
        if (Can_log)
            Debug.Log(s);
        Logs_this_frame++;
    }

    /// <summary>
    /// Enable/Disable error logs.
    /// </summary>
    /// <param name="new_mode"></param>
    public static void Set_Debug_Mode(bool new_mode)
    {
        DEBUG_MODE = new_mode;
    }

    public static bool OutputToFileStatusLog(string s)
    {
        if (DEBUG_MODE && Can_log && DISPLAY_FILE_STATUS_LOG)
        {
            Debug.Log(s);
            Logs_this_frame++;
            return true;
        }
        return false;
    }

    public static bool OutputToLog(System.Exception e)
    {
        if (DEBUG_MODE && Can_log && DISPLAY_ERROR_LOGS)
        {
            Debug.Log(e.ToString());
            Logs_this_frame++;
            return true;
        }
        return false;
    }

    public static bool OutputToWarningLog(System.Exception e)
    {
        if (DEBUG_MODE && Can_log && DISPLAY_WARNING_LOGS)
        {
            Debug.Log(e.ToString());
            Logs_this_frame++;
            return true;
        }
        return false;
    }

    public static bool OutputToVoxelStatusLog(System.Exception e)
    {
        if (DEBUG_MODE && Can_log && DISPLAY_VOXEL_STATUS_LOGS)
        {
            Debug.Log(e.ToString());
            Logs_this_frame++;
            return true;
        }
        return false;
    }

    public static bool OutputToWarningLog(string s){
        if (DEBUG_MODE && Can_log && DISPLAY_WARNING_LOGS)
        {
            Debug.Log(s);
            Logs_this_frame++;
            return true;
        }
        return false;
	}

	public static bool VoxelStatusOutput(string s){
		if (DEBUG_MODE && Can_log && DISPLAY_VOXEL_STATUS_LOGS)
        {
			Debug.Log (s);
            Logs_this_frame++;
            return true;
		}
		return false;
	}

    public static bool TaskStatusOutput(string s)
    {
        if (DEBUG_MODE && Can_log && DISPLAY_TASKING_STATUS_LOGS)
        {
            Debug.Log(s);
            Logs_this_frame++;
            return true;
        }
        return false;
    }

    public static bool OutputToObjectLog(string s)
    {
        if (DEBUG_MODE && Can_log && DISPLAY_OBJECT_STATUS_LOGS)
        {
            Debug.Log(s);
            Logs_this_frame++;
            return true;
        }
        return false;
    }

    public static bool OutputToEventLog(string s)
    {
        if (DEBUG_MODE && Can_log && DISPLAY_EVENT_STATUS_LOG)
        {
            Debug.Log(s);
            Logs_this_frame++;
            return true;
        }
        return false;
    }

    public static bool OutputToInputLog(KeySet input)
    {
        if (DEBUG_MODE && Can_log && DISPLAY_INPUT_LOG)
        {
            Debug.Log(input);
            Logs_this_frame++;
            return true;
        }
        return false;
    }

}	


