﻿using System.Collections.Generic;
using UnityEngine;
using System;

/// <summary>
/// Global Inout Handler, no local stuff
/// </summary>
public static class InputHandler
{
    private static Dictionary<KeySet, List<Action>> global_input = new Dictionary<KeySet, List<Action>>();

    private static float scrl_whl;
    private static Vector2 ms_chg;
    public static Vector2 MouseDelta
    {
        get
        {
            return ms_chg;
        }
    }

    /// <summary>
    /// Object under the mouse. Null if there is none.
    /// </summary>
    public static GameObject Active3DObject;

    /// <summary>
    /// Add a global input. This input delegate will be called whenever the specified Input set is activated
    /// </summary>
    /// <param name="input"></param>
    /// <param name="f"></param>
    public static void AddGlobalInput(KeySet input, Action f)
    {
        //include keyset into inputevent logic
        if (KeySet.IsMultiKey(input))
            InputEvents.AddKeyCombo(input);

        List<Action> l = null;
        if (global_input.TryGetValue(input, out l))
        {
            l.Add(f);
        }
        else
        {
            l = new List<Action>(); l.Add(f);
            global_input.Add(input, l);
        }
    }

    /// <summary>
    /// Remove the specified input
    /// </summary>
    /// <param name="input"></param>
    /// <param name="f"></param>
    public static void RemoveGlobalInput(KeySet input, Action f)
    {
        List<Action> l = null;
        if (global_input.TryGetValue(input, out l))
        {
            l.Remove(f);
        }

        //no need to look for keysets that arent being used
        if (l.Count == 0)
            InputEvents.remove_key_combo(input);
    }

    /// <summary>
    /// Runs all functions for the input keyset
    /// </summary>
    /// <param name="input"></param>
    public static void RunInputCheck(KeySet input)
    {
        List<Action> l;
        if (global_input.TryGetValue(input, out l))
        {
            foreach (Action d in l)
            {
                d();
            }
        }
    }

    public static void AssignScrollWheelChange(float t)
    {
        scrl_whl = t;
    }
    public static float GetScrollWheelChange()
    {
        return scrl_whl;
    }
    public static void AssignMouseChange(float x, float y)
    {
        ms_chg = new Vector2(x, y);
    }
}