﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public struct Dimensions : IEquatable<Dimensions>, IComparable
{
    public int x, y, z;
    /// <summary>
    /// corresponds to z-axis, width of this object in 3D space
    /// </summary>
	public int width;
    /// <summary>
    /// Corresponds to y-axis, height of this object in 3D space
    /// </summary>
	public int height;
    /// <summary>
    /// Corresponds to x-axis, length of this Object in 3D space
    /// </summary>
	public int length;

    /// <summary>
    /// Compares positions of dimensions
    /// </summary>
    /// <param name="d1"></param>
    /// <param name="d2"></param>
    /// <returns></returns>
    public static bool operator ==(Dimensions d1, Dimensions d2)
    {
        if (d1.x == d2.x && d1.y == d2.y && d1.z == d2.z)
            return true;
        return false;
    }
    /// <summary>
    /// compares psotions of dimensions
    /// </summary>
    /// <param name="d1"></param>
    /// <param name="d2"></param>
    /// <returns></returns>
    public static bool operator !=(Dimensions d1, Dimensions d2)
    {
        return !(d1 == d2);
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Dimensions"/> struct. Fixed Object constructor
    /// </summary>
    /// <param name="X">X.</param>
    /// <param name="Y">Y.</param>
    /// <param name="Z">Z.</param>
    /// <param name="Width">Width.</param>
    /// <param name="Height">Height.</param>
    /// <param name="Length">Length.</param>
    public Dimensions(int X, int Y, int Z, int Width, int Height, int Length)
    {
        x = X;
        y = Y;
        z = Z;
        width = Width;
        height = Height;
        length = Length;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Dimensions"/> struct. Mean as a ObjectType.Dynamic constructor.
    /// Width,Length,Height all set to zero.
    /// </summary>
    /// <param name="X">X.</param>
    /// <param name="Y">Y.</param>
    /// <param name="Z">Z.</param>
    public Dimensions(int X, int Y, int Z)
    {
        x = X;
        y = Y;
        z = Z;
        width = default(int);
        height = default(int);
        length = default(int);
    }

    /// <summary>
    /// Compares position of dimensions
    /// </summary>
    /// <param name="d"></param>
    /// <returns></returns>
    public bool Equals(Dimensions d)
    {
        if (d.x == x && d.y == y && d.z == z)
            return true;
        return false;
    }

    /// <summary>
    /// This comparer only compares through object position (x, y ,z)
    /// </summary>
    /// <param name="obj"></param>
    /// <returns></returns>
    public override bool Equals(object obj)
    {
        return obj is Dimensions ? Equals((Dimensions)obj) : false;
    }

    /// <summary>
    /// Equality comparison for all fields
    /// </summary>
    /// <param name="d"></param>
    /// <returns></returns>
    public bool Complete_Equals(Dimensions d)
    {
        if (x == d.x && y == d.y && z == d.z && length == d.length && width == d.width && height == d.height)
            return true;
        return false;
    }

    /// <summary>
    /// Compares to 'obj'. If this vector is greater, return 1. Determined by distance from origin
    /// </summary>
    /// <returns>The to.</returns>
    /// <param name="obj">Object.</param>
    public int CompareTo(object obj)
    {
        return obj is Dimensions ? Compare((Dimensions)obj) : 0;
    }

    private int Compare(Dimensions d)
    {
        int thisDistance = x * x + y * y + z * z;
        int otherDistance = d.x * d.x + d.y * d.y + d.z * d.z;

        if (thisDistance > otherDistance)
            return 1;
        else if (thisDistance < otherDistance)
            return -1;
        else
            return 0;
    }

    public override int GetHashCode()
    {
        unchecked // Overflow is fine, just wrap
        {
            int hash = 17;
            // Suitable nullity checks etc, of course :)
            hash = hash * 29 + x.GetHashCode();
            hash = hash * 29 + y.GetHashCode();
            hash = hash * 29 + z.GetHashCode();
            return hash;
        }
    }

    public static Dimensions ToDimensions(Vector3 v)
    {
        return new Dimensions((int)v.x, (int)v.y, (int)v.z);
    }

    public static Dimensions ToDimensions(Vector3 v, int width, int height, int length)
    {
        return new Dimensions((int)v.x, (int)v.y, (int)v.z);
    }

    /// <summary>
    /// returns new Vector3S(x,y,z)
    /// </summary>
    public Vector3S Position
    {
        get
        {
            return new Vector3S(x, y, z);
        }
    }

    public override string ToString()
    {
        return "Dimensions:\n  -Position(" + x + ", " + y + ", " + z + "),\n  " +
            "-Size(" + length + ", " + width + ", " + height + ")";
    }

    public string ToStringSimple()
    {
        return x + "," + y + "," + z + "," + length + "," + width + "," + height;
    }

    public string ToStringSimplePosition()
    {
        return x + "," + y + "," + z;
    }

    /// <summary>
    /// Volume in voxel space of this Dimensions
    /// </summary>
    public int Volume
    {
        get
        {
            return length * width * height;
        }
    }

    /// <summary>
    /// Returns true if the input coordinates intersect this Dimensions object.
    /// Inclusive for edges.
    /// </summary>
    /// <param name="gx"></param>
    /// <param name="gy"></param>
    /// <param name="gz"></param>
    /// <returns></returns>
    public bool is_intersecting(int gx, int gy, int gz)
    {
        if (gx >= x && gx <= x + length &&
            gy >= y && gy <= y + height &&
            gz >= z && gz < z + width)
            return true;
        return false;
    }
}
