﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;


public class Tuple
{

}

//param 1, p2,p3,p4,return type
public sealed class Tuple<T1, T2, T3, T4, T5> : Tuple
{
    public T1 t1;
    public T2 t2;
    public T3 t3;
    public T4 t4;
    public T5 t5;

    public Tuple() { }

    public Tuple(T1 t1i, T2 t2i, T3 t3i, T4 t4i, T5 t5i)
    {
        t1 = t1i;
        t2 = t2i;
        t3 = t3i;
        t4 = t4i;
        t5 = t5i;
    }
}

//param 1, p2, p3, return type, action is 4 params
public sealed class Tuple<T1, T2, T3, T4> : Tuple
{
    public T1 t1;
    public T2 t2;
    public T3 t3;
    public T4 t4;

    public Tuple() { }

    public Tuple(T1 t1i, T2 t2i, T3 t3i, T4 t4i)
    {
        t1 = t1i;
        t2 = t2i;
        t3 = t3i;
        t4 = t4i;
    }
}

//param1, p2, return type, action is 3 params
public sealed class Tuple<T1, T2, T3> : Tuple
{
    public T1 t1;
    public T2 t2;
    public T3 t3;

    public Tuple() { }

    public Tuple(T1 t1i, T2 t2i, T3 t3i)
    {
        t1 = t1i;
        t2 = t2i;
        t3 = t3i;
    }
}

//param1, return type, action is 2 params
public sealed class Tuple<T1, T2> : Tuple
{
    public T1 t1;
    public T2 t2;

    public Tuple() { }

    public Tuple(T1 t1i, T2 t2i)
    {
        t1 = t1i;
        t2 = t2i;
    }
}

//just a return type included, action is one param
public sealed class Tuple<T1> : Tuple
{
    public T1 t1;

    public Tuple() { }

    public Tuple(T1 t1i)
    {
        t1 = t1i;
    }

}

