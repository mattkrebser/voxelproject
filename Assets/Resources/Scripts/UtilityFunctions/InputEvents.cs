using System.Collections.Generic;
using UnityEngine;
using System;

public sealed class InputEvents : MonoBehaviour
{
	private static int dragCounter = 0;

    /// <summary>
    /// Are we currently dragging?
    /// </summary>
    public static bool Is_Dragging
    {
        get
        {
            if (dragCounter >= 69)
                return true;
            return false;
        }
    }

    private Event e;

    private static Dictionary<KeyCode, List<KeySet>> allowedMultiKeys = new Dictionary<KeyCode, List<KeySet>>();
    private static Dictionary<KeyCode, KeyStatus> key_status = new Dictionary<KeyCode, KeyStatus>();
    private static HashSet<KeySet> multiKeyEventFired = new HashSet<KeySet>();

    private static HashSet<KeySet> beingHeldDown = new HashSet<KeySet>();

    void Awake()
    {
        Array enums = System.Enum.GetValues(typeof(KeyCode));
        foreach (KeyCode e in enums)
        {
            //None is always 'DOWN' (being pressed)
            if (e == KeyCode.None)
            {
                key_status.Add(e, KeyStatus.DOWN);
            }
            else if (!key_status.ContainsKey(e))
                key_status.Add(e, default(KeyStatus));
        }
    }

    /// <summary>
    /// Try to run a multi key event on the current key
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    private static KeySet try_multi(KeyCode key)
    {
        if (allowedMultiKeys.ContainsKey(key))
        {
            List<KeySet> l = null;
            if (allowedMultiKeys.TryGetValue(key, out l))
            {
                if (l == null)
                    return KeySet.zero;
                //length of l is the number of different combos that this key is involved in,
                //which gets specified by you (using AddKeyCombo(KeySet))
                for (int i = 0; i < l.Count; i++)
                {
                    if (key_status[l[i].key1] == KeyStatus.DOWN && key_status[l[i].key2] == KeyStatus.DOWN &&
                        key_status[l[i].key3] == KeyStatus.DOWN)
                    {
                        return l[i];
                    }
                }
            }
            else
            {
                GameLog.OutputToEventLog("Internal Error, KeyCode listed as multi input, but no combos found.");
                return KeySet.zero;
            }
        }
        return KeySet.zero;
    }

    /// <summary>
    /// Returns true if the input key is part of multi-key sets
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    private static bool is_multi(KeyCode key)
    {
        List<KeySet> l = null;
        if (allowedMultiKeys.TryGetValue(key, out l))
            if (l != null && l.Count > 0)
                return true;
        return false;
    }

    /// <summary>
    /// Fires an inputkey up for each multi key combination associated with the input key
    /// </summary>
    /// <param name="key"></param>
    private static void multi_key_up(KeyCode key)
    {
        if (allowedMultiKeys.ContainsKey(key))
        {
            List<KeySet> l = null;
            if (allowedMultiKeys.TryGetValue(key, out l))
            {
                if (l == null)
                    return;
                //length of l is the number of different combos that this key is involved in,
                //which gets specified by you (using AddKeyCombo(KeySet))
                for (int i = 0; i < l.Count; i++)
                {
                    if (beingHeldDown.Contains(l[i]))
                        InputHandler.RunInputCheck(new KeySet(l[i].key1, l[i].key2, l[i].key3, GameEventTypes.inputKeyUp));
                }
            }
            else
            {
                GameLog.OutputToEventLog("Internal Error, KeyCode listed as multi input, but no combos found.");
                return;
            }
        }
    }
    private static void remove_multi_key_event(KeyCode key)
    {
        if (allowedMultiKeys.ContainsKey(key))
        {
            List<KeySet> l = null;
            if (allowedMultiKeys.TryGetValue(key, out l))
            {
                if (l == null)
                    return;
                //length of l is the number of different combos that this key is involved in,
                //which gets specified by you (using AddKeyCombo(KeySet))
                for (int i = 0; i < l.Count; i++)
                {
                    if (multiKeyEventFired.Contains(l[i]))
                    {
                        multiKeyEventFired.Remove(l[i]);
                    }
                }
            }
            else
            {
                GameLog.OutputToEventLog("Internal Error, KeyCode listed as multi input, but no combos found.");
                return;
            }
        }
    }
    private static void remove_held_down(KeyCode key)
    {
        if (beingHeldDown.Contains(new KeySet(key, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
        {
            beingHeldDown.Remove(new KeySet(key, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
        }

        if (!is_multi(key))
            return;

        if (allowedMultiKeys.ContainsKey(key))
        {
            List<KeySet> l = null;
            if (allowedMultiKeys.TryGetValue(key, out l))
            {
                if (l == null)
                    return;
                //length of l is the number of different combos that this key is involved in,
                //which gets specified by you (using AddKeyCombo(KeySet))
                for (int i = 0; i < l.Count; i++)
                {
                    if (beingHeldDown.Contains(l[i]))
                    {
                        beingHeldDown.Remove(l[i]);
                    }
                }
            }
            else
            {
                GameLog.OutputToEventLog("Internal Error, KeyCode listed as multi input, but no combos found.");
                return;
            }
        }
    }
    /// <summary>
    /// Add any key combo (from KeyCode enum) of up to three keys. EventType is ignored.
    /// </summary>
    /// <param name="combo"></param>
    public static void AddKeyCombo(KeySet combo)
    {
        if (combo.key1 == combo.key2 || combo.key1 == combo.key3 || combo.key2 == combo.key3)
        {
            GameLog.OutputToEventLog("Error, combinations have to be unique keys!");
            return;
        }
        if (KeySet.IsForbiddenKey(combo.key1) || KeySet.IsForbiddenKey(combo.key2) || KeySet.IsForbiddenKey(combo.key3))
        {
            GameLog.OutputToEventLog("Error, a input key in the combination is invalid! Use KeySet.IsForbidden()");
            return;
        }
        if (combo == KeySet.zero)
        {
            GameLog.OutputToLog("Zero KeySet is not allowed!");
            return;
        }
            List<KeySet> l = null;
        //Add each key/KeySet pair. We don't need to add the key if it is none
        if (combo.key1 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key1, out l))
        {

            if (l.Contains(combo))
                if (GameLog.DISPLAY_EVENT_STATUS_LOG)
                    GameLog.OutputToEventLog("Error, already have a multi key combo " + combo.ToString());
            else
                l.Add(combo);
        }
        else if (combo.key1 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key1, l);
        }

        l = null;
        if (combo.key2 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key2, out l))
        {

            if (l.Contains(combo))
                GameLog.OutputToEventLog("Error, already have a multi key combo " + combo.ToString());
            else
                l.Add(combo);
        }
        else if (combo.key2 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key2, l);
        }

        l = null;
        if (combo.key3 != KeyCode.None && allowedMultiKeys.TryGetValue(combo.key3, out l))
        {

            if (l.Contains(combo))
                GameLog.OutputToEventLog("Error, already have a multi key combo " + combo.ToString());
            else
                l.Add(combo);
        }
        else if (combo.key3 != KeyCode.None)
        {
            l = new List<KeySet>();
            l.Add(combo);
            allowedMultiKeys.Add(combo.key3, l);
        }
    }

    /// <summary>
    /// remove a KeySet
    /// </summary>
    /// <param name="combo"></param>
    public static void remove_key_combo(KeySet combo)
    {
        if (combo.key1 != KeyCode.None)
        {
            List<KeySet> combos = null;
            if (allowedMultiKeys.TryGetValue(combo.key1, out combos))
            {
                if (combos != null)
                {
                    for (int i = combos.Count - 1; i >= 0; i--)
                    {
                        if (combo == combos[i])
                        {
                            combos.RemoveAt(i);
                        }
                    }
                    if (combos.Count == 0)
                        allowedMultiKeys.Remove(combo.key1);
                }
            }
        }
        if (combo.key2 != KeyCode.None)
        {
            List<KeySet> combos = null;
            if (allowedMultiKeys.TryGetValue(combo.key2, out combos))
            {
                if (combos != null)
                {
                    for (int i = combos.Count - 1; i >= 0; i--)
                    {
                        if (combo == combos[i])
                        {
                            combos.RemoveAt(i);
                        }
                    }
                    if (combos.Count == 0)
                        allowedMultiKeys.Remove(combo.key1);
                }
            }
        }
        if (combo.key3 != KeyCode.None)
        {
            List<KeySet> combos = null;
            if (allowedMultiKeys.TryGetValue(combo.key3, out combos))
            {
                if (combos != null)
                {
                    for (int i = combos.Count - 1; i >= 0; i--)
                    {
                        if (combo == combos[i])
                        {
                            combos.RemoveAt(i);
                        }
                    }
                    if (combos.Count == 0)
                        allowedMultiKeys.Remove(combo.key1);
                }
            }
        }
    }

    /// <summary>
    /// Key is DOWN/UP
    /// </summary>
    /// <param name="key"></param>
    /// <param name="stat"></param>
    private static void set_key_status(KeyCode key, KeyStatus stat)
    {
        if (key != KeyCode.None)
            key_status[key] = stat;
    }

    private static int _framesLast = -1;
    private static int FramesSinceLastShiftDown
    {
        get
        {
            return _framesLast;
        }
        set
        {
            if (value > 10)
                _framesLast = 10;
            else
                _framesLast = value;
        }
    }

    //Side note*
    /*

    For multi key events, the last held down key will continue to generate onDown events after the multi key event ends

        -unless it is the key released that ends the onDown event

    If the last key held down is a mouse button, then the most recently held down keyboard key will keep generating
    onDown events

    You can test the behaviour by adding a debug statement in the InputHandler.RunInputCheck function
    Debug(input);
    
    */

    void OnGUI(){
		e = Event.current;
        KeySet multi = KeySet.zero;

        //the events are listed in order of their priority in this if/else (this only effects multi key mouse/keyboard conflicts)
            
        //on key up event
        if (e.type == EventType.KeyUp)
        {
            if (is_multi(e.keyCode))
            {
                multi_key_up(e.keyCode);
                remove_multi_key_event(e.keyCode);
            }
            //key up gets called for everything (don't want to miss any key up)
            InputHandler.RunInputCheck(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            //remove key down because that is what we added
            remove_held_down(e.keyCode);
            set_key_status(e.keyCode, KeyStatus.UP);
        }

        //on keyhelddown event
        else if (e.type == EventType.KeyDown && e.keyCode != KeyCode.None)
        {
            set_key_status(e.keyCode, KeyStatus.DOWN);
            multi = try_multi(e.keyCode);
            //If this current key is involved in a multi-key event, only the multi key event gets called for KeyDown events
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(e.keyCode, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
        }

        HandleShift(e, ref multi);

        if (e.type == EventType.MouseDown)
        {
            TryMouseDown(e, ref multi);
        }

        else if (e.type == EventType.MouseUp)
        {
            TryMouseUp(e, ref multi);
        }

        if (e.type == EventType.ScrollWheel)
        {
            InputHandler.AssignScrollWheelChange(Input.GetAxis("Mouse ScrollWheel"));
            InputHandler.RunInputCheck(InputHandler.GetScrollWheelChange() < 0 ? KeySet.ScrollDown : KeySet.ScrollUp);
        }
        else
            InputHandler.AssignScrollWheelChange(0);

        //handle drags. Drag end calls on mouse release. Drag events only use the mouse
        if (e.type == EventType.mouseDrag)
        {
            if (dragCounter == 0)
            {
                InputHandler.RunInputCheck(KeySet.BeginDrag);
                dragCounter = 69;
            }
        }
        else if (e.type == EventType.MouseUp && dragCounter >= 69)
        {
            InputHandler.RunInputCheck(KeySet.EndDrag);
            dragCounter = 0;
        }
    }

    /// <summary>
    /// If the application looses focues, then release anything being held down
    /// </summary>
    /// <param name="focusStatus"></param>
    void OnApplicationFocus(bool focusStatus)
    {
        beingHeldDown.Clear();
    }

	void Update(){

        //handle mouse input
        float dx = Input.GetAxis("MouseX");
        float dy = Input.GetAxis("MouseY");
        if (dx != 0 || dy != 0)
        {
            InputHandler.AssignMouseChange(dx, dy);
            InputHandler.RunInputCheck(KeySet.MoveMouse);
        }
        else
            InputHandler.AssignMouseChange(0, 0);

        //handle Shift button (allow for a max of one shift button down event per frame)
        if (FramesSinceLastShiftDown != -1)
            FramesSinceLastShiftDown++;

        //since Collecting Events in the OnGUI loop cancels out all previous buttonDowns
        //we have to remember each button down and continue to call them!
        foreach (KeySet set in beingHeldDown)
        {
            InputHandler.RunInputCheck(new KeySet(set.key1, set.key2, set.key3, GameEventTypes.inputKeyDown));
        }
    }

    /// <summary>
    /// Check for each mouse button
    /// </summary>
    /// <param name="e"></param>
    /// <param name="multi"></param>
    void TryMouseDown(Event e, ref KeySet multi)
    {
        if (e.button == 0)
        {
            set_key_status(KeyCode.Mouse0, KeyStatus.DOWN);
            multi = try_multi(KeyCode.Mouse0);
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
            
        }
        else if (e.button == 1)
        {
            set_key_status(KeyCode.Mouse1, KeyStatus.DOWN);
            multi = try_multi(KeyCode.Mouse1);
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
        }
        else if (e.button == 2)
        {
            set_key_status(KeyCode.Mouse2, KeyStatus.DOWN);
            multi = try_multi(KeyCode.Mouse2);
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(KeyCode.Mouse2, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(KeyCode.Mouse2, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse2, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
        }
    }

    /// <summary>
    /// check for each mouse button up
    /// </summary>
    /// <param name="e"></param>
    /// <param name="multi"></param>
    void TryMouseUp(Event e, ref KeySet multi)
    {
        
        if (e.button == 0)
        {
            if (is_multi(KeyCode.Mouse0))
            {
                multi_key_up(KeyCode.Mouse0);
                remove_multi_key_event(KeyCode.Mouse0);
            }
            //key up gets called for everything (don't want to miss any key up)
            InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            remove_held_down(KeyCode.Mouse0);
            set_key_status(KeyCode.Mouse0, KeyStatus.UP);
        }
        else if (e.button == 1)
        {
            if (is_multi(KeyCode.Mouse1))
            {
                //if a multi key group ended
                multi_key_up(KeyCode.Mouse1);
                remove_multi_key_event(KeyCode.Mouse1);
            }
            //key up gets called for everything (don't want to miss any key up)
            InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse1, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            remove_held_down(KeyCode.Mouse1);
            set_key_status(KeyCode.Mouse1, KeyStatus.UP);
        }
        else if (e.button == 2)
        {
            if (is_multi(KeyCode.Mouse2))
            {
                multi_key_up(KeyCode.Mouse2);
                remove_multi_key_event(KeyCode.Mouse2);
            }
            //key up gets called for everything (don't want to miss any key up)
            InputHandler.RunInputCheck(new KeySet(KeyCode.Mouse2, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            remove_held_down(KeyCode.Mouse2);
            set_key_status(KeyCode.Mouse2, KeyStatus.UP);
        }
    }

    void HandleShift(Event e, ref KeySet multi)
    {
        //shift key down. Unity doesn't consider shift to be a normal keydown event. bleh
        if (e.shift)
        {
            if (FramesSinceLastShiftDown == 0)
                return;
            FramesSinceLastShiftDown = 0;
            set_key_status(KeyCode.LeftShift, KeyStatus.DOWN);
            multi = try_multi(KeyCode.LeftShift);
            //If this current key is involved in a multi-key event
            if (!multi.Equals(KeySet.zero))
            {
                if (!multiKeyEventFired.Contains(multi))
                {
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.multiKey));
                    multiKeyEventFired.Add(multi);
                }
                if (!beingHeldDown.Contains(multi))
                {
                    beingHeldDown.Add(multi);
                    InputHandler.RunInputCheck(new KeySet(multi.key1, multi.key2, multi.key3, GameEventTypes.inputKeyDown));
                }
            }
            if (!beingHeldDown.Contains(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown)))
            {
                beingHeldDown.Add(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
                InputHandler.RunInputCheck(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown));
            }
        }
        //shift key up, we have to fake it
        if (FramesSinceLastShiftDown > 4)
        {
            FramesSinceLastShiftDown = -1;
            multi = try_multi(KeyCode.LeftShift);
            //if a multi key group ended
            if (is_multi(KeyCode.LeftShift))
            {
                multi_key_up(KeyCode.LeftShift);
                remove_multi_key_event(KeyCode.LeftShift);
            }
            remove_held_down(KeyCode.LeftShift);
            //key up gets called for everything (don't want to miss any key up). We don't have to remove shift from the beingHeldDown table
            InputHandler.RunInputCheck(new KeySet(KeyCode.LeftShift, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp));
            set_key_status(KeyCode.LeftShift, KeyStatus.UP);
        }
    }
}

/// <summary>
/// Types of events that get recognized as inputs
/// </summary>
public enum GameEventTypes
{
    none,
    /// <summary>
    /// pressed any type of input up (realesed)
    /// </summary>
	inputKeyDown,
    /// <summary>
    /// pressed any type of input down
    /// </summary>
	inputKeyUp,
    /// <summary>
    /// on mousewheel scroll up
    /// </summary>
	scrollUp,
    /// <summary>
    /// on mousewheel scroll down
    /// </summary>
	scrollDown,
    /// <summary>
    /// pointer starts hovering over a component. Only works with 
    /// </summary>
	pointerEnter,
    /// <summary>
    /// pointer stops hovering above a component (exits) Only works with 
    /// </summary>
	pointerExit,
    /// <summary>
    /// pointer hovering above component. Only works with
    /// </summary>
	hover,
    /// <summary>
    /// being dragging a component. Only works with
    /// </summary>
	beginDrag,
    /// <summary>
    /// end drag. Called on the dragged object and the active object. This gets called
    /// on the dragged object before the activeObject. Only works with 
    /// </summary>
	endDrag,
    /// <summary>
    /// called by various components, when a specific valeu changes. Such as the text in a textinput field.
    /// </summary>
    valueChange,
    /// <summary>
    /// called by textfield inputs when loosing 'text' focus
    /// </summary>
    endEdit,
    /// <summary>
    /// Only gets called if the activeObject != dragged object. Gets called as
    /// a result of a dragend event on the activeObject (the object below the dragged object). Callorder:
    /// dragend (dragged object) dragend (active object) dropped (active object).
    /// Only works with 
    /// </summary>
    dropped,
    /// <summary>
    /// the instant a multi key combo is pressed down once
    /// </summary>
    multiKey,
    /// <summary>
    /// The mouse was moved.
    /// </summary>
    mouseMove
}

/// <summary>
/// Data type for input, can contain 3 keys and a type
/// </summary>
public struct KeySet
{
    public KeyCode key1;
    public KeyCode key2;
    public KeyCode key3;
    public GameEventTypes type1;

    public KeySet(KeyCode k1, KeyCode k2, KeyCode k3, GameEventTypes t1)
    {
        key1 = k1;
        key2 = k2;
        key3 = k3;
        type1 = t1;
    }

    public KeySet(KeyCode k1)
    {
        key1 = k1;
        key2 = default(KeyCode);
        key3 = default(KeyCode);
        type1 = GameEventTypes.none;
    }

    public KeySet(KeyCode k1, GameEventTypes t1)
    {
        key1 = k1;
        key2 = default(KeyCode);
        key3 = default(KeyCode);
        type1 = t1;
    }

    public static readonly KeySet noInputHover = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.hover);
    public static readonly KeySet zero = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.none);
    public static readonly KeySet ValueChange = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.valueChange);
    public static readonly KeySet EndEdit = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.endEdit);
    public static readonly KeySet Mouse0Up = new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyUp);
    public static readonly KeySet EndDrag = new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.endDrag);
    public static readonly KeySet BeginDrag = new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.beginDrag);
    public static readonly KeySet PointerEnter = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.pointerEnter);
    public static readonly KeySet PointerExit = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.pointerExit);
    public static readonly KeySet Dropped = new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.dropped);
    public static readonly KeySet ScrollUp = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.scrollUp);
    public static readonly KeySet ScrollDown = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.scrollDown);
    public static readonly KeySet MoveMouse = new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.mouseMove);

    /// <summary>
    /// Returns default KeySet for input event enum
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public static KeySet GetDefault(GameEventTypes type)
    {
        if (type == GameEventTypes.beginDrag)
        {
            return KeySet.BeginDrag;
        }
        else if (type == GameEventTypes.endDrag)
        {
            return KeySet.EndDrag;
        }
        else if (type == GameEventTypes.dropped)
        {
            return KeySet.Dropped;
        }
        else if (type == GameEventTypes.endEdit)
        {
            return KeySet.EndEdit;
        }
        else if (type == GameEventTypes.hover)
        {
            return KeySet.noInputHover;
        }
        else if (type == GameEventTypes.inputKeyDown)
        {
            return new KeySet(KeyCode.Mouse0, KeyCode.None, KeyCode.None, GameEventTypes.inputKeyDown);
        }
        else if (type == GameEventTypes.inputKeyUp)
        {
            return KeySet.Mouse0Up;
        }
        else if (type == GameEventTypes.none)
        {
            return KeySet.zero;
        }
        else if (type == GameEventTypes.pointerEnter)
        {
            return KeySet.PointerEnter;
        }
        else if (type == GameEventTypes.pointerExit)
        {
            return KeySet.PointerExit;
        }
        else if (type == GameEventTypes.scrollDown)
        {
            return new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.scrollDown);
        }
        else if (type == GameEventTypes.scrollUp)
        {
            return new KeySet(KeyCode.None, KeyCode.None, KeyCode.None, GameEventTypes.scrollUp);
        }
        else if (type == GameEventTypes.valueChange)
        {
            return KeySet.ValueChange;
        }
        else
            return KeySet.zero;
    }

    /// <summary>
    /// Returns true if the input Key isForbidden (Won't trigger a valid input key event)
    /// </summary>
    /// <param name="inputKey"></param>
    /// <returns></returns>
    public static bool IsForbiddenKey(KeyCode inputKey)
    {
        if (inputKey == KeyCode.RightShift)
            return true;
        return false;
    }

    /// <summary>
    /// Returns true if the input keyset contains multiple keys
    /// </summary>
    /// <param name="inputKey"></param>
    /// <returns></returns>
    public static bool IsMultiKey(KeySet inputKey)
    {
        int count = 0;
        if (inputKey.key1 != KeyCode.None)
            count++;
        if (inputKey.key2 != KeyCode.None)
            count++;
        if (inputKey.key3 != KeyCode.None)
            count++;

        if (count > 1)
            return true;
        return false;
    }

    public override bool Equals(object obj)
    {
        return obj is KeySet ? equals((KeySet)obj) : false;
    }

    public override int GetHashCode()
    {
        unchecked
        {
            int hash = 29;
            hash = key1.GetHashCode() + hash * 23;
            hash = key2.GetHashCode() + hash * 23;
            hash = key3.GetHashCode() + hash * 23;
            hash = type1.GetHashCode() + hash * 23;
            return hash;
        }
    }

    public override string ToString()
    {
        object[] stuff = { key1, key2, key3, type1 };
        return String.Format("Key1: {0}, Key2: {1}, Key3: {2}, Event: {3}", stuff);
    }

    /// <summary>
    /// Determines if a KeySet is equal to another. Ordering does not matter.
    /// </summary>
    /// <param name="k"></param>
    /// <returns></returns>
    private bool equals(KeySet k)
    {
        if (k.type1 != type1)
            return false;

        if (k.key1 == key1)
        {
            if (k.key2 == key2)
            {
                if (k.key3 == key3)
                    return true;
            }
            else if (k.key2 == key3)
            {
                if (k.key3 == key2)
                    return true;
            }
        }
        else if (k.key1 == key2)
        {
            if (k.key2 == key1)
            {
                if (k.key3 == key3)
                    return true;
            }
            else if (k.key2 == key3)
            {
                if (k.key3 == key1)
                    return true;
            }
        }
        else if (k.key1 == key3)
        {
            if (k.key2 == key1)
            {
                if (k.key3 == key2)
                    return true;
            }
            else if (k.key2 == key2)
            {
                if (k.key3 == key1)
                    return true;
            }
        }
        return false;
    }

    public static bool operator ==(KeySet k1, KeySet k2)
    {
        return k1.Equals(k2);
    }

    public static bool operator !=(KeySet k1, KeySet k2)
    {
        return !(k1 == k2);
    }
}

public enum KeyStatus
{
    UP = 0,
    DOWN = 1
}