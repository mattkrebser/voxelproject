using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;

//All UI objects are assumed to be upper left corner anchored
// min = max = pivot = Vector2(0,1)
//This script also assumes only the base canvas can have a scale that is not (1,1,1)
//and that all Scales are x=y=z
public sealed class UIScript : MonoBehaviour
{
	//this gameobject is active on start?
	public bool ActiveOnStart = true;

	void Awake(){
		if(ActiveOnStart == false)
			gameObject.SetActive(false);
		else
			gameObject.SetActive(true);
	}

	void Start(){

	}

	/// <summary>
	/// GameObject.Active state is matched to the passed parameter's toggle component
	/// </summary>
	/// <param name="g">The green component.</param>
	public void matchToggle(GameObject g){

		bool val = g.GetComponent<Toggle>().isOn;

		if (val == true)
			gameObject.SetActive(true);
		else
			gameObject.SetActive(false);
	}

	//Sets gameObject attatched to this to not active
	public void setThisToNotActive(){
		gameObject.SetActive (false);
	}

	//Sets gameObject attachted to this active
	public void setThisToActive(){
		gameObject.SetActive (true);
	}

	//Sets this GameObject's input field to match a slider value
	public void SetInputFieldValue(GameObject g){

		this.gameObject.GetComponent<InputField> ().text = Convert.ToString(g.GetComponent<Slider> ().value);

	}

	//Sets a slider value based on an input field, also corrects invalid values
	public void SetSliderValue(GameObject g){

		float val;
		bool tru = Single.TryParse (g.GetComponent<InputField> ().text, out val);

		if (tru){
			if (val <= this.gameObject.GetComponent<Slider>().maxValue && val >= this.gameObject.GetComponent<Slider>().minValue){
				this.gameObject.GetComponent<Slider>().value = val;
			}
			else{
				g.GetComponent<InputField>().text = Convert.ToString (this.gameObject.GetComponent<Slider>().value);
			}
		}
		else
			g.GetComponent<InputField>().text = Convert.ToString (this.gameObject.GetComponent<Slider>().value);

	}

}

[System.Serializable]
public enum Positioning
{
	none,
	down,
	left,
	right
}






















