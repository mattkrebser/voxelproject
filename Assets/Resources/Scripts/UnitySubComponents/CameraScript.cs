﻿using UnityEngine;
using System.Collections;
using System;


//Cameras use local position, in fact every 3D object uses local position

[RequireComponent(typeof(Camera))]
public class CameraScript : MonoBehaviour {
    /// <summary>
    /// Previous position of the object being followed
    /// </summary>
    Vector3 prev_position;

    /// <summary>
    /// Transform the camera is following, use 'SetCameraFollow' to set this value
    /// </summary>
    public Transform follow_this;

    /// <summary>
    /// If the object being followed has a CharacterMoverScript, then this is it.
    /// </summary>
    internal CharacterMoverScript char_mover;

    /// <summary>
    /// Determines how the camera will follow an object
    /// </summary>
    public CameraModes CameraMode;

    /// <summary>
    /// Distance at which the Camera will preferably follow the follow object
    /// </summary>
    public float follow_distance = 10.0f;
    private float minimum_angle = -50.0f;
    private float maximum_angle = 80.0f;
    /// <summary>
    /// Amount the camera can look down. Between 0 and -90 degrees
    /// </summary>
    public float MinAngle
    {
        get
        {
            return minimum_angle;
        }
        set
        {
            if (value > -1)
                minimum_angle = -1;
            else if (value <= -89)
                minimum_angle = -89;
            else
                minimum_angle = value;
        }
    }
    /// <summary>
    /// Amount the Camera can look up. Between 0 and 90 degrees.
    /// </summary>
    public float MaxAngle
    {
        get
        {
            return maximum_angle;
        }
        set
        {
            if (value > 89)
                maximum_angle = 89;
            else if (value < 1)
                maximum_angle = 1;
            else
                maximum_angle = value;
        }
    }

    /// <summary>
    /// Prepare for destruction
    /// </summary>
    public void Destruct()
    {
        follow_this = null;
    }

    void LateUpdate()
    {
        CharacterMoverScript cms = null;
        if (follow_this != null && (cms = follow_this.GetComponent<CharacterMoverScript>()) != null)
        {
            if (cms != char_mover)
            {
                char_mover = cms;
                char_mover.attatched_camera = this;
            }
        }
        CameraMovement();
    }

    void Start()
    {
        transform.parent = null;
    }

    private void CameraMovement()
    {
        if (follow_this != null)
        {
            //follow a character
            if (char_mover != null)
            {
                if (CameraMode == CameraModes.first_person)
                {
                    //get mouse inputs
                    float dx = InputHandler.MouseDelta.x;
                    float dy = InputHandler.MouseDelta.y;

                    //multiply by the turn speed
                    float additive_y_axis = dx * char_mover.turn_speed;
                    float additive_x_axis = dy * char_mover.turn_speed * 5;

                    //get rotation of camera and the collider
                    Vector3 curr_rot = char_mover.char_body.this_RigidBody.rotation.eulerAngles;
                    Vector3 cam_rot = transform.rotation.eulerAngles;

                    //compute camera rotation
                    Quaternion cam_rotation = Quaternion.Euler(new Vector3(cam_rot.x - additive_x_axis,
                        curr_rot.y + additive_y_axis,
                        0));

                    //set rotation
                    transform.rotation = Quaternion.Lerp(transform.rotation, cam_rotation, Time.deltaTime * 15);

                    if (prev_position != Vector3.zero)
                    {
                        //lerp the camera, the intiger 10 is a speed modifier to make the lerp faster
                        transform.position = Vector3.Lerp(transform.position, prev_position, Time.deltaTime * 10);
                    }

                    prev_position = char_mover.CamFollowPosition;
                }
                else if (CameraMode == CameraModes.third_person_free_rotation)
                {
                    if (prev_position != Vector3.zero)
                    {
                        //get mouse delta
                        float dy = InputHandler.MouseDelta.x * char_mover.turn_speed;
                        float dx = InputHandler.MouseDelta.y * char_mover.turn_speed;


                        //transform rotation
                        Quaternion rotation = Quaternion.Euler(ClampAngle(transform.rotation.eulerAngles.x - dx,
                             minimum_angle, maximum_angle),
                             transform.rotation.eulerAngles.y + dy, 0);

                        float modified_follow_distance = ReduceCameraDistanceByAngle(10, rotation.eulerAngles, char_mover.Width);

                        //The position of the camera is the position of the transform being followed minus
                        //the camera's look direction vector multiplied by the follow distance
                        Vector3 position = prev_position - (rotation * Vector3.forward * modified_follow_distance);

                        Vector3 collision_hit; RaycastHit ray_hit;
                        if (PhysicsManager.voxel_cast_sphere((position - prev_position).normalized, prev_position,
                            modified_follow_distance, 0.2f, char_mover.moving_in_world, out collision_hit))
                            
                        {
                            //there was a collision, lerp to collision location
                            transform.position = Vector3.Lerp(transform.position, collision_hit, Time.deltaTime * 15);
                        }
                        else if(Physics.SphereCast(prev_position, 0.2f, (position - prev_position).normalized, out ray_hit,
                            modified_follow_distance, 1 << PhysicsManager.EnvironmentColliderLayer))
                        {
                            //Calculate where the center of the sphere is
                            Vector3 point = prev_position + ((position - prev_position).normalized * ray_hit.distance);
                            transform.position = Vector3.Lerp(transform.position, point, Time.deltaTime * 15);
                        }
                        else
                        {
                            //lerp to position if there was no collision
                            transform.position = position;
                        }

                        transform.rotation = rotation;
                    }

                    prev_position = char_mover.CamFollowPosition;
                }
                else if (CameraMode == CameraModes.third_person)
                {
                    if (prev_position != Vector3.zero)
                    {
                        float dx = InputHandler.MouseDelta.x;
                        float dy = InputHandler.MouseDelta.y;

                        //multiply by the turn speed
                        float additive_y_axis = dx * char_mover.turn_speed;
                        float additive_x_axis = dy * char_mover.turn_speed * 5;

                        //get rotation of camera and the collider
                        Vector3 curr_rot = char_mover.char_body.transform.rotation.eulerAngles;
                        Vector3 cam_rot = transform.rotation.eulerAngles;

                        //compute camera rotation
                        Quaternion cam_rotation = Quaternion.Euler(new Vector3(cam_rot.x - additive_x_axis,
                            curr_rot.y + additive_y_axis,
                            0));
                        //set rotation
                        transform.rotation = Quaternion.Lerp(transform.rotation, cam_rotation, Time.deltaTime * 15);


                        //Reduce the follow distance if the camera is under th object being followed
                        float modified_follow_distance = ReduceCameraDistanceByAngle(10, transform.rotation.eulerAngles,
                            char_mover.Width);
                        Vector3 position = prev_position - (transform.rotation * Vector3.forward * modified_follow_distance);

                        Vector3 collision_hit; RaycastHit ray_hit;
                        if (PhysicsManager.voxel_cast_sphere((position - prev_position).normalized, prev_position,
                            modified_follow_distance, 0.2f, char_mover.moving_in_world, out collision_hit))
                        {
                            //there was a collision, lerp to collision location
                            transform.position = Vector3.Lerp(transform.position, collision_hit, Time.deltaTime * 25);
                        }
                        else if (Physics.SphereCast(prev_position, 0.2f, (position - prev_position).normalized, out ray_hit,
                            modified_follow_distance, 1 << PhysicsManager.EnvironmentColliderLayer))
                        {
                            //Calculate where the center of the sphere is
                            Vector3 point = prev_position + ((position - prev_position).normalized * ray_hit.distance);
                            transform.position = Vector3.Lerp(transform.position, point, Time.deltaTime * 15);
                        }
                        else
                        {
                            //lerp to position if there was no collision
                            transform.position = position;
                        }
                    }
                    prev_position = char_mover.CamFollowPosition;
                }
                else
                {

                }
            }
            //follow something else
            else
            {

            }
        }
    }

    /// <summary>
    /// Clamp the angle.
    /// </summary>
    /// <param name="angle"></param>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private float ClampAngle(float angle, float min, float max)
    {
        if (min > 0)
            min = 0;
        if (max > 180)
            max = 180;

        if (angle >= 180 && angle < 360 + min)
            angle = 360 + min;
        if (angle > 80 && angle < 180)
            angle = 80;
        return angle;
    }

    /// <summary>
    /// Reduces the follow distance based on how far under the camera is compared to the
    /// object being followed. (Zoom in when underneath the object). Only works for angle_threshold
    /// 1 to 90
    /// </summary>
    /// <param name="angle_threshold"></param>
    /// <returns></returns>
    private float ReduceCameraDistanceByAngle(float angle_threshold, Vector3 rotation_euler, float min_follow)
    {
        if (angle_threshold > 0 || angle_threshold <= 90)
        {
            if (rotation_euler.x > 90)
            {
                float total_angular_distance = angle_threshold + 90;
                float multiplier = (90 - (360 - rotation_euler.x)) / total_angular_distance;
                return follow_distance * multiplier * multiplier < min_follow ? min_follow : follow_distance * multiplier * multiplier; 
            }
            else
            {
                if (rotation_euler.x > angle_threshold)
                    return follow_distance;
                else
                {
                    float total_angular_distance = angle_threshold + 90;
                    float multiplier = (rotation_euler.x + 90) / total_angular_distance;
                    return follow_distance * multiplier * multiplier < min_follow ? min_follow : follow_distance * multiplier * multiplier;
                }
            }
        }
        else
            return follow_distance;
    }
}

public enum CameraModes
{
    first_person,
    third_person_free_rotation,
    third_person,
    non_moving
}