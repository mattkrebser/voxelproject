﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class ColliderScript : MonoBehaviour {

    /// <summary>
    /// Id for this object
    /// </summary>
    private int collider_id;
    /// <summary>
    /// Read only id
    /// </summary>
    public int ColliderKey
    {
        get
        {
            return collider_id;
        }
    }

    /// <summary>
    /// Keeps a reference to all collider vertices
    /// </summary>
    private static Dictionary<int, tuple<Vector3[], int[], Vector3[]>> collider_vertices =
        new Dictionary<int, tuple<Vector3[], int[], Vector3[]>>();

    /// <summary>
    /// Rigid body reference
    /// </summary>
    [System.NonSerialized]
    public Rigidbody this_RigidBody;

    private Collider this_collider;
    /// <summary>
    /// The collider type used
    /// </summary>
    public Collider this_Collider
    {
        get
        {
            return this_collider;
        }
    }

    /// <summary>
    /// Current movement vector (applies force according to this vector)
    /// </summary>
    [System.NonSerialized]
    public Vector3 movement_vector;
    /// <summary>
    /// The world used to determine voxel collisions
    /// </summary>
    public WorldData colliding_with
    {
        get
        {
            return WorldManager.ActiveWorld;
        }
    }

    /// <summary>
    /// previous position.
    /// </summary>
    [System.NonSerialized]
    public Vector3 prev_pos;

    /// <summary>
    /// Colliders being used by this object
    /// </summary>
    public Dictionary<Vector3Int32, GameObject> my_colliders = new Dictionary<Vector3Int32, GameObject>();

    /// <summary>
    /// If the collider spawns in an area whos chunks are not yet loaded,
    /// it will be held in place until the voxels are initialized
    /// </summary>
    [System.NonSerialized]
    public bool chunk_is_loaded = false;

    RigidbodyConstraints original_constraints;
    /// <summary>
    /// The constraints applied to this collider. Please set this value and not the rigid body value.
    /// </summary>
    public RigidbodyConstraints constraints
    {
        get
        {
            return this_RigidBody == null ? RigidbodyConstraints.None : this_RigidBody.constraints;
        }
        set
        {
            original_constraints = value;
            this_RigidBody.constraints = value;
        }
    }

    private CharacterMoverScript char_mover;
    public CharacterMoverScript CharacterMover
    {
        get
        {
            return char_mover;
        }
    }

    /// <summary>
    /// Material this collider uses
    /// </summary>
    private PhysicMaterial collider_material
    {
        get
        {
            return this_Collider.material;
        }
        set
        {
            this_Collider.material = value;
        }
    }
    /// <summary>
    /// Set the friction level
    /// </summary>
    public float friction
    {
        set
        {
            collider_material.dynamicFriction = value;
        }
        get
        {
            return collider_material == null ? 0 : collider_material.dynamicFriction;
        }
    }

    Vector3Int32 vox_ext_swb;
    /// <summary>
    /// Bottom corner of the voxel extents to search for voxel collisions. (Think of a box
    /// around this collider as the area to search for collisions)
    /// </summary>
    public Vector3Int32 VoxelExtent_SWB
    {
        get
        {
            return vox_ext_swb;
        }
    }
    Vector3Int32 vox_ext_neu;
    /// <summary>
    /// Top corner of the voxel extents to search for voxel collisions.(Think of a box
    /// around this collider as the area to search for collisions)
    /// </summary>
    public Vector3Int32 VoxelExtent_NEU
    {
        get
        {
            return vox_ext_neu;
        }
    }

    /// <summary>
    /// Previous scale of this object before resizing
    /// </summary>
    [System.NonSerialized]
    public Vector3 prevScale;

    //we keep a collection of vertices referenced to avoid making a copy every access
    /// <summary>
    /// Returns a new lisy representing all triangles and a normal for that triangle
    /// </summary>
    public List<pairx4> ColliderVertices
    {
        get
        {
            var thisMatrix = transform.localToWorldMatrix;
            tuple<Vector3[], int[], Vector3[]> m;
            List<pairx4> l = new List<pairx4>();
            if (collider_vertices.TryGetValue(ColliderKey, out m))
            {
                int n = 0;
                //if verts is null, use character collider shape
                if (m.v1 != null && m.v2 != null)
                {
                    for (int i = 0; i < m.v2.Length; i += 3)
                    {
                        l.Add(new pairx4(thisMatrix.MultiplyPoint3x4(m.v1[m.v2[i]]),
                            thisMatrix.MultiplyPoint3x4(m.v1[m.v2[i + 1]]),
                            thisMatrix.MultiplyPoint3x4(m.v1[m.v2[i + 2]]),
                            m.v3[m.v2[n]]));
                        n++;
                    }
                    return l;
                }
                else
                {
                    for (int i = 0; i < Shapes.DiamondTris.Length; i += 3)
                    {
                        l.Add(new pairx4(thisMatrix.MultiplyPoint3x4(Shapes.DiamondVertices[Shapes.DiamondTris[i]]),
                            thisMatrix.MultiplyPoint3x4(Shapes.DiamondVertices[Shapes.DiamondTris[i + 1]]),
                            thisMatrix.MultiplyPoint3x4(Shapes.DiamondVertices[Shapes.DiamondTris[i + 2]]),
                            Shapes.DiamondNormals[n]));
                        n++;
                    }
                    return l;
                }
            }
            else
            {
                GameLog.Out("Error, couldn't find this collider's vertices!");
                return l;
            }
        }
    }
    /// <summary>
    /// Initializes this ColliderScript
    /// </summary>
    /// <param name="component_parent"></param>
    /// <returns></returns>
	void Start()
    {
        this_RigidBody = gameObject.GetComponent<Rigidbody>();
        this_collider = gameObject.GetComponent<Collider>();

        constraints = this_RigidBody.constraints;

        CalculateColliderExtents();

        prevScale = transform.localScale;

        if (this_RigidBody != null)
        {
            //determine if the spot at which this collider was placed is initialized (voxel)
            if (colliding_with != null && colliding_with.position_exists_or_air(transform.localPosition))
            {
                chunk_is_loaded = true;
            }
            else
                this_RigidBody.constraints = RigidbodyConstraints.FreezeAll;
        }

        //initialize previous position
        prev_pos = transform.localPosition;

        //try to retreive a character mover
        TryUpdateCharMover(false);

        //initialize
        if (colliding_with != null)
        {
            //Add this collider to the physics manager and return its identification key
            int val = colliding_with.GetComponent<PhysicsManager>().AddCollider(this);
            collider_id = val;
            collider_vertices.Add(val, new tuple<Vector3[], int[], Vector3[]>());
        }
        else
            StartCoroutine(DelayedPhysicsAdd());
    }

    /// <summary>
    /// Try Add this collider to physics manager, keep trying until the world is not null
    /// </summary>
    /// <returns></returns>
    IEnumerator DelayedPhysicsAdd()
    {
        while (colliding_with == null && this != null) yield return 0;
        if (this != null)
        {
            int val = colliding_with.GetComponent<PhysicsManager>().AddCollider(this);
            collider_id = val;
            collider_vertices.Add(val, new tuple<Vector3[], int[], Vector3[]>());
        }
    }

    void Update()
    {
        if (this_RigidBody != null)
        {
            //determine if the spot at which this collider was placed is initialized (voxel)
            if (colliding_with != null && colliding_with.position_exists_or_air(transform.localPosition))
            {
                chunk_is_loaded = true;
                if (this_RigidBody.constraints != original_constraints)
                    constraints = original_constraints;
            }
            else
            {
                this_RigidBody.constraints = RigidbodyConstraints.FreezeAll;
                chunk_is_loaded = false;
            }
        }
    }

    /// <summary>
    /// Attempt to update any attatched Character Mover Scripts
    /// </summary>
    /// <param name="debug_mode"></param>
    public void TryUpdateCharMover(bool debug_mode)
    {
        char_mover = GetComponent<CharacterMoverScript>();

        if (debug_mode)
        {
            GameLog.OutputToLog("Failed to retreive a rigidy body for character mover! Cannot move!");
        }
    }

    /// <summary>
    /// Recalculate voxel collision detection bounds
    /// </summary>
    public void CalculateColliderExtents()
    {
        //this only handles boxes and spheres! make it better!
        Vector3 ls = transform.localScale;
        vox_ext_swb = new Vector3Int32((int)(-1 - ls.x / 2), (int)(-1 - ls.y / 2), (int)(-1 - ls.z / 2));
        vox_ext_neu = new Vector3Int32((int)(1 + ls.x / 2), (int)(1 + ls.y / 2), (int)(1 + ls.z / 2));
    }

    /// <summary>
    /// Clear this collider from the physics manager, and the world its colliding with.
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="key"></param>
    internal void Destruct()
    {
        colliding_with.GetComponent<PhysicsManager>().RemoveCollider(collider_id, this);
        collider_vertices.Remove(collider_id);
        Destroy(this_RigidBody);
        Destroy(this_collider);
        my_colliders = null;
        char_mover = null;
    }

    /// <summary>
    /// Returns true if this Collider's ID == the input Collider's ID
    /// </summary>
    /// <param name="compare_with"></param>
    /// <returns></returns>
    public bool IDEquals(ColliderScript compare_with)
    {
        return compare_with.ColliderKey == ColliderKey;
    }

    /// <summary>
    /// A tuple
    /// </summary>
    struct tuple<V1, V2, V3>
    {
       public V1 v1;
       public V2 v2;
       public V3 v3;

        public tuple(V1 _v1, V2 _v2, V3 _v3)
        {
            v1 = _v1;
            v2 = _v2;
            v3 = _v3;
        }
    }
}

/// <summary>
/// Three vertices and a normal, used for passing triangles
/// </summary>
public struct pairx4
{
    public Vector3 v1;
    public Vector3 v2;
    public Vector3 v3;
    public Vector3 n;

    public pairx4(Vector3 t1, Vector3 t2, Vector3 t3, Vector3 n1)
    {
        v1 = t1;
        v2 = t2;
        v3 = t3;
        n = n1;
    }
}