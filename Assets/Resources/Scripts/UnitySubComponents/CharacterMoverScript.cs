﻿using UnityEngine;
using System.Collections.Generic;
using System;

[RequireComponent(typeof(ColliderScript))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public sealed class CharacterMoverScript : MonoBehaviour {

    /// <summary>
    /// The amount of time that must pass before another jump is allowed.
    /// This applys for all character movers. Best to leave it at 0.2f.
    /// </summary>
    public static readonly float Jump_Time_Limit = 0.2f;

    /// <summary>
    /// A set to keep track of all active character movers
    /// </summary>
    private static HashSet<int> active_movers = new HashSet<int>();

    /// <summary>
    /// Returns a KeyCode that is not none if the input keySet is a single key being held down
    /// </summary>
    /// <param name="inp"></param>
    /// <returns></returns>
    public static KeyCode is_single(KeySet inp)
    {
        KeyCode collected = KeyCode.None;
        int count = 0;
        if (inp.key1 != KeyCode.None)
        {
            collected = inp.key1;
            count++;
        }
        if (inp.key2 != KeyCode.None)
        {
            collected = inp.key2;
            count++;
        }
        if (inp.key3 != KeyCode.None)
        {
            collected = inp.key3;
            count++;
        }
        if (count == 1)
            return collected;
        return KeyCode.None;
    }

    /// <summary>
    /// How fast the controller moves
    /// </summary>
    public float move_speed = 5.0f;
    /// <summary>
    /// How high the controller jumps
    /// </summary>
    public float jump_height = 2.5f;
    /// <summary>
    /// The movement multiplier in air
    /// </summary>
    public float air_ctrl = 0.5f;
    /// <summary>
    /// Move multiplier on the ground
    /// </summary>
    public float move_mult = 1.3f;
    /// <summary>
    /// gravity strength multiplier. Eg) gravity_mult = 1.0 is 1.0 * -9.8 gravity
    /// </summary>
    public float gravity_mult = 1.0f;
    /// <summary>
    /// Movement penalty for pressing the crouch button
    /// </summary>
    public float crouch_mult = 0.3f;
    /// <summary>
    /// Number of jumps that can be performed before needing to touch the ground
    /// </summary>
    public int jump_count = 1;
    /// <summary>
    /// Mouse/Turn sensitivity
    /// </summary>
    public float turn_speed = 10.0f;

    /// <summary>
    /// Movement mode
    /// </summary>
    public CharacterMovementMode MovementMode;

    /// <summary>
    /// Only used if this character mover is a capsule Collider, returns the bottom of
    /// the cyclinder part of th capsule.
    /// </summary>
    private Vector3 BottomOfCylinder
    {
        get
        {
            return new Vector3(transform.localPosition.x, transform.localPosition.y - Height / 2 + Width / 2,
                transform.position.z);
        }
    }

    /// <summary>
    /// Returns a raycast hit pointed straight down. The raycast is a length of 0.3f
    /// </summary>
    public RaycastHit GroundInfo
    {
        get
        {
            RaycastHit hit;
            if (Physics.SphereCast(BottomOfCylinder, Width * 0.5f, Vector3.down, out hit, 0.3f,
                (1 << PhysicsManager.EnvironmentColliderLayer) | (1 << PhysicsManager.VoxelColliderLayer)))
            {
                return hit;
            }
            else
                return hit;
        }
    }

    /// <summary>
    /// Key to jump
    /// </summary>
    public KeyCode Jump;
    /// <summary>
    /// key to move forward
    /// </summary>
    public KeyCode Forward;
    /// <summary>
    /// key to move backward
    /// </summary>
    public KeyCode BackWard;
    /// <summary>
    /// hey to move left
    /// </summary>
    public KeyCode Left;
    /// <summary>
    /// key to move right
    /// </summary>
    public KeyCode Right;
    /// <summary>
    /// key to sprint
    /// </summary>
    public KeyCode Sprint;
    /// <summary>
    /// key to crouch
    /// </summary>
    public KeyCode Crouch;

    /// <summary>
    /// Id of this script in the char_movers dictionary
    /// </summary>
    int this_mover_id = 0;

    /// <summary>
    /// Rigid body we are moving
    /// </summary>
    [System.NonSerialized]
    public ColliderScript char_body;
    /// <summary>
    /// current jumps in sequence
    /// </summary>
    int current_jump_increment = 0;
    /// <summary>
    /// The recorded time of the last jump.
    /// </summary>
    [System.NonSerialized]
    public float time_of_last_jump = 0.0f;

    /// <summary>
    /// Attatched Camera
    /// </summary>
    public CameraScript attatched_camera;

    /// <summary>
    /// The amount of time this character mover has been falling since the last fall
    /// </summary>
    [System.NonSerialized]
    public float total_time_falling = 0.0f;

    private bool _grounded;
    /// <summary>
    /// Are we jumping/not jumping
    /// </summary>
    public bool grounded
    {
        get
        {
            return _grounded;
        }
        set
        {
            _grounded = value;

            if (value == true)
            {
                current_jump_increment = 0;
            }
        }
    }

    /// <summary>
    /// The world we are currently moving in
    /// </summary>
    public WorldData moving_in_world
    {
        get
        {
            return WorldManager.ActiveWorld;
        }
    }

    /// <summary>
    /// The constraints applied to char colliders
    /// </summary>
    public RigidbodyConstraints constraints
    {
        get
        {
            return char_body.this_RigidBody.constraints;
        }
    }

    /// <summary>
    /// Used for determining if the character is grounded or not, returns the bottom
    /// most point in world space on this character
    /// </summary>
    public float CharBottomYCoord
    {
        get
        {
            return transform.localPosition.y - transform.localScale.y * 0.5f + 0.05f; ;
        }
    }
    /// <summary>
    /// Bottom Point on Char Mover
    /// </summary>
    public Vector3 CharBottomPoint
    {
        get
        {
            return new Vector3(transform.localPosition.x, CharBottomYCoord, transform.localPosition.z);
        }
    }

    /// <summary>
    /// Height of this char mover (the collider)
    /// </summary>
    public float Height
    {
        get
        {
            return transform.localScale.y;
        }
        set
        {
            if (char_body != null)
                char_body.transform.localScale = new Vector3(char_body.transform.localScale.x,
                    value, char_body.transform.localScale.z);
        }
    }
    /// <summary>
    /// Width of this char mover (the collider)
    /// </summary>
    public float Width
    {
        get
        {
            return transform.localScale.x;
        }
        set
        {
            if (char_body != null)
                char_body.transform.localScale = new Vector3(value,
                    char_body.transform.localScale.y, value);
        }
    }
    /// <summary>
    /// Camera Pivot
    /// </summary>
    public Vector3 CamFollowPosition
    {
        get
        {
            return new Vector3(transform.localPosition.x, CharBottomYCoord + Height * 0.82f, transform.localPosition.z);
        }
    }
    /// <summary>
    /// Shoot ray casts/projectiles from here
    /// </summary>
    public Vector3 RayCastShoot
    {
        get
        {
            //TODO add weapons
            return CamFollowPosition;
        }
    }

    /// <summary>
    /// Returns an unused ID
    /// </summary>
    int free_id
    {
        get
        {
            if (active_movers.Count > 4900000)
            {
                GameLog.Out("Error, at max char movers");
                return 0;
            }
            System.Random r = new System.Random(GetInstanceID());
            int val = r.Next();
            while (active_movers.Contains(val))
            {
                val = r.Next();
            }
            return val;
        }
    }

    /// <summary>
    /// Initialize this component
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="base_obj"></param>
    void Start()
    {
        GetComponent<ColliderScript>().this_RigidBody = GetComponent<Rigidbody>();
        TryUpdateRigidBody(false);
        this_mover_id = GetInstanceID();
        if (this_mover_id == 0)
        {
            this_mover_id = free_id;
            active_movers.Add(this_mover_id);
        }
    }

    void Update()
    {
        CharacterTurn();

        if (Input.GetKey(Forward))
        {
            move_f = true;
        }
        else move_f = false;

        if (Input.GetKey(BackWard))
        {
            move_b = true;
        }
        else move_b = false;

        if (Input.GetKey(Left))
        {
            move_l = true;
        }
        else move_l = false;

        if (Input.GetKey(Right))
        {
            move_r = true;
        }
        else move_r = false;

        if (Input.GetKeyDown(Jump))
        {
            MoveJump(false);
        }
        else if (Input.GetKeyUp(Jump))
        {
            MoveJump(true);
        }

        if (Input.GetKeyDown(Crouch))
        {
            MoveCrouch(false);
        }
        else if (Input.GetKeyUp(Crouch))
        {
            MoveCrouch(true);
        }

        if (Input.GetKeyDown(Sprint))
        {
            MoveSpeedModify(false);
        }
        else if (Input.GetKeyUp(Sprint))
        {
            MoveSpeedModify(true);
        }
    }

    public void TryUpdateRigidBody(bool debug_mode)
    {
        char_body = GetComponent<ColliderScript>() == null ? null : GetComponent<ColliderScript>();
        if (char_body != null)
        {
            char_body.transform.localEulerAngles = new Vector3(0, 0, 0);
            return;
        }

        if (debug_mode)
        {
            GameLog.OutputToLog("Failed to retreive a rigidy body for character mover! Cannot move!");
        }
    }

    /// <summary>
    /// Destruct this char mover
    /// </summary>
    /// <param name="key_params"></param>
    public void Destruct(KeyCode[] key_params)
    {
        active_movers.Remove(this_mover_id);
        char_body = null;
    }

    /// <summary>
    /// Stops the character mover if focus to the application is lost
    /// </summary>
    /// <param name="focusState"></param>
    void OnApplicationFocus(bool focusState)
    {
        move_b = false;
        move_f = false;
        move_r = false;
        move_l = false;
    }

    bool move_f = false;
    bool move_b = false;
    bool move_l = false;
    bool move_r = false;

    /// <summary>
    /// Is the character moving forward?
    /// </summary>
    public bool MovingForward
    {
        get
        {
            return move_f;
        }
    }
    /// <summary>
    /// Is the character moving backward?
    /// </summary>
    public bool MovingBackward
    {
        get
        {
            return move_b;
        }
    }
    /// <summary>
    /// Is the character moving left?
    /// </summary>
    public bool MovingLeft
    {
        get
        {
            return move_l;
        }
    }
    /// <summary>
    /// Is the character moving right?
    /// </summary>
    public bool MovingRight
    {
        get
        {
            return move_r;
        }
    }

    private void CharacterTurn()
    {
        if (char_body != null && attatched_camera != null && MovementMode == CharacterMovementMode.character_mouse_turn)
        {
            //get mouse inputs
            float dx = InputHandler.MouseDelta.x;

            //multiply by the turn speed
            float additive_y_axis = dx * turn_speed;

            //get rotation of camera and the collider
            Vector3 curr_rot = char_body.this_RigidBody.rotation.eulerAngles;
            //compute collider rotation
            char_body.this_RigidBody.rotation = Quaternion.Euler(new Vector3(curr_rot.x,
                curr_rot.y + additive_y_axis,
                0));
        }
    }
    private void MoveJump(bool key_up)
    {
        if (char_body != null)
        {
            if (Time.realtimeSinceStartup - time_of_last_jump > Jump_Time_Limit && 
                ((key_up && grounded) || (key_up && current_jump_increment < jump_count)))
            {
                //After jumping, we are not grounded
                grounded = false;
                //Add the jumping force

                //If this is a double jump or more, then we
                //cancel out the current velocity and jump in the players
                //desired direction
                if (current_jump_increment >= 1)
                {
                    char_body.this_RigidBody.velocity = Vector3.zero;
                    Vector3 move_dir = Vector3.zero;

                    if (MovingForward)
                        move_dir += char_body.this_RigidBody.rotation * Vector3.forward;
                    if (MovingBackward)
                        move_dir += char_body.this_RigidBody.rotation * Vector3.back;
                    if (MovingLeft)
                        move_dir += char_body.this_RigidBody.rotation * Vector3.left;
                    if (MovingRight)
                        move_dir += char_body.this_RigidBody.rotation * Vector3.right;

                    move_dir = PhysicsManager.ConvertToMagnitude(move_dir, move_speed);
                    char_body.this_RigidBody.velocity = move_dir;

                    char_body.this_RigidBody.AddForce(Vector3.up * jump_height * 3, ForceMode.VelocityChange);
                }
                //not a double jump, so we are grounded. Jump in the direction of the velocity
                else
                {
                    char_body.this_RigidBody.AddForce(Vector3.up * jump_height * 3, ForceMode.VelocityChange);
                }

                //Increment the current_jump_count
                current_jump_increment++;
                //Record the time of jump
                time_of_last_jump = Time.realtimeSinceStartup;
            }
        }
    }
    private void MoveCrouch(bool key_up)
    {
        if (char_body != null)
        {

        }
    }
    private void MoveSpeedModify(bool key_up)
    {
        if (char_body != null)
        {

        }
    }
}

/// <summary>
/// Describes how the object moves based off input
/// </summary>
public enum CharacterMovementMode
{
    /// <summary>
    /// Move like a person, the mouse is used for rotating
    /// </summary>
    character_mouse_turn,
    /// <summary>
    /// Moves like a person, the 'Right' and 'Left' keys are used for rotating
    /// </summary>
    character_leftright_turn
}
