﻿using UnityEngine;
using System.Collections;

public class DestroyAfterSeconds : MonoBehaviour {

    float time = 5.0f;

	public void StartTimer(float t = 5.0f)
    {
        time = t;
        StartCoroutine(he());
    }

	IEnumerator he(){
		yield return new WaitForSeconds(time);
		Destroy (this.gameObject);
	}

}
