﻿using UnityEngine;
using System.Collections;

public class StartMainMenu : MonoBehaviour {

	public GameObject mainMenu;

	// Use this for initialization
	void Awake () 
	{
		QualitySettings.SetQualityLevel(3, true);
		Instantiate(mainMenu, new Vector3(0,0,0), Quaternion.identity);
		Destroy (this.gameObject);
	}
	

}
