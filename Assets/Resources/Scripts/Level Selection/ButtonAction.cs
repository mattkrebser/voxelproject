using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//holds function used by buttons, their onClick Action is one of these functions
public class ButtonAction : MonoBehaviour
{
	//used for loading in a new level, selects name
	public LoadLevelUI loadLevelUI;
	public string buttonname;

	public void SetLoadPath(){
		loadLevelUI.worldName = buttonname;
	}


}

