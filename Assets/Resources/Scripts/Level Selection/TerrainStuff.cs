using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//used in UnityEditorBlockEditor
[System.Serializable]
public class blockitem : Block
{
	public string path;
	public blockID block;
	
	public Texture texture;
    public Texture texture2;

	public Material material;

	public blockitem(){
	}

	public blockitem(Block inputBlock,byte i1,byte i2){
		this.uy = inputBlock.uy;
		this.u = inputBlock.u;
		this.UVCoordinates = new Vector2S (inputBlock.UVCoordinates.x, inputBlock.UVCoordinates.y);
		this.materialPosition = inputBlock.materialPosition;
		this.texArrayPosition = inputBlock.texArrayPosition;
		this.itemName = inputBlock.itemName;
		this.isTransparent = inputBlock.isTransparent;
		this.physicsDisabled = inputBlock.physicsDisabled;
		this.type = inputBlock.type;
		this.blockType = inputBlock.blockType;
		this.Rotation = inputBlock.Rotation;

        this.imagePath = inputBlock.imagePath;
        this.itemName = inputBlock.itemName;
        this.BaseTexture = inputBlock.BaseTexture;
        this.textureName = inputBlock.textureName;
        this.type = inputBlock.type;

		block.blockID1 = i1;
		block.blockID2 = i2;
	}

	public blockitem(byte i1, byte i2){
		this.block.blockID1 = i1;
		this.block.blockID2 = i2;
	}
	
}

//Different block types, each type uses a different delegate to draw themselves
[System.Serializable]
public enum primitiveBlockType
{
	empty,
	cube,
	slope,
	cornerSlope,
	sunkenCornerSlope,
	small_billboard,
	medium_billboard,
	large_billboard,
	other
};

