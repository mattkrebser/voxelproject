using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;

public class WorldBiomes
{
	public string worldName = "";
	public List<Biome> biomesList = new List<Biome>();

	//World ends at the edges
	public bool fixedWorld = true;

	//Diameter of world
	public int worldSize = 8;

	//Water level.
	public int waterLevel = 0;
	
	//gets initialized on generation, used to select biome
	//Storage: Each element contains a reference to adjacent elements, never a null element
	[System.NonSerialized]
	public BiomeRef[] biomes = new BiomeRef[100];

	//apply maximum of 0.49f min 0.01f
	public float BiomeBlendPercent = 0.25f;

	//Interpolate between biome layers
	public float BiomeGradient = 1.0f;

	public float biomeSize = 100.0f;

	public float SubBiomeSize = 1f;

    public long seed = 127368777712;

	//Finalizer
	~WorldBiomes(){
		for (int i = 0; i < biomes.Length; i++) {
			if (biomes[i] != null){
				for (int n = 0; n < biomes[i].biome.NearSubBiomes.Length; i++){
					if (biomes[i].biome.NearSubBiomes[n] != null){
						biomes[i].biome.NearSubBiomes[n].LBiomeRef.biome = null;
						biomes[i].biome.NearSubBiomes[n].UBiomeRef.biome = null;
						biomes[i].biome.NearSubBiomes[n].LBiomeRef = null;
						biomes[i].biome.NearSubBiomes[n].UBiomeRef = null;
						biomes[i].biome.NearSubBiomes[n] = null;
					}
				}
				biomes[i].LBiomeRef.biome = null;
				biomes[i].UBiomeRef.biome = null;
				biomes[i].LBiomeRef = null;
				biomes[i].UBiomeRef = null;
				biomes[i] = null;
			}
		}
	}

	/// <summary>
	/// Gets all biome names and world name into one string.
	/// Appends a ".W" to worldNames, ".B" to biomes, ".S" to subBiomes.
	/// </summary>
	/// <returns>The all biomes.</returns>
	public string GetAllBiomes(){
		string s = "";
		s += worldName + ".W";

		for (int i = 0; i < biomesList.Count; i++){
			s += biomesList[i].biomeName + ".B";
			for (int n = 0; n < biomesList[i].SubBiomes.Count; n++){
				s += biomesList[i].SubBiomes[n].biomeName + ".S";
			}
		}

		return s;

	}

	public Biome GetBiome(string BiomeName){
		
		Biome b = null;
		
		for (int i = 0; i < biomesList.Count; i++){
			
			if (BiomeName == biomesList[i].biomeName)
				b = biomesList[i];
			
			for (int n = 0; n < biomesList[i].SubBiomes.Count; n++){
				if (BiomeName == biomesList[i].SubBiomes[n].biomeName)
					b = biomesList[i].SubBiomes[n];
			}
		}
		
		return b;
	}

    /// <summary>
    /// Initialize this world Biome so that the chunk generator can access it quickly
    /// </summary>
    /// <param name="blocks"></param>
    public void InitializeWorldBiomeData(List<L> blocks)
    {
        //If they didn't bother to make anything...
        if (biomesList.Count == 0)
            biomesList.Add(new Biome());

        //Verify each biome has atleast one layer, if it doesn't, then make one
        //max of around 300 operations per biome
        for (int i = 0; i < biomesList.Count; i++)
        {
            //if there is no layers, make a new one and add grass block to it
            if (biomesList[i].layers.Count == 0)
            {
                biomesList[i].layers.Add(new BlockLayer());
                //grass cube is base block, please don't delete it from ItemDataBase
                biomesList[i].layers[0].layers[0].Add(new BlockIDInt32(108, 0));
            }
            else
            {
                for (int n = 0; n < biomesList[i].layers.Count; n++)
                {
                    //If the first block in the layer doesnt exist
                    if (biomesList[i].layers[n].layers[0].Count == 0)
                        biomesList[i].layers[0].layers[0].Add(new BlockIDInt32(0, 0));
                    //for each layer, fill out it's height in the biome's layerRef's
                    for (int p = biomesList[i].layers[n].layerPlacementHeightMin;
                         p <= biomesList[i].layers[n].layerPlacementHeightMax; p++)
                    {
                        biomesList[i].layerRefs[p] = n;
                    }

                    //sort any sloped type blocks on the top leveled layer (1)
                    RotationTask(0, biomesList[i].layers[n], blocks);
                }
            }

            //Fill layer arrays on sub biomes, same process as above but for each sub biome
            for (int a = 0; a < biomesList[i].SubBiomes.Count; a++)
            {
                if (biomesList[i].SubBiomes[a].layers.Count == 0)
                {
                    biomesList[i].SubBiomes[a].layers.Add(new BlockLayer());
                    //grass cube is base block, please don't delete it from ItemDataBase (blockitem(108,0))
                    biomesList[i].SubBiomes[a].layers[0].layers[0].Add(new BlockIDInt32(108, 0));
                }
                else
                {
                    for (int b = 0; b < biomesList[i].SubBiomes[a].layers.Count; b++)
                    {
                        if (biomesList[i].SubBiomes[a].layers[b].layers[0].Count == 0)
                            biomesList[i].SubBiomes[a].layers[0].layers[0].Add(new BlockIDInt32(0, 0));
                        //iterate from min to max for each layer
                        for (int c = biomesList[i].SubBiomes[a].layers[b].layerPlacementHeightMin;
                             c < biomesList[i].SubBiomes[a].layers[b].layerPlacementHeightMax; c++)
                        {
                            biomesList[i].SubBiomes[a].layerRefs[c] = b;
                        }

                        //sort any sloped type blocks on the top leveled layer (1)
                        RotationTask(0, biomesList[i].SubBiomes[a].layers[b], blocks);
                    }
                }
            }

            //Fill biome selection array, located on WorldBiomes.cs
            //iterate from min to max
            int min = (int)biomesList[i].biomeSelectParams[0].min;
            int max = (int)biomesList[i].biomeSelectParams[0].max;
            bool started = false;
            BiomeRef bbRef = null;
            //for min to max..
            for (int n = min; n < max; n++)
            {
                //start to fill in a new section of biome
                if (biomes[n] == null && !started)
                {
                    bbRef = new BiomeRef(biomesList[i], n, i);
                    biomes[n] = bbRef;
                    started = true;
                }
                //continue filling in
                else if (biomes[n] == null && started)
                {
                    //if this is the last increment of the for loop, initialize the bime
                    if (n + 1 >= max)
                    {
                        if (bbRef != null)
                        {
                            bbRef.UBound = n;
                            float length = bbRef.UBound - bbRef.LBound;
                            bbRef.median = bbRef.LBound + length * 0.5f;
                            bbRef.LQuartile = bbRef.LBound + length * BiomeBlendPercent;
                            bbRef.UQuartile = bbRef.LBound + length * (1 - BiomeBlendPercent);
                            started = false;
                            biomes[n] = bbRef;
                        }
                    }
                    //otherwise continue filling references
                    else
                        biomes[n] = bbRef;
                }
                //if this position is already filled, end the current section and initialize the biome
                else if (biomes[n] != null && started)
                {
                    if (bbRef != null)
                    {
                        bbRef.UBound = n - 1;
                        float length = bbRef.UBound - bbRef.LBound;
                        bbRef.median = bbRef.LBound + length * 0.5f;
                        bbRef.LQuartile = bbRef.LBound + length * BiomeBlendPercent;
                        bbRef.UQuartile = bbRef.LBound + length * (1 - BiomeBlendPercent);
                        started = false;
                    }
                }
                else
                    started = false;
            }

            //Fill sub biome selection arrays on each biome
            for (int m = 0; m < biomesList[i].SubBiomes.Count; m++)
            {
                int smin = (int)biomesList[i].SubBiomes[m].biomeSelectParams[0].min;
                int smax = (int)biomesList[i].SubBiomes[m].biomeSelectParams[0].max;
                bool sstarted = false;
                BiomeRef sRef = null;

                for (int k = smin; k < smax; k++)
                {
                    //start to fill in a new section of biome
                    if (biomesList[i].NearSubBiomes[k] == null && !sstarted)
                    {
                        sRef = new BiomeRef(biomesList[i].SubBiomes[m], k, i);
                        biomesList[i].NearSubBiomes[k] = sRef;
                        sstarted = true;
                    }
                    //continue filling in
                    else if (biomesList[i].NearSubBiomes[k] == null && sstarted)
                    {
                        //if this is the last increment of the for loop, initialize the bime
                        if (k + 1 >= (int)smax)
                        {
                            if (sRef != null)
                            {
                                sRef.UBound = k;
                                float slength = sRef.UBound - sRef.LBound;
                                sRef.median = sRef.LBound + slength * 0.5f;
                                sRef.LQuartile = sRef.LBound + slength * BiomeBlendPercent;
                                sRef.UQuartile = sRef.LBound + slength * (1 - BiomeBlendPercent);
                                sstarted = false;
                                biomesList[i].NearSubBiomes[k] = sRef;
                            }
                        }
                        //otherwise continue filling references
                        else
                           biomesList[i].NearSubBiomes[k] = sRef;
                    }
                    //if this position is already filled, end the current section and initialize the biome
                    else if (biomesList[i].NearSubBiomes[k] != null && sstarted)
                    {
                        if (sRef != null)
                        {
                            sRef.UBound = k - 1;
                            float slength = sRef.UBound - sRef.LBound;
                            sRef.median = sRef.LBound + slength * 0.5f;
                            sRef.LQuartile = sRef.LBound + slength * BiomeBlendPercent;
                            sRef.UQuartile = sRef.LBound + slength * (1 - BiomeBlendPercent);
                            sstarted = false;
                        }
                    }
                    else
                        sstarted = false;
                }
            }

            /*for (int j = 0; j < 100; j++) {
				if (worldData.biomesList[i].NearSubBiomes[j] != null)
					Debug.Log("- Current - " + j + " Q's: "  + worldData.biomesList[i].NearSubBiomes[j].biome.biomeName + " " + worldData.biomesList[i].NearSubBiomes[j].LQuartile + "  " + worldData.biomesList[i].NearSubBiomes[j].UQuartile + " L,U " + worldData.biomesList[i].NearSubBiomes[j].LBound + " " + worldData.biomesList[i].NearSubBiomes[j].UBound +"   *****************************  ");	
			}*/

            //Assign lower and upper adjacent biomes in NearSubBiomes array
            //Assign Upper and lower Biomes for each BiomeRef
            bool newBiome = true;
            BiomeRef cB = null;
            BiomeRef pB = null;
            //for the length of NearSubBiomes array
            for (int p = 0; p < 100; p++)
            {
                //If the array is null at this position, make a new reference assign it in the array
                if (biomesList[i].NearSubBiomes[p] == null && newBiome)
                {
                    newBiome = false;

                    int fsmin = 0;
                    if (pB != null)
                        fsmin = (int)pB.UBound;
                    cB = new BiomeRef(biomesList[i], fsmin, -1);

                    biomesList[i].NearSubBiomes[p] = cB;
                }
                //if the position in the array is null and we just made a new biomeRef
                else if (biomesList[i].NearSubBiomes[p] == null && !newBiome)
                {
                    biomesList[i].NearSubBiomes[p] = cB;
                }
                //if the position in the array is not null
                else if (biomesList[i].NearSubBiomes[p] != null)
                {
                    if (!newBiome)
                    {
                        cB.UBound = biomesList[i].NearSubBiomes[p].LBound;
                        float fslength = cB.UBound - cB.LBound;
                        cB.median = cB.LBound + fslength * 0.5f;
                        cB.LQuartile = cB.LBound + fslength * BiomeBlendPercent;
                        cB.UQuartile = cB.LBound + fslength * (1 - BiomeBlendPercent);
                    }
                    newBiome = true;
                    cB = biomesList[i].NearSubBiomes[p];
                }
                if (p + 1 >= 100 && biomesList[i].NearSubBiomes[p].pos == -1)
                {
                    cB.UBound = 100;
                    float fslength = cB.UBound - cB.LBound;
                    cB.median = cB.LBound + fslength * 0.5f;
                    cB.LQuartile = cB.LBound + fslength * BiomeBlendPercent;
                    cB.UQuartile = cB.LBound + fslength * (1 - BiomeBlendPercent);
                }
                //if this position is different than the previous one
                if (pB != null && pB.LBound != cB.LBound)
                {
                    pB.UBiomeRef = cB;
                    cB.LBiomeRef = pB;
                }
                pB = cB;
            }

            /*for (int j = 0; j < 100; j++) {
				Debug.Log("------------ Current ---------" + worldData.biomesList[i].NearSubBiomes[j].biome.biomeName + " " + worldData.biomesList[i].NearSubBiomes[j].LQuartile + " " + worldData.biomesList[i].NearSubBiomes[j].UQuartile + " " + worldData.biomesList[i].NearSubBiomes[j].LBound+ " " + worldData.biomesList[i].NearSubBiomes[j].UBound +"   -----------------------------------------------------   ");	
			if (worldData.biomesList[i].NearSubBiomes[j].UBiomeRef != null)
					Debug.Log ("Upper " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.UBound + " " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.LBound + " q's " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.UQuartile +" " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.LQuartile + " " + worldData.biomesList[i].NearSubBiomes[j].UBiomeRef.biome.biomeName);
			if (worldData.biomesList[i].NearSubBiomes[j].LBiomeRef != null)
					Debug.Log (" Lower " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.UBound + " " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.LBound + " q's " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.UQuartile +" " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.LQuartile + " " + worldData.biomesList[i].NearSubBiomes[j].LBiomeRef.biome.biomeName);
		}*/
        }

        /*for (int j = 0; j < 100; j++) {
			if (worldData.biomes[j] != null)
			Debug.Log("- Current - " + j + " Q's: "  + worldData.biomes[j].biome.biomeName + " " + worldData.biomes[j].LQuartile + "  " + worldData.biomes[j].UQuartile + " L,U " + worldData.biomes[j].LBound + " " + worldData.biomes[j].UBound +"   *****************************  ");	
		}*/

        //Assign Upper and lower Biomes for each BiomeRef, works same as above
        bool bnewBiome = true;
        BiomeRef bcB = null;
        BiomeRef bpB = null;
        for (int p = 0; p < 100; p++)
        {
            //array at [p] is empty, fill it with base biome
            if (biomes[p] == null && bnewBiome)
            {
                //no new initialization is needed
                bnewBiome = false;

                int bfsmin = 0;

                if (bpB != null)
                    bfsmin = (int)bpB.UBound;

                bcB = new BiomeRef(biomesList[0], bfsmin, 0);

                biomes[p] = bcB;
            }
            //array at [p] is empty, base biome reference is already initialized
            else if (biomes[p] == null && !bnewBiome)
            {
                biomes[p] = bcB;

            }
            //if the array at [p] is not empty ( a biome has already been placed there)
            else if (biomes[p] != null)
            {
                //if the base biome has already been initialized
                if (!bnewBiome)
                {
                    bcB.UBound = biomes[p].LBound;
                    float bfslength = bcB.UBound - bcB.LBound;
                    bcB.median = bcB.LBound + bfslength * 0.5f;
                    bcB.LQuartile = bcB.LBound + bfslength * BiomeBlendPercent;
                    bcB.UQuartile = bcB.LBound + bfslength * (1 - BiomeBlendPercent);
                }
                //set base biome initialization boolean to true, allows for the creation of another biome
                bnewBiome = true;
                bcB = biomes[p];
            }
            if (p + 1 >= 100 && biomes[p].pos == 0)
            {
                bcB.UBound = 100;
                float bfslength = bcB.UBound - bcB.LBound;
                bcB.median = bcB.LBound + bfslength * 0.5f;
                bcB.LQuartile = bcB.LBound + bfslength * BiomeBlendPercent;
                bcB.UQuartile = bcB.LBound + bfslength * (1 - BiomeBlendPercent);
            }
            //if the current biome at [p] is different than the previous one
            if (bpB != null && bpB.LBound != bcB.LBound)
            {
                bpB.UBiomeRef = bcB;
                bcB.LBiomeRef = bpB;
            }
            bpB = bcB;
        }

        /*for (int j = 0; j < 100; j++) {
			Debug.Log("------------ Current ---------" + worldData.biomes[j].biome.biomeName + " " + worldData.biomes[j].LQuartile + " " + worldData.biomes[j].UQuartile + "   -----------------------------------------------------   ");	
			if (worldData.biomes[j].UBiomeRef != null)
				Debug.Log ("Upper " + worldData.biomes[j].UBiomeRef.UBound + " " + worldData.biomes[j].UBiomeRef.LBound + " q's " + worldData.biomes[j].UBiomeRef.UQuartile +" " + worldData.biomes[j].UBiomeRef.LQuartile + " " + worldData.biomes[j].UBiomeRef.biome.biomeName);
			if (worldData.biomes[j].LBiomeRef != null)
				Debug.Log (" Lower " + worldData.biomes[j].LBiomeRef.UBound + " " + worldData.biomes[j].LBiomeRef.LBound + " q's " + worldData.biomes[j].LBiomeRef.UQuartile +" " + worldData.biomes[j].LBiomeRef.LQuartile + " " + worldData.biomes[j].LBiomeRef.biome.biomeName);
		}*/
    }

    /// <summary>
    /// Reformats blocks listed in a layer so that a cube type is at the front
    /// followed by slope types, and so that each slope type is also alloted into a seperate list
    /// </summary>
    /// <param name="i">The index.</param>
    /// <param name="layer">Layer.</param>
    void RotationTask(int i, BlockLayer layer, List<L> blocksList)
    {
        if (layer.slopes == null || layer.Sslopes == null || layer.Cslopes == null)
        {
            layer.slopes = new BlockIDInt32[3][];
            layer.Sslopes = new BlockIDInt32[3][];
            layer.Cslopes = new BlockIDInt32[3][];

            layer.slopes[0] = new BlockIDInt32[25];
            layer.slopes[1] = new BlockIDInt32[25];
            layer.slopes[2] = new BlockIDInt32[25];

            layer.Sslopes[0] = new BlockIDInt32[25];
            layer.Sslopes[1] = new BlockIDInt32[25];
            layer.Sslopes[2] = new BlockIDInt32[25];

            layer.Cslopes[0] = new BlockIDInt32[25];
            layer.Cslopes[1] = new BlockIDInt32[25];
            layer.Cslopes[2] = new BlockIDInt32[25];
        }

        int pos = -1;
        if (blocksList[layer.layers[i][0].blockID1].l[layer.layers[i][0].blockID2].blockType != primitiveBlockType.cube)
        {
            for (int b = 0; b < layer.layers[i].Count; b++)
            {
                if (blocksList[layer.layers[i][b].blockID1].l[layer.layers[i][b].blockID2].blockType == primitiveBlockType.cube)
                    pos = b;
            }
        }
        else
            pos = 0;

        if (pos != -1)
        {
            //swap cube type with first element
            BlockIDInt32 b = layer.layers[i][pos];
            layer.layers[i][pos] = layer.layers[i][0];
            layer.layers[i][0] = b;
        }
        else
        {
            layer.layers[i].Add(layer.layers[i][0]);
            //place default grass if no cube exists
            layer.layers[i][0] = new BlockIDInt32(108, 0);
        }

        //usually n will go up to around 4 or so
        for (int n = 1; n < layer.layers[i].Count; n++)
        {
            if (blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].blockType == primitiveBlockType.slope)
            {
                layer.slopes[i][blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].Rotation] = layer.layers[i][n];
            }
            else if (blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].blockType == primitiveBlockType.cornerSlope)
                layer.Cslopes[i][blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].Rotation] = layer.layers[i][n];
            else if (blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].blockType == primitiveBlockType.sunkenCornerSlope)
                layer.Sslopes[i][blocksList[layer.layers[i][n].blockID1].l[layer.layers[i][n].blockID2].Rotation] = layer.layers[i][n];
        }
    }
}

//used for storage only
[System.Serializable]
public class WorldBiomesVariant{
	public string worldName = "New World";
	public List<BiomeVariant> biomesList = new List<BiomeVariant>();
	
	//World ends at the edges
	public bool fixedWorld = true;
	
	//Diameter of world
	public int worldSize = 8;
	
	//Water level.
	public int waterLevel = 0;
	
	//apply maximum of 0.49f min 0.01f
	public float BiomeBlendPercent = 0.25f;
	
	//Interpolate between biome layers
	public float BiomeGradient = 1.0f;
	
	public float biomeSize = 100.0f;
	
	public float SubBiomeSize = 1f;

    public long seed = 127368777712;


	/// <summary>
	/// Gets all biome names and world name into one string.
	/// Appends a ".W" to worldNames, ".B" to biomes, ".S" to subBiomes.
	/// </summary>
	/// <returns>The all biomes.</returns>
	public string GetAllBiomes(){
		string s = "";
		s += worldName + ".W";
		
		for (int i = 0; i < biomesList.Count; i++){
			s += biomesList[i].biomeName + ".B";
			for (int n = 0; n < biomesList[i].SubBiomes.Count; n++){
				s += biomesList[i].SubBiomes[n].biomeName + ".S";
			}
		}
		
		return s;
		
	}
}




















