using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Events;

public class LoadLevelUI : MonoBehaviour
{
	[System.NonSerialized]
	public string worldName = "NO_WORLD_NAME";
	[System.NonSerialized]
	public string datapath;

	private string[] names;
	private string[] paths;

	public GameObject button;
	public GameObject editorUI;

	private Transform panel;

	//Get path of save location, Initialize cutOff which edits the path of the worldName
	void Awake(){
		datapath = FileClass.GetBasePath ();
		//get all the world names saved to disk
		names = FileClass.GetAllWorldNames ();
		paths = FileClass.GetAllSaves ();
		//Reference Content Panel which will create a list of buttons, each for a different world
		panel = this.transform.Find ("Canvas/Scroll View/Content Panel");
		//make buttons
		AddButtons ();
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Chunky");
        foreach (GameObject g in objs)
            Destroy(g);
	}

	//Iterate through string Array, create a button for each element
	void AddButtons(){

		for (int i =0; i < names.Length; i++) {
			//instantiate
			GameObject newButton = Instantiate(button) as GameObject;
			//set parent
			newButton.transform.SetParent(panel);
			//set the Minheight button property
			newButton.GetComponent<LayoutElement>().minHeight = Screen.height / 50;
			//edit string to be displayed
			newButton.GetComponent<Text>().text = names[i];
			newButton.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
			//set paramters on ButtonAction >;)
			newButton.GetComponent<ButtonAction>().loadLevelUI = this;
			newButton.GetComponent<ButtonAction>().buttonname = paths[i];
			//Add an onClick event
			newButton.GetComponent<Button>().onClick.AddListener(delegate {newButton.GetComponent<ButtonAction>().SetLoadPath();});
		}
	}

	//Load main menu.
	public void Back(){
		Instantiate(Resources.Load("UIPrefabs/mainmenu"), new Vector3(0,0,0), Quaternion.identity);
		Destroy(this.gameObject);
	}

	//Load world in question
	public void Load(bool b){
		if (worldName != "NO_WORLD_NAME" && worldName != null)
		{
			//if the path exists
			if (System.IO.Directory.Exists(FileClass.GetRegionsDirectoryPath(FileClass.GetWorldName(worldName)))){

                GameObject g = GameObject.FindWithTag("Chunky");

                if (g != null)
                    Destroy(g.transform.parent.gameObject);

				string infoName = FileClass.GetWorldName(worldName);
                WorldManager.LoadWorld(infoName, Vector3.zero, Quaternion.identity, b);

				GameObject[] objs = GameObject.FindGameObjectsWithTag("Menu");
				for (int i = 0; i < objs.Length; i++){
					Destroy(objs[i]);
				}
			}
			else
				Debug.Log ("path doesn't exist");
		}
	}

}

