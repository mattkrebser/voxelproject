using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Biome
{

	public string biomeName = "New Biome";

	//Sub Biomes
	public List<Biome> SubBiomes = new List<Biome> ();

	//List of layers, differentiate between layers using height of block being placed
	public List<BlockLayer> layers = new List<BlockLayer>();

	//plants are cool
	public List<Foliage> foliage = new List<Foliage> ();

	//trees, buildings, voxely goodness
	public List<VoxelStructures> voxels = new List<VoxelStructures>();

	//This Biome's parameters (typically things like temperature,humidity,height)
	//used to select which biome is used when creating world
	public List<BiomeParameter> biomeSelectParams = new List<BiomeParameter> ();

	//Each level in the array is an index of a layer in layers list
	public int[] layerRefs = new int[256];

	[System.NonSerialized]
	//Gets initialized on generation, used to select active sub biome
	public BiomeRef[] NearSubBiomes = new BiomeRef[100];

	//Gets terrain type in noise function
	//Horizontal stretch increase widthxlength of terrain features
	public float HorizontalStretch = 50f;
	//vertical stretch increase height of terrain features
	public float VerticalStretch = 30f;
	//Adds + baseHeight to all heights in heightmap
	public int BaseHeight = 0;
	//nothing can go below min height
	public int MinimumHeight = 0;
	//floor inversion
	public int InversionHeight = 0;
	//2nd and 3rd noise manipulators, they are multiplied by the original horizontal and vertical stretch values
	//to attain terrain size
	public float TurbulenceWidth1 = 1;
	public float TurbulenceHeight1 = 1;
	public float TurbulenceWidth2 = 1;
	public float TurbulenceHeight2 = 1;
	//powers are used to manipulate the pitch(slope) of heightmaps
	//Think of it as a 2D graph, a height with a power of 0.5 would level off quickly while
	//a heightmap value with power of 2 would increase drastically
	public float power1 = 1;
	//Height levels at when to apply respective slope modifications
	public int power1Height = 0;
	public float power2 = 1;
	public int power2Height = 0;

	//distance from world borders to level heightmap values to 0
	public int FallOffDistance = 0;
	//between 0 and 1
	public float FallOffVariability = 0.0f;

	//inversion ceiling, all blocks above this height are inverted below
	public int ReverseInversionHeight = 0;

	public Biome(){
		//biomeSelectParams.Add (new BiomeParameter("Humidity"));
		//biomeSelectParams.Add (new BiomeParameter("Height"));
		biomeSelectParams.Add (new BiomeParameter("Biome Range"));
	}
}

/// <summary>
/// Types of terrain modifiers used
/// </summary>
public enum mods{
	HorizontalStretch,
	VerticalStretch,
	BaseHeight,
	MinimumHeight,
	InversionHeight,
	TurbulenceWidth1,
	TurbulenceHeight1,
	TurbulenceWidth2,
	TurbulenceHeight2,
	power1,
	power1Height,
	power2,
	power2Height,
	FallOffDistance,
	ReverseInversionHeight,
	Ratio,
	FallOffVariability
};

[System.Serializable]
public class BiomeParameter
{
	//params range from 0 to 1
	public string param_name;
	public float min = 0.0f;
	public float max = 100.0f;

	public BiomeParameter(string name){
		param_name = name;
	}
	
}

[System.Serializable]
public class BlockLayer
{
	public string layerName = "New Layer";
	//Ideas: level height for this layer
	//blend between different layers (optional?)
	//A layer should use the entire column (0 to 255), An air block above a solid makes a new layer
	//An Air block below a solid stops the layer

	//layer thickness

	//Is the layer underwater?
	public bool underWater = false;

	//Blend with other layers
	public bool allowBlending = false;

	//blend quickly or gradually
	public float blendIntensity = 0.0f;

	public int layerPlacementHeightMax = 255;
	public int layerPlacementHeightMin = 0;

	public int layerRange = 3;

    //Each Block Layer allows for 3 different block types. The blocktype at layers[0] will
    //take up the top 3 levels of the layer. The blocktype at layers[1] will take the next 3,
    //and the layers[2] will take all other levels below.


    //An example:
    // using layers array: |array[0] = b1|array[1] = b2|array[2] = b3|
    /*  
        y=255   Air Voxel
        .
        .
        .
        y=11    Air voxel, the position at y=10 is not air, so the layer starts below
        y=10    b1
        y=9     b1
        y=8     b1
        y=7     b2
        y=6     b2
        y=5     b2
        y=4     b3
        y=3     b3
        y=2     b3
        y=1     b3
        y=0     b3
        
    */

	//layers in an array of lists.
	//cube block type needs to be here. Sloped, corner sloped, sunken sloped are optionals
	public List<BlockIDInt32>[] layers = new List<BlockIDInt32>[3];
	
	//used during generating, sorted by rotations, they are jagged arrays
    //because sloped blocks each have rotations, the jagged part marks
    //which rotation is being used. For example, the block in slopes[0][4] is
    //the top level in the layer and has a rotation of 4.
	[System.NonSerialized]
	public BlockIDInt32[][] slopes = new BlockIDInt32[3][];
	[System.NonSerialized]
	public BlockIDInt32[][] Cslopes = new BlockIDInt32[3][];
	[System.NonSerialized]
	public BlockIDInt32[][] Sslopes = new BlockIDInt32[3][];

	public BlockLayer(){
		layers [0] = new List<BlockIDInt32> ();
		layers [1] = new List<BlockIDInt32> ();
		layers [2] = new List<BlockIDInt32> ();

        //set to 25 because the highest rotation possible is 24
		slopes [0] = new BlockIDInt32[25];
        slopes[1] = new BlockIDInt32[25];
        slopes[2] = new BlockIDInt32[25];

        Sslopes [0] = new BlockIDInt32[25];
        Sslopes[1] = new BlockIDInt32[25];
        Sslopes[2] = new BlockIDInt32[25];

        Cslopes [0] = new BlockIDInt32[25];
        Cslopes[1] = new BlockIDInt32[25];
        Cslopes[2] = new BlockIDInt32[25];
    }
	
}

[System.Serializable]
public class Foliage
{
	//foliage block(grass,flowers,bushes,etc.. billboard stuff)
	public BlockIDInt32 foliage;

	public bool randomPatches = false;
	public bool randomPlacement = false;

	//Patch Density is rolled once per chunk
	public float patchDensity = 0.0f;
	//Inside density rolled once per surface block in the bounds of the patch
	public float insidePatchDensity = 0.0f;
	//rolled once for every surface block
	public float randomPlacementDensity = 0.0f;

	//approx size of patch
	public float patchSize = 0.0f;

	//can place this foliage on top of...
	public List<BlockIDInt32> allowedPlacement = new List<BlockIDInt32>();

	public Foliage(){
	}

	public Foliage(BlockIDInt32 block){
		foliage = block;
	}
	
}

[System.Serializable]
public class VoxelStructures : Foliage
{
	public int voxelStruct;

	public VoxelStructures(int vs){
		voxelStruct = vs;
	}
	
}

//only used for storage (serialization)
[System.Serializable]
public class SubBiome{
	public string biomeName = "New Biome";
	//List of layers, differentiate between layers using height of block being placed
	public List<BlockLayer> layers = new List<BlockLayer>();
	//plants are cool
	public List<Foliage> foliage = new List<Foliage> ();
	//trees, buildings, voxely goodness
	public List<VoxelStructures> voxels = new List<VoxelStructures>();
	//This Biome's parameters (typically things like temperature,humidity,height)
	//used to select which biome is used when creating world
	public List<BiomeParameter> biomeSelectParams = new List<BiomeParameter> ();
	//Gets terrain type in noise function
	//Horizontal stretch increase widthxlength of terrain features
	public float HorizontalStretch = 50f;
	//vertical stretch increase height of terrain features
	public float VerticalStretch = 30f;
	//Adds + baseHeight to all heights in heightmap
	public int BaseHeight = 0;
	//nothing can go below min height
	public int MinimumHeight = 0;
	//floor inversion
	public int InversionHeight = 0;
	//2nd and 3rd noise manipulators, they are multiplied by the original horizontal and vertical stretch values
	//to attain terrain size
	public float TurbulenceWidth1 = 1;
	public float TurbulenceHeight1 = 1;
	public float TurbulenceWidth2 = 1;
	public float TurbulenceHeight2 = 1;
	//powers are used to manipulate the pitch(slope) of heightmaps
	//Think of it as a 2D graph, a height with a power of 0.5 would level off quickly while
	//a heightmap value with power of 2 would increase drastically
	public float power1 = 1;
	//Height levels at when to apply respective slope modifications
	public int power1Height = 0;
	public float power2 = 1;
	public int power2Height = 0;
	//distance from world borders to level heightmap values to 0
	public int FallOffDistance = 0;
	//between 0 and 1
	public float FallOffVariability = 0.0f;
	//inversion ceiling, all blocks above this height are inverted below
	public int ReverseInversionHeight = 0;
}

//used for file storage only
[System.Serializable]
public class BiomeVariant{
	public string biomeName = "New Biome";
	//Sub Biomes
	public List<SubBiome> SubBiomes = new List<SubBiome> ();
	//List of layers, differentiate between layers using height of block being placed
	public List<BlockLayer> layers = new List<BlockLayer>();
	//plants are cool
	public List<Foliage> foliage = new List<Foliage> ();
	//trees, buildings, voxely goodness
	public List<VoxelStructures> voxels = new List<VoxelStructures>();
	//This Biome's parameters (typically things like temperature,humidity,height)
	//used to select which biome is used when creating world
	public List<BiomeParameter> biomeSelectParams = new List<BiomeParameter> ();
	//Gets terrain type in noise function
	//Horizontal stretch increase widthxlength of terrain features
	public float HorizontalStretch = 50f;
	//vertical stretch increase height of terrain features
	public float VerticalStretch = 30f;
	//Adds + baseHeight to all heights in heightmap
	public int BaseHeight = 0;
	//nothing can go below min height
	public int MinimumHeight = 0;
	//floor inversion
	public int InversionHeight = 0;
	//2nd and 3rd noise manipulators, they are multiplied by the original horizontal and vertical stretch values
	//to attain terrain size
	public float TurbulenceWidth1 = 1;
	public float TurbulenceHeight1 = 1;
	public float TurbulenceWidth2 = 1;
	public float TurbulenceHeight2 = 1;
	//powers are used to manipulate the pitch(slope) of heightmaps
	//Think of it as a 2D graph, a height with a power of 0.5 would level off quickly while
	//a heightmap value with power of 2 would increase drastically
	public float power1 = 1;
	//Height levels at when to apply respective slope modifications
	public int power1Height = 0;
	public float power2 = 1;
	public int power2Height = 0;
	//distance from world borders to level heightmap values to 0
	public int FallOffDistance = 0;
	//between 0 and 1
	public float FallOffVariability = 0.0f;
	//inversion ceiling, all blocks above this height are inverted below
	public int ReverseInversionHeight = 0;
}


public static class ConvertBiome{

	/// <summary>
	/// Converts a WorldBiomes to a WorldBiomesVariant.
	/// </summary>
	/// <returns>The to serializable.</returns>
	/// <param name="w">The width.</param>
	public static WorldBiomesVariant ConvertToSerializable(WorldBiomes w){

		if (w == null)
			return null;

		WorldBiomesVariant b = new WorldBiomesVariant ();
		b.worldName = w.worldName;
		b.fixedWorld = w.fixedWorld;
		b.worldSize = w.worldSize;
		b.waterLevel = w.waterLevel;
		b.BiomeBlendPercent = w.BiomeBlendPercent;
		b.BiomeGradient = w.BiomeGradient;
		b.biomeSize = w.biomeSize;
		b.SubBiomeSize = w.SubBiomeSize;
        b.seed = w.seed;

		for (int i = 0; i < w.biomesList.Count; i++) {
			b.biomesList.Add (ConvertToBiomeVariant(w.biomesList[i]));
		}

		return b;
	}

	/// <summary>
	/// Converts a WorldBiomesVariant to a WorldBiomes.
	/// </summary>
	/// <returns>The from serializable.</returns>
	/// <param name="w">The width.</param>
	public static WorldBiomes ConvertFromSerializable(WorldBiomesVariant w){

		if (w == null)
			return null;

		WorldBiomes b = new WorldBiomes ();
		b.worldName = w.worldName;
		b.fixedWorld = w.fixedWorld;
		b.worldSize = w.worldSize;
		b.waterLevel = w.waterLevel;
		b.BiomeBlendPercent = w.BiomeBlendPercent;
		b.BiomeGradient = w.BiomeGradient;
		b.biomeSize = w.biomeSize;
		b.SubBiomeSize = w.SubBiomeSize;
        b.seed = w.seed;
		
		for (int i = 0; i < w.biomesList.Count; i++) {
			b.biomesList.Add (ConvertFromBiomeVariant(w.biomesList[i]));
		}

		return b;
	}

	/// <summary>
	/// Converts from biome variant to biome.
	/// </summary>
	/// <returns>The from biome variant.</returns>
	/// <param name="w">The width.</param>
	public static Biome ConvertFromBiomeVariant(BiomeVariant w){
		Biome b = new Biome ();
		b.biomeName = w.biomeName;
		b.layers = w.layers;
		b.foliage = w.foliage;
		b.voxels = w.voxels;
		b.biomeSelectParams = w.biomeSelectParams;
		b.HorizontalStretch =  w.HorizontalStretch;
		b.VerticalStretch =  w.VerticalStretch;
		b.BaseHeight =  w.BaseHeight;
		b.MinimumHeight =  w.MinimumHeight;
		b.InversionHeight =  w.InversionHeight;
		b.TurbulenceWidth1 =  w.TurbulenceWidth1;
		b.TurbulenceHeight1 =  w.TurbulenceHeight1;
		b.TurbulenceWidth2 =  w.TurbulenceWidth2;
		b.TurbulenceHeight2 =  w.TurbulenceHeight2;
		b.power1 =  w.power1;
		b.power1Height =  w.power1Height;
		b.power2 =  w.power2;
		b.power2Height =  w.power2Height;
		b.FallOffDistance =  w.FallOffDistance;
		b.FallOffVariability =  w.FallOffVariability;
		b.ReverseInversionHeight =  w.ReverseInversionHeight;


		for (int i = 0; i < w.SubBiomes.Count; i++){
			b.SubBiomes.Add(ConvertFromSubBiomeVariant(w.SubBiomes[i]));
		}

		return b;
	}

	/// <summary>
	/// Converts from sub biome to biome.
	/// </summary>
	/// <returns>The from sub biome variant.</returns>
	/// <param name="w">The width.</param>
	public static Biome ConvertFromSubBiomeVariant(SubBiome w){
		Biome b = new Biome ();
		b.biomeName = w.biomeName;
		b.layers = w.layers;
		b.foliage = w.foliage;
		b.voxels = w.voxels;
		b.biomeSelectParams = w.biomeSelectParams;
		b.HorizontalStretch =  w.HorizontalStretch;
		b.VerticalStretch =  w.VerticalStretch;
		b.BaseHeight =  w.BaseHeight;
		b.MinimumHeight =  w.MinimumHeight;
		b.InversionHeight =  w.InversionHeight;
		b.TurbulenceWidth1 =  w.TurbulenceWidth1;
		b.TurbulenceHeight1 =  w.TurbulenceHeight1;
		b.TurbulenceWidth2 =  w.TurbulenceWidth2;
		b.TurbulenceHeight2 =  w.TurbulenceHeight2;
		b.power1 =  w.power1;
		b.power1Height =  w.power1Height;
		b.power2 =  w.power2;
		b.power2Height =  w.power2Height;
		b.FallOffDistance =  w.FallOffDistance;
		b.FallOffVariability =  w.FallOffVariability;
		b.ReverseInversionHeight =  w.ReverseInversionHeight;

		return b;
	}

	/// <summary>
	/// Converts from biome to biome variant.
	/// </summary>
	/// <returns>The to biome variant.</returns>
	/// <param name="w">The width.</param>
	public static BiomeVariant ConvertToBiomeVariant(Biome w){
		BiomeVariant b = new BiomeVariant ();
		b.biomeName = w.biomeName;
		b.layers = w.layers;
		b.foliage = w.foliage;
		b.voxels = w.voxels;
		b.biomeSelectParams = w.biomeSelectParams;
		b.HorizontalStretch =  w.HorizontalStretch;
		b.VerticalStretch =  w.VerticalStretch;
		b.BaseHeight =  w.BaseHeight;
		b.MinimumHeight =  w.MinimumHeight;
		b.InversionHeight =  w.InversionHeight;
		b.TurbulenceWidth1 =  w.TurbulenceWidth1;
		b.TurbulenceHeight1 =  w.TurbulenceHeight1;
		b.TurbulenceWidth2 =  w.TurbulenceWidth2;
		b.TurbulenceHeight2 =  w.TurbulenceHeight2;
		b.power1 =  w.power1;
		b.power1Height =  w.power1Height;
		b.power2 =  w.power2;
		b.power2Height =  w.power2Height;
		b.FallOffDistance =  w.FallOffDistance;
		b.FallOffVariability =  w.FallOffVariability;
		b.ReverseInversionHeight =  w.ReverseInversionHeight;
		
		
		for (int i = 0; i < w.SubBiomes.Count; i++){
			b.SubBiomes.Add(ConvertToSubBiomeVariant(w.SubBiomes[i]));
		}

		return b;
	}

	/// <summary>
	/// Converts to sub biome variant from biome.
	/// </summary>
	/// <returns>The to sub biome variant.</returns>
	/// <param name="w">The width.</param>
	public static SubBiome ConvertToSubBiomeVariant(Biome w){
		SubBiome b = new SubBiome ();
		b.biomeName = w.biomeName;
		b.layers = w.layers;
		b.foliage = w.foliage;
		b.voxels = w.voxels;
		b.biomeSelectParams = w.biomeSelectParams;
		b.HorizontalStretch =  w.HorizontalStretch;
		b.VerticalStretch =  w.VerticalStretch;
		b.BaseHeight =  w.BaseHeight;
		b.MinimumHeight =  w.MinimumHeight;
		b.InversionHeight =  w.InversionHeight;
		b.TurbulenceWidth1 =  w.TurbulenceWidth1;
		b.TurbulenceHeight1 =  w.TurbulenceHeight1;
		b.TurbulenceWidth2 =  w.TurbulenceWidth2;
		b.TurbulenceHeight2 =  w.TurbulenceHeight2;
		b.power1 =  w.power1;
		b.power1Height =  w.power1Height;
		b.power2 =  w.power2;
		b.power2Height =  w.power2Height;
		b.FallOffDistance =  w.FallOffDistance;
		b.FallOffVariability =  w.FallOffVariability;
		b.ReverseInversionHeight =  w.ReverseInversionHeight;

		return b;
	}
}


























