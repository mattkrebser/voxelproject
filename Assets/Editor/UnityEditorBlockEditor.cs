using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;

public class UnityEditorBlockEditor : EditorWindow
{
	Vector2 scrollPos;
	private string searchBlock = "";

	bool canDisplay = false;
	
	public List<blockitem> display = new List<blockitem>();
	private List<bool> visible = new List<bool> ();

    List<Material> materials;

    List<L> blocks;

    /// <summary>
    /// Text field for world search
    /// </summary>
    string current_world;
    /// <summary>
    /// Name of world being used, null if none
    /// </summary>
    string loaded_world;

	[MenuItem ("Window/Block Editor")]
	public static void  ShowWindow () {
		EditorWindow.GetWindow(typeof(UnityEditorBlockEditor));
	}

	public void OnGUI(){

        if (materials == null)
            get_materials();

        if (!FileClass.WorldExists(loaded_world))
        {
            display.Clear();
            visible.Clear();
            canDisplay = false;
        }

        if (canDisplay)
            GUILayout.Label(loaded_world == null ? "None" : loaded_world, EditorStyles.boldLabel);

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(500), GUILayout.Height(800));
        GUILayout.Label("World To Search For...", EditorStyles.boldLabel);
        current_world= EditorGUILayout.TextField("Text Field", current_world);

        Rect worldrect = EditorGUILayout.BeginHorizontal("Button");
        if (GUI.Button(worldrect, GUIContent.none))
        {
            load_world_blocks();
        }
        GUILayout.Label("Search for World...");
        EditorGUILayout.EndHorizontal();

        GUILayout.Label("Use the search input '_' to get blocks with no name");
        GUILayout.Label ("Block To Search For...", EditorStyles.boldLabel);
		searchBlock = EditorGUILayout.TextField ("Text Field", searchBlock);
		Rect r = EditorGUILayout.BeginHorizontal ("Button");
		if (GUI.Button (r, GUIContent.none)){
                search();
        }
        GUILayout.Label ("Search for Block");	
		EditorGUILayout.EndHorizontal ();

        if (canDisplay)
            displayList();

		Rect rr = EditorGUILayout.BeginHorizontal ("Button");
		if (GUI.Button (rr, GUIContent.none)){
            ApplyChanges();
        }
		GUILayout.Label ("Apply Changes to expanded blocks");	
		EditorGUILayout.EndHorizontal ();


        Rect newblockrect = EditorGUILayout.BeginHorizontal("Button");
        if (GUI.Button(newblockrect, GUIContent.none))
        {
            AddEmptyBlockSlot();
        }
        GUILayout.Label("Add Empty Block Slot");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.EndScrollView();
	}

    void AddEmptyBlockSlot()
    {
        for (int i = 1; i < 256; i++)
        {
            for (int n = 0; n < 256; n++)
            {
                if (blocks[i].l.Count <= n)
                {
                    blocks[i].l.Add(new Block());
                    return;
                }
            }
        }
    }

    void get_materials()
    {
        GameObject g = Resources.Load("World") as GameObject;
        materials = g.GetComponent<MeshObjectManager>().materialsList;
    }

    int get_material_position(Material m)
    {
        for (int i = 0; i < materials.Count; i++)
        {
            if (m == materials[i])
                return i;
        }
        return 0;
    }

    void load_world_blocks()
    {
        display.Clear();
        visible.Clear();
        if (!FileClass.WorldExists(current_world))
        {
            canDisplay = false;
            return;
        }
        loaded_world = current_world;
        blocks = FileClass.LoadBlockLibrary(current_world);
        canDisplay = true;
    }

	void ApplyChanges(){

		for (int i = 0; i < visible.Count; i++) {
			if (visible[i] == true){
				Block refBlock = blocks[display[i].block.blockID1].l[display[i].block.blockID2];
				refBlock.itemName = display[i].itemName;
                refBlock.materialPosition = get_material_position(display[i].material);
				refBlock.UVCoordinates = new Vector2S(display[i].UVCoordinates.x,display[i].UVCoordinates.y);
				refBlock.u = display[i].u;
				refBlock.uy = display[i].uy;
				refBlock.isTransparent = display[i].isTransparent;
				refBlock.physicsDisabled = display[i].physicsDisabled;
				refBlock.blockType = display[i].blockType;
                refBlock.textureName = display[i].texture.name;
                refBlock.BaseTexture = display[i].BaseTexture;
                refBlock.imagePath = display[i].texture2.name;
			}
		}

        FileClass.SaveBlockLibrary(loaded_world, blocks);
        load_world_blocks();
        search();
	}

	private void displayList(){

		for (int i = 0; i < display.Count; i++){
			visible[i] = EditorGUILayout.Foldout (visible[i], display [i].itemName);
			if (visible[i]){ 
				display[i].itemName = EditorGUILayout.TextField ("block name", display[i].itemName);

				display[i].UVCoordinates = new Vector2S(EditorGUILayout.Vector2Field ("Position of Texture in pixels on Tile Texture", display[i].UVCoordinates.ToVector2()));
				GUILayout.Label ("Width of texture, PixelWidth of Texture / Width of Entire Tile Texture");
				display[i].u = EditorGUILayout.FloatField (display[i].u);
				GUILayout.Label ("Height of texture, PixelHeight of Texture / Height of Entire Tile Texture");
				display[i].uy= EditorGUILayout.FloatField (display[i].uy);
				GUILayout.Label ("Blockid1, Blockid2: " + display[i].block.blockID1 + ", " + display[i].block.blockID2);

				GUILayout.Label ("Block Material, the material must exist on the world Object prefab.");
				display[i].material = (Material)EditorGUILayout.ObjectField("material", display[i].material, typeof(Material),false);

				GUILayout.Label ("Block Texture,\n Must be located in Resources/Textures/Blocks/ iff BaseTexture = true,\n" +
                   "Otherwise, must be located in Saves/WorldName/Textures/blocks ");
				display[i].texture = (Texture)(EditorGUILayout.ObjectField("Block Texture",display[i].texture,typeof(Texture),false));

                GUILayout.Label("Block Item Texture, used for 2D Icon,\nMust be stored in Saves/WorldName/Textures/objects/\n and must be a .png image");
                display[i].texture2 = (Texture)(EditorGUILayout.ObjectField("2D Icon", display[i].texture2, typeof(Texture), false));

                GUILayout.Label ("Block Rotation: " + display[i].Rotation, EditorStyles.boldLabel);
				display[i].physicsDisabled = EditorGUILayout.Toggle("Physics Disabled",display[i].physicsDisabled);
				display[i].isTransparent = EditorGUILayout.Toggle("Transparent Block",display[i].isTransparent);
                display[i].BaseTexture = EditorGUILayout.Toggle("Base Texture", display[i].BaseTexture);
                display[i].blockType = (primitiveBlockType)EditorGUILayout.EnumPopup("Block Type",display[i].blockType);
			}
		}
	}

	private bool search(){

		display.Clear ();
		visible.Clear ();

		searchName ();
		canDisplay = true;

		return true;
	}

	private void searchName()
    {
		for (int i = 0; i < 256; i++)
        {
            for (int n = 0; n < blocks[i].l.Count; n++)
            {
                if (searchBlock == "_" && (blocks[i].l[n].itemName == "" || blocks[i].l[n].itemName == null))
                {
                    blockitem nBlock = new blockitem();
                    nBlock.u = blocks[i].l[n].u;
                    nBlock.uy = blocks[i].l[n].uy;
                    nBlock.materialPosition = blocks[i].l[n].materialPosition;
                    nBlock.material = materials[nBlock.materialPosition];
                    nBlock.isTransparent = blocks[i].l[n].isTransparent;
                    nBlock.physicsDisabled = blocks[i].l[n].physicsDisabled;
                    nBlock.UVCoordinates = new Vector2S(blocks[i].l[n].UVCoordinates.x, blocks[i].l[n].UVCoordinates.y);
                    nBlock.blockType = blocks[i].l[n].blockType;
                    nBlock.Rotation = blocks[i].l[n].Rotation;
                    nBlock.itemName = blocks[i].l[n].itemName;
                    nBlock.textureName = blocks[i].l[n].textureName;
                    nBlock.BaseTexture = blocks[i].l[n].BaseTexture;
                    nBlock.imagePath = blocks[i].l[n].imagePath;

                    if (!(nBlock.textureName == "" || nBlock.textureName == null))
                    {
                        if (nBlock.BaseTexture)
                            nBlock.texture = FileClass.TextureLoader.ResourceLoadBlock(nBlock.textureName);
                        else
                            nBlock.texture = FileClass.TextureLoader.LoadBlockTexture(nBlock.textureName, loaded_world);
                    }

                    if (!(nBlock.imagePath == "" || nBlock.imagePath == null))
                    {
                        nBlock.texture2 = FileClass.TextureLoader.LoadObjectTexture(nBlock.imagePath, loaded_world);
                    }

                    display.Add(nBlock);
                    visible.Add(false);
                    nBlock.block.blockID1 = (byte)(i);
                    nBlock.block.blockID2 = (byte)(n);
                }
                else if (searchBlock != "_" && (searchBlock == "" || searchBlock == null ||
                    blocks[i].l[n].itemName.Contains(searchBlock)))
                {
                    blockitem nBlock = new blockitem();
                    nBlock.u = blocks[i].l[n].u;
                    nBlock.uy = blocks[i].l[n].uy;
                    nBlock.materialPosition = blocks[i].l[n].materialPosition;
                    nBlock.material = materials[nBlock.materialPosition];
                    nBlock.isTransparent = blocks[i].l[n].isTransparent;
                    nBlock.physicsDisabled = blocks[i].l[n].physicsDisabled;
                    nBlock.UVCoordinates = new Vector2S(blocks[i].l[n].UVCoordinates.x, blocks[i].l[n].UVCoordinates.y);
                    nBlock.blockType = blocks[i].l[n].blockType;
                    nBlock.Rotation = blocks[i].l[n].Rotation;
                    nBlock.itemName = blocks[i].l[n].itemName;
                    nBlock.textureName = blocks[i].l[n].textureName;
                    nBlock.BaseTexture = blocks[i].l[n].BaseTexture;
                    nBlock.imagePath = blocks[i].l[n].imagePath;

                    if (!(nBlock.textureName == "" || nBlock.textureName == null))
                    {
                        if (nBlock.BaseTexture)
                            nBlock.texture = FileClass.TextureLoader.ResourceLoadBlock(nBlock.textureName);
                        else
                            nBlock.texture = FileClass.TextureLoader.LoadBlockTexture(nBlock.textureName, loaded_world);
                    }

                    if (!(nBlock.imagePath == "" || nBlock.imagePath == null))
                    {
                        nBlock.texture2 = FileClass.TextureLoader.LoadObjectTexture(nBlock.imagePath, loaded_world);
                    }

                    display.Add(nBlock);
                    visible.Add(false);
                    nBlock.block.blockID1 = (byte)(i);
                    nBlock.block.blockID2 = (byte)(n);
                }
            }
		}
	}
}

























